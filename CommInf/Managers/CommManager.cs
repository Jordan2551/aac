﻿using CommInf.Handlers;
using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CommInf.Managers
{
    public abstract class CommManager
    {

        protected HandlerMap myHandlerMap;
        
        public CommManager()
        {
            myHandlerMap = new HandlerMap();
            MapHandlers();
        }
        
        public virtual Collection<TransferItem> HandleTransferItem(TransferItem tItem)
        {
            return myHandlerMap.ActivateHandler(tItem);
        }

        protected abstract void MapHandlers();

        public void MapHandler(TransferItemHandler handler)
        {
            Type targetType = ((HandleTypeDef)handler.GetMethodInfo().GetCustomAttributes(typeof(HandleTypeDef)).ToArray<Attribute>()[0]).HandlerType;

            myHandlerMap.MapHandler(targetType, handler);
        }

        protected void UnmapHandler(Type transferType)
        {
            myHandlerMap.UnmapHandler(transferType);
        }
    }

    public abstract class VoidCommManager
    {

        protected VoidHandlerMap myHandlerMap;

        public VoidCommManager()
        {
            myHandlerMap = new VoidHandlerMap();
            MapHandlers();
        }

        public virtual void HandleTransferItem(TransferItem tItem)
        {
            myHandlerMap.ActivateHandler(tItem);
        }

        protected abstract void MapHandlers();

        public void MapHandler(VoidTransferItemHandler handler)
        {
            Type targetType = ((HandleTypeDef)handler.GetMethodInfo().GetCustomAttributes(typeof(HandleTypeDef)).ToArray<Attribute>()[0]).HandlerType;

            myHandlerMap.MapHandler(targetType, handler);
        }

        protected void UnmapHandler(Type transferType)
        {
            myHandlerMap.UnmapHandler(transferType);
        }
    }
}
