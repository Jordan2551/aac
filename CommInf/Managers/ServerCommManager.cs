﻿using CommInf.Handlers;
using CommInf.Items;
using CommInf.Items.SessionItems;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommInf.Managers
{
    public abstract class ServerCommManager<T> : CommManager where T : SessionCommManager, new()
    {
        private int SessionIDCounter;

        private Dictionary<int, CommManager> sessionDictionary;

        public ServerCommManager()
            : base()
        {
            SessionIDCounter = 0;
            sessionDictionary = new Dictionary<int, CommManager>();
            MapHandler(SessionRequestTransferItemHandler);
        }

        public override Collection<TransferItem> HandleTransferItem(TransferItem tItem)
        {
            if((tItem as SessionTransferItem) != null)
            {
                return sessionDictionary[(tItem as SessionTransferItem).SessionID].HandleTransferItem(tItem);
            }
            else
            {
                return base.HandleTransferItem(tItem);
            }
        }

        [HandleTypeDef(typeof(SessionRequestTransferItem))]
        private Collection<TransferItem> SessionRequestTransferItemHandler(TransferItem tItem)
        {
            SessionRequestTransferItem reqItem = (SessionRequestTransferItem)tItem;

            int sessionID = SessionIDCounter++;

            sessionDictionary.Add(sessionID, new T() { SessionID = sessionID });

            return new SessionAcceptTransferItem() { SessionID = sessionID };
        }

        protected Collection<TransferItem> DisconnectSession(int sessionID)
        {
            sessionDictionary.Remove(sessionID);
            return new SessionReleaseTransferItem() { SessionID = sessionID };
        }
    }
}
