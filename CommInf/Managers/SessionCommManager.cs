﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommInf.Managers
{
    public abstract class SessionCommManager : CommManager
    {
        public int SessionID { get; set; }

        public SessionCommManager(): base()
        {
        }
    }
}
