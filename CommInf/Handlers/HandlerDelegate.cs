﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CommInf.Handlers
{
    public delegate Collection<TransferItem> TransferItemHandler(TransferItem item);
    public delegate void VoidTransferItemHandler(TransferItem item);
    public class HandleTypeDef : Attribute
    {
        public Type HandlerType { get; set; }
        public HandleTypeDef(Type transferItemType) : base()
        {
            HandlerType = transferItemType;
        }

    }


    //public delegate void SessionTransferItemHandler(SessionTransferItem item);

}
