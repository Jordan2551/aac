﻿using CommInf.Items;
using CommInf.Items.SessionItems;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommInf.Handlers
{
    public class HandlerMap
    {
        private Dictionary<Type, TransferItemHandler> _handlerDictionary;

        public HandlerMap()
        {
            _handlerDictionary = new Dictionary<Type, TransferItemHandler>();
        }

        public Collection<TransferItem> ActivateHandler(TransferItem tItem)
        {
            Type transferType = tItem.GetType();
            if (_handlerDictionary.ContainsKey(transferType) && (_handlerDictionary[transferType] != null))
                return _handlerDictionary[transferType](tItem);

            return new GeneralErrorTransferItem();
        }

        public void UnmapHandler(Type transferType)
        {
            if (_handlerDictionary.ContainsKey(transferType))
                _handlerDictionary.Remove(transferType);
        }

        public void MapHandler(Type transferType, TransferItemHandler handler)
        {
            UnmapHandler(transferType);
            _handlerDictionary.Add(transferType, handler);
        }
    }

    public class VoidHandlerMap
    {
        private Dictionary<Type, VoidTransferItemHandler> _handlerDictionary;

        public VoidHandlerMap()
        {
            _handlerDictionary = new Dictionary<Type, VoidTransferItemHandler>();
        }

        public void ActivateHandler(TransferItem tItem)
        {
            Type transferType = tItem.GetType();
            if (_handlerDictionary.ContainsKey(transferType) && (_handlerDictionary[transferType] != null))
                _handlerDictionary[transferType](tItem);
        }

        public void UnmapHandler(Type transferType)
        {
            if (_handlerDictionary.ContainsKey(transferType))
                _handlerDictionary.Remove(transferType);
        }

        public void MapHandler(Type transferType, VoidTransferItemHandler handler)
        {
            UnmapHandler(transferType);
            _handlerDictionary.Add(transferType, handler);
        }
    }
}
