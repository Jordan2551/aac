﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.ObjectModel;

namespace CommInf.Items
{
    public class TransferItem
    {
        public Type MyType { get; set; }
        public TransferItem()
        {
            MyType = this.GetType();
        }
        public string SerializeWithJSon()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public static TransferItem DeserializeJsonString(string serializedString)
        {
            JObject newObj = (JObject)JsonConvert.DeserializeObject(serializedString);
            return newObj.ToObject(Type.GetType((string)newObj.GetValue("MyType"))) as TransferItem;
        }

        public static implicit operator Collection<TransferItem>(TransferItem tItem)
        {
            Collection<TransferItem> retCollection = new Collection<TransferItem>();
            retCollection.Add(tItem);
            return retCollection;
        }
    }
}
