﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CommInf.Items
{
    public abstract class SessionTransferItem : TransferItem
    {
        public int SessionID { get; set; }
        public SessionTransferItem()
        {
        }
    }
}
