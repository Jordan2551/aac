﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CommInf.UserControls
{
    public partial class InvokeableProgressBar : ProgressBar
    {
        private uint progressValue;
        public uint ProgressValue
        {
            get
            {
                return progressValue;
            }
            set
            {
                if(value <= progressMax)
                    progressValue = value;
                else
                    progressValue = progressMax;

                //this.Invoke(UpdateProgressValue, null);
            }

        }

        private uint progressMax;
        public uint ProgressMax
        {
            get
            {
                return progressMax;
            }
            set
            {
                progressMax = value;
                if (progressValue > progressMax)
                    progressValue = progressMax;

                //this.Invoke(UpdateProgressMax, null);
            }
        }

        public InvokeableProgressBar()
        {
            InitializeComponent();
        }

        private void updateProgressValue()
        {
            this.Value = (int)progressValue;
        }

        private void updateProgressMax()
        {
            this.Maximum = (int)progressMax;
        }

        private delegate void UpdateDelegate();

        private UpdateDelegate UpdateProgressValue
        {
            get
            {
                return updateProgressValue;
            }
        }

        private UpdateDelegate UpdateProgressMax
        {
            get
            {
                return updateProgressMax;
                
            }
        }
    }
}
