﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using testhing.Table_Objects.Emdadata_Objects;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.IO;
using System.Globalization;

namespace testhing
{
    public partial class Converter : Form//FIX INVALID STRING ERROR FOR THE EMDADATA TABLE//FIX THE ACTIVECARFILE TABLE
    {

        #region Init and Define DB Objects
        //Initialize Access Objects
        /*
        private const string AccessConnectString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\\Users\\jorda_000\\Desktop\\AAC Related\\Data Base Related\\aac.mdb;" + "Jet OLEDB:Database Password=AAC86342;";//For aac.mdb
        private const string AccessConnectStringForInfo = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\\Users\\jorda_000\\Desktop\\AAC Related\\Data Base Related\\Info.mdb;" + "Jet OLEDB:Database Password=AAC86342;";//Separate connection string for Info.mdb
        */

        private const string AccessConnectString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\\Users\\Work PC\\Desktop\\DataBase Related\\aac.mdb;" + "Jet OLEDB:Database Password=AAC86342;";//For aac.mdb
        private const string AccessConnectStringForInfo = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\\Users\\Work PC\\Desktop\\DataBase Related\\Info.mdb;" + "Jet OLEDB:Database Password=AAC86342;";//Separate connection string for Info.mdb
        private const string AccessConnectStringForAAC_Local = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\\Users\\Work PC\\Desktop\\DataBase Related\\AAC_local.mdb;" + "Jet OLEDB:Database Password=AAC86342;";//Separate connection string for Info.mdb

        private OleDbConnection AccessConnection = new OleDbConnection(AccessConnectString);
        private OleDbDataReader AccessDataReader;
        private OleDbCommand AccessCommand = new OleDbCommand();
        private string AccessQuery;

        //Initialize mySQL Objects
        //Online Connection: "server=74.50.87.131;userid=moshech_Admin;password=aac1234;database=moshech_aac;CharSet=utf8;"
        private const String MySQLConnectionString = "server=localhost;userid=root;password=admin;database=moshech_aac;CharSet=utf8;";
        private static MySqlConnection MySQLDBConnection = new MySqlConnection(MySQLConnectionString);
        private static MySqlDataReader MySQLReader;
        private static MySqlCommand MySQLCommand = new MySqlCommand();
        private static string MySQLQuery;
        
        List<Button> ButtonList = new List<Button>();

        #endregion

        #region Members

        private class CarModelPair
        {
            public string CarModel;
            public string CarModelID;
        }

        #endregion

        public Converter()
        {
            InitializeComponent();

        }

        #region GUI Actions
        private void ButtonMouseDown(object sender, MouseEventArgs e)
        {
            foreach (Button ButtonSelected in ButtonList)
            {
          
            }
        }

        private void ToReceiveVehicle_Click(object sender, EventArgs e)
        {
            new Thread(new ThreadStart(ActiveCarFileConversion)).Start();
        }

        private void ResetRecordsButton_Click(object sender, EventArgs e)
        {

            if (ResetTableCB.SelectedIndex == -1) { MessageBox.Show("Select an item to begin table reset"); }

            else
            {
                ResetTable(ResetTableCB.Text, AccessConnectString);
            }

        }

        private void TableRecordCountButton_Click(object sender, EventArgs e)
        {

            if (RecordCountCB.SelectedIndex == -1) { MessageBox.Show("Select an item to count table records"); }

            else
            {
                MessageBox.Show(CountTableRecords(RecordCountCB.Text.ToString(), AccessConnectString) + " Records Found");
            }

        }

        private void ToEmdadata_Click(object sender, EventArgs e)
        {
            HashSet<int> UniqueCheckIDs = new HashSet<int>();//A hashset holds unique values inside of it

            AccessConnection.Open();
            AccessQuery = "SELECT CheckID FROM ActiveTestLikui";
            AccessCommand = new OleDbCommand(AccessQuery, AccessConnection);
            AccessDataReader = AccessCommand.ExecuteReader();

            while (AccessDataReader.Read())
            {
                UniqueCheckIDs.Add(Convert.ToInt32(AccessDataReader["CheckID"]));
            }

            AccessConnection.Close();

            foreach (int UniqueCheckID in UniqueCheckIDs)
            {
                ActiveEmbdaInfoAndActiveTestLikuiConversion(UniqueCheckID);
            }

        }

        private void ToCarmanufacturers_Click(object sender, EventArgs e)
        {
            CarsMotorsFirmsToCarmanufacturers();
        }

        private void ToCarmodels_Click(object sender, EventArgs e)
        {
            new Thread(new ThreadStart(CarDegemToCarModels)).Start();
        }
        private void ToCarColors_Click(object sender, EventArgs e)
        {
            CarColorTypeToCarColors();
        }
        private void ToTestType_Click(object sender, EventArgs e)
        {
            CarTestTypeToTestType();
        }
        private void ToVehicleType_Click(object sender, EventArgs e)
        {
            CarTypeToVehicleType();
        }
        private void ToEngineData_Click(object sender, EventArgs e)
        {
            CarsDataToEngineData();
        }
        private void ToDefectList_Click(object sender, EventArgs e)
        {
            LikuiListToDefectList();
        }

        private void ToCarEntranceHistory_Click(object sender, EventArgs e)
        {
            CarEnteranceHistoryToCarEntranceHistory();
        }
        private void ToServerSettings_Click(object sender, EventArgs e)
        {
            SettingsToServerSettings();
        }

        private void ToCar_Test_List_Report_Click(object sender, EventArgs e)
        {
            PrintTestListRptToCar_Test_List_Report();
        }
        #endregion 

        #region Conversion Functions

        #region Convert Access to MySQL Data - ActiveCarFile -> receivevehicle
        public void ActiveCarFileConversion()
        {

            MainProgressBar.ProgressMax = (uint)CountTableRecords("ActiveCarFile", AccessConnectString);

            AccessConnection = new OleDbConnection(AccessConnectString);
            AccessQuery = "SELECT * FROM ActiveCarFile";
            AccessConnection.Open();
            AccessCommand = new OleDbCommand(AccessQuery, AccessConnection);
            AccessDataReader = AccessCommand.ExecuteReader();

            MySQLDBConnection.Open();

                //Values Between "+" = Old Table Values Comments = New Table Values
                while (AccessDataReader.Read())//Access Loop Start Index(x) Fill info into MySQL DB
                {

                    try
                    {

                        //These 2 lines deal with converting the datetime string into a yyyy-MM-dd format which is the format MySQL requires in order to compare and order datetimes!
                        DateTime LastCheckDateInDateTime = Convert.ToDateTime(AccessDataReader["LastCheckDate"]);
                        string LastCheckDateYMDFormat = LastCheckDateInDateTime.ToString("yyyy-MM-dd");
                        //MySQLQuery = "insert into moshech_aac.receivevehicle(isRecOnline, IsCheckClosedOpen, TestType, CarNumber, CarModel, CarOwnerName, CarOwnerID, CarYear, CarType, CarShilda, EngineNumber ,EngineVolume , CarMaker, FuelType, CarFrontTire, CarRearTire, PriviledgedID, PriviledgedName, Phone1, Phone2, VavGrira, NumOfSeren, NumOfTries, CarColor, TotalWeight, CheckExpired, TesterName, LastStation, LastCheckDate, LastCheckTime, Awd, CarKm, CarKmStatus, LicenseCode, OpenTestDateTime, CloseTestDateTime, CarCheckFinal, CarLBL, Transmitted, DateTimeOfTrans, CarMonth, TransID, EngineModel, CarManufacturerID, CarModelID, IncMaxLambda, IncMinLambda, IncMaxCo, IncMaxRpm, IncMinRpm, IdleMaxCo, OilTempMax, IdleSpeedMax, IdleSpeedMin, MaxK, IdleSpeedMaxHC, NoLoadSpeedMin, NoLoadSpeedMax) values('"

                        MySQLQuery = "insert into moshech_aac.receivevehicle(isRecOnline, IsCheckClosedOpen, TestType, CarNumber, CarModel, CarOwnerName, CarOwnerID, CarYear, CarType, CarShilda, EngineNumber ,EngineVolume , CarMaker, FuelType, CarFrontTire, CarRearTire, PriviledgedID, PriviledgedName, Phone1, Phone2, VavGrira, NumOfSeren, NumOfTries, CarColor, TotalWeight, CheckExpired, TesterName, LastStation, LastCheckDate, LastCheckTime, Awd, CarKm, CarKmStatus, LicenseCode, OpenTestDateTime, CloseTestDateTime, CarCheckFinal, CarLBL, Transmitted, DateTimeOfTrans, CarMonth, TransID, EngineModel, CarManufacturerID, CarModelID, IncMaxLambda, IncMinLambda,IncMaxCo,IncMaxRpm,IncMinRpm,IdleMaxCo, OilTempMax,IdleSpeedMax,IdleSpeedMin, MaxK, IdleSpeedMaxHC, NoLoadSpeedMin, NoLoadSpeedMax) values('"
                        + Convert.ToInt16(AccessDataReader["isRecOnline"]) + "','"//34.IsRecOnline
                        + Convert.ToInt32(AccessDataReader["CheckClosedOpened"]) + "','"//1.IsCheckClosedOpen
                        + AccessDataReader["CarTestType"] + "','"//2.CarTestType
                        + StringFromReader(AccessDataReader, "CarNumber") + "','"//3.CarNumber
                        + StringFromReader(AccessDataReader, "CarNameDegem") + "','"//4.CarModel
                        + StringFromReader(AccessDataReader, "CarOwnerName") + "','"//5.CarOwnerName
                        + AccessDataReader["CarOwnerID"] + "','"//6.CarOwnerID
                        + AccessDataReader["CarYear"] + "','"//7.CarYear
                        + AccessDataReader["CarType"]+ "','"//8.CarType
                        + AccessDataReader["CarShilda"] + "','"//9.CarShilda
                        + StringFromReader(AccessDataReader, "CarDegemManoa") + "','"//10.EngineNumber
                        + AccessDataReader["CarNefach"] + "','"//11.EngineVolume
                        + StringFromReader(AccessDataReader, "CarManoaMake") + "','"//12.CarMaker
                        + AccessDataReader["CarFuel"] + "','"//13.FuelType
                        + StringFromReader(AccessDataReader, "CarFrontTire") + "','"//14.CarFrontTire
                        + StringFromReader(AccessDataReader, "CarRearTire") + "','"//15.CarRearTire
                        + StringFromReader(AccessDataReader, "CarBringID") + "','"//16.PriviledgedID
                        + StringFromReader(AccessDataReader, "CarBringFirstName") + "" + StringFromReader(AccessDataReader, "CarBringLastName") + "','"//17.PriviledgedName
                        + StringFromReader(AccessDataReader, "phone1") + "','"//18.Phone1
                        + StringFromReader(AccessDataReader, "phone2") + "','"//19.Phone2
                        + Convert.ToInt32(AccessDataReader["VavGrira"]) + "','"//20.VavGrira
                        + AccessDataReader["NumOfSeren"] + "','"//21.NumOfSeren
                        + AccessDataReader["NumOfTries"] + "','"//22.NumOfTries
                        + AccessDataReader["CarColor"] + "','"//23.CarColor
                        + AccessDataReader["TotalWieght"] + "','"//24.TotalWeight*
                        + Convert.ToInt32(AccessDataReader["CheckExpired"]) + "','"//25.CheckExpired
                        + StringFromReader(AccessDataReader, "RecievePersonName") + "','"//27.TesterName
                        + IntFromReader(AccessDataReader, "LastRunWayNum") + "','"//28.LastStation
                        + LastCheckDateYMDFormat + "','"//Add the string representing the date in the "yyyy-MM-dd" format
                        + Convert.ToDateTime(AccessDataReader["LastCheckTime"]).ToShortTimeString() + "','"//29.LastCheckTime
                        + Convert.ToInt32(AccessDataReader["Awd"]) + "','"//30.Awd
                        + AccessDataReader["CarKm"] + "','"//31.CarKm
                        + AccessDataReader["CarKmStatus"] + "','"//32.CarKmStatus
                        + StringFromReader(AccessDataReader, "LicCode") + "','"//33.LicenseCode
                        + AccessDataReader["OpenTestDate"] + "','"//35.OpenTestDateTime
                        + AccessDataReader["CloseTestDate"] + "','"//36.CloseTestDateTime
                        + Convert.ToInt32(AccessDataReader["CarCheckFinal"]) + "','"//37.CarCheckFinal
                        + StringFromReader(AccessDataReader, "CarLBL") + "','"//38.CarLBL
                        + Convert.ToInt32(AccessDataReader["Transmited"]) + "','"//39.Transmitted
                        + AccessDataReader["DateOfTrans"] + "','"//40.DateTimeOfTrans
                        + AccessDataReader["CarMonth"] + "','"//41.CarMonth
                        + AccessDataReader["TransID"] + "','"//40.TransID
                        + StringFromReader(AccessDataReader, "CarDegemManoa") + "','"//41.EngineModel 
                        + StringFromReader(AccessDataReader, "CarMakeCode") + "','"//42.CarManufacturerID 
                        + StringFromReader(AccessDataReader, "CarDegemCodeID") + "','"//43.CarDegemCodeID  
                        + Convert.ToDouble(StringFromReader(AccessDataReader, "IncMaxLambda")) + "','"//44.IncMaxLambda  
                        + Convert.ToDouble(StringFromReader(AccessDataReader, "IncMinLambda")) + "','"//45.IncMinLambda   
                        + Convert.ToDouble(StringFromReader(AccessDataReader, "IncMaxCo")) + "','"//46.IncMaxCo     
                        + Convert.ToInt32(StringFromReader(AccessDataReader, "IncMaxlSld")) + "','"//47.IncMaxRpm     
                        + Convert.ToInt32(StringFromReader(AccessDataReader, "IncMinSld")) + "','"//48.IncMinRpm   
                        + Convert.ToDouble(StringFromReader(AccessDataReader, "IdleMaxCo")) + "','"//49.IdleMaxCo   
                        + Convert.ToInt32(StringFromReader(AccessDataReader, "OilTemp_Max")) + "','"//50.OilTempMax  
                        + Convert.ToInt32(StringFromReader(AccessDataReader, "IdleSpeed_Max")) + "','"//51.IdleSpeedMax  
                        + Convert.ToInt32(StringFromReader(AccessDataReader, "IdleSpeed_Min")) + "','"//52.IdleSpeedMin  
                        + Convert.ToDouble(AccessDataReader["MaxK"]) + "','"//53.MaxK  
                        + Convert.ToDouble(AccessDataReader["idlespeedMaxHC"]) + "','"//54.IdleSpeedMaxHC  
                        + Convert.ToInt32(StringFromReader(AccessDataReader, "NoLoadSpeedMin")) + "','"//55.NoLoadSpeedMin  
                        + Convert.ToInt32(StringFromReader(AccessDataReader, "NoLoadSpeedMax")) + "')";//56.NoLoadSpeedMax  

                         MySQLCommand = new MySqlCommand(MySQLQuery, MySQLDBConnection);
                         MySQLReader = MySQLCommand.ExecuteReader();
                         MySQLReader.Close();
                    }

                    catch(Exception ){}
              
                }   //Access Loop Stop Restart the operation(Fill into a new row for MySQL DB)

              

            AccessConnection.Close();
            MySQLDBConnection.Close();
            MessageBox.Show("All Done!");

        }

        #endregion

        #region Convert Access to MySQL Data - ActiveEmbdaInfo + ActiveTestLikui -> Emdadata //FIX CRASH AT CERTAIN POINT
        public void ActiveEmbdaInfoAndActiveTestLikuiConversion(int CheckIDToConvert)
        {
                //Progress Bar
                int TableRecordCount = CountTableRecords("ActiveEmbdaInfo", AccessConnectString);
   
                //int i = 0;//Select index of Array(increments)

                AccessConnection = new OleDbConnection(AccessConnectString);
                AccessConnection.Open();

                AccessQuery = "SELECT * FROM ActiveEmbdaInfo WHERE CheckID = " + CheckIDToConvert + "";
                AccessCommand = new OleDbCommand(AccessQuery, AccessConnection);
                AccessDataReader = AccessCommand.ExecuteReader();

                //An object of type EmdadatainJson for every station!
                EmdaDataInJsonGeneralStationInfo IDStationData = new EmdaDataInJsonGeneralStationInfo();
                EmdaDataInJsonGeneralStationInfo LightStationData = new EmdaDataInJsonGeneralStationInfo();
                EmdaDataInJsonGeneralStationInfo PitStationData = new EmdaDataInJsonGeneralStationInfo();
                EmdaDataInJsonGeneralStationInfo PollutionStationData = new EmdaDataInJsonGeneralStationInfo();
                EmdaDataInJsonGeneralStationInfo BrakesStationData = new EmdaDataInJsonGeneralStationInfo();
                EmdaDataInJsonGeneralStationInfo WheelAlignmentStationData = new EmdaDataInJsonGeneralStationInfo();

             while(AccessDataReader.Read())//First loop to get the data from the "ActiveEmbdaInfo" table. Runs only if the CheckIDs match
             {
            

                 switch(Convert.ToInt32(AccessDataReader["EmbdaNum"]))
                 {

                     #region ID
                     case 1://If it's the ID Station then add that station's info from ActiveEmbdaInfo
                       
                       //From ActiveEmbdaInfo
                       
                           try
                           {

                               if (!(AccessDataReader["BochenID"] == DBNull.Value)) { IDStationData.TesterIDOfSignedStation = Convert.ToInt32(AccessDataReader["BochenID"]); }

                               if (!(AccessDataReader["EmbdaNum"] == DBNull.Value)) { IDStationData.StationNumber = Convert.ToInt32(AccessDataReader["EmbdaNum"]); }

                               if (!(AccessDataReader["BochenName"] == DBNull.Value)) { IDStationData.TesterNameOfSignedStation = AccessDataReader["BochenName"].ToString(); }

                               if (!(AccessDataReader["EmbdaPassFail"] == DBNull.Value)) { IDStationData.StationFailed = Convert.ToInt16(AccessDataReader["EmbdaPassFail"]); }

                               if (!(AccessDataReader["DateEmbdaSighn"] == DBNull.Value)) { IDStationData.StationSignDate = AccessDataReader["DateEmbdaSighn"].ToString(); }

                               if (!(AccessDataReader["EmbdaClosedOpened"] == DBNull.Value)) { IDStationData.IsStationClosed = Convert.ToInt16(AccessDataReader["EmbdaClosedOpened"]); }

                               if (!(AccessDataReader["TimeExpiredReleseEmda"] == DBNull.Value)) { IDStationData.TimeExpiredReleaseStation = AccessDataReader["TimeExpiredReleseEmda"].ToString(); } else { IDStationData.TimeExpiredReleaseStation = ""; }
                       
                           }
                       
                           catch { }

                           break;
                     #endregion 

                     #region Light Station
                     case 2:

                           //From ActiveEmbdaInfo

                           try
                           {

                               if (!(AccessDataReader["BochenID"] == DBNull.Value)) { LightStationData.TesterIDOfSignedStation = Convert.ToInt32(AccessDataReader["BochenID"]); }

                               if (!(AccessDataReader["EmbdaNum"] == DBNull.Value)) { LightStationData.StationNumber = Convert.ToInt32(AccessDataReader["EmbdaNum"]); }

                               if (!(AccessDataReader["BochenName"] == DBNull.Value)) { LightStationData.TesterNameOfSignedStation = AccessDataReader["BochenName"].ToString(); }

                               if (!(AccessDataReader["EmbdaPassFail"] == DBNull.Value)) { LightStationData.StationFailed = Convert.ToInt16(AccessDataReader["EmbdaPassFail"]); }

                               if (!(AccessDataReader["DateEmbdaSighn"] == DBNull.Value)) { LightStationData.StationSignDate = AccessDataReader["DateEmbdaSighn"].ToString(); }

                               if (!(AccessDataReader["EmbdaClosedOpened"] == DBNull.Value)) { LightStationData.IsStationClosed = Convert.ToInt16(AccessDataReader["EmbdaClosedOpened"]); }

                               if (!(AccessDataReader["TimeExpiredReleseEmda"] == DBNull.Value)) { LightStationData.TimeExpiredReleaseStation = AccessDataReader["TimeExpiredReleseEmda"].ToString(); } else { LightStationData.TimeExpiredReleaseStation = ""; }

                           }

                           catch { }

                           break;
                     #endregion 

                     #region Pit Station
                     case 3:

                           //From ActiveEmbdaInfo

                           try
                           {

                               if (!(AccessDataReader["BochenID"] == DBNull.Value)) { PitStationData.TesterIDOfSignedStation = Convert.ToInt32(AccessDataReader["BochenID"]); }

                               if (!(AccessDataReader["EmbdaNum"] == DBNull.Value)) { PitStationData.StationNumber = Convert.ToInt32(AccessDataReader["EmbdaNum"]); }

                               if (!(AccessDataReader["BochenName"] == DBNull.Value)) { PitStationData.TesterNameOfSignedStation = AccessDataReader["BochenName"].ToString(); }

                               if (!(AccessDataReader["EmbdaPassFail"] == DBNull.Value)) { PitStationData.StationFailed = Convert.ToInt16(AccessDataReader["EmbdaPassFail"]); }

                               if (!(AccessDataReader["DateEmbdaSighn"] == DBNull.Value)) { PitStationData.StationSignDate = AccessDataReader["DateEmbdaSighn"].ToString(); }

                               if (!(AccessDataReader["EmbdaClosedOpened"] == DBNull.Value)) { PitStationData.IsStationClosed = Convert.ToInt16(AccessDataReader["EmbdaClosedOpened"]); }

                               if (!(AccessDataReader["TimeExpiredReleseEmda"] == DBNull.Value)) { PitStationData.TimeExpiredReleaseStation = AccessDataReader["TimeExpiredReleseEmda"].ToString(); } else { PitStationData.TimeExpiredReleaseStation = ""; }

                           }

                           catch { }

                           break;
                     #endregion 

                     #region Pollution Station
                     case 4:

                           //From ActiveEmbdaInfo

                           try
                           {

                               if (!(AccessDataReader["BochenID"] == DBNull.Value)) { PollutionStationData.TesterIDOfSignedStation = Convert.ToInt32(AccessDataReader["BochenID"]); }

                               if (!(AccessDataReader["EmbdaNum"] == DBNull.Value)) { PollutionStationData.StationNumber = Convert.ToInt32(AccessDataReader["EmbdaNum"]); }

                               if (!(AccessDataReader["BochenName"] == DBNull.Value)) { PollutionStationData.TesterNameOfSignedStation = AccessDataReader["BochenName"].ToString(); }

                               if (!(AccessDataReader["EmbdaPassFail"] == DBNull.Value)) { PollutionStationData.StationFailed = Convert.ToInt16(AccessDataReader["EmbdaPassFail"]); }

                               if (!(AccessDataReader["DateEmbdaSighn"] == DBNull.Value)) { PollutionStationData.StationSignDate = AccessDataReader["DateEmbdaSighn"].ToString(); }

                               if (!(AccessDataReader["EmbdaClosedOpened"] == DBNull.Value)) { PollutionStationData.IsStationClosed = Convert.ToInt16(AccessDataReader["EmbdaClosedOpened"]); }

                               if (!(AccessDataReader["TimeExpiredReleseEmda"] == DBNull.Value)) { PollutionStationData.TimeExpiredReleaseStation = AccessDataReader["TimeExpiredReleseEmda"].ToString(); } else { PollutionStationData.TimeExpiredReleaseStation = ""; }

                           }

                           catch { }

                           break;
                     #endregion 

                     #region Brakes Station
                     case 5:

                           //From ActiveEmbdaInfo

                           try
                           {

                               if (!(AccessDataReader["BochenID"] == DBNull.Value)) { BrakesStationData.TesterIDOfSignedStation = Convert.ToInt32(AccessDataReader["BochenID"]); }

                               if (!(AccessDataReader["EmbdaNum"] == DBNull.Value)) { BrakesStationData.StationNumber = Convert.ToInt32(AccessDataReader["EmbdaNum"]); }

                               if (!(AccessDataReader["BochenName"] == DBNull.Value)) { BrakesStationData.TesterNameOfSignedStation = AccessDataReader["BochenName"].ToString(); }

                               if (!(AccessDataReader["EmbdaPassFail"] == DBNull.Value)) { BrakesStationData.StationFailed = Convert.ToInt16(AccessDataReader["EmbdaPassFail"]); }

                               if (!(AccessDataReader["DateEmbdaSighn"] == DBNull.Value)) { BrakesStationData.StationSignDate = AccessDataReader["DateEmbdaSighn"].ToString(); }

                               if (!(AccessDataReader["EmbdaClosedOpened"] == DBNull.Value)) { BrakesStationData.IsStationClosed = Convert.ToInt16(AccessDataReader["EmbdaClosedOpened"]); }

                               if (!(AccessDataReader["TimeExpiredReleseEmda"] == DBNull.Value)) { BrakesStationData.TimeExpiredReleaseStation = AccessDataReader["TimeExpiredReleseEmda"].ToString(); } else { BrakesStationData.TimeExpiredReleaseStation = ""; }

                           }

                           catch { }

                           break;
                     #endregion 

                     #region Wheel Alignment Station
                     case 6:

                           //From ActiveEmbdaInfo

                           try
                           {

                               if (!(AccessDataReader["BochenID"] == DBNull.Value)) { WheelAlignmentStationData.TesterIDOfSignedStation = Convert.ToInt32(AccessDataReader["BochenID"]); }

                               if (!(AccessDataReader["EmbdaNum"] == DBNull.Value)) { WheelAlignmentStationData.StationNumber = Convert.ToInt32(AccessDataReader["EmbdaNum"]); }

                               if (!(AccessDataReader["BochenName"] == DBNull.Value)) { WheelAlignmentStationData.TesterNameOfSignedStation = AccessDataReader["BochenName"].ToString(); }

                               if (!(AccessDataReader["EmbdaPassFail"] == DBNull.Value)) { WheelAlignmentStationData.StationFailed = Convert.ToInt16(AccessDataReader["EmbdaPassFail"]); }

                               if (!(AccessDataReader["DateEmbdaSighn"] == DBNull.Value)) { WheelAlignmentStationData.StationSignDate = AccessDataReader["DateEmbdaSighn"].ToString(); }

                               if (!(AccessDataReader["EmbdaClosedOpened"] == DBNull.Value)) { WheelAlignmentStationData.IsStationClosed = Convert.ToInt16(AccessDataReader["EmbdaClosedOpened"]); }

                               if (!(AccessDataReader["TimeExpiredReleseEmda"] == DBNull.Value)) { WheelAlignmentStationData.TimeExpiredReleaseStation = AccessDataReader["TimeExpiredReleseEmda"].ToString(); } else { WheelAlignmentStationData.TimeExpiredReleaseStation = ""; }

                           }

                           catch { }

                           break;
                     #endregion 

                 }

              }


                AccessQuery = "SELECT * FROM ActiveTestLikui WHERE CheckID = " + CheckIDToConvert + "";
                AccessCommand = new OleDbCommand(AccessQuery, AccessConnection);
                AccessDataReader = AccessCommand.ExecuteReader();

             while (AccessDataReader.Read())//Second loop to get the data from the "ActiveTestLikui" table makes sure the CheckID is the same as the one from the previous table. We already know what the CheckID is thanks to the previous table. So now we know to set a boundry for the second reader's query!
             {
                 //Different switch cases below so we know which Station gets what defect info added to their "DefectList"

                 switch(Convert.ToInt32(AccessDataReader["TestEmbda"]))
                 {
                     
                     #region ID
                     case 1:

                        try{
                               
                               IDStationData.DefectList.Add(new EmdaDataInJsonStationDefectInfo());

                               if (!(AccessDataReader["BochenID"] == DBNull.Value)) { IDStationData.DefectList[IDStationData.DefectList.Count - 1].TesterIDOfSignedDefect = Convert.ToInt32(AccessDataReader["BochenID"]); }

                               if (!(AccessDataReader["BochenName"] == DBNull.Value)) { IDStationData.DefectList[IDStationData.DefectList.Count - 1].TesterNameOfSignedDefect = AccessDataReader["BochenName"].ToString(); }

                               if (!(AccessDataReader["DateLikui"] == DBNull.Value) && (AccessDataReader["TimeLikui"] == DBNull.Value)) { IDStationData.DefectList[IDStationData.DefectList.Count - 1].DefectSignDateTime = AccessDataReader["DateLikui"].ToString() + AccessDataReader["TimeLikui"].ToString(); }

                               if (!(AccessDataReader["LikuiID"] == DBNull.Value)) { IDStationData.DefectList[IDStationData.DefectList.Count - 1].DefectID = Convert.ToInt16(AccessDataReader["LikuiID"]); }

                               if (!(AccessDataReader["LikuiInfoLarge"] == DBNull.Value)) { IDStationData.DefectList[IDStationData.DefectList.Count - 1].DefectInfo = AccessDataReader["LikuiInfoLarge"].ToString(); }

                               //If the LikuInfoLarge is empty  then refer to LikuiInfo for the data 
                               if (IDStationData.DefectList[IDStationData.DefectList.Count - 1].DefectInfo == null) { IDStationData.DefectList[IDStationData.DefectList.Count - 1].DefectInfo = AccessDataReader["LikuiInfo"].ToString(); }

                               if (!(AccessDataReader["Transmited"] == DBNull.Value)) { IDStationData.DefectList[IDStationData.DefectList.Count - 1].Transmitted = Convert.ToInt16(AccessDataReader["Transmited"]); }

                               if (!(AccessDataReader["Fixed"] == DBNull.Value)) { IDStationData.DefectList[IDStationData.DefectList.Count - 1].DefectFixed = Convert.ToInt16(AccessDataReader["Fixed"]); }

                               if (!(AccessDataReader["YeshorRequest"] == DBNull.Value)) { IDStationData.DefectList[IDStationData.DefectList.Count - 1].PermitRequest = Convert.ToInt16(AccessDataReader["YeshorRequest"]); }

                               if (!(AccessDataReader["YeshorNum"] == DBNull.Value)) { IDStationData.DefectList[IDStationData.DefectList.Count - 1].PermitNumber = Convert.ToInt32(AccessDataReader["YeshorNum"]); }

                               if (!(AccessDataReader["LikuiDanger"] == DBNull.Value)) { IDStationData.DefectList[WheelAlignmentStationData.DefectList.Count - 1].DangerousDefect = Convert.ToInt32(AccessDataReader["LikuiDanger"]); }

                            }
 

                        catch { }     

                        break;
                        #endregion 

                         
                     #region Light Station
                     case 2:

                        try{
                               
                               LightStationData.DefectList.Add(new EmdaDataInJsonStationDefectInfo());

                               if (!(AccessDataReader["BochenID"] == DBNull.Value)) { LightStationData.DefectList[LightStationData.DefectList.Count - 1].TesterIDOfSignedDefect = Convert.ToInt32(AccessDataReader["BochenID"]); }

                               if (!(AccessDataReader["BochenName"] == DBNull.Value)) { LightStationData.DefectList[LightStationData.DefectList.Count - 1].TesterNameOfSignedDefect = AccessDataReader["BochenName"].ToString(); }

                               if (!(AccessDataReader["DateLikui"] == DBNull.Value) && (AccessDataReader["TimeLikui"] == DBNull.Value)) { LightStationData.DefectList[LightStationData.DefectList.Count - 1].DefectSignDateTime = AccessDataReader["DateLikui"].ToString() + AccessDataReader["TimeLikui"].ToString(); }

                               if (!(AccessDataReader["LikuiID"] == DBNull.Value)) { LightStationData.DefectList[LightStationData.DefectList.Count - 1].DefectID = Convert.ToInt16(AccessDataReader["LikuiID"]); }

                               if (!(AccessDataReader["LikuiInfoLarge"] == DBNull.Value)) { LightStationData.DefectList[LightStationData.DefectList.Count - 1].DefectInfo = AccessDataReader["LikuiInfoLarge"].ToString(); }

                               //If the LikuInfoLarge is empty  then refer to LikuiInfo for the data 
                               if (LightStationData.DefectList[LightStationData.DefectList.Count - 1].DefectInfo == null) { LightStationData.DefectList[LightStationData.DefectList.Count - 1].DefectInfo = AccessDataReader["LikuiInfo"].ToString(); }

                               if (!(AccessDataReader["Transmited"] == DBNull.Value)) { LightStationData.DefectList[LightStationData.DefectList.Count - 1].Transmitted = Convert.ToInt16(AccessDataReader["Transmited"]); }

                               if (!(AccessDataReader["Fixed"] == DBNull.Value)) { LightStationData.DefectList[LightStationData.DefectList.Count - 1].DefectFixed = Convert.ToInt16(AccessDataReader["Fixed"]); }

                               if (!(AccessDataReader["YeshorRequest"] == DBNull.Value)) { LightStationData.DefectList[LightStationData.DefectList.Count - 1].PermitRequest = Convert.ToInt16(AccessDataReader["YeshorRequest"]); }

                               if (!(AccessDataReader["YeshorNum"] == DBNull.Value)) { LightStationData.DefectList[LightStationData.DefectList.Count - 1].PermitNumber = Convert.ToInt32(AccessDataReader["YeshorNum"]); }

                               if (!(AccessDataReader["LikuiDanger"] == DBNull.Value)) { LightStationData.DefectList[WheelAlignmentStationData.DefectList.Count - 1].DangerousDefect = Convert.ToInt32(AccessDataReader["LikuiDanger"]); }

                            }
 

                        catch { }     

                        break;
                        #endregion 

                         
                     #region Pit Station
                     case 3:

                        try{
                               
                               PitStationData.DefectList.Add(new EmdaDataInJsonStationDefectInfo());

                               if (!(AccessDataReader["BochenID"] == DBNull.Value)) { PitStationData.DefectList[PitStationData.DefectList.Count - 1].TesterIDOfSignedDefect = Convert.ToInt32(AccessDataReader["BochenID"]); }

                               if (!(AccessDataReader["BochenName"] == DBNull.Value)) { PitStationData.DefectList[PitStationData.DefectList.Count - 1].TesterNameOfSignedDefect = AccessDataReader["BochenName"].ToString(); }

                               if (!(AccessDataReader["DateLikui"] == DBNull.Value) && (AccessDataReader["TimeLikui"] == DBNull.Value)) { PitStationData.DefectList[PitStationData.DefectList.Count - 1].DefectSignDateTime = AccessDataReader["DateLikui"].ToString() + AccessDataReader["TimeLikui"].ToString(); }

                               if (!(AccessDataReader["LikuiID"] == DBNull.Value)) { PitStationData.DefectList[PitStationData.DefectList.Count - 1].DefectID = Convert.ToInt16(AccessDataReader["LikuiID"]); }

                               if (!(AccessDataReader["LikuiInfoLarge"] == DBNull.Value)) { PitStationData.DefectList[PitStationData.DefectList.Count - 1].DefectInfo = AccessDataReader["LikuiInfoLarge"].ToString(); }

                               //If the LikuInfoLarge is empty  then refer to LikuiInfo for the data 
                               if (PitStationData.DefectList[PitStationData.DefectList.Count - 1].DefectInfo == null) { PitStationData.DefectList[PitStationData.DefectList.Count - 1].DefectInfo = AccessDataReader["LikuiInfo"].ToString(); }

                               if (!(AccessDataReader["Transmited"] == DBNull.Value)) { PitStationData.DefectList[PitStationData.DefectList.Count - 1].Transmitted = Convert.ToInt16(AccessDataReader["Transmited"]); }

                               if (!(AccessDataReader["Fixed"] == DBNull.Value)) { PitStationData.DefectList[PitStationData.DefectList.Count - 1].DefectFixed = Convert.ToInt16(AccessDataReader["Fixed"]); }

                               if (!(AccessDataReader["YeshorRequest"] == DBNull.Value)) { PitStationData.DefectList[PitStationData.DefectList.Count - 1].PermitRequest = Convert.ToInt16(AccessDataReader["YeshorRequest"]); }

                               if (!(AccessDataReader["YeshorNum"] == DBNull.Value)) { PitStationData.DefectList[PitStationData.DefectList.Count - 1].PermitNumber = Convert.ToInt32(AccessDataReader["YeshorNum"]); }

                               if (!(AccessDataReader["LikuiDanger"] == DBNull.Value)) { PitStationData.DefectList[WheelAlignmentStationData.DefectList.Count - 1].DangerousDefect = Convert.ToInt32(AccessDataReader["LikuiDanger"]); }

                            }
 

                        catch { }     

                        break;
                        #endregion 

                         
                     #region Pollution Station
                     case 4:

                        try{
                               
                               PollutionStationData.DefectList.Add(new EmdaDataInJsonStationDefectInfo());

                               if (!(AccessDataReader["BochenID"] == DBNull.Value)) { PollutionStationData.DefectList[PollutionStationData.DefectList.Count - 1].TesterIDOfSignedDefect = Convert.ToInt32(AccessDataReader["BochenID"]); }

                               if (!(AccessDataReader["BochenName"] == DBNull.Value)) { PollutionStationData.DefectList[PollutionStationData.DefectList.Count - 1].TesterNameOfSignedDefect = AccessDataReader["BochenName"].ToString(); }

                               if (!(AccessDataReader["DateLikui"] == DBNull.Value) && (AccessDataReader["TimeLikui"] == DBNull.Value)) { PollutionStationData.DefectList[PollutionStationData.DefectList.Count - 1].DefectSignDateTime = AccessDataReader["DateLikui"].ToString() + AccessDataReader["TimeLikui"].ToString(); }

                               if (!(AccessDataReader["LikuiID"] == DBNull.Value)) { PollutionStationData.DefectList[PollutionStationData.DefectList.Count - 1].DefectID = Convert.ToInt16(AccessDataReader["LikuiID"]); }

                               if (!(AccessDataReader["LikuiInfoLarge"] == DBNull.Value)) { PollutionStationData.DefectList[PollutionStationData.DefectList.Count - 1].DefectInfo = AccessDataReader["LikuiInfoLarge"].ToString(); }

                               //If the LikuInfoLarge is empty  then refer to LikuiInfo for the data 
                               if (PollutionStationData.DefectList[PollutionStationData.DefectList.Count - 1].DefectInfo == null) { PollutionStationData.DefectList[PollutionStationData.DefectList.Count - 1].DefectInfo = AccessDataReader["LikuiInfo"].ToString(); }

                               if (!(AccessDataReader["Transmited"] == DBNull.Value)) { PollutionStationData.DefectList[PollutionStationData.DefectList.Count - 1].Transmitted = Convert.ToInt16(AccessDataReader["Transmited"]); }

                               if (!(AccessDataReader["Fixed"] == DBNull.Value)) { PollutionStationData.DefectList[PollutionStationData.DefectList.Count - 1].DefectFixed = Convert.ToInt16(AccessDataReader["Fixed"]); }

                               if (!(AccessDataReader["YeshorRequest"] == DBNull.Value)) { PollutionStationData.DefectList[PollutionStationData.DefectList.Count - 1].PermitRequest = Convert.ToInt16(AccessDataReader["YeshorRequest"]); }

                               if (!(AccessDataReader["YeshorNum"] == DBNull.Value)) { PollutionStationData.DefectList[PollutionStationData.DefectList.Count - 1].PermitNumber = Convert.ToInt32(AccessDataReader["YeshorNum"]); }

                               if (!(AccessDataReader["LikuiDanger"] == DBNull.Value)) { PollutionStationData.DefectList[WheelAlignmentStationData.DefectList.Count - 1].DangerousDefect = Convert.ToInt32(AccessDataReader["LikuiDanger"]); }

                            }
 

                        catch { }     

                        break;
                        #endregion 

                         
                     #region Brakes Station
                     case 5:

                        try{
                               
                               BrakesStationData.DefectList.Add(new EmdaDataInJsonStationDefectInfo());

                               if (!(AccessDataReader["BochenID"] == DBNull.Value)) { BrakesStationData.DefectList[BrakesStationData.DefectList.Count - 1].TesterIDOfSignedDefect = Convert.ToInt32(AccessDataReader["BochenID"]); }

                               if (!(AccessDataReader["BochenName"] == DBNull.Value)) { BrakesStationData.DefectList[BrakesStationData.DefectList.Count - 1].TesterNameOfSignedDefect = AccessDataReader["BochenName"].ToString(); }

                               if (!(AccessDataReader["DateLikui"] == DBNull.Value) && (AccessDataReader["TimeLikui"] == DBNull.Value)) { BrakesStationData.DefectList[BrakesStationData.DefectList.Count - 1].DefectSignDateTime = AccessDataReader["DateLikui"].ToString() + AccessDataReader["TimeLikui"].ToString(); }

                               if (!(AccessDataReader["LikuiID"] == DBNull.Value)) { BrakesStationData.DefectList[BrakesStationData.DefectList.Count - 1].DefectID = Convert.ToInt16(AccessDataReader["LikuiID"]); }

                               if (!(AccessDataReader["LikuiInfoLarge"] == DBNull.Value)) { BrakesStationData.DefectList[BrakesStationData.DefectList.Count - 1].DefectInfo = AccessDataReader["LikuiInfoLarge"].ToString(); }

                               //If the LikuInfoLarge is empty  then refer to LikuiInfo for the data 
                               if (BrakesStationData.DefectList[BrakesStationData.DefectList.Count - 1].DefectInfo == null) { BrakesStationData.DefectList[BrakesStationData.DefectList.Count - 1].DefectInfo = AccessDataReader["LikuiInfo"].ToString(); }

                               if (!(AccessDataReader["Transmited"] == DBNull.Value)) { BrakesStationData.DefectList[BrakesStationData.DefectList.Count - 1].Transmitted = Convert.ToInt16(AccessDataReader["Transmited"]); }

                               if (!(AccessDataReader["Fixed"] == DBNull.Value)) { BrakesStationData.DefectList[BrakesStationData.DefectList.Count - 1].DefectFixed = Convert.ToInt16(AccessDataReader["Fixed"]); }

                               if (!(AccessDataReader["YeshorRequest"] == DBNull.Value)) { BrakesStationData.DefectList[BrakesStationData.DefectList.Count - 1].PermitRequest = Convert.ToInt16(AccessDataReader["YeshorRequest"]); }

                               if (!(AccessDataReader["YeshorNum"] == DBNull.Value)) { BrakesStationData.DefectList[BrakesStationData.DefectList.Count - 1].PermitNumber = Convert.ToInt32(AccessDataReader["YeshorNum"]); }

                               if (!(AccessDataReader["LikuiDanger"] == DBNull.Value)) { BrakesStationData.DefectList[WheelAlignmentStationData.DefectList.Count - 1].DangerousDefect = Convert.ToInt32(AccessDataReader["LikuiDanger"]); }

                            }
 

                        catch { }     

                        break;
                        #endregion 

                         
                     #region Wheel Alignment Station
                     case 6:

                        try{
                               
                               WheelAlignmentStationData.DefectList.Add(new EmdaDataInJsonStationDefectInfo());

                               if (!(AccessDataReader["BochenID"] == DBNull.Value)) { WheelAlignmentStationData.DefectList[WheelAlignmentStationData.DefectList.Count - 1].TesterIDOfSignedDefect = Convert.ToInt32(AccessDataReader["BochenID"]); }

                               if (!(AccessDataReader["BochenName"] == DBNull.Value)) { WheelAlignmentStationData.DefectList[WheelAlignmentStationData.DefectList.Count - 1].TesterNameOfSignedDefect = AccessDataReader["BochenName"].ToString(); }

                               if (!(AccessDataReader["DateLikui"] == DBNull.Value) && (AccessDataReader["TimeLikui"] == DBNull.Value)) { WheelAlignmentStationData.DefectList[WheelAlignmentStationData.DefectList.Count - 1].DefectSignDateTime = AccessDataReader["DateLikui"].ToString() + AccessDataReader["TimeLikui"].ToString(); }

                               if (!(AccessDataReader["LikuiID"] == DBNull.Value)) { WheelAlignmentStationData.DefectList[WheelAlignmentStationData.DefectList.Count - 1].DefectID = Convert.ToInt16(AccessDataReader["LikuiID"]); }

                               if (!(AccessDataReader["LikuiInfoLarge"] == DBNull.Value)) { WheelAlignmentStationData.DefectList[WheelAlignmentStationData.DefectList.Count - 1].DefectInfo = AccessDataReader["LikuiInfoLarge"].ToString(); }

                               //If the LikuInfoLarge is empty  then refer to LikuiInfo for the data 
                               if (WheelAlignmentStationData.DefectList[WheelAlignmentStationData.DefectList.Count - 1].DefectInfo == null) { WheelAlignmentStationData.DefectList[WheelAlignmentStationData.DefectList.Count - 1].DefectInfo = AccessDataReader["LikuiInfo"].ToString(); }

                               if (!(AccessDataReader["Transmited"] == DBNull.Value)) { WheelAlignmentStationData.DefectList[WheelAlignmentStationData.DefectList.Count - 1].Transmitted = Convert.ToInt16(AccessDataReader["Transmited"]); }

                               if (!(AccessDataReader["Fixed"] == DBNull.Value)) { WheelAlignmentStationData.DefectList[WheelAlignmentStationData.DefectList.Count - 1].DefectFixed = Convert.ToInt16(AccessDataReader["Fixed"]); }

                               if (!(AccessDataReader["YeshorRequest"] == DBNull.Value)) { WheelAlignmentStationData.DefectList[WheelAlignmentStationData.DefectList.Count - 1].PermitRequest = Convert.ToInt16(AccessDataReader["YeshorRequest"]); }

                               if (!(AccessDataReader["YeshorNum"] == DBNull.Value)) { WheelAlignmentStationData.DefectList[WheelAlignmentStationData.DefectList.Count - 1].PermitNumber = Convert.ToInt32(AccessDataReader["YeshorNum"]); }

                               if (!(AccessDataReader["LikuiDanger"] == DBNull.Value)) { WheelAlignmentStationData.DefectList[WheelAlignmentStationData.DefectList.Count - 1].DangerousDefect = Convert.ToInt32(AccessDataReader["LikuiDanger"]); }

                            }
 

                        catch { }     

                        break;
                        #endregion 



                 }
                    
             
            }
             
                string IDStationDataInJson = JsonConvert.SerializeObject(IDStationData);//Serialize ID Station Object
                string LightStationDataInJson = JsonConvert.SerializeObject(LightStationData);
                string PitStationDataInJson = JsonConvert.SerializeObject(PitStationData);
                string PollutionStationDataInJson = JsonConvert.SerializeObject(PollutionStationData);
                string BrakesStationDataInJson = JsonConvert.SerializeObject(BrakesStationData);
                string WheelAlignmentStationDataInJson = JsonConvert.SerializeObject(WheelAlignmentStationData);

                IDStationDataInJson = IDStationDataInJson.Replace("'", @"\'");//Escape "'" character to prevent crashes(IMPORTANT!)
                LightStationDataInJson = LightStationDataInJson.Replace("'", @"\'");//Escape "'" character to prevent crashes(IMPORTANT!)
                PitStationDataInJson = PitStationDataInJson.Replace("'", @"\'");//Escape "'" character to prevent crashes(IMPORTANT!)
                PollutionStationDataInJson = PollutionStationDataInJson.Replace("'", @"\'");//Escape "'" character to prevent crashes(IMPORTANT!)
                BrakesStationDataInJson = BrakesStationDataInJson.Replace("'", @"\'");//Escape "'" character to prevent crashes(IMPORTANT!)
                WheelAlignmentStationDataInJson = WheelAlignmentStationDataInJson.Replace("'", @"\'");//Escape "'" character to prevent crashes(IMPORTANT!)

                //Insert into MySQL DB:
                MySQLDBConnection.Open();
                MySQLQuery = "insert into moshech_aac.emdadata(CarCheckID, IDStation, LightsStation, PitStation, PollutionStation, BrakesStation, WheelAlignmentStation) values('"
                    + CheckIDToConvert + "','"
                    + IDStationDataInJson + "','"
                    + LightStationDataInJson + "','"
                    + PitStationDataInJson + "','"
                    + PollutionStationDataInJson + "','"
                    + BrakesStationDataInJson + "','"
                    + WheelAlignmentStationDataInJson + "')";

                MySQLCommand = new MySqlCommand(MySQLQuery, MySQLDBConnection);
                MySQLReader = MySQLCommand.ExecuteReader();
                MySQLReader.Close();

                AccessConnection.Close();
                MySQLDBConnection.Close();
         }

        #endregion

        #region Convert Access to MySQL Data - CarsMotorsFirms -> Carmanufacturers

        public void CarsMotorsFirmsToCarmanufacturers()
        {
            AccessConnection = new OleDbConnection(AccessConnectStringForInfo);
            AccessConnection.Open();
            AccessQuery = "SELECT * FROM CarsMotorsFirms ORDER BY ManuID";
            AccessCommand = new OleDbCommand(AccessQuery, AccessConnection);
            AccessDataReader = AccessCommand.ExecuteReader();

            MySQLDBConnection.Open();

            while(AccessDataReader.Read())
            {

                MySQLQuery = "insert into moshech_aac.carmanufacturers(ManufacturerNameHeb, ManufacturerNameEng, MOTManufacturerID) values('" + AccessDataReader["DESCS1"].ToString().Replace("'", @"\'") + "','" + AccessDataReader["Descs"].ToString().Replace("'", @"\'") + "','" + AccessDataReader["ManuID"] + "')";
                MySQLCommand = new MySqlCommand(MySQLQuery, MySQLDBConnection);
                MySQLReader = MySQLCommand.ExecuteReader();
                MySQLReader.Close();
            }

            AccessConnection.Close();
            MySQLDBConnection.Close();
        }

        #endregion 

        #region Convert Access to MySQL Data - CarDegem -> CarModels

        public void CarDegemToCarModels()
        {

            MainProgressBar.ProgressMax = (uint)CountTableRecords("CarDegem", AccessConnectString);
            MainProgressBar.ProgressValue = 0;

            MySQLDBConnection.Open();

            List<int> CarManufacturerOrder = new List<int>();

            //We need to sort the car models order just like the car manufacturers order or else the combobox selection for the manufacturer will select models from a different manufacturer because the two tables it's based on are ordered differently...
            AccessConnection = new OleDbConnection(AccessConnectStringForInfo);
            AccessConnection.Open();
            AccessQuery = "SELECT * FROM CarsMotorsFirms ORDER BY ManuID";
            AccessCommand = new OleDbCommand(AccessQuery, AccessConnection);
            AccessDataReader = AccessCommand.ExecuteReader();
            
            //Here we populate a list full of all the possible Manu IDs in order of CarsMotorsFirms
            while (AccessDataReader.Read()) 
            {
                if (!(AccessDataReader["ManuID"] == DBNull.Value)) { CarManufacturerOrder.Add(Convert.ToInt32(AccessDataReader["ManuID"])); }
            }

            CarManufacturerOrder = CarManufacturerOrder.Distinct().ToList();//Make it unique
            int duplicatecounter = 0;
            AccessDataReader.Close();

            foreach(int CarManufacturerID in CarManufacturerOrder)//Insert the models in order of the manu IDs from our unique ID list
            {

                AccessQuery = "SELECT * FROM CarDegem WHERE CarManufactureID = " + CarManufacturerID + "";
                AccessCommand = new OleDbCommand(AccessQuery, AccessConnection);
                AccessDataReader = AccessCommand.ExecuteReader();

                while (AccessDataReader.Read())//Insert all the models from that ID
                {//We want to keep this table unique, without duplicates so if we use "replace" it will find a duplicate value and overrite it, if not found then that value is created


                    MySQLQuery = "replace into moshech_aac.carmodels(ModelCode, MOTManufacturerID, CarModelName) values('" + AccessDataReader["CarDegemCode"].ToString().Replace("'", @"\'") + "','" + AccessDataReader["CarManuFactureID"] + "','" + AccessDataReader["CarDegemName"].ToString().Replace("'", @"\'") + "')";
                
                    MySQLCommand = new MySqlCommand(MySQLQuery, MySQLDBConnection);
                    MySQLReader = MySQLCommand.ExecuteReader();
                    MySQLReader.Close();
                
                    MainProgressBar.ProgressValue++;

                }
                
            }
            AccessConnection.Close();
            MySQLDBConnection.Close();
            //progressValue = progressMaximum
        }


        #endregion 

        #region Convert Access to MySQL Data - CarColorType -> CarColors
        private void CarColorTypeToCarColors()
        {
            AccessConnection = new OleDbConnection(AccessConnectString);
            AccessConnection.Open();
            AccessQuery = "SELECT * FROM CarColorType";
            AccessCommand = new OleDbCommand(AccessQuery, AccessConnection);
            AccessDataReader = AccessCommand.ExecuteReader();

            MySQLDBConnection.Open();

            while (AccessDataReader.Read())
            {

                MySQLQuery = "insert into moshech_aac.carcolors(ColorID, Color) values('" + AccessDataReader["Code"] + "','" + AccessDataReader["Descs"].ToString().Replace("'", @"\'") + "')";
                MySQLCommand = new MySqlCommand(MySQLQuery, MySQLDBConnection);
                MySQLReader = MySQLCommand.ExecuteReader();
                MySQLReader.Close();
            }

            AccessConnection.Close();
            MySQLDBConnection.Close();
        }

        #endregion 

        #region Convert Access to MySQL Data - CarTestType -> TestType

        public void CarTestTypeToTestType()
        {

            AccessConnection = new OleDbConnection(AccessConnectString);
            AccessConnection.Open();
            AccessQuery = "SELECT * FROM CarTestType";
            AccessCommand = new OleDbCommand(AccessQuery, AccessConnection);
            AccessDataReader = AccessCommand.ExecuteReader();

            MySQLDBConnection.Open();

            while (AccessDataReader.Read())
            {

                MySQLQuery = "insert into moshech_aac.testtype(ID, TestType) values('" + AccessDataReader["Code"] + "','" + AccessDataReader["Descs"].ToString().Replace("'", @"\'") + "')";
                MySQLCommand = new MySqlCommand(MySQLQuery, MySQLDBConnection);
                MySQLReader = MySQLCommand.ExecuteReader();
                MySQLReader.Close();
            }

            AccessConnection.Close();
            MySQLDBConnection.Close();

        }

        #endregion

        #region Convert Access to MySQL Data - CarType -> VehicleType

        public void CarTypeToVehicleType()
        {

            AccessConnection = new OleDbConnection(AccessConnectStringForInfo);
            AccessConnection.Open();
            AccessQuery = "SELECT * FROM CarType";
            AccessCommand = new OleDbCommand(AccessQuery, AccessConnection);
            AccessDataReader = AccessCommand.ExecuteReader();

            MySQLDBConnection.Open();

            while (AccessDataReader.Read())
            {

                MySQLQuery = "insert into moshech_aac.vehicletypes(ID, VehicleType, TransferCode, CodeName) values('" + AccessDataReader["Code"] + "','" + AccessDataReader["Descs"].ToString().Replace("'", @"\'") + "','" + AccessDataReader["TransCode"] + "','" + AccessDataReader["CodeName"] + "')";
                MySQLCommand = new MySqlCommand(MySQLQuery, MySQLDBConnection);
                MySQLReader = MySQLCommand.ExecuteReader();
                MySQLReader.Close();

            }

            AccessConnection.Close();
            MySQLDBConnection.Close();

        }

        #endregion 

        #region Convert Access to MySQL Data - CarsData -> EngineData
        public void CarsDataToEngineData()
        {

            AccessConnection = new OleDbConnection(AccessConnectStringForInfo);
            AccessConnection.Open();
            AccessQuery = "SELECT * FROM CarsData";
            AccessCommand = new OleDbCommand(AccessQuery, AccessConnection);
            AccessDataReader = AccessCommand.ExecuteReader();

            MySQLDBConnection.Open();

            try
            {

                while (AccessDataReader.Read())
                {

                    MySQLQuery = "insert into moshech_aac.enginedata(ManufacturerID, EngineModel, CarModelID) values('"
                    + AccessDataReader["MANUFACTURER"] + "','"
                    + AccessDataReader["MOTOR_MODEL"] + "','"
                    + AccessDataReader["CarDegemCode"] + "')";

                    MySQLCommand = new MySqlCommand(MySQLQuery, MySQLDBConnection);
                    MySQLReader = MySQLCommand.ExecuteReader();
                    MySQLReader.Close();

                }
            }
            catch (Exception) { }//When there is no correct manufacturer/engine model/car model etc then just don't even add it to the table


            List<CarModelPair> ModelPair = new List<CarModelPair>();
            int ModelPairIterate = 0;

            try
            {

                MySQLQuery = "SELECT * FROM moshech_aac.carmodels";
                MySQLCommand = new MySqlCommand(MySQLQuery, MySQLDBConnection);
                MySQLReader = MySQLCommand.ExecuteReader();

                while (MySQLReader.Read())
                {

                    ModelPair.Add(new CarModelPair());
                    ModelPair[ModelPairIterate].CarModelID = (MySQLReader.GetString("ModelCode"));
                    ModelPair[ModelPairIterate].CarModel = (MySQLReader.GetString("CarModelName").Replace("'", @"\'"));

                    ModelPairIterate++;
                }

                MySQLReader.Close();

                foreach (CarModelPair ModelPairFromList in ModelPair)//Throw all the model names in from our ModelNames list
                {

                    MySQLQuery = "UPDATE moshech_aac.enginedata SET CarModel = '" + ModelPairFromList.CarModel + "' WHERE CarModelID LIKE '%" + ModelPairFromList.CarModelID + "%' ";
                    MySQLCommand = new MySqlCommand(MySQLQuery, MySQLDBConnection);
                    MySQLReader = MySQLCommand.ExecuteReader();
                    MySQLReader.Close();
                }
            }
            catch (Exception e) { MessageBox.Show(e.ToString()); }

            AccessConnection.Close();
            MySQLDBConnection.Close();

        }

        #endregion

        #region Convert Access to MySQL Data - LikuiList -> DefectList

        public void LikuiListToDefectList()
        {

            AccessConnection = new OleDbConnection(AccessConnectString);
            AccessConnection.Open();
            AccessQuery = "SELECT * FROM LikuiList";
            AccessCommand = new OleDbCommand(AccessQuery, AccessConnection);
            AccessDataReader = AccessCommand.ExecuteReader();

            MySQLDBConnection.Open();

            while (AccessDataReader.Read())
            {

                try
                {

                    MySQLQuery = "insert into moshech_aac.defectinfo(DefectID, DefectInfo, Remark, StationNumber, Permit, RequiresPay) values('"
                    + AccessDataReader["LikuiID"] + "','"
                    + StringFromReader(AccessDataReader, "Info") + "','"
                    + StringFromReader(AccessDataReader, "Remark") + "','"
                    + AccessDataReader["embda"] + "','"
                    + Convert.ToInt32(AccessDataReader["Yeshor"]) + "','"
                    + Convert.ToInt32(AccessDataReader["IsLikuiCash"]) + "')";

                    MySQLCommand = new MySqlCommand(MySQLQuery, MySQLDBConnection);
                    MySQLReader = MySQLCommand.ExecuteReader();
                    MySQLReader.Close();

                }

                catch 
                {

                    MySQLQuery = "insert into moshech_aac.defectinfo(DefectID, DefectInfo, Remark, StationNumber, Permit, RequiresPay) values('"
                    + AccessDataReader["LikuiID"] + "','"
                    + StringFromReader(AccessDataReader, "Info") + "','"
                    + "" + "','"
                    + AccessDataReader["embda"] + "','"
                    + Convert.ToInt32(AccessDataReader["Yeshor"]) + "','"
                    + Convert.ToInt32(AccessDataReader["IsLikuiCash"]) + "')";

                    MySQLCommand = new MySqlCommand(MySQLQuery, MySQLDBConnection);
                    MySQLReader = MySQLCommand.ExecuteReader();
                    MySQLReader.Close();

                }
            }

            AccessConnection.Close();
            MySQLDBConnection.Close();

        }

        #endregion 

        #region Convert Access to MySQL Data - CarEnteranceHistory -> CarEntranceHistory
        public void CarEnteranceHistoryToCarEntranceHistory()
        {

            AccessConnection = new OleDbConnection(AccessConnectString);
            AccessConnection.Open();
            AccessQuery = "SELECT * FROM CarEnteranceHistory";
            AccessCommand = new OleDbCommand(AccessQuery, AccessConnection);
            AccessDataReader = AccessCommand.ExecuteReader();

            //These strings will stay empty if the date or time of entrance is empty from the access table
            string EntranceDate = "";
            string EntranceTime = "";

            MySQLDBConnection.Open();

                while (AccessDataReader.Read())
                {

                    try
                    {

                    //Check if the date time fields of entrance are filled or not. If they are null send an empty string
                    if (!(AccessDataReader["EnteranceDate"] == DBNull.Value)) { EntranceDate = Convert.ToDateTime(AccessDataReader["EnteranceDate"]).ToShortDateString(); }
                    if (!(AccessDataReader["EnteranceTime"] == DBNull.Value)) { EntranceTime = Convert.ToDateTime(AccessDataReader["EnteranceTime"]).ToShortTimeString(); }

                    MySQLQuery = "insert into moshech_aac.carentrancehistory(CarCheckID, CarNumber, EntranceDate, EntranceTime, NumOfAttempts) values('"
                    + AccessDataReader["CheckID"] + "','"
                    + AccessDataReader["CarNumber"] + "','"
                    + EntranceDate + "','"
                    + EntranceTime + "','"
                    + AccessDataReader["NumOfTimes"] + "')";

                    MySQLCommand = new MySqlCommand(MySQLQuery, MySQLDBConnection);
                    MySQLReader = MySQLCommand.ExecuteReader();
                    MySQLReader.Close();

                    }

                    catch (Exception e) { MessageBox.Show(e.ToString()); }//When there is no correct manufacturer/engine model/car model etc then just don't even add it to the table

                }

            AccessConnection.Close();
            MySQLDBConnection.Close();
        
        }
          
        #endregion

        #region Convert Access to MySQL Data - Settings -> ServerSettings

        private void SettingsToServerSettings()
        {

            AccessConnection = new OleDbConnection(AccessConnectString);
            AccessConnection.Open();
            AccessQuery = "SELECT * FROM Settings";
            AccessCommand = new OleDbCommand(AccessQuery, AccessConnection);
            AccessDataReader = AccessCommand.ExecuteReader();

            MySQLDBConnection.Open();

            while (AccessDataReader.Read())
            {

                try
                {

                    MySQLQuery = "insert into moshech_aac.server_settings(InstituteAddress, InstituteName, InstituteNumber, MisradRishuiMechozi, Arka_Days, TestMonthsPeriod, LightsComPort, LightsSetting, GasComPort, GasSettings, EndTestReport, StartTestReport, PrintEndReport, Serial, DateOfLicense, PurchaseLicense, OnlinePassword, OnlineDataSupply, KodGorem, LicensePlateSerial, KniaSerial, OnlineUsage, OnlineAddress) values('"
                    + AccessDataReader["MachonAddress"] + "','"
                    + AccessDataReader["MachonName"] + "','"
                    + AccessDataReader["MachonNum"] + "','"
                    + AccessDataReader["MisradRishuiMechozi"] + "','"
                    + AccessDataReader["Arka_Days"] + "','"
                    + AccessDataReader["Test_Months_Period"] + "','"
                    + AccessDataReader["LightsComPort"] + "','"
                    + AccessDataReader["LightsSetting"] + "','"
                    + AccessDataReader["GasComPort"] + "','"
                    + AccessDataReader["GasSettings"] + "','"
                    + AccessDataReader["EndTestReport"] + "','"
                    + AccessDataReader["StartTestReport"] + "','"
                    + Convert.ToInt32(AccessDataReader["PrintEndReport"]) + "','"
                    + AccessDataReader["serial"] + "','"
                    + AccessDataReader["DateOfLicense"] + "','"
                    + AccessDataReader["PurchaseLicense"] + "','"
                    + AccessDataReader["OnlinePwd"] + "','"
                    + AccessDataReader["OnLineDataSupply"] + "','"
                    + AccessDataReader["KodGorem"] + "','"
                    + AccessDataReader["LicensePlateSerial"] + "','"
                    + AccessDataReader["KniaSerial"] + "','"
                    + Convert.ToInt32(AccessDataReader["OnlineUsage"]) + "','"
                    + AccessDataReader["OnLineAddress"] + "')";

                    MySQLCommand = new MySqlCommand(MySQLQuery, MySQLDBConnection);
                    MySQLReader = MySQLCommand.ExecuteReader();
                    MySQLReader.Close();

                }

                catch (Exception e) { MessageBox.Show(e.Message); }

            }

            AccessConnection.Close();
            MySQLDBConnection.Close();
        

        }

        #endregion

        #region Convert Access to MySQL Data - PrintTestListRpt -> Car_Test_List_Report

        private void PrintTestListRptToCar_Test_List_Report()
        {

            AccessConnection = new OleDbConnection(AccessConnectStringForAAC_Local);
            AccessConnection.Open();
            AccessQuery = "SELECT * FROM PrintTestListRpt";
            AccessCommand = new OleDbCommand(AccessQuery, AccessConnection);
            AccessDataReader = AccessCommand.ExecuteReader();

            MySQLDBConnection.Open();

            while (AccessDataReader.Read())
            {

                try
                {

                    MySQLQuery = "insert into moshech_aac.car_test_list_report(CarCheckID, CarNumber, LastCheckDateTime, TestType, CarCheckFinal, CarType, CarOwnerName, LBL, ReportID) values('"
                    + AccessDataReader["CarCheckID"] + "','"
                    + AccessDataReader["CarNumber"] + "','"
                    + AccessDataReader["LastCheckDate"] + "','"
                    + AccessDataReader["TestType"] + "','"
                    + Convert.ToInt32(AccessDataReader["Pass"]) + "','"
                    + AccessDataReader["CarType"] + "','"
                    + AccessDataReader["CarOwnerName"].ToString().Replace("'", @"\'") + "','"
                    + AccessDataReader["LBL"] + "','"
                    + AccessDataReader["RptID"] + "')";

                    MySQLCommand = new MySqlCommand(MySQLQuery, MySQLDBConnection);
                    MySQLReader = MySQLCommand.ExecuteReader();
                    MySQLReader.Close();

                }

                catch (Exception e) { MessageBox.Show(e.Message); }

            }

            AccessConnection.Close();
            MySQLDBConnection.Close();
        


        }


        #endregion 

        #region Safe reader functions
        private int IntFromReader(OleDbDataReader DataReader, string MemberName)
        {
            try
            {
                return (int)DataReader[MemberName];
            }
            catch (Exception)
            {
                return 0;
            }
        }

        private string StringFromReader(OleDbDataReader dataReader, string memberName)
        {
            try
            {
                string ReadString = (string)dataReader[memberName];  
                ReadString = ReadString.Replace("'", @"\'");

                return ReadString;
            }
            catch (Exception)
            {
                return "";
            }
        }

        #endregion



        #region Test Functions

        public void CatchGibbrishCharsFromTable()
        {
            AccessConnection = new OleDbConnection(AccessConnectString);
            AccessConnection.Open();
            AccessQuery = "SELECT * FROM ActiveCarFile";
            AccessCommand = new OleDbCommand(AccessQuery, AccessConnection);
            AccessDataReader = AccessCommand.ExecuteReader();

            MySQLDBConnection.Open();
           //Catch specific var
            while (AccessDataReader.Read())
            {
                    try
                    {
                    
                        string CarOwnerName = AccessDataReader["CarOwnerName"].ToString().Replace("'", @"\'");
                        string CarDegemManoa = AccessDataReader["CarOwnerName"].ToString().Replace("'", @"\'");
                        string CarBringFirstName = AccessDataReader["CarOwnerName"].ToString().Replace("'", @"\'");
                        string CarBringLastName = AccessDataReader["CarOwnerName"].ToString().Replace("'", @"\'");
                        string CarMake = AccessDataReader["CarOwnerName"].ToString().Replace("'", @"\'");


                    MySQLQuery = "insert into moshech_aac.testtable(TestString, TestString2, TestString3, TestString4, TestString5) values('"
                    + AccessDataReader["CarOwnerName"] + "','"
                    + AccessDataReader["CarOwnerName"] + "','"
                    + CarBringFirstName + "','"
                    + CarBringLastName + "','" 
                    + CarMake + "')";

                    MySQLCommand = new MySqlCommand(MySQLQuery, MySQLDBConnection);
                    MySQLReader = MySQLCommand.ExecuteReader();
                    MySQLReader.Close();
                    }

                    catch (Exception)
                    {
                    
                    
                    MySQLQuery = "insert into moshech_aac.testtable(TestString, TestString2, TestString3, TestString4, TestString5) values('" + '_' + "','" + '_' + "','" + '_' + "','" + '_' + "','" + '_' + "')"; 
                    MySQLCommand = new MySqlCommand(MySQLQuery, MySQLDBConnection);
                    MySQLReader = MySQLCommand.ExecuteReader();
                    MySQLReader.Close();

                    }
            }

            
            AccessConnection.Close();
            MySQLDBConnection.Close();
        }
        #endregion 

        #endregion

        #region Conversion Related Functions
        public void ResetTable(string TableName, string AccessConnectionString)//Reset any table data. Args: name of table to reset
        {

            AccessConnection = new OleDbConnection(AccessConnectionString);
            MySQLDBConnection.Open();
            MySQLQuery = "TRUNCATE TABLE " + TableName + ";";

            MySQLCommand = new MySqlCommand(MySQLQuery, MySQLDBConnection);
            MySQLReader = MySQLCommand.ExecuteReader();

            MySQLDBConnection.Close();
            MessageBox.Show("מחיקת נתונים הושלמה");

        }
        public int CountTableRecords(string TableName, string AccessConnectionString)
        {
            AccessConnection = new OleDbConnection(AccessConnectionString);
            AccessConnection.Open();
            AccessQuery = "SELECT COUNT(*) FROM " + TableName + ";";

            AccessCommand = new OleDbCommand(AccessQuery, AccessConnection);
            int RecordCount = Convert.ToInt32(AccessCommand.ExecuteScalar());

            AccessConnection.Close();
            return RecordCount;
        }

        #endregion

        private void Converter_Load(object sender, EventArgs e)
        {
            
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }




     }
  }

