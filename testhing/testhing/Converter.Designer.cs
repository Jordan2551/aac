﻿namespace testhing
{
    partial class Converter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Converter));
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ToServerSettings = new RishuiClient.GUIUserControls.AdvancedButton();
            this.ToTestType = new RishuiClient.GUIUserControls.AdvancedButton();
            this.ToCarEntranceHistory = new RishuiClient.GUIUserControls.AdvancedButton();
            this.ToEmdadata = new RishuiClient.GUIUserControls.AdvancedButton();
            this.ToReceiveVehicle = new RishuiClient.GUIUserControls.AdvancedButton();
            this.ToEngineData = new RishuiClient.GUIUserControls.AdvancedButton();
            this.ToCarColors = new RishuiClient.GUIUserControls.AdvancedButton();
            this.ToCarManufacturers = new RishuiClient.GUIUserControls.AdvancedButton();
            this.ToDefectList = new RishuiClient.GUIUserControls.AdvancedButton();
            this.ToCarmodels = new RishuiClient.GUIUserControls.AdvancedButton();
            this.ToVehicleType = new RishuiClient.GUIUserControls.AdvancedButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.RecordCountCB = new System.Windows.Forms.ComboBox();
            this.CountRecordsButton = new RishuiClient.GUIUserControls.AdvancedButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ResetTableCB = new System.Windows.Forms.ComboBox();
            this.ResetRecordsButton = new RishuiClient.GUIUserControls.AdvancedButton();
            this.MainProgressBar = new CommInf.UserControls.InvokeableProgressBar();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ToCar_Test_List_Report = new RishuiClient.GUIUserControls.AdvancedButton();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ToServerSettings);
            this.groupBox1.Controls.Add(this.ToTestType);
            this.groupBox1.Controls.Add(this.ToCarEntranceHistory);
            this.groupBox1.Controls.Add(this.ToEmdadata);
            this.groupBox1.Controls.Add(this.ToReceiveVehicle);
            this.groupBox1.Controls.Add(this.ToEngineData);
            this.groupBox1.Controls.Add(this.ToCarColors);
            this.groupBox1.Controls.Add(this.ToCarManufacturers);
            this.groupBox1.Controls.Add(this.ToDefectList);
            this.groupBox1.Controls.Add(this.ToCarmodels);
            this.groupBox1.Controls.Add(this.ToVehicleType);
            this.groupBox1.Location = new System.Drawing.Point(12, 47);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(498, 364);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // ToServerSettings
            // 
            this.ToServerSettings.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ToServerSettings.BackgroundImage")));
            this.ToServerSettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ToServerSettings.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("ToServerSettings.ClickedHoverImage")));
            this.ToServerSettings.ClickedImage = ((System.Drawing.Image)(resources.GetObject("ToServerSettings.ClickedImage")));
            this.ToServerSettings.ClickState = false;
            this.ToServerSettings.FlatAppearance.BorderSize = 0;
            this.ToServerSettings.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ToServerSettings.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ToServerSettings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ToServerSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ToServerSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToServerSettings.ForeColor = System.Drawing.Color.White;
            this.ToServerSettings.HoverImage = ((System.Drawing.Image)(resources.GetObject("ToServerSettings.HoverImage")));
            this.ToServerSettings.IdleImage = ((System.Drawing.Image)(resources.GetObject("ToServerSettings.IdleImage")));
            this.ToServerSettings.InactiveImage = null;
            this.ToServerSettings.IsToggleable = false;
            this.ToServerSettings.Location = new System.Drawing.Point(11, 294);
            this.ToServerSettings.Name = "ToServerSettings";
            this.ToServerSettings.Size = new System.Drawing.Size(234, 50);
            this.ToServerSettings.TabIndex = 29;
            this.ToServerSettings.Text = "Settings -> ServerSettings";
            this.ToServerSettings.UseVisualStyleBackColor = true;
            this.ToServerSettings.Click += new System.EventHandler(this.ToServerSettings_Click);
            // 
            // ToTestType
            // 
            this.ToTestType.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ToTestType.BackgroundImage")));
            this.ToTestType.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ToTestType.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("ToTestType.ClickedHoverImage")));
            this.ToTestType.ClickedImage = ((System.Drawing.Image)(resources.GetObject("ToTestType.ClickedImage")));
            this.ToTestType.ClickState = false;
            this.ToTestType.FlatAppearance.BorderSize = 0;
            this.ToTestType.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ToTestType.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ToTestType.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ToTestType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ToTestType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToTestType.ForeColor = System.Drawing.Color.White;
            this.ToTestType.HoverImage = ((System.Drawing.Image)(resources.GetObject("ToTestType.HoverImage")));
            this.ToTestType.IdleImage = ((System.Drawing.Image)(resources.GetObject("ToTestType.IdleImage")));
            this.ToTestType.InactiveImage = null;
            this.ToTestType.IsToggleable = false;
            this.ToTestType.Location = new System.Drawing.Point(11, 128);
            this.ToTestType.Name = "ToTestType";
            this.ToTestType.Size = new System.Drawing.Size(234, 50);
            this.ToTestType.TabIndex = 23;
            this.ToTestType.Text = "CarTestType -> TestType";
            this.ToTestType.UseVisualStyleBackColor = true;
            this.ToTestType.Click += new System.EventHandler(this.ToTestType_Click);
            // 
            // ToCarEntranceHistory
            // 
            this.ToCarEntranceHistory.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ToCarEntranceHistory.BackgroundImage")));
            this.ToCarEntranceHistory.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ToCarEntranceHistory.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("ToCarEntranceHistory.ClickedHoverImage")));
            this.ToCarEntranceHistory.ClickedImage = ((System.Drawing.Image)(resources.GetObject("ToCarEntranceHistory.ClickedImage")));
            this.ToCarEntranceHistory.ClickState = false;
            this.ToCarEntranceHistory.FlatAppearance.BorderSize = 0;
            this.ToCarEntranceHistory.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ToCarEntranceHistory.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ToCarEntranceHistory.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ToCarEntranceHistory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ToCarEntranceHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToCarEntranceHistory.ForeColor = System.Drawing.Color.White;
            this.ToCarEntranceHistory.HoverImage = ((System.Drawing.Image)(resources.GetObject("ToCarEntranceHistory.HoverImage")));
            this.ToCarEntranceHistory.IdleImage = ((System.Drawing.Image)(resources.GetObject("ToCarEntranceHistory.IdleImage")));
            this.ToCarEntranceHistory.InactiveImage = null;
            this.ToCarEntranceHistory.IsToggleable = false;
            this.ToCarEntranceHistory.Location = new System.Drawing.Point(247, 238);
            this.ToCarEntranceHistory.Name = "ToCarEntranceHistory";
            this.ToCarEntranceHistory.Size = new System.Drawing.Size(234, 50);
            this.ToCarEntranceHistory.TabIndex = 28;
            this.ToCarEntranceHistory.Text = "CarEnteranceHistory  -> carentrancehistory";
            this.ToCarEntranceHistory.UseVisualStyleBackColor = true;
            this.ToCarEntranceHistory.Click += new System.EventHandler(this.ToCarEntranceHistory_Click);
            // 
            // ToEmdadata
            // 
            this.ToEmdadata.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ToEmdadata.BackgroundImage")));
            this.ToEmdadata.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ToEmdadata.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("ToEmdadata.ClickedHoverImage")));
            this.ToEmdadata.ClickedImage = ((System.Drawing.Image)(resources.GetObject("ToEmdadata.ClickedImage")));
            this.ToEmdadata.ClickState = false;
            this.ToEmdadata.FlatAppearance.BorderSize = 0;
            this.ToEmdadata.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ToEmdadata.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ToEmdadata.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ToEmdadata.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ToEmdadata.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToEmdadata.ForeColor = System.Drawing.Color.White;
            this.ToEmdadata.HoverImage = ((System.Drawing.Image)(resources.GetObject("ToEmdadata.HoverImage")));
            this.ToEmdadata.IdleImage = ((System.Drawing.Image)(resources.GetObject("ToEmdadata.IdleImage")));
            this.ToEmdadata.InactiveImage = null;
            this.ToEmdadata.IsToggleable = false;
            this.ToEmdadata.Location = new System.Drawing.Point(247, 73);
            this.ToEmdadata.Name = "ToEmdadata";
            this.ToEmdadata.Size = new System.Drawing.Size(234, 50);
            this.ToEmdadata.TabIndex = 22;
            this.ToEmdadata.Text = "ActiveEmbdaInfo + ActiveTestLikui -> Emdadata   ";
            this.ToEmdadata.UseVisualStyleBackColor = true;
            this.ToEmdadata.Click += new System.EventHandler(this.ToEmdadata_Click);
            // 
            // ToReceiveVehicle
            // 
            this.ToReceiveVehicle.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ToReceiveVehicle.BackgroundImage")));
            this.ToReceiveVehicle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ToReceiveVehicle.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("ToReceiveVehicle.ClickedHoverImage")));
            this.ToReceiveVehicle.ClickedImage = ((System.Drawing.Image)(resources.GetObject("ToReceiveVehicle.ClickedImage")));
            this.ToReceiveVehicle.ClickState = false;
            this.ToReceiveVehicle.FlatAppearance.BorderSize = 0;
            this.ToReceiveVehicle.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ToReceiveVehicle.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ToReceiveVehicle.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ToReceiveVehicle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ToReceiveVehicle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToReceiveVehicle.ForeColor = System.Drawing.Color.White;
            this.ToReceiveVehicle.HoverImage = ((System.Drawing.Image)(resources.GetObject("ToReceiveVehicle.HoverImage")));
            this.ToReceiveVehicle.IdleImage = ((System.Drawing.Image)(resources.GetObject("ToReceiveVehicle.IdleImage")));
            this.ToReceiveVehicle.InactiveImage = null;
            this.ToReceiveVehicle.IsToggleable = false;
            this.ToReceiveVehicle.Location = new System.Drawing.Point(247, 17);
            this.ToReceiveVehicle.Name = "ToReceiveVehicle";
            this.ToReceiveVehicle.Size = new System.Drawing.Size(234, 50);
            this.ToReceiveVehicle.TabIndex = 21;
            this.ToReceiveVehicle.Text = "ActiveCarFile - > ReceiveVehicle";
            this.ToReceiveVehicle.UseVisualStyleBackColor = true;
            this.ToReceiveVehicle.Click += new System.EventHandler(this.ToReceiveVehicle_Click);
            // 
            // ToEngineData
            // 
            this.ToEngineData.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ToEngineData.BackgroundImage")));
            this.ToEngineData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ToEngineData.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("ToEngineData.ClickedHoverImage")));
            this.ToEngineData.ClickedImage = ((System.Drawing.Image)(resources.GetObject("ToEngineData.ClickedImage")));
            this.ToEngineData.ClickState = false;
            this.ToEngineData.FlatAppearance.BorderSize = 0;
            this.ToEngineData.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ToEngineData.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ToEngineData.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ToEngineData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ToEngineData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToEngineData.ForeColor = System.Drawing.Color.White;
            this.ToEngineData.HoverImage = ((System.Drawing.Image)(resources.GetObject("ToEngineData.HoverImage")));
            this.ToEngineData.IdleImage = ((System.Drawing.Image)(resources.GetObject("ToEngineData.IdleImage")));
            this.ToEngineData.InactiveImage = null;
            this.ToEngineData.IsToggleable = false;
            this.ToEngineData.Location = new System.Drawing.Point(247, 184);
            this.ToEngineData.Name = "ToEngineData";
            this.ToEngineData.Size = new System.Drawing.Size(234, 50);
            this.ToEngineData.TabIndex = 25;
            this.ToEngineData.Text = "CarsData -> EngineData";
            this.ToEngineData.UseVisualStyleBackColor = true;
            this.ToEngineData.Click += new System.EventHandler(this.ToEngineData_Click);
            // 
            // ToCarColors
            // 
            this.ToCarColors.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ToCarColors.BackgroundImage")));
            this.ToCarColors.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ToCarColors.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("ToCarColors.ClickedHoverImage")));
            this.ToCarColors.ClickedImage = ((System.Drawing.Image)(resources.GetObject("ToCarColors.ClickedImage")));
            this.ToCarColors.ClickState = false;
            this.ToCarColors.FlatAppearance.BorderSize = 0;
            this.ToCarColors.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ToCarColors.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ToCarColors.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ToCarColors.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ToCarColors.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToCarColors.ForeColor = System.Drawing.Color.White;
            this.ToCarColors.HoverImage = ((System.Drawing.Image)(resources.GetObject("ToCarColors.HoverImage")));
            this.ToCarColors.IdleImage = ((System.Drawing.Image)(resources.GetObject("ToCarColors.IdleImage")));
            this.ToCarColors.InactiveImage = null;
            this.ToCarColors.IsToggleable = false;
            this.ToCarColors.Location = new System.Drawing.Point(11, 73);
            this.ToCarColors.Name = "ToCarColors";
            this.ToCarColors.Size = new System.Drawing.Size(234, 50);
            this.ToCarColors.TabIndex = 21;
            this.ToCarColors.Text = "CarColorType -> CarColors";
            this.ToCarColors.UseVisualStyleBackColor = true;
            this.ToCarColors.Click += new System.EventHandler(this.ToCarColors_Click);
            // 
            // ToCarManufacturers
            // 
            this.ToCarManufacturers.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ToCarManufacturers.BackgroundImage")));
            this.ToCarManufacturers.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ToCarManufacturers.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("ToCarManufacturers.ClickedHoverImage")));
            this.ToCarManufacturers.ClickedImage = ((System.Drawing.Image)(resources.GetObject("ToCarManufacturers.ClickedImage")));
            this.ToCarManufacturers.ClickState = false;
            this.ToCarManufacturers.FlatAppearance.BorderSize = 0;
            this.ToCarManufacturers.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ToCarManufacturers.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ToCarManufacturers.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ToCarManufacturers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ToCarManufacturers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToCarManufacturers.ForeColor = System.Drawing.Color.White;
            this.ToCarManufacturers.HoverImage = ((System.Drawing.Image)(resources.GetObject("ToCarManufacturers.HoverImage")));
            this.ToCarManufacturers.IdleImage = ((System.Drawing.Image)(resources.GetObject("ToCarManufacturers.IdleImage")));
            this.ToCarManufacturers.InactiveImage = null;
            this.ToCarManufacturers.IsToggleable = false;
            this.ToCarManufacturers.Location = new System.Drawing.Point(247, 128);
            this.ToCarManufacturers.Name = "ToCarManufacturers";
            this.ToCarManufacturers.Size = new System.Drawing.Size(234, 50);
            this.ToCarManufacturers.TabIndex = 21;
            this.ToCarManufacturers.Text = "CarsMotorsFirms -> Carmanufacturers";
            this.ToCarManufacturers.UseVisualStyleBackColor = true;
            this.ToCarManufacturers.Click += new System.EventHandler(this.ToCarmanufacturers_Click);
            // 
            // ToDefectList
            // 
            this.ToDefectList.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ToDefectList.BackgroundImage")));
            this.ToDefectList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ToDefectList.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("ToDefectList.ClickedHoverImage")));
            this.ToDefectList.ClickedImage = ((System.Drawing.Image)(resources.GetObject("ToDefectList.ClickedImage")));
            this.ToDefectList.ClickState = false;
            this.ToDefectList.FlatAppearance.BorderSize = 0;
            this.ToDefectList.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ToDefectList.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ToDefectList.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ToDefectList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ToDefectList.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToDefectList.ForeColor = System.Drawing.Color.White;
            this.ToDefectList.HoverImage = ((System.Drawing.Image)(resources.GetObject("ToDefectList.HoverImage")));
            this.ToDefectList.IdleImage = ((System.Drawing.Image)(resources.GetObject("ToDefectList.IdleImage")));
            this.ToDefectList.InactiveImage = null;
            this.ToDefectList.IsToggleable = false;
            this.ToDefectList.Location = new System.Drawing.Point(11, 238);
            this.ToDefectList.Name = "ToDefectList";
            this.ToDefectList.Size = new System.Drawing.Size(234, 50);
            this.ToDefectList.TabIndex = 26;
            this.ToDefectList.Text = "LikuiList - > DefectList";
            this.ToDefectList.UseVisualStyleBackColor = true;
            this.ToDefectList.Click += new System.EventHandler(this.ToDefectList_Click);
            // 
            // ToCarmodels
            // 
            this.ToCarmodels.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ToCarmodels.BackgroundImage")));
            this.ToCarmodels.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ToCarmodels.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("ToCarmodels.ClickedHoverImage")));
            this.ToCarmodels.ClickedImage = ((System.Drawing.Image)(resources.GetObject("ToCarmodels.ClickedImage")));
            this.ToCarmodels.ClickState = false;
            this.ToCarmodels.FlatAppearance.BorderSize = 0;
            this.ToCarmodels.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ToCarmodels.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ToCarmodels.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ToCarmodels.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ToCarmodels.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToCarmodels.ForeColor = System.Drawing.Color.White;
            this.ToCarmodels.HoverImage = ((System.Drawing.Image)(resources.GetObject("ToCarmodels.HoverImage")));
            this.ToCarmodels.IdleImage = ((System.Drawing.Image)(resources.GetObject("ToCarmodels.IdleImage")));
            this.ToCarmodels.InactiveImage = null;
            this.ToCarmodels.IsToggleable = false;
            this.ToCarmodels.Location = new System.Drawing.Point(11, 17);
            this.ToCarmodels.Name = "ToCarmodels";
            this.ToCarmodels.Size = new System.Drawing.Size(234, 50);
            this.ToCarmodels.TabIndex = 20;
            this.ToCarmodels.Text = "CarDegem -> Carmodels";
            this.ToCarmodels.UseVisualStyleBackColor = true;
            this.ToCarmodels.Click += new System.EventHandler(this.ToCarmodels_Click);
            // 
            // ToVehicleType
            // 
            this.ToVehicleType.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ToVehicleType.BackgroundImage")));
            this.ToVehicleType.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ToVehicleType.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("ToVehicleType.ClickedHoverImage")));
            this.ToVehicleType.ClickedImage = ((System.Drawing.Image)(resources.GetObject("ToVehicleType.ClickedImage")));
            this.ToVehicleType.ClickState = false;
            this.ToVehicleType.FlatAppearance.BorderSize = 0;
            this.ToVehicleType.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ToVehicleType.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ToVehicleType.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ToVehicleType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ToVehicleType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToVehicleType.ForeColor = System.Drawing.Color.White;
            this.ToVehicleType.HoverImage = ((System.Drawing.Image)(resources.GetObject("ToVehicleType.HoverImage")));
            this.ToVehicleType.IdleImage = ((System.Drawing.Image)(resources.GetObject("ToVehicleType.IdleImage")));
            this.ToVehicleType.InactiveImage = null;
            this.ToVehicleType.IsToggleable = false;
            this.ToVehicleType.Location = new System.Drawing.Point(11, 184);
            this.ToVehicleType.Name = "ToVehicleType";
            this.ToVehicleType.Size = new System.Drawing.Size(234, 50);
            this.ToVehicleType.TabIndex = 24;
            this.ToVehicleType.Text = "CarType - > VehicleType";
            this.ToVehicleType.UseVisualStyleBackColor = true;
            this.ToVehicleType.Click += new System.EventHandler(this.ToVehicleType_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(83, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(344, 25);
            this.label1.TabIndex = 14;
            this.label1.Text = "Access AAC -> MySQL Converters";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label2.Location = new System.Drawing.Point(655, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 25);
            this.label2.TabIndex = 15;
            this.label2.Text = "Table Functions";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.RecordCountCB);
            this.groupBox2.Controls.Add(this.CountRecordsButton);
            this.groupBox2.Location = new System.Drawing.Point(528, 133);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(476, 70);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Access Table Functions";
            // 
            // RecordCountCB
            // 
            this.RecordCountCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RecordCountCB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.RecordCountCB.FormattingEnabled = true;
            this.RecordCountCB.Items.AddRange(new object[] {
            "ActiveCarFile",
            "CarDegem",
            "ActiveEmbdaInfo",
            "ActiveTestLikui",
            "CarColorType",
            "CarsMotorsFirm",
            "CarTestType",
            "CarsData",
            "CarType",
            "CarEnteranceHistory",
            "LikuiList",
            "Settings"});
            this.RecordCountCB.Location = new System.Drawing.Point(325, 29);
            this.RecordCountCB.Name = "RecordCountCB";
            this.RecordCountCB.Size = new System.Drawing.Size(140, 28);
            this.RecordCountCB.TabIndex = 28;
            // 
            // CountRecordsButton
            // 
            this.CountRecordsButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CountRecordsButton.BackgroundImage")));
            this.CountRecordsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CountRecordsButton.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("CountRecordsButton.ClickedHoverImage")));
            this.CountRecordsButton.ClickedImage = ((System.Drawing.Image)(resources.GetObject("CountRecordsButton.ClickedImage")));
            this.CountRecordsButton.ClickState = false;
            this.CountRecordsButton.FlatAppearance.BorderSize = 0;
            this.CountRecordsButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.CountRecordsButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.CountRecordsButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.CountRecordsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CountRecordsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CountRecordsButton.ForeColor = System.Drawing.Color.White;
            this.CountRecordsButton.HoverImage = ((System.Drawing.Image)(resources.GetObject("CountRecordsButton.HoverImage")));
            this.CountRecordsButton.IdleImage = ((System.Drawing.Image)(resources.GetObject("CountRecordsButton.IdleImage")));
            this.CountRecordsButton.InactiveImage = null;
            this.CountRecordsButton.IsToggleable = false;
            this.CountRecordsButton.Location = new System.Drawing.Point(6, 26);
            this.CountRecordsButton.Name = "CountRecordsButton";
            this.CountRecordsButton.Size = new System.Drawing.Size(313, 33);
            this.CountRecordsButton.TabIndex = 27;
            this.CountRecordsButton.Text = "Access Table Record Count";
            this.CountRecordsButton.UseVisualStyleBackColor = true;
            this.CountRecordsButton.Click += new System.EventHandler(this.TableRecordCountButton_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ResetTableCB);
            this.groupBox3.Controls.Add(this.ResetRecordsButton);
            this.groupBox3.Location = new System.Drawing.Point(528, 47);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(476, 80);
            this.groupBox3.TabIndex = 17;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "MySQL Table Functions";
            // 
            // ResetTableCB
            // 
            this.ResetTableCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ResetTableCB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.ResetTableCB.FormattingEnabled = true;
            this.ResetTableCB.Items.AddRange(new object[] {
            "receivevehicle",
            "carmodels",
            "emdadata",
            "carcolors",
            "carmanufacturers",
            "testtype",
            "enginedata",
            "vehicletype",
            "carentrancehistory",
            "defectlist",
            "server_settings"});
            this.ResetTableCB.Location = new System.Drawing.Point(325, 36);
            this.ResetTableCB.Name = "ResetTableCB";
            this.ResetTableCB.Size = new System.Drawing.Size(140, 28);
            this.ResetTableCB.TabIndex = 27;
            // 
            // ResetRecordsButton
            // 
            this.ResetRecordsButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ResetRecordsButton.BackgroundImage")));
            this.ResetRecordsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ResetRecordsButton.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("ResetRecordsButton.ClickedHoverImage")));
            this.ResetRecordsButton.ClickedImage = ((System.Drawing.Image)(resources.GetObject("ResetRecordsButton.ClickedImage")));
            this.ResetRecordsButton.ClickState = false;
            this.ResetRecordsButton.FlatAppearance.BorderSize = 0;
            this.ResetRecordsButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ResetRecordsButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ResetRecordsButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ResetRecordsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ResetRecordsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResetRecordsButton.ForeColor = System.Drawing.Color.White;
            this.ResetRecordsButton.HoverImage = ((System.Drawing.Image)(resources.GetObject("ResetRecordsButton.HoverImage")));
            this.ResetRecordsButton.IdleImage = ((System.Drawing.Image)(resources.GetObject("ResetRecordsButton.IdleImage")));
            this.ResetRecordsButton.InactiveImage = null;
            this.ResetRecordsButton.IsToggleable = false;
            this.ResetRecordsButton.Location = new System.Drawing.Point(6, 33);
            this.ResetRecordsButton.Name = "ResetRecordsButton";
            this.ResetRecordsButton.Size = new System.Drawing.Size(313, 33);
            this.ResetRecordsButton.TabIndex = 26;
            this.ResetRecordsButton.Text = "Reset MySQL Table";
            this.ResetRecordsButton.UseVisualStyleBackColor = true;
            this.ResetRecordsButton.Click += new System.EventHandler(this.ResetRecordsButton_Click);
            // 
            // MainProgressBar
            // 
            this.MainProgressBar.Location = new System.Drawing.Point(515, 165);
            this.MainProgressBar.Name = "MainProgressBar";
            this.MainProgressBar.ProgressMax = ((uint)(0u));
            this.MainProgressBar.ProgressValue = ((uint)(0u));
            this.MainProgressBar.Size = new System.Drawing.Size(13, 25);
            this.MainProgressBar.TabIndex = 19;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ToCar_Test_List_Report);
            this.groupBox4.Location = new System.Drawing.Point(532, 231);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(471, 179);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            // 
            // ToCar_Test_List_Report
            // 
            this.ToCar_Test_List_Report.BackgroundImage = global::MSDToMySQLConverter.Properties.Resources.Button_0001s_0002_Idle;
            this.ToCar_Test_List_Report.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ToCar_Test_List_Report.ClickedHoverImage = global::MSDToMySQLConverter.Properties.Resources.ButtonHoverClick_0002s_0002_Clicked_copy;
            this.ToCar_Test_List_Report.ClickedImage = global::MSDToMySQLConverter.Properties.Resources.Button_0000s_0000_Clicked;
            this.ToCar_Test_List_Report.ClickState = false;
            this.ToCar_Test_List_Report.FlatAppearance.BorderSize = 0;
            this.ToCar_Test_List_Report.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.ToCar_Test_List_Report.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ToCar_Test_List_Report.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ToCar_Test_List_Report.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ToCar_Test_List_Report.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToCar_Test_List_Report.ForeColor = System.Drawing.Color.White;
            this.ToCar_Test_List_Report.HoverImage = global::MSDToMySQLConverter.Properties.Resources.Button_0001s_0001_Hover;
            this.ToCar_Test_List_Report.IdleImage = global::MSDToMySQLConverter.Properties.Resources.Button_0001s_0002_Idle;
            this.ToCar_Test_List_Report.InactiveImage = null;
            this.ToCar_Test_List_Report.IsToggleable = false;
            this.ToCar_Test_List_Report.Location = new System.Drawing.Point(6, 14);
            this.ToCar_Test_List_Report.Name = "ToCar_Test_List_Report";
            this.ToCar_Test_List_Report.Size = new System.Drawing.Size(196, 50);
            this.ToCar_Test_List_Report.TabIndex = 30;
            this.ToCar_Test_List_Report.Text = "PrintTestListRpt -> Car_Test_List_Report";
            this.ToCar_Test_List_Report.UseVisualStyleBackColor = true;
            this.ToCar_Test_List_Report.Click += new System.EventHandler(this.ToCar_Test_List_Report_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label3.Location = new System.Drawing.Point(579, 206);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(365, 25);
            this.label3.TabIndex = 21;
            this.label3.Text = "Access -> MySQL Report Converters";
            // 
            // Converter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1011, 415);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.MainProgressBar);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Converter";
            this.Text = "Converter";
            this.Load += new System.EventHandler(this.Converter_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private CommInf.UserControls.InvokeableProgressBar MainProgressBar;
        private RishuiClient.GUIUserControls.AdvancedButton ToTestType;
        private RishuiClient.GUIUserControls.AdvancedButton ToCarEntranceHistory;
        private RishuiClient.GUIUserControls.AdvancedButton ToEmdadata;
        private RishuiClient.GUIUserControls.AdvancedButton ToReceiveVehicle;
        private RishuiClient.GUIUserControls.AdvancedButton ToEngineData;
        private RishuiClient.GUIUserControls.AdvancedButton ToCarColors;
        private RishuiClient.GUIUserControls.AdvancedButton ToCarManufacturers;
        private RishuiClient.GUIUserControls.AdvancedButton ToDefectList;
        private RishuiClient.GUIUserControls.AdvancedButton ToCarmodels;
        private RishuiClient.GUIUserControls.AdvancedButton ToVehicleType;
        private RishuiClient.GUIUserControls.AdvancedButton CountRecordsButton;
        private RishuiClient.GUIUserControls.AdvancedButton ResetRecordsButton;
        private System.Windows.Forms.ComboBox RecordCountCB;
        private System.Windows.Forms.ComboBox ResetTableCB;
        private RishuiClient.GUIUserControls.AdvancedButton ToServerSettings;
        private System.Windows.Forms.GroupBox groupBox4;
        private RishuiClient.GUIUserControls.AdvancedButton ToCar_Test_List_Report;
        private System.Windows.Forms.Label label3;

    }
}

