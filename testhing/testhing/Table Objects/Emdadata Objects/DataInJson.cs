﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testhing.Table_Objects.Emdadata_Objects
{
    public class EmdaDataInJsonGeneralStationInfo//Containts the general data about the Station and who signed it, etc
    {
       public int TesterIDOfSignedStation {get; set;}
       public int StationNumber {get; set;}
       public string TesterNameOfSignedStation {get; set;}
       public int StationFailed {get; set;}
       public string StationSignDate {get; set;}
       public int IsStationClosed {get; set;}
       public string TimeExpiredReleaseStation {get; set;}

       public List<EmdaDataInJsonStationDefectInfo> DefectList = new List<EmdaDataInJsonStationDefectInfo>();//This list contains a defect in every entry

    }

    public class EmdaDataInJsonStationDefectInfo//Contains the info per defect for every station
    {
        public int DefectID { get; set; }
        public string DefectInfo { get; set; }
        public int Transmitted { get; set; }
        public int DefectFixed { get; set; }
        public int PermitRequest { get; set; }
        public int PermitNumber { get; set; }
        public string PermitDate { get; set; }
        public int RequiresPay { get; set; }
        public string FixDate { get; set; }
        public int TesterIDOfSignedDefect { get; set; }
        public string TesterNameOfSignedDefect { get; set; }
        public string DefectSignDateTime { get; set; }
        public int DangerousDefect { get; set; }
    }
}
