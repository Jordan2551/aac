﻿using CommInf.Items;
using CommInf.Items.SessionItems;
using RishuiClient.CommManagers;
using RishuiClient.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RishuiClient
{
    static class Program
    {
        #region CommInf setup for client

        public static RishuiClientCommManager myCommManager;
        static RishuiServerProxy.ServerWebServiceSoapClient webService;
        public static void SendTransferItem(TransferItem tItem)
        {
            string serializedTransferItem = tItem.SerializeWithJSon();
            string[] retStringArr = webService.TransferData(tItem.SerializeWithJSon()).ToArray();
            foreach(string str in retStringArr)
            {
                if(str != "")
                    myCommManager.HandleTransferItem(TransferItem.DeserializeJsonString(str));
            }
        }

        public static int SessionID
        {
            get
            {
                if (myCommManager.IsConnected)
                {
                    return myCommManager.SessionID;
                }
                else
                    return 0;
            }
        }

        #endregion
        //fix: how many columns and rows should be put when drawing the layout for the defect buttons
        //Why are things like the more info button and the UC for idstation drawing at off places
        //User switching causes multiple windows to open...maybe make the form static....
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            myCommManager = new RishuiClientCommManager();
            webService = new RishuiServerProxy.ServerWebServiceSoapClient ();
            SendTransferItem(new SessionRequestTransferItem());

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());

            MainWindow.ServerInformation.InstituteName = "אוטו טסט בעמ"; 
            MainWindow.UserInformation.testerName = "ירדן כהן";
            MainWindow.UserInformation.userID = 2048;



        }
    }

    static class FormPointers
    {
        public static MainWindow MainWindowFormPointer = new MainWindow();
        public static LoginForm LoginFormPointer = new LoginForm();
    }
}
