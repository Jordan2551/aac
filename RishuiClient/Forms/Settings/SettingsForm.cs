﻿using CommInf.Handlers;
using RishuiClient.User_Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RishuiClient.Forms
{
    public partial class SettingsForm : Form
    {
        #region Members

        //Example member:
        //private int myInt;

        #endregion

        public SettingsForm()
        {
            InitializeComponent();

            #region Mapping

            //Here you map all the handlers to the client's communication manager.

            //Example:
            //Program.myCommManager.MapHandler(userSettings1);

            #endregion
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormPointers.MainWindowFormPointer.Show();
            this.Close();
        }

        private void Settings_Load(object sender, EventArgs e)
        {


        }

        private void Settings_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.userSettings1.RFIDComm.Close();//When this form is closed, make sure to close the RFIDComm port so that other parts of the program can use it
        }

        #region GUI Handlers


        #endregion

        #region TransferItemHandlers

        #endregion
    }
}
