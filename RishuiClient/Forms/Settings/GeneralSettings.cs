﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommInf.Handlers;
using RishuiCommDefines.TransferItems.GeneralSettingsItems;
using CommInf.Items;
using RishuiClient.Types_and_Functions;

namespace RishuiClient.Forms.Settings_Forms
{
    public partial class GeneralSettings : UserControl
    {

        public GeneralSettings()
        {

            InitializeComponent();

            #region Mapping

            if (Program.myCommManager != null)
            {

                Program.myCommManager.MapHandler(HandleGeneralSettingsResultTransferItem);
                Program.myCommManager.MapHandler(HandleGeneralSettingsSaveResultTransferItem);

                GeneralSettingsLoadRequestTransferItem LoadRequestItem = new GeneralSettingsLoadRequestTransferItem();
                Program.SendTransferItem(LoadRequestItem);

            }

            #endregion

        }

        #region Definitions

        struct GeneralSettingsStruct
        {
            public int ID;
            public string InstituteName;
            public int LicenseNumber;
            public string instAddress;
            public string InstituteRegion;
            public int WorkStation;
            public int mandatoryStationSign;
            public int CourseNumber;
            public int passwordBetweenStands;
            public int printReportUponEndOfInspection;
            public int LockStation;
            public int printReportEndOfStation;
            public int lockTime;
            public int enableMultipleScreens;
            public int numOfConnectedScreens;
            public int ScreenSway;
        }

        GeneralSettingsStruct Settings;

        #endregion

        #region GUI Handlers

        private void GeneralSettings_Load(object sender, EventArgs e)
        {

        }

        private void saveButton_Click(object sender, EventArgs e)
        {

            //Check if the boxes are full BEFORE sending the info to the server
            if (InstituteName.Text == null | LicenseNumber.Text == null | InstituteAddress.Text == null | InstituteRegion.Text == null | InstituteName.Text == null | StationLockTime.Text == null | NumberOfConnectedScreens.Text == null | ScreenSway.Text == null)
            {
                MessageBox.Show("בבקשה מלא את כל הסעיפים");
            }

            else
            {
                GeneralSettingsSaveRequestTransferItem SaveRequestItem = new GeneralSettingsSaveRequestTransferItem();

                updateGeneralSettings();

                SaveRequestItem.ID = Settings.ID;
                SaveRequestItem.InstituteName = Settings.InstituteName;
                SaveRequestItem.instAddress = Settings.instAddress;
                SaveRequestItem.InstituteRegion = Settings.InstituteRegion;
                SaveRequestItem.WorkStation = Settings.WorkStation;
                SaveRequestItem.mandatoryStationSign = Settings.mandatoryStationSign;
                SaveRequestItem.CourseNumber = Settings.CourseNumber;
                SaveRequestItem.passwordBetweenStands = Settings.passwordBetweenStands;
                SaveRequestItem.printReportUponEndOfInspection = Settings.printReportUponEndOfInspection;
                SaveRequestItem.LockStation = Settings.LockStation;
                SaveRequestItem.printReportEndOfStation = Settings.printReportEndOfStation;
                SaveRequestItem.lockTime = Settings.lockTime;
                SaveRequestItem.enableMultipleScreens = Settings.enableMultipleScreens;
                SaveRequestItem.numOfConnectedScreens = Settings.numOfConnectedScreens;
                SaveRequestItem.ScreenSway = Settings.ScreenSway;

                Program.SendTransferItem(SaveRequestItem);
            }
        }

        #endregion

        #region TransferItemHandlers


        #region Load General Settings
        //An example of a TransferItem handler. We mapped this handler to handle it's type in the Form constructor,
        //under the 'Mappings' region.

        [HandleTypeDef(typeof(GeneralSettingsLoadResultTransferItem))]
        private void HandleGeneralSettingsResultTransferItem(TransferItem tItem)
        {

            GeneralSettingsLoadResultTransferItem LoadItem = (GeneralSettingsLoadResultTransferItem)tItem;

            //Load up the struct object so we can use it to save the info later
            Settings.ID = LoadItem.ID;
            Settings.InstituteName = LoadItem.InstituteName;
            Settings.LicenseNumber = LoadItem.LicenseNumber;
            Settings.instAddress = LoadItem.InstituteAddress;
            Settings.InstituteRegion = LoadItem.InstituteRegion;
            Settings.WorkStation = LoadItem.WorkStation;
            Settings.mandatoryStationSign = LoadItem.mandatoryStationSign;
            Settings.CourseNumber = LoadItem.CourseNumber;
            Settings.passwordBetweenStands = LoadItem.passwordBetweenStands;
            Settings.printReportUponEndOfInspection = LoadItem.printReportUponEndOfInspection;
            Settings.printReportEndOfStation = LoadItem.printReportEndOfStation;
            Settings.lockTime = LoadItem.lockTime;
            Settings.LockStation = LoadItem.LockStand;
            Settings.enableMultipleScreens = LoadItem.enableMultipleScreens;
            Settings.numOfConnectedScreens = LoadItem.numOfConnectedScreens;
            Settings.ScreenSway = LoadItem.ScreenSway;

            //Set the General Settings options and boxes accordingly(from the above information)
            InstituteName.Text = Settings.InstituteName;
            LicenseNumber.Text = Settings.LicenseNumber.ToString();
            InstituteAddress.Text = Settings.instAddress;
            InstituteRegion.Text = Settings.InstituteRegion;
            WorkStation.SelectedIndex = Settings.WorkStation;
            CourseNum.SelectedIndex = Settings.CourseNumber;
            StationLockTime.Text = Settings.lockTime.ToString();
            NumberOfConnectedScreens.Text = Settings.numOfConnectedScreens.ToString();
            ScreenSway.Text = Settings.ScreenSway.ToString();

            //Ifs for checkboxes in general settings. If enabled then (value is 1 for true or 0 for false) then execute/don't execute the option
            enableMultipleScreens.Checked = (Settings.enableMultipleScreens == 1);
            passwordBetweenStands.Checked = (Settings.passwordBetweenStands == 1);
            LockStation.Checked = (Settings.LockStation == 1);
            printReportEndOfStation.Checked = (Settings.printReportEndOfStation == 1);
            printReportUponEndOfInspection.Checked = (Settings.printReportUponEndOfInspection == 1);
            mandatorySignStationSign.Checked = (Settings.mandatoryStationSign == 1);

        }

        #endregion

        #region Save General Settings

        [HandleTypeDef(typeof(GeneralSettingsSaveResultTransferItem))]
        private void HandleGeneralSettingsSaveResultTransferItem(TransferItem tItem)
        {
            GeneralSettingsSaveResultTransferItem SaveItem = (GeneralSettingsSaveResultTransferItem)tItem;
            
                MessageBox.Show("נתונים נשמרו בהצלחה");
        }
        #endregion

        #endregion

            #region Extra Functions

            public void updateGeneralSettings()
            {

            Settings.InstituteName = InstituteName.Text;
            Settings.LicenseNumber = Convert.ToInt32(LicenseNumber.Text);
            Settings.instAddress = InstituteAddress.Text;
            Settings.InstituteRegion = InstituteRegion.Text;
            Settings.WorkStation = Convert.ToInt32(WorkStation.SelectedIndex);
            Settings.CourseNumber = Convert.ToInt32(CourseNum.SelectedIndex);
            Settings.lockTime = Convert.ToInt32(StationLockTime.Text);
            Settings.ScreenSway = Convert.ToInt32(ScreenSway.Text);
            
             //Ifs for saving the settings. The checkboxes are checked to see if they have been selected or not. If they have been then we set the state as 1 for true or 0 for false.
            Settings.enableMultipleScreens = System.Convert.ToInt16(enableMultipleScreens.Checked);
            Settings.passwordBetweenStands = System.Convert.ToInt16(passwordBetweenStands.Checked);
            Settings.LockStation = System.Convert.ToInt16(LockStation.Checked);
            Settings.printReportEndOfStation = System.Convert.ToInt16(printReportEndOfStation.Checked);
            Settings.printReportUponEndOfInspection = System.Convert.ToInt16(printReportUponEndOfInspection.Checked);
            Settings.mandatoryStationSign = System.Convert.ToInt16(mandatorySignStationSign.Checked);
            Settings.enableMultipleScreens = System.Convert.ToInt16(enableMultipleScreens.Checked);
            }

            #endregion

            private void NumberTextBox_KeyPress(object sender, KeyPressEventArgs e)
            {

                //If the resulted key press is a character then mark the press as handled. 
                if (UniversalFunctions.IsCharacterANumber(e)) { e.Handled = true; }//Calls the function that returns true if the char entered is a number and false if the char is not a number

            }

            private void RestSettingsToDefaultButton_Click(object sender, EventArgs e)
            {

                //Loop through the relevant controls in the form and reset their values

                //First loop through each group box
                foreach(GroupBox GroupBoxInForm in this.Controls)
                {

                    //Once the loop hits a group box, make another loop to loop through that group boxe's controls
                    foreach (Control ControlInGroupBox in GroupBoxInForm.Controls)
                    {

                        //Depending on the type of the control in the group box, reset it

                        if (ControlInGroupBox is TextBox)
                        {
                            TextBox SenderAsTextBox = ControlInGroupBox as TextBox;
                            SenderAsTextBox.Text = "";
                        }

                        if (ControlInGroupBox is CheckBox)
                        {
                            CheckBox SenderAsCheckBox = ControlInGroupBox as CheckBox;
                            SenderAsCheckBox.Checked = false;
                        }

                        if (ControlInGroupBox is ComboBox)
                        {
                            ComboBox SenderAsComboBox = ControlInGroupBox as ComboBox;
                            SenderAsComboBox.SelectedIndex = -1;
                        }

                    }

                }

            }

            #region Check Boxes Check Changed

            private void IsOnlineUsageEnabled_CheckedChanged(object sender, EventArgs e)
            {
                if (this.IsOnlineUsageEnabled.Checked == true) { this.OnlinePasswordTextBox.Enabled = true; this.OnlinePasswordTextBox.BackColor = Color.MediumSeaGreen; }
                else { this.OnlinePasswordTextBox.Enabled = false; this.OnlinePasswordTextBox.BackColor = Color.Silver; }
            }

            private void LockStation_CheckedChanged(object sender, EventArgs e)
            {
                if (this.LockStation.Checked == true) { this.StationLockTime.Enabled = true; this.StationLockTime.BackColor = Color.MediumSeaGreen; }
                else { this.StationLockTime.Enabled = false; this.StationLockTime.BackColor = Color.Silver; }
            }

            private void enableMultipleScreens_CheckedChanged(object sender, EventArgs e)
            {
                if (this.enableMultipleScreens.Checked == true)
                {
                    this.NumberOfConnectedScreens.Enabled = true; this.NumberOfConnectedScreens.BackColor = Color.MediumSeaGreen;
                    this.ScreenSway.Enabled = true; this.ScreenSway.BackColor = Color.MediumSeaGreen;
                }
                else
                {
                    this.NumberOfConnectedScreens.Enabled = false; this.NumberOfConnectedScreens.BackColor = Color.Silver;
                    this.ScreenSway.Enabled = false; this.ScreenSway.BackColor = Color.Silver;
                }
            }

            #endregion 

    }

}
