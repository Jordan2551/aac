﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace VBTCSCodeConversion
{
    public partial class Login : Form
    {

        MySqlConnection sqlConnection;
        MySqlDataReader READER;
        MySqlCommand Command;
        loadDB loadDBObj = new loadDB();
        public static int IDIndex;

        public Login()
        {
            InitializeComponent();
        }

        private void ID_Load(object sender, EventArgs e)
        {
            //carDetails.Show()
            loadDBObj.loadLogIn();
            PW.PasswordChar = '*';

        }

        //Set and get the ID index to know which institute to load from the table on the main screen

        private void setIDIndex(int value)
        {
            IDIndex = value;

        }

        public int getIDIndex()
        {
            //So that mainWindow can use the information from the correct index

            return IDIndex;

        }


        private void signIn_Click(object sender, EventArgs e)
        {
            sqlConnection = new MySqlConnection();
            sqlConnection.ConnectionString = "server=74.50.87.131;userid=moshech_Admin;password=aac1234;database=moshech_aac";

            MySqlDataReader READER = default(MySqlDataReader);


            try
            {
                sqlConnection.Open();

                string query = "select * from moshech_aac.log_in_details where Password ='" + PW.Text + "'";
                Command = new MySqlCommand(query, sqlConnection);
                READER = Command.ExecuteReader();

                READER.Read();


                for (int i = 0; i <= loadDB.instituteLogInList.Count - 1; i++)
                {
                    //Check if the ID field = any of the ID fields in our inst list so we don't have to compare every value
                    if (PW.Text.Equals(loadDB.instituteLogInList.ElementAt(i).password))
                    {

                        MainWindow MW = new MainWindow();
                        setIDIndex(i);
                        this.Hide();
                        MW.Show();

                        return;

                    }

                }

                MessageBox.Show("Invalid Password");
                PW.Text = null;

            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);


            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();

            }


        }

    }
}
