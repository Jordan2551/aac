﻿using CommInf.Handlers;
using CommInf.Items;
using RishuiCommDefines.TransferItems.DefectWindowItems;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RishuiClient.Forms
{
    public partial class DefectForm : Form
    {
        #region Members

        int CarCheckID;//Associated CarCheckID with the defect request

        #endregion

        public DefectForm(int CarCheckID)
        {
            InitializeComponent();

            this.CarCheckID = CarCheckID;

            #region Mapping

            Program.myCommManager.MapHandler(HandleDefectWindowLoadResultTransferItem);
            
            #endregion
        }

        private void DefectForm_Load(object sender, EventArgs e)//Load contents of the DefectForm
        {

            DefectWindowLoadRequestTransferItem LoadRequest = new DefectWindowLoadRequestTransferItem();

            LoadRequest.CarCheckID = this.CarCheckID;
            Program.SendTransferItem(LoadRequest);

            this.DefectGrid.ClearSelection();//Make sure the DefectGrid doesn't have a selected row upon loading

            //Disable all withdrawl/adding of permits at start
            this.WithdrawPermit.Enabled = false;
            DisablePermitAdding();

        }

        #region GUI Handlers


        #endregion

        #region TransferItemHandlers

        [HandleTypeDef(typeof(DefectWindowLoadResultTransferItem))]
        private void HandleDefectWindowLoadResultTransferItem(TransferItem tItem)
        {
            DefectWindowLoadResultTransferItem LoadDefects = (DefectWindowLoadResultTransferItem)tItem;

            string[] Row;

            #region Fill defect grid with defect info from all the stations

            #region ID Station

            for (int i = 0; i < LoadDefects.IDStationData.DefectList.Count; i++)//Fill defect grid with defect info
            {

                if(LoadDefects.IDStationData.DefectList[i].PermitRequest == 0)
                {
                    Row = new string[] { LoadDefects.IDStationData.DefectList[i].PermitDate, LoadDefects.IDStationData.DefectList[i].FixDate, "לא", LoadDefects.IDStationData.DefectList[i].DefectInfo, LoadDefects.IDStationData.DefectList[i].DefectID.ToString() };
                }

                else
                {
                    Row = new string[] { LoadDefects.IDStationData.DefectList[i].PermitDate, LoadDefects.IDStationData.DefectList[i].FixDate, "כן", LoadDefects.IDStationData.DefectList[i].DefectInfo, LoadDefects.IDStationData.DefectList[i].DefectID.ToString() };
                }

                DefectGrid.Rows.Add(Row);
            }

            #endregion

            #region Lights Station

            for (int i = 0; i < LoadDefects.LightsStationData.DefectList.Count; i++)//Fill defect grid with defect info
            {

                if (LoadDefects.LightsStationData.DefectList[i].PermitRequest == 0)
                {
                    Row = new string[] { LoadDefects.LightsStationData.DefectList[i].PermitDate, LoadDefects.LightsStationData.DefectList[i].FixDate, "לא", LoadDefects.LightsStationData.DefectList[i].DefectInfo, LoadDefects.LightsStationData.DefectList[i].DefectID.ToString() };
                }

                else
                {
                    Row = new string[] { LoadDefects.LightsStationData.DefectList[i].PermitDate, LoadDefects.LightsStationData.DefectList[i].FixDate, "כן", LoadDefects.LightsStationData.DefectList[i].DefectInfo, LoadDefects.LightsStationData.DefectList[i].DefectID.ToString() };
                }

                DefectGrid.Rows.Add(Row);
            }

            #endregion

            #region PitStation


            for (int i = 0; i < LoadDefects.PitStationData.DefectList.Count; i++)//Fill defect grid with defect info
            {

                if (LoadDefects.PitStationData.DefectList[i].PermitRequest == 0)
                {
                    Row = new string[] { LoadDefects.PitStationData.DefectList[i].PermitDate, LoadDefects.PitStationData.DefectList[i].FixDate, "לא", LoadDefects.PitStationData.DefectList[i].DefectInfo, LoadDefects.PitStationData.DefectList[i].DefectID.ToString() };
                }

                else
                {
                    Row = new string[] { LoadDefects.PitStationData.DefectList[i].PermitDate, LoadDefects.PitStationData.DefectList[i].FixDate, "כן", LoadDefects.PitStationData.DefectList[i].DefectInfo, LoadDefects.PitStationData.DefectList[i].DefectID.ToString() };
                }

                DefectGrid.Rows.Add(Row);
            }

            #endregion 

            #region Pollution Station

            for (int i = 0; i < LoadDefects.SmokeStationData.DefectList.Count; i++)//Fill defect grid with defect info
            {

                if (LoadDefects.SmokeStationData.DefectList[i].PermitRequest == 0)
                {
                    Row = new string[] { LoadDefects.SmokeStationData.DefectList[i].PermitDate, LoadDefects.SmokeStationData.DefectList[i].FixDate, "לא", LoadDefects.SmokeStationData.DefectList[i].DefectInfo, LoadDefects.SmokeStationData.DefectList[i].DefectID.ToString() };
                }

                else
                {
                    Row = new string[] { LoadDefects.SmokeStationData.DefectList[i].PermitDate, LoadDefects.SmokeStationData.DefectList[i].FixDate, "כן", LoadDefects.SmokeStationData.DefectList[i].DefectInfo, LoadDefects.SmokeStationData.DefectList[i].DefectID.ToString() };
                }

                DefectGrid.Rows.Add(Row);
            }

            #endregion 

            #region Brakes Station

            for (int i = 0; i < LoadDefects.BrakesStationData.DefectList.Count; i++)//Fill defect grid with defect info
            {

                if (LoadDefects.BrakesStationData.DefectList[i].PermitRequest == 0)
                {
                    Row = new string[] { LoadDefects.BrakesStationData.DefectList[i].PermitDate, LoadDefects.BrakesStationData.DefectList[i].FixDate, "לא", LoadDefects.BrakesStationData.DefectList[i].DefectInfo, LoadDefects.BrakesStationData.DefectList[i].DefectID.ToString() };
                }

                else
                {
                    Row = new string[] { LoadDefects.BrakesStationData.DefectList[i].PermitDate, LoadDefects.BrakesStationData.DefectList[i].FixDate, "כן", LoadDefects.BrakesStationData.DefectList[i].DefectInfo, LoadDefects.BrakesStationData.DefectList[i].DefectID.ToString() };
                }

                DefectGrid.Rows.Add(Row);
            }
            #endregion

            #region Wheel Alignment Station 

            for (int i = 0; i < LoadDefects.WheelAlignmentStationData.DefectList.Count; i++)//Fill defect grid with defect info
            {

                if (LoadDefects.WheelAlignmentStationData.DefectList[i].PermitRequest == 0)
                {
                    Row = new string[] { LoadDefects.WheelAlignmentStationData.DefectList[i].PermitDate, LoadDefects.WheelAlignmentStationData.DefectList[i].FixDate, "לא", LoadDefects.WheelAlignmentStationData.DefectList[i].DefectInfo, LoadDefects.WheelAlignmentStationData.DefectList[i].DefectID.ToString() };
                }

                else
                {
                    Row = new string[] { LoadDefects.WheelAlignmentStationData.DefectList[i].PermitDate, LoadDefects.WheelAlignmentStationData.DefectList[i].FixDate, "כן", LoadDefects.WheelAlignmentStationData.DefectList[i].DefectInfo, LoadDefects.WheelAlignmentStationData.DefectList[i].DefectID.ToString() };
                }

                DefectGrid.Rows.Add(Row);
            }


            #endregion

            #endregion

        }

        #endregion 


        private void DefectGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex != -1)//Check that the column headers are not clicked
            {

                DataGridView SelectedDataGrid = sender as DataGridView;//Cast the sender as a datagridview

                if (SelectedDataGrid == DefectGrid)//If the grid view that had it's cell clicked is the Defectgrid
                {

                    this.EnteredPermitsGrid.ClearSelection();//Clear the row selection from the Entered permits grid(to resolve problem with datagrids always keeping a row selected)

                    this.WithdrawPermit.Enabled = false;//Disable the ability to withdraw permits when this grid is selected

                    if (DefectGrid[2, e.RowIndex].Value.Equals("לא"))//If a permit is not needed for this defect fix, then don't allow the permit number box and the add permit button
                    {
                        DisablePermitAdding();
                    }

                    else//If a permit is needed then allow the box and the add permit number
                    {

                        if (EnteredPermitsGrid.Rows.Count < 2)//If the EnteredPermitsGrid has less than 2 rows then enable it, else don't
                        {
                            EnablePermitAdding();
                        }
                    }
                
                }

                if (SelectedDataGrid == EnteredPermitsGrid)//if the grid view that had it's cell clicked is the Enteredpermitsgrid
                {

                    //Disable the ability to add permit numbers when a row in the Enteredpermits grid is selected
                    DisablePermitAdding();

                    this.WithdrawPermit.Enabled = true;//Enable permit withdrawl

                    this.DefectGrid.ClearSelection();//Clear the row selection from the Defectgrid
                }

            }

        }

        #region Adding/Removing Permits Related Functions

        private void EnterPermit_Click(object sender, EventArgs e)//Adds a permit to EnteredPermitsGrid
        {

            if (this.PermitNumber.Text.Length > 0)//There is at least a number in there
            {

                //Make datetime versions of the datetime picker.values so we can get rid of the time extension as well as compare the strings
                DateTime PermitDateForComparison = Convert.ToDateTime(this.PermitDate.Value.ToShortDateString());
                DateTime FixDateForComparison = Convert.ToDateTime(this.FixDate.Value.ToShortDateString());

                if (FixDateForComparison < PermitDateForComparison)
                {
                    MessageBox.Show("נא וודא שתאריך האישור בא לפני תאריך התיקון", "שגיאה: בחירת תאריך לא תקינה", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RtlReading);
                }

                else
                {

                    Boolean DuplicatePermitNumberFound = false;

                    foreach (DataGridViewRow Row in this.EnteredPermitsGrid.Rows)
                    {

                        if (Row.Cells[2].Value.Equals(PermitNumber.Text))//Loop through all of the rows in the EnteredPermitsGrid and see if the permit number entered is = to any of the one's in the row
                        {
                            //If so then prompt the user and set DupliatePermitNumberFound to true
                            MessageBox.Show("מספר אישור זה כבר קיים באישורים שהוכנסו. נא וודא כי מספרי האישורים שונים", "שגיאה: מספר אישור כפול", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RtlReading);
                            DuplicatePermitNumberFound = true;
                        }

                        if (Row.Cells[4].Value.Equals(this.DefectGrid.SelectedRows[0].Cells[4].Value))//Loop through all of the rows in the EnteredPermitsGrid and see if the defect code that was chosen to be added as part of the permit adding doesnt already exist in the added permits
                        {
                            //If so then prompt the user and set DupliatePermitNumberFound to true
                            MessageBox.Show("קוד ליקוי זה כבר קיים באישורים שהוכנסו. נא וודא כי קודי הליקויים שונים", "שגיאה: קוד ליקוי כפול", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RtlReading);
                            DuplicatePermitNumberFound = true;
                        }

                    }

                    if (DuplicatePermitNumberFound == false)//If no duplicate permit number entries were found
                    {

                        this.EnteredPermitsGrid.Rows.Add(PermitDateForComparison.ToShortDateString(), FixDateForComparison.ToShortDateString(), PermitNumber.Text, DefectGrid.SelectedRows[0].Cells[3].Value, DefectGrid.SelectedRows[0].Cells[4].Value);//Add the row of details about the permit

                        DisablePermitAdding();//After a permit has been added to this grid then just disable permit adding(for convenience. When the user adds a permit the add permit button is still usable which is not logical)

                    }
                }
            }
        }

        private void WithdrawPermit_Click(object sender, EventArgs e)//Withdrawing a permit from EnteredPermitsGrid
        {

            this.EnteredPermitsGrid.Rows.RemoveAt(this.EnteredPermitsGrid.SelectedRows[0].Index);//Delete the selected cell

            if (this.EnteredPermitsGrid.Rows.Count == 0) { this.WithdrawPermit.Enabled = false; }//When all cells are cleared don't let the user remove cells

            else { this.WithdrawPermit.Enabled = true; }

        }


        private void PermitNumber_KeyPress(object sender, KeyPressEventArgs e)//Makes sure only numbers are entered
        {

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

        }

        private void EnablePermitAdding() { this.PermitNumber.Enabled = true; this.EnterPermit.Enabled = true; this.FixDate.Enabled = true; this.PermitDate.Enabled = true; }
        private void DisablePermitAdding() { this.PermitNumber.Enabled = false; this.EnterPermit.Enabled = false; this.PermitNumber.Clear(); this.FixDate.Enabled = false; this.PermitDate.Enabled = false; }

        #endregion 

        private void SavePermits_Click(object sender, EventArgs e)
        {

            #region Online(CarData Save)




            #endregion 

            #region Offline(DB Save)

            //Save the permits added for the specified defects from this form into the db
            DefectWindowSavePermitsRequestTransferItem SavePermits = new DefectWindowSavePermitsRequestTransferItem();

            //Properties for saving permits
            SavePermits.CarCheckID = this.CarCheckID;

            foreach(DataGridViewRow Row in this.EnteredPermitsGrid.Rows)
            {

                //Add the PermitDates, PermitFixDates, PermitNumbers and DefectCodeIDs found in the EnteredPermitsGrid control
                SavePermits.PermitDates.Add(Row.Cells[0].Value.ToString());
                SavePermits.PermitFixDates.Add(Row.Cells[1].Value.ToString());
                SavePermits.PermitNumbers.Add(Convert.ToInt32(Row.Cells[2].Value));
                SavePermits.DefectCodeIDs.Add(Convert.ToInt32(Row.Cells[4].Value));

            }

            Program.SendTransferItem(SavePermits);

            #endregion 

        }

    }
}
