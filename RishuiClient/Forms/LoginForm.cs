﻿using CommInf.Handlers;
using CommInf.Items;
using RishuiClient.Forms;
using RishuiClient.Reports.ReceiveVehicle;
using RishuiCommDefines.Managers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RishuiClient
{
    public partial class LoginForm : Form
    {

        private string SignInID { get; set; }//String to use with classes that would need to know which user logged in(will use string of Password field)

        Thread RFIDCommThread;
        int RFIDCommBitCounter = 0;

        #region Initialization
        public LoginForm()
        {
            InitializeComponent();

            #region Mappings

            //Here, we map all the handlers to the client's communication manager.
            Program.myCommManager.MapHandler(HandleLoginResultTransferItem);

            #endregion
        }

        #endregion

        #region GUI Callbacks

        private void LoginForm_Load(object sender, EventArgs e)
        {

   

        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            //Checking if the program succesfully connected.
            if (Program.myCommManager.IsConnected)
            {
                //Creating a new instance of a defined TransferItem
                LoginRequestTransferItem reqItem = new LoginRequestTransferItem()
                {
                    Password = passwordField.Text,
                    RFID = "",
                    SessionID = Program.SessionID
                };

                //Sending the TransferItem to the server.
                Program.SendTransferItem(reqItem);
            }
            else
            {
                MessageBox.Show("אין חיבור");
            }
        }

        private void passwordField_KeyDown(object sender, KeyEventArgs e)//Pressing enter on password field
        {
            if (e.KeyCode == Keys.Enter)
            {

                //Checking if the program succesfully connected.
                if (Program.myCommManager.IsConnected)
                {
                    //Creating a new instance of a defined TransferItem
                    LoginRequestTransferItem reqItem = new LoginRequestTransferItem()
                    {
                        Password = passwordField.Text,//Fill the password
                        RFID = "",//Keep RFID blank because this was done via a typed request
                        SessionID = Program.SessionID,
                    };

                    //Sending the TransferItem to the server.
                    Program.SendTransferItem(reqItem);
                }
                else
                {
                    MessageBox.Show("אין חיבור");
                }

            }

        }

        #endregion

        #region RFID Communication - Threading and Comm Port Management

        /* The RFID device works through a Serial Port in the program, which communicates through one of the computer's Commports(usb in this case)
         * The RFID device's communication is handled on a separate thread than the main one which opposes a problem: dangerous interaction of user controls
         * on the main thread(GUI controls like textboxes for example) and any interaction made with them on the background thread
         * (such as trying to change the text of a textbox in the event of recieveing data from the RFID device).
         * The way of handling this is transfering data in a thread safe way. This is all demonstrated below:
         */

        public bool PortCheck()//Function to check that the port for this device is connected and working to avoid crashes and bugs
        {

            string[] CommPortNames = SerialPort.GetPortNames();//List of strings that represents the name of the comm ports currently in use

            foreach (string ComPortName in CommPortNames)//Loop through each comm port name
            {

                if (ComPortName.Equals(this.RFIDComm.PortName))//If the comm port name from the string list = the comm port that was passed 
                {

                    return true;//Port was found! Return true to allow further operation with this class

                }

            }

            return false;//If the port was not found then disable the use of pmup on and off functions! Because those require port communication which will load to a crash without a port to communicate with

        }

        private void RFIDComm_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)//When interaction is made with the software and RFID(when bytes are receieved, this event gets called)
        {

            if (this.RFIDCommBitCounter == 9)//After 9 bits of information have been transfered over(which means all of the data is been transfered over) then we can procceed to read from the Serial Port
            {

                this.RFIDCommThread = new Thread(new ThreadStart(RFIDCommSafeThreadInteraction));
                this.RFIDCommThread.Start();//Start the function

                RFIDCommBitCounter = 0;

            }

            else { RFIDCommBitCounter++; }

        }

        //This is sort of a "bridge" function. It's only point is to call the "ThreadSafe" function(more info on that below)
        private void RFIDCommSafeThreadInteraction()
        {

            byte[] RFIDOutputinAscii = ASCIIEncoding.ASCII.GetBytes(RFIDComm.ReadLine());//Get the output from the RFID(represented as hex chars) and then convert them to Ascii

            //If the start of the ascii code is 2(which is the STX) then add the rfid normally
            LoginRequestTransferItem RequestRFIDLogin = new LoginRequestTransferItem();

            //If the start of the ascii code is 2(which is the STX) then add the rfid normally
            if (RFIDOutputinAscii[0] == 2)
            {
                RequestRFIDLogin.RFID = (string.Join(string.Empty, RFIDOutputinAscii));
                RequestRFIDLogin.Password = "";
                Program.SendTransferItem(RequestRFIDLogin);
            }

            //If it's not(will start from 3 after first rfid read) then remove the first index of the rfid
            else
            {

                StringBuilder ChangeString = new StringBuilder();

                ChangeString.Append(string.Join(string.Empty, RFIDOutputinAscii));//Add the whole output to a stringbuilder
                ChangeString.Remove(0, 1);

                RequestRFIDLogin.RFID = ChangeString.ToString();
                RequestRFIDLogin.Password = "";
                Program.SendTransferItem(RequestRFIDLogin);

            }
        }

        delegate void CallLoginResultTIDelegate(LoginResultTransferItem LoginResultItem);//Delegate function that takes a LoginResultTI

        #endregion 

        #region TransferItem handlers

        //An example of a TransferItem handler. We mapped this handler to handle it's type in the Form constructor,
        //under the 'Mappings' region.
        [HandleTypeDef(typeof(LoginResultTransferItem))]
        private void HandleLoginResultTransferItem(TransferItem tItem)
        {
            //Casting the TransferItem into the specific type we want to handle.
            LoginResultTransferItem resItem = (LoginResultTransferItem)tItem;

            if (this.InvokeRequired)//If invoke is required
            {

                CallLoginResultTIDelegate Delegate = new CallLoginResultTIDelegate(HandleLoginResultTransferItemInvokeReq);//Provide a function similar to this as the delegate parameter. This function itself cannot be passed in so a similar function to this one will solve the issue
                this.Invoke(Delegate, new object[] { resItem } );//Call the function, which takes in a LoginResultTI

            }

            else
            {
                //Handler code goes here.
                if (resItem.LoginResult == true)
                {


                    FormPointers.MainWindowFormPointer.ResetTesterInfo(resItem.TesterName);//Call the function in the MainWindow to reset the testers info according to the name passed
                    SignInID = passwordField.Text;

                    passwordField.Clear();

                    FormPointers.LoginFormPointer.Hide();
                    FormPointers.MainWindowFormPointer.Show();

                    //this.RFIDComm.Close();//Close the port for the RFID Communication

                }
                else
                {
                    MessageBox.Show("סיסמא שגויה או כתובת IP לא משודרת במערכת", "שגיאת כניסה למערכת", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RtlReading);
                }
            }
        }

        private void HandleLoginResultTransferItemInvokeReq(LoginResultTransferItem LoginResultItem)//Delegate function's parameter
        {


            //Handler code goes here.
            if (LoginResultItem.LoginResult == true)
            {


                FormPointers.MainWindowFormPointer.ResetTesterInfo(LoginResultItem.TesterName);//Call the function in the MainWindow to reset the testers info according to the name passed
                SignInID = passwordField.Text;

                passwordField.Clear();

                FormPointers.LoginFormPointer.Hide();
                FormPointers.MainWindowFormPointer.Show();//Causes System.invalidoperation exception because we're trying to access the MainWindow class with this line, which has not been
                //delegated like this LoginForm here. 

            }
            else
            {
                MessageBox.Show("סיסמא שגויה או כתובת IP לא משודרת במערכת", "שגיאת כניסה למערכת", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RtlReading);
            }

        }

        #endregion

        private void LoginForm_Visible(object sender, EventArgs e)
        {


            if (this.Visible == true)//When the form is visible do a com port check. If found to be true enable the use of rfid
            {

                #region RFID Comm Port Properties and Set-Up

                //Serial port set-up
                RFIDComm = new SerialPort("COM" + MainWindow.StationInformation.RFIDPort + "", 9600, Parity.None, 8, StopBits.One);//Set the name of the rfid com port from the station settings when the program is loaded
                RFIDComm.DataReceived += RFIDComm_DataReceived;
                RFIDComm.Handshake = Handshake.None;

                #endregion

                if (PortCheck() == true)//If the function that checks that this port is found returns true(found) then open the com port for work. If not then don't open the port(no rfid functionality)
                {
                    this.RFIDComm.Open();
                    this.RFIDNotFoundLabel.Hide();
                }

                else { this.RFIDNotFoundLabel.Show(); }
            }

            else { RFIDComm.Close(); }//Disable the comm port connection every time the window is not present(for example someone put in a right password, so we hide the window and disable the connection)

        }
    }
}

        