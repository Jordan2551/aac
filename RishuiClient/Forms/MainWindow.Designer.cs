﻿namespace RishuiClient.Forms
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            AnimatorNS.Animation animation1 = new AnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.InstituteName = new System.Windows.Forms.TextBox();
            this.ExternalIP = new System.Windows.Forms.TextBox();
            this.UserName = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Animator = new AnimatorNS.Animator(this.components);
            this.SwitchUser = new RishuiClient.GUIUserControls.AdvancedButton();
            this.Settings = new System.Windows.Forms.Button();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.advancedButton1 = new RishuiClient.GUIUserControls.AdvancedButton();
            this.OfficeButton = new System.Windows.Forms.Button();
            this.TestCourse = new System.Windows.Forms.Button();
            this.advancedButton2 = new RishuiClient.GUIUserControls.AdvancedButton();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // InstituteName
            // 
            this.InstituteName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Animator.SetDecoration(this.InstituteName, AnimatorNS.DecorationType.None);
            this.InstituteName.Font = new System.Drawing.Font("Microsoft NeoGothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InstituteName.Location = new System.Drawing.Point(199, 12);
            this.InstituteName.Name = "InstituteName";
            this.InstituteName.ReadOnly = true;
            this.InstituteName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.InstituteName.Size = new System.Drawing.Size(400, 46);
            this.InstituteName.TabIndex = 23;
            this.InstituteName.Text = "שם מכון";
            this.InstituteName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ExternalIP
            // 
            this.ExternalIP.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Animator.SetDecoration(this.ExternalIP, AnimatorNS.DecorationType.None);
            this.ExternalIP.Font = new System.Drawing.Font("Microsoft NeoGothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExternalIP.Location = new System.Drawing.Point(199, 136);
            this.ExternalIP.Name = "ExternalIP";
            this.ExternalIP.ReadOnly = true;
            this.ExternalIP.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ExternalIP.Size = new System.Drawing.Size(400, 46);
            this.ExternalIP.TabIndex = 24;
            this.ExternalIP.Text = "IP";
            this.ExternalIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // UserName
            // 
            this.UserName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Animator.SetDecoration(this.UserName, AnimatorNS.DecorationType.None);
            this.UserName.Font = new System.Drawing.Font("Microsoft NeoGothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserName.Location = new System.Drawing.Point(199, 74);
            this.UserName.Name = "UserName";
            this.UserName.ReadOnly = true;
            this.UserName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.UserName.Size = new System.Drawing.Size(400, 46);
            this.UserName.TabIndex = 25;
            this.UserName.Text = "שם בוחן";
            this.UserName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Animator.SetDecoration(this.textBox1, AnimatorNS.DecorationType.None);
            this.textBox1.Font = new System.Drawing.Font("Microsoft NeoGothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(199, 194);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBox1.Size = new System.Drawing.Size(400, 46);
            this.textBox1.TabIndex = 26;
            this.textBox1.Text = "רמת הרשאות";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Animator
            // 
            this.Animator.AnimationType = AnimatorNS.AnimationType.VertSlide;
            this.Animator.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 0F;
            this.Animator.DefaultAnimation = animation1;
            // 
            // SwitchUser
            // 
            this.SwitchUser.BackgroundImage = global::RishuiClient.Properties.Resources.SwitchUser_Idle;
            this.SwitchUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SwitchUser.ClickedHoverImage = global::RishuiClient.Properties.Resources.SwitchUser_Hover_Click;
            this.SwitchUser.ClickedImage = global::RishuiClient.Properties.Resources.SwitchUser_Click;
            this.SwitchUser.ClickState = false;
            this.Animator.SetDecoration(this.SwitchUser, AnimatorNS.DecorationType.None);
            this.SwitchUser.FlatAppearance.BorderSize = 0;
            this.SwitchUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SwitchUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.SwitchUser.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.SwitchUser.HoverImage = global::RishuiClient.Properties.Resources.SwitchUser_Hover;
            this.SwitchUser.IdleImage = global::RishuiClient.Properties.Resources.SwitchUser_Idle;
            this.SwitchUser.InactiveImage = null;
            this.SwitchUser.IsLocked = false;
            this.SwitchUser.IsToggleable = false;
            this.SwitchUser.Location = new System.Drawing.Point(409, 379);
            this.SwitchUser.Name = "SwitchUser";
            this.SwitchUser.Size = new System.Drawing.Size(187, 127);
            this.SwitchUser.TabIndex = 27;
            this.SwitchUser.Text = "החלפת משתמש";
            this.SwitchUser.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.SwitchUser.UseVisualStyleBackColor = true;
            this.SwitchUser.Click += new System.EventHandler(this.SwitchUser_Click);
            // 
            // Settings
            // 
            this.Settings.BackColor = System.Drawing.Color.Transparent;
            this.Settings.BackgroundImage = global::RishuiClient.Properties.Resources.SettingsButtonIdle;
            this.Settings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Animator.SetDecoration(this.Settings, AnimatorNS.DecorationType.None);
            this.Settings.FlatAppearance.BorderSize = 0;
            this.Settings.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Settings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Settings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Settings.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Settings.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Settings.Location = new System.Drawing.Point(2, 246);
            this.Settings.Name = "Settings";
            this.Settings.Size = new System.Drawing.Size(198, 127);
            this.Settings.TabIndex = 22;
            this.Settings.UseVisualStyleBackColor = false;
            this.Settings.Click += new System.EventHandler(this.Settings_Click);
            // 
            // PictureBox1
            // 
            this.PictureBox1.BackgroundImage = global::RishuiClient.Properties.Resources.useridicon;
            this.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Animator.SetDecoration(this.PictureBox1, AnimatorNS.DecorationType.None);
            this.PictureBox1.InitialImage = global::RishuiClient.Properties.Resources.useridicon;
            this.PictureBox1.Location = new System.Drawing.Point(7, 12);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(187, 228);
            this.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox1.TabIndex = 15;
            this.PictureBox1.TabStop = false;
            // 
            // advancedButton1
            // 
            this.advancedButton1.BackgroundImage = global::RishuiClient.Properties.Resources.ReportForm_Idle;
            this.advancedButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.advancedButton1.ClickedHoverImage = global::RishuiClient.Properties.Resources.ReportForm_Hover_Clicked;
            this.advancedButton1.ClickedImage = global::RishuiClient.Properties.Resources.ReportForm_Clicked;
            this.advancedButton1.ClickState = false;
            this.Animator.SetDecoration(this.advancedButton1, AnimatorNS.DecorationType.None);
            this.advancedButton1.FlatAppearance.BorderSize = 0;
            this.advancedButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.advancedButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.advancedButton1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.advancedButton1.HoverImage = global::RishuiClient.Properties.Resources.ReportForm_Hover;
            this.advancedButton1.IdleImage = global::RishuiClient.Properties.Resources.ReportForm_Idle;
            this.advancedButton1.InactiveImage = null;
            this.advancedButton1.IsLocked = false;
            this.advancedButton1.IsToggleable = false;
            this.advancedButton1.Location = new System.Drawing.Point(203, 379);
            this.advancedButton1.Name = "advancedButton1";
            this.advancedButton1.Size = new System.Drawing.Size(187, 127);
            this.advancedButton1.TabIndex = 28;
            this.advancedButton1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.advancedButton1.UseVisualStyleBackColor = true;
            this.advancedButton1.Click += new System.EventHandler(this.ReportsForm_Click);
            // 
            // OfficeButton
            // 
            this.OfficeButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OfficeButton.AutoSize = true;
            this.OfficeButton.BackgroundImage = global::RishuiClient.Properties.Resources.OfficeButtonIdle;
            this.OfficeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Animator.SetDecoration(this.OfficeButton, AnimatorNS.DecorationType.None);
            this.OfficeButton.FlatAppearance.BorderSize = 0;
            this.OfficeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.OfficeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.OfficeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OfficeButton.Location = new System.Drawing.Point(199, 246);
            this.OfficeButton.Name = "OfficeButton";
            this.OfficeButton.Size = new System.Drawing.Size(193, 127);
            this.OfficeButton.TabIndex = 32;
            this.OfficeButton.UseVisualStyleBackColor = true;
            this.OfficeButton.Click += new System.EventHandler(this.OfficeButton_Click);
            // 
            // TestCourse
            // 
            this.TestCourse.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TestCourse.AutoSize = true;
            this.TestCourse.BackgroundImage = global::RishuiClient.Properties.Resources.TestCourseButtonIdle;
            this.TestCourse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Animator.SetDecoration(this.TestCourse, AnimatorNS.DecorationType.None);
            this.TestCourse.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.TestCourse.FlatAppearance.BorderSize = 0;
            this.TestCourse.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.TestCourse.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.TestCourse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TestCourse.Location = new System.Drawing.Point(406, 246);
            this.TestCourse.Name = "TestCourse";
            this.TestCourse.Size = new System.Drawing.Size(193, 127);
            this.TestCourse.TabIndex = 31;
            this.TestCourse.UseVisualStyleBackColor = true;
            this.TestCourse.Click += new System.EventHandler(this.TestCourseButon_Click);
            // 
            // advancedButton2
            // 
            this.advancedButton2.BackgroundImage = global::RishuiClient.Properties.Resources.CriticalDefectIdle;
            this.advancedButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.advancedButton2.ClickedHoverImage = global::RishuiClient.Properties.Resources.CriticalDefectHoverClicked;
            this.advancedButton2.ClickedImage = global::RishuiClient.Properties.Resources.CriticalDefectClicked;
            this.advancedButton2.ClickState = false;
            this.Animator.SetDecoration(this.advancedButton2, AnimatorNS.DecorationType.None);
            this.advancedButton2.FlatAppearance.BorderSize = 0;
            this.advancedButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.advancedButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.advancedButton2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.advancedButton2.HoverImage = global::RishuiClient.Properties.Resources.CriticalDefectHover;
            this.advancedButton2.IdleImage = global::RishuiClient.Properties.Resources.CriticalDefectIdle;
            this.advancedButton2.InactiveImage = null;
            this.advancedButton2.IsLocked = false;
            this.advancedButton2.IsToggleable = false;
            this.advancedButton2.Location = new System.Drawing.Point(12, 379);
            this.advancedButton2.Name = "advancedButton2";
            this.advancedButton2.Size = new System.Drawing.Size(182, 127);
            this.advancedButton2.TabIndex = 33;
            this.advancedButton2.Text = "בדיקת בלמים ידנית";
            this.advancedButton2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.advancedButton2.UseVisualStyleBackColor = true;
            this.advancedButton2.Click += new System.EventHandler(this.advancedButton2_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(610, 513);
            this.Controls.Add(this.advancedButton2);
            this.Controls.Add(this.OfficeButton);
            this.Controls.Add(this.TestCourse);
            this.Controls.Add(this.advancedButton1);
            this.Controls.Add(this.SwitchUser);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.UserName);
            this.Controls.Add(this.ExternalIP);
            this.Controls.Add(this.InstituteName);
            this.Controls.Add(this.Settings);
            this.Controls.Add(this.PictureBox1);
            this.Animator.SetDecoration(this, AnimatorNS.DecorationType.None);
            this.MaximumSize = new System.Drawing.Size(626, 551);
            this.Name = "MainWindow";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "חלון ראשי";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Settings;
        private System.Windows.Forms.TextBox InstituteName;
        private System.Windows.Forms.TextBox ExternalIP;
        private System.Windows.Forms.TextBox UserName;
        private System.Windows.Forms.TextBox textBox1;
        private AnimatorNS.Animator Animator;
        private GUIUserControls.AdvancedButton SwitchUser;
        internal System.Windows.Forms.PictureBox PictureBox1;
        private GUIUserControls.AdvancedButton advancedButton1;
        private System.Windows.Forms.Button OfficeButton;
        private System.Windows.Forms.Button TestCourse;
        private GUIUserControls.AdvancedButton advancedButton2;
    }
}