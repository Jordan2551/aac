﻿namespace RishuiClient
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Label3 = new System.Windows.Forms.Label();
            this.passwordField = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.RFIDComm = new System.IO.Ports.SerialPort(this.components);
            this.loginButton = new RishuiClient.GUIUserControls.AdvancedButton();
            this.RFIDNotFoundLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.BackColor = System.Drawing.Color.Transparent;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.Label3.Location = new System.Drawing.Point(306, 99);
            this.Label3.Name = "Label3";
            this.Label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label3.Size = new System.Drawing.Size(101, 31);
            this.Label3.TabIndex = 19;
            this.Label3.Text = "סיסמא:";
            // 
            // passwordField
            // 
            this.passwordField.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.passwordField.Location = new System.Drawing.Point(95, 133);
            this.passwordField.Name = "passwordField";
            this.passwordField.PasswordChar = '*';
            this.passwordField.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.passwordField.Size = new System.Drawing.Size(306, 38);
            this.passwordField.TabIndex = 17;
            this.passwordField.KeyDown += new System.Windows.Forms.KeyEventHandler(this.passwordField_KeyDown);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Label1.Location = new System.Drawing.Point(108, 16);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(311, 37);
            this.Label1.TabIndex = 16;
            this.Label1.Text = "הקש סיסמא או העבר תג";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.groupBox1.Controls.Add(this.Label1);
            this.groupBox1.Location = new System.Drawing.Point(0, -6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(508, 73);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::RishuiClient.Properties.Resources.KeyIcon;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(407, 113);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(56, 85);
            this.pictureBox1.TabIndex = 23;
            this.pictureBox1.TabStop = false;
            // 
            // RFIDComm
            // 
            this.RFIDComm.PortName = "COM3";
            this.RFIDComm.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.RFIDComm_DataReceived);
            // 
            // loginButton
            // 
            this.loginButton.BackgroundImage = global::RishuiClient.Properties.Resources.Button_0002s_0000s_0000_Layer_1;
            this.loginButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.loginButton.ClickedHoverImage = global::RishuiClient.Properties.Resources.ButtonHoverClick_0002s_0003_Clicked;
            this.loginButton.ClickedImage = global::RishuiClient.Properties.Resources.Button_0002s_0001_Clicked;
            this.loginButton.ClickState = false;
            this.loginButton.FlatAppearance.BorderSize = 0;
            this.loginButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.loginButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.loginButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loginButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loginButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.loginButton.HoverImage = global::RishuiClient.Properties.Resources.Button_0002s_0000_Hover;
            this.loginButton.IdleImage = global::RishuiClient.Properties.Resources.Button_0002s_0000s_0000_Layer_1;
            this.loginButton.InactiveImage = null;
            this.loginButton.IsLocked = false;
            this.loginButton.IsToggleable = false;
            this.loginButton.Location = new System.Drawing.Point(95, 215);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(306, 40);
            this.loginButton.TabIndex = 24;
            this.loginButton.Text = "התחבר";
            this.loginButton.UseVisualStyleBackColor = true;
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // RFIDNotFoundLabel
            // 
            this.RFIDNotFoundLabel.AutoSize = true;
            this.RFIDNotFoundLabel.BackColor = System.Drawing.Color.Transparent;
            this.RFIDNotFoundLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RFIDNotFoundLabel.ForeColor = System.Drawing.Color.Red;
            this.RFIDNotFoundLabel.Location = new System.Drawing.Point(115, 70);
            this.RFIDNotFoundLabel.Name = "RFIDNotFoundLabel";
            this.RFIDNotFoundLabel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RFIDNotFoundLabel.Size = new System.Drawing.Size(248, 16);
            this.RFIDNotFoundLabel.TabIndex = 25;
            this.RFIDNotFoundLabel.Text = "*תקשורת מכשיר תג לא זמינה/מוגדרת";
            // 
            // LoginForm
            // 
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(507, 292);
            this.ControlBox = false;
            this.Controls.Add(this.RFIDNotFoundLabel);
            this.Controls.Add(this.loginButton);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.passwordField);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.VisibleChanged += new System.EventHandler(this.LoginForm_Visible);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox passwordField;
        internal System.Windows.Forms.Label Label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private GUIUserControls.AdvancedButton loginButton;
        private System.IO.Ports.SerialPort RFIDComm;
        internal System.Windows.Forms.Label RFIDNotFoundLabel;
    }
}

