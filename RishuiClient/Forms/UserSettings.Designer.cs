﻿namespace RishuiClient.Forms
{
    partial class UserSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.smokeID = new System.Windows.Forms.NumericUpDown();
            this.tz = new System.Windows.Forms.NumericUpDown();
            this.userID = new System.Windows.Forms.NumericUpDown();
            this.isReleaseEmdaTimeLimitchk = new System.Windows.Forms.CheckBox();
            this.deleteUser = new System.Windows.Forms.Button();
            this.saveUser = new System.Windows.Forms.Button();
            this.addUser = new System.Windows.Forms.Button();
            this.userBorCHK = new System.Windows.Forms.CheckBox();
            this.userZhumCHK = new System.Windows.Forms.CheckBox();
            this.userBlamimCHK = new System.Windows.Forms.CheckBox();
            this.userGalgalimCHK = new System.Windows.Forms.CheckBox();
            this.changeRPMCHK = new System.Windows.Forms.CheckBox();
            this.userZihuiCHK = new System.Windows.Forms.CheckBox();
            this.userOrotCHK = new System.Windows.Forms.CheckBox();
            this.userOfficeCHK = new System.Windows.Forms.CheckBox();
            this.userReleaseStation = new System.Windows.Forms.CheckBox();
            this.userTechniCHK = new System.Windows.Forms.CheckBox();
            this.userReportEnable = new System.Windows.Forms.CheckBox();
            this.userRepeatCHK = new System.Windows.Forms.CheckBox();
            this.userReprintCHK = new System.Windows.Forms.CheckBox();
            this.userAllowSignatureWithoutDeviceResults = new System.Windows.Forms.CheckBox();
            this.userLicensePlateModule = new System.Windows.Forms.CheckBox();
            this.userAdminCHK = new System.Windows.Forms.CheckBox();
            this.Label17 = new System.Windows.Forms.Label();
            this.Label16 = new System.Windows.Forms.Label();
            this.Label15 = new System.Windows.Forms.Label();
            this.testerName = new System.Windows.Forms.TextBox();
            this.testerPassword = new System.Windows.Forms.TextBox();
            this.rfid = new System.Windows.Forms.TextBox();
            this.Label14 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.Label12 = new System.Windows.Forms.Label();
            this.userSetting = new System.Windows.Forms.CheckBox();
            this.testerList = new System.Windows.Forms.ListBox();
            this.backButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.smokeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userID)).BeginInit();
            this.SuspendLayout();
            // 
            // smokeID
            // 
            this.smokeID.Location = new System.Drawing.Point(73, 108);
            this.smokeID.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.smokeID.Name = "smokeID";
            this.smokeID.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.smokeID.Size = new System.Drawing.Size(199, 20);
            this.smokeID.TabIndex = 165;
            // 
            // tz
            // 
            this.tz.Location = new System.Drawing.Point(73, 162);
            this.tz.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.tz.Name = "tz";
            this.tz.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tz.Size = new System.Drawing.Size(199, 20);
            this.tz.TabIndex = 164;
            // 
            // userID
            // 
            this.userID.Location = new System.Drawing.Point(73, 53);
            this.userID.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.userID.Name = "userID";
            this.userID.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.userID.Size = new System.Drawing.Size(199, 20);
            this.userID.TabIndex = 163;
            // 
            // isReleaseEmdaTimeLimitchk
            // 
            this.isReleaseEmdaTimeLimitchk.AutoSize = true;
            this.isReleaseEmdaTimeLimitchk.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.isReleaseEmdaTimeLimitchk.Location = new System.Drawing.Point(84, 392);
            this.isReleaseEmdaTimeLimitchk.Name = "isReleaseEmdaTimeLimitchk";
            this.isReleaseEmdaTimeLimitchk.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.isReleaseEmdaTimeLimitchk.Size = new System.Drawing.Size(220, 19);
            this.isReleaseEmdaTimeLimitchk.TabIndex = 155;
            this.isReleaseEmdaTimeLimitchk.Text = "הפעלת הגבלת זמן בשיחרור עמדה";
            this.isReleaseEmdaTimeLimitchk.UseVisualStyleBackColor = true;
            // 
            // deleteUser
            // 
            this.deleteUser.Location = new System.Drawing.Point(784, 407);
            this.deleteUser.Name = "deleteUser";
            this.deleteUser.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.deleteUser.Size = new System.Drawing.Size(83, 46);
            this.deleteUser.TabIndex = 162;
            this.deleteUser.Text = "מחק";
            this.deleteUser.UseVisualStyleBackColor = true;
            // 
            // saveUser
            // 
            this.saveUser.Location = new System.Drawing.Point(873, 407);
            this.saveUser.Name = "saveUser";
            this.saveUser.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.saveUser.Size = new System.Drawing.Size(83, 46);
            this.saveUser.TabIndex = 161;
            this.saveUser.Text = "שמור";
            this.saveUser.UseVisualStyleBackColor = true;
            this.saveUser.Click += new System.EventHandler(this.saveUser_Click);
            // 
            // addUser
            // 
            this.addUser.Location = new System.Drawing.Point(962, 407);
            this.addUser.Name = "addUser";
            this.addUser.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.addUser.Size = new System.Drawing.Size(83, 46);
            this.addUser.TabIndex = 160;
            this.addUser.Text = "הוסף";
            this.addUser.UseVisualStyleBackColor = true;
            this.addUser.Click += new System.EventHandler(this.addUser_Click);
            // 
            // userBorCHK
            // 
            this.userBorCHK.AutoSize = true;
            this.userBorCHK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userBorCHK.Location = new System.Drawing.Point(223, 267);
            this.userBorCHK.Name = "userBorCHK";
            this.userBorCHK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.userBorCHK.Size = new System.Drawing.Size(81, 19);
            this.userBorCHK.TabIndex = 159;
            this.userBorCHK.Text = "עמדת בור";
            this.userBorCHK.UseVisualStyleBackColor = true;
            // 
            // userZhumCHK
            // 
            this.userZhumCHK.AutoSize = true;
            this.userZhumCHK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userZhumCHK.Location = new System.Drawing.Point(211, 292);
            this.userZhumCHK.Name = "userZhumCHK";
            this.userZhumCHK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.userZhumCHK.Size = new System.Drawing.Size(93, 19);
            this.userZhumCHK.TabIndex = 158;
            this.userZhumCHK.Text = "זיהום אוויר";
            this.userZhumCHK.UseVisualStyleBackColor = true;
            // 
            // userBlamimCHK
            // 
            this.userBlamimCHK.AutoSize = true;
            this.userBlamimCHK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userBlamimCHK.Location = new System.Drawing.Point(206, 317);
            this.userBlamimCHK.Name = "userBlamimCHK";
            this.userBlamimCHK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.userBlamimCHK.Size = new System.Drawing.Size(98, 19);
            this.userBlamimCHK.TabIndex = 157;
            this.userBlamimCHK.Text = "עמדת בלמים";
            this.userBlamimCHK.UseVisualStyleBackColor = true;
            // 
            // userGalgalimCHK
            // 
            this.userGalgalimCHK.AutoSize = true;
            this.userGalgalimCHK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userGalgalimCHK.Location = new System.Drawing.Point(176, 342);
            this.userGalgalimCHK.Name = "userGalgalimCHK";
            this.userGalgalimCHK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.userGalgalimCHK.Size = new System.Drawing.Size(128, 19);
            this.userGalgalimCHK.TabIndex = 156;
            this.userGalgalimCHK.Text = "עמדת כוון גלגלים";
            this.userGalgalimCHK.UseVisualStyleBackColor = true;
            // 
            // changeRPMCHK
            // 
            this.changeRPMCHK.AutoSize = true;
            this.changeRPMCHK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.changeRPMCHK.Location = new System.Drawing.Point(127, 367);
            this.changeRPMCHK.Name = "changeRPMCHK";
            this.changeRPMCHK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.changeRPMCHK.Size = new System.Drawing.Size(177, 19);
            this.changeRPMCHK.TabIndex = 154;
            this.changeRPMCHK.Text = "הפעלת מנגנון זיהום אוויר";
            this.changeRPMCHK.UseVisualStyleBackColor = true;
            // 
            // userZihuiCHK
            // 
            this.userZihuiCHK.AutoSize = true;
            this.userZihuiCHK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userZihuiCHK.Location = new System.Drawing.Point(214, 242);
            this.userZihuiCHK.Name = "userZihuiCHK";
            this.userZihuiCHK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.userZihuiCHK.Size = new System.Drawing.Size(90, 19);
            this.userZihuiCHK.TabIndex = 153;
            this.userZihuiCHK.Text = "עמדת זיהוי";
            this.userZihuiCHK.UseVisualStyleBackColor = true;
            // 
            // userOrotCHK
            // 
            this.userOrotCHK.AutoSize = true;
            this.userOrotCHK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userOrotCHK.Location = new System.Drawing.Point(209, 217);
            this.userOrotCHK.Name = "userOrotCHK";
            this.userOrotCHK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.userOrotCHK.Size = new System.Drawing.Size(95, 19);
            this.userOrotCHK.TabIndex = 152;
            this.userOrotCHK.Text = "עמדת אורות";
            this.userOrotCHK.UseVisualStyleBackColor = true;
            // 
            // userOfficeCHK
            // 
            this.userOfficeCHK.AutoSize = true;
            this.userOfficeCHK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userOfficeCHK.Location = new System.Drawing.Point(593, 217);
            this.userOfficeCHK.Name = "userOfficeCHK";
            this.userOfficeCHK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.userOfficeCHK.Size = new System.Drawing.Size(124, 19);
            this.userOfficeCHK.TabIndex = 151;
            this.userOfficeCHK.Text = "אפשר מצב משרד";
            this.userOfficeCHK.UseVisualStyleBackColor = true;
            // 
            // userReleaseStation
            // 
            this.userReleaseStation.AutoSize = true;
            this.userReleaseStation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userReleaseStation.Location = new System.Drawing.Point(576, 242);
            this.userReleaseStation.Name = "userReleaseStation";
            this.userReleaseStation.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.userReleaseStation.Size = new System.Drawing.Size(141, 19);
            this.userReleaseStation.TabIndex = 150;
            this.userReleaseStation.Text = "אפשר שחרור עמדות";
            this.userReleaseStation.UseVisualStyleBackColor = true;
            // 
            // userTechniCHK
            // 
            this.userTechniCHK.AutoSize = true;
            this.userTechniCHK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userTechniCHK.Location = new System.Drawing.Point(566, 267);
            this.userTechniCHK.Name = "userTechniCHK";
            this.userTechniCHK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.userTechniCHK.Size = new System.Drawing.Size(151, 19);
            this.userTechniCHK.TabIndex = 149;
            this.userTechniCHK.Text = "אפשר שינויים טכניים";
            this.userTechniCHK.UseVisualStyleBackColor = true;
            // 
            // userReportEnable
            // 
            this.userReportEnable.AutoSize = true;
            this.userReportEnable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userReportEnable.Location = new System.Drawing.Point(584, 292);
            this.userReportEnable.Name = "userReportEnable";
            this.userReportEnable.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.userReportEnable.Size = new System.Drawing.Size(133, 19);
            this.userReportEnable.TabIndex = 148;
            this.userReportEnable.Text = "אפשר הפקת דוחות";
            this.userReportEnable.UseVisualStyleBackColor = true;
            // 
            // userRepeatCHK
            // 
            this.userRepeatCHK.AutoSize = true;
            this.userRepeatCHK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userRepeatCHK.Location = new System.Drawing.Point(535, 317);
            this.userRepeatCHK.Name = "userRepeatCHK";
            this.userRepeatCHK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.userRepeatCHK.Size = new System.Drawing.Size(182, 19);
            this.userRepeatCHK.TabIndex = 147;
            this.userRepeatCHK.Text = "אפשר קבלה לבדיקה חוזרת";
            this.userRepeatCHK.UseVisualStyleBackColor = true;
            // 
            // userReprintCHK
            // 
            this.userReprintCHK.AutoSize = true;
            this.userReprintCHK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userReprintCHK.Location = new System.Drawing.Point(555, 342);
            this.userReprintCHK.Name = "userReprintCHK";
            this.userReprintCHK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.userReprintCHK.Size = new System.Drawing.Size(162, 19);
            this.userReprintCHK.TabIndex = 146;
            this.userReprintCHK.Text = "הדפסה חוזרת של דוחות";
            this.userReprintCHK.UseVisualStyleBackColor = true;
            // 
            // userAllowSignatureWithoutDeviceResults
            // 
            this.userAllowSignatureWithoutDeviceResults.AutoSize = true;
            this.userAllowSignatureWithoutDeviceResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userAllowSignatureWithoutDeviceResults.Location = new System.Drawing.Point(499, 367);
            this.userAllowSignatureWithoutDeviceResults.Name = "userAllowSignatureWithoutDeviceResults";
            this.userAllowSignatureWithoutDeviceResults.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.userAllowSignatureWithoutDeviceResults.Size = new System.Drawing.Size(218, 19);
            this.userAllowSignatureWithoutDeviceResults.TabIndex = 145;
            this.userAllowSignatureWithoutDeviceResults.Text = "אפשר חתימה ללא תוצאות מכשיר";
            this.userAllowSignatureWithoutDeviceResults.UseVisualStyleBackColor = true;
            // 
            // userLicensePlateModule
            // 
            this.userLicensePlateModule.AutoSize = true;
            this.userLicensePlateModule.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userLicensePlateModule.Location = new System.Drawing.Point(497, 392);
            this.userLicensePlateModule.Name = "userLicensePlateModule";
            this.userLicensePlateModule.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.userLicensePlateModule.Size = new System.Drawing.Size(220, 19);
            this.userLicensePlateModule.TabIndex = 144;
            this.userLicensePlateModule.Text = "אפשר הפעלת מודול לוחיות רישוי";
            this.userLicensePlateModule.UseVisualStyleBackColor = true;
            // 
            // userAdminCHK
            // 
            this.userAdminCHK.AutoSize = true;
            this.userAdminCHK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userAdminCHK.Location = new System.Drawing.Point(483, 421);
            this.userAdminCHK.Name = "userAdminCHK";
            this.userAdminCHK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.userAdminCHK.Size = new System.Drawing.Size(234, 19);
            this.userAdminCHK.TabIndex = 143;
            this.userAdminCHK.Text = "מנהל מערכת (מאפשר שינוי הגדרות)";
            this.userAdminCHK.UseVisualStyleBackColor = true;
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label17.Location = new System.Drawing.Point(278, 53);
            this.Label17.Name = "Label17";
            this.Label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label17.Size = new System.Drawing.Size(47, 15);
            this.Label17.TabIndex = 142;
            this.Label17.Text = "תעודה:";
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label16.Location = new System.Drawing.Point(278, 108);
            this.Label16.Name = "Label16";
            this.Label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label16.Size = new System.Drawing.Size(74, 15);
            this.Label16.TabIndex = 141;
            this.Label16.Text = "תעודת עשן:";
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label15.Location = new System.Drawing.Point(278, 162);
            this.Label15.Name = "Label15";
            this.Label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label15.Size = new System.Drawing.Size(29, 15);
            this.Label15.TabIndex = 140;
            this.Label15.Text = "ת.ז.";
            // 
            // testerName
            // 
            this.testerName.Location = new System.Drawing.Point(396, 53);
            this.testerName.Name = "testerName";
            this.testerName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.testerName.Size = new System.Drawing.Size(195, 20);
            this.testerName.TabIndex = 135;
            // 
            // testerPassword
            // 
            this.testerPassword.Location = new System.Drawing.Point(396, 109);
            this.testerPassword.Name = "testerPassword";
            this.testerPassword.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.testerPassword.Size = new System.Drawing.Size(195, 20);
            this.testerPassword.TabIndex = 134;
            this.testerPassword.UseSystemPasswordChar = true;
            // 
            // rfid
            // 
            this.rfid.Location = new System.Drawing.Point(396, 167);
            this.rfid.Name = "rfid";
            this.rfid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.rfid.Size = new System.Drawing.Size(195, 20);
            this.rfid.TabIndex = 133;
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label14.Location = new System.Drawing.Point(597, 53);
            this.Label14.Name = "Label14";
            this.Label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label14.Size = new System.Drawing.Size(59, 15);
            this.Label14.TabIndex = 139;
            this.Label14.Text = "שם בוחן:";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label13.Location = new System.Drawing.Point(597, 109);
            this.Label13.Name = "Label13";
            this.Label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label13.Size = new System.Drawing.Size(49, 15);
            this.Label13.TabIndex = 138;
            this.Label13.Text = "סיסמא:";
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label12.Location = new System.Drawing.Point(597, 167);
            this.Label12.Name = "Label12";
            this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label12.Size = new System.Drawing.Size(37, 15);
            this.Label12.TabIndex = 137;
            this.Label12.Text = "Rfid:";
            // 
            // userSetting
            // 
            this.userSetting.AutoSize = true;
            this.userSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userSetting.Location = new System.Drawing.Point(266, 12);
            this.userSetting.Name = "userSetting";
            this.userSetting.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.userSetting.Size = new System.Drawing.Size(136, 19);
            this.userSetting.TabIndex = 136;
            this.userSetting.Text = "אפשר מצב משתמש";
            this.userSetting.UseVisualStyleBackColor = true;
            this.userSetting.CheckedChanged += new System.EventHandler(this.userSetting_CheckedChanged);
            // 
            // testerList
            // 
            this.testerList.FormattingEnabled = true;
            this.testerList.Location = new System.Drawing.Point(784, 12);
            this.testerList.Name = "testerList";
            this.testerList.Size = new System.Drawing.Size(261, 394);
            this.testerList.TabIndex = 132;
            this.testerList.SelectedIndexChanged += new System.EventHandler(this.testerList_SelectedIndexChanged);
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(12, 430);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 23);
            this.backButton.TabIndex = 166;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // UserSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1057, 456);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.smokeID);
            this.Controls.Add(this.tz);
            this.Controls.Add(this.userID);
            this.Controls.Add(this.isReleaseEmdaTimeLimitchk);
            this.Controls.Add(this.deleteUser);
            this.Controls.Add(this.saveUser);
            this.Controls.Add(this.addUser);
            this.Controls.Add(this.userBorCHK);
            this.Controls.Add(this.userZhumCHK);
            this.Controls.Add(this.userBlamimCHK);
            this.Controls.Add(this.userGalgalimCHK);
            this.Controls.Add(this.changeRPMCHK);
            this.Controls.Add(this.userZihuiCHK);
            this.Controls.Add(this.userOrotCHK);
            this.Controls.Add(this.userOfficeCHK);
            this.Controls.Add(this.userReleaseStation);
            this.Controls.Add(this.userTechniCHK);
            this.Controls.Add(this.userReportEnable);
            this.Controls.Add(this.userRepeatCHK);
            this.Controls.Add(this.userReprintCHK);
            this.Controls.Add(this.userAllowSignatureWithoutDeviceResults);
            this.Controls.Add(this.userLicensePlateModule);
            this.Controls.Add(this.userAdminCHK);
            this.Controls.Add(this.Label17);
            this.Controls.Add(this.Label16);
            this.Controls.Add(this.Label15);
            this.Controls.Add(this.testerName);
            this.Controls.Add(this.testerPassword);
            this.Controls.Add(this.rfid);
            this.Controls.Add(this.Label14);
            this.Controls.Add(this.Label13);
            this.Controls.Add(this.Label12);
            this.Controls.Add(this.userSetting);
            this.Controls.Add(this.testerList);
            this.Name = "UserSettings";
            this.Text = "NewForm";
            this.Load += new System.EventHandler(this.UserSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.smokeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userID)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown smokeID;
        private System.Windows.Forms.NumericUpDown tz;
        private System.Windows.Forms.NumericUpDown userID;
        internal System.Windows.Forms.CheckBox isReleaseEmdaTimeLimitchk;
        internal System.Windows.Forms.Button deleteUser;
        internal System.Windows.Forms.Button saveUser;
        internal System.Windows.Forms.Button addUser;
        internal System.Windows.Forms.CheckBox userBorCHK;
        internal System.Windows.Forms.CheckBox userZhumCHK;
        internal System.Windows.Forms.CheckBox userBlamimCHK;
        internal System.Windows.Forms.CheckBox userGalgalimCHK;
        internal System.Windows.Forms.CheckBox changeRPMCHK;
        internal System.Windows.Forms.CheckBox userZihuiCHK;
        internal System.Windows.Forms.CheckBox userOrotCHK;
        internal System.Windows.Forms.CheckBox userOfficeCHK;
        internal System.Windows.Forms.CheckBox userReleaseStation;
        internal System.Windows.Forms.CheckBox userTechniCHK;
        internal System.Windows.Forms.CheckBox userReportEnable;
        internal System.Windows.Forms.CheckBox userRepeatCHK;
        internal System.Windows.Forms.CheckBox userReprintCHK;
        internal System.Windows.Forms.CheckBox userAllowSignatureWithoutDeviceResults;
        internal System.Windows.Forms.CheckBox userLicensePlateModule;
        internal System.Windows.Forms.CheckBox userAdminCHK;
        internal System.Windows.Forms.Label Label17;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.TextBox testerName;
        internal System.Windows.Forms.TextBox testerPassword;
        internal System.Windows.Forms.TextBox rfid;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.CheckBox userSetting;
        internal System.Windows.Forms.ListBox testerList;
        private System.Windows.Forms.Button backButton;
    }
}