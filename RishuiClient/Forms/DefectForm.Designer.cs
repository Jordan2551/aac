﻿namespace RishuiClient.Forms
{
    partial class DefectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DefectForm));
            this.label1 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.PermitNumber = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PermitDate = new System.Windows.Forms.DateTimePicker();
            this.FixDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.EnteredPermitsGrid = new System.Windows.Forms.DataGridView();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DefectGrid = new System.Windows.Forms.DataGridView();
            this.DateOfPermit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FixDateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PermitNumberColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DefectInfo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DefectID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SavePermits = new RishuiClient.GUIUserControls.AdvancedButton();
            this.EnterPermit = new RishuiClient.GUIUserControls.AdvancedButton();
            this.WithdrawPermit = new RishuiClient.GUIUserControls.AdvancedButton();
            this.defectFormBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EnteredPermitsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefectGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.defectFormBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Red;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label1.Font = new System.Drawing.Font("Narkisim", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(316, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(356, 30);
            this.label1.TabIndex = 1;
            this.label1.Text = "רשימת ליקויים לבדיקה חוזרת";
            // 
            // Label9
            // 
            this.Label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label9.AutoSize = true;
            this.Label9.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.Label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label9.Font = new System.Drawing.Font("Narkisim", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Label9.Location = new System.Drawing.Point(287, 380);
            this.Label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label9.Name = "Label9";
            this.Label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Label9.Size = new System.Drawing.Size(453, 22);
            this.Label9.TabIndex = 209;
            this.Label9.Text = "אישורים שהוכנסו (ניתן להכניס עד 2 אישורים בלבד)";
            this.Label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // PermitNumber
            // 
            this.PermitNumber.Font = new System.Drawing.Font("Narkisim", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.PermitNumber.Location = new System.Drawing.Point(623, 19);
            this.PermitNumber.Margin = new System.Windows.Forms.Padding(4);
            this.PermitNumber.MaxLength = 6;
            this.PermitNumber.Name = "PermitNumber";
            this.PermitNumber.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PermitNumber.Size = new System.Drawing.Size(231, 28);
            this.PermitNumber.TabIndex = 211;
            this.PermitNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PermitNumber_KeyPress);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Font = new System.Drawing.Font("Narkisim", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(862, 19);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(138, 26);
            this.label2.TabIndex = 214;
            this.label2.Text = "מספר אישור:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.EnterPermit);
            this.groupBox1.Controls.Add(this.PermitDate);
            this.groupBox1.Controls.Add(this.FixDate);
            this.groupBox1.Controls.Add(this.WithdrawPermit);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.PermitNumber);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Narkisim", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.groupBox1.Location = new System.Drawing.Point(13, 496);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox1.Size = new System.Drawing.Size(1005, 106);
            this.groupBox1.TabIndex = 215;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "הכנסת אישורים";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.SavePermits);
            this.groupBox2.Font = new System.Drawing.Font("Narkisim", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.groupBox2.Location = new System.Drawing.Point(6, 11);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox2.Size = new System.Drawing.Size(210, 87);
            this.groupBox2.TabIndex = 216;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "שמירה ושידור אישורים";
            // 
            // PermitDate
            // 
            this.PermitDate.CustomFormat = "dd-MM-yyyy";
            this.PermitDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.PermitDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.PermitDate.Location = new System.Drawing.Point(235, 21);
            this.PermitDate.Name = "PermitDate";
            this.PermitDate.Size = new System.Drawing.Size(200, 26);
            this.PermitDate.TabIndex = 219;
            // 
            // FixDate
            // 
            this.FixDate.CustomFormat = "dd-MM-yyyy";
            this.FixDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.FixDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FixDate.Location = new System.Drawing.Point(235, 72);
            this.FixDate.Name = "FixDate";
            this.FixDate.Size = new System.Drawing.Size(200, 26);
            this.FixDate.TabIndex = 218;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Font = new System.Drawing.Font("Narkisim", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(445, 72);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(140, 26);
            this.label4.TabIndex = 217;
            this.label4.Text = "תאריך תיקון:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("Narkisim", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(445, 21);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(145, 26);
            this.label3.TabIndex = 216;
            this.label3.Text = "תאריך אישור:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // EnteredPermitsGrid
            // 
            this.EnteredPermitsGrid.AllowUserToAddRows = false;
            this.EnteredPermitsGrid.AllowUserToDeleteRows = false;
            this.EnteredPermitsGrid.AllowUserToResizeColumns = false;
            this.EnteredPermitsGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.EnteredPermitsGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.EnteredPermitsGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.EnteredPermitsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.EnteredPermitsGrid.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.EnteredPermitsGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.EnteredPermitsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.EnteredPermitsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EnteredPermitsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.Column1,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.EnteredPermitsGrid.EnableHeadersVisualStyles = false;
            this.EnteredPermitsGrid.Location = new System.Drawing.Point(13, 404);
            this.EnteredPermitsGrid.Margin = new System.Windows.Forms.Padding(4);
            this.EnteredPermitsGrid.MultiSelect = false;
            this.EnteredPermitsGrid.Name = "EnteredPermitsGrid";
            this.EnteredPermitsGrid.ReadOnly = true;
            this.EnteredPermitsGrid.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.EnteredPermitsGrid.RowHeadersVisible = false;
            this.EnteredPermitsGrid.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.EnteredPermitsGrid.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Aharoni", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.EnteredPermitsGrid.RowTemplate.Height = 33;
            this.EnteredPermitsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.EnteredPermitsGrid.Size = new System.Drawing.Size(1005, 91);
            this.EnteredPermitsGrid.TabIndex = 213;
            this.EnteredPermitsGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DefectGrid_CellClick);
            // 
            // Column2
            // 
            this.Column2.HeaderText = "תאריך אישור";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "תאריך תיקון";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.FillWeight = 73.81519F;
            this.dataGridViewTextBoxColumn3.HeaderText = "מס\' אישור";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.FillWeight = 203.1544F;
            this.dataGridViewTextBoxColumn4.HeaderText = "שם ליקוי";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn5.FillWeight = 74.34797F;
            this.dataGridViewTextBoxColumn5.HeaderText = "קוד ליקוי";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 86;
            // 
            // DefectGrid
            // 
            this.DefectGrid.AllowUserToAddRows = false;
            this.DefectGrid.AllowUserToDeleteRows = false;
            this.DefectGrid.AllowUserToResizeColumns = false;
            this.DefectGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.DefectGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.DefectGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DefectGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DefectGrid.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DefectGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DefectGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DefectGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DefectGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DateOfPermit,
            this.FixDateColumn,
            this.PermitNumberColumn,
            this.DefectInfo,
            this.DefectID});
            this.DefectGrid.EnableHeadersVisualStyles = false;
            this.DefectGrid.Location = new System.Drawing.Point(13, 50);
            this.DefectGrid.Margin = new System.Windows.Forms.Padding(4);
            this.DefectGrid.MultiSelect = false;
            this.DefectGrid.Name = "DefectGrid";
            this.DefectGrid.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DefectGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.DefectGrid.RowHeadersVisible = false;
            this.DefectGrid.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DefectGrid.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Aharoni", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.DefectGrid.RowTemplate.Height = 40;
            this.DefectGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DefectGrid.Size = new System.Drawing.Size(1005, 320);
            this.DefectGrid.TabIndex = 0;
            this.DefectGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DefectGrid_CellClick);
            // 
            // DateOfPermit
            // 
            this.DateOfPermit.FillWeight = 74.33111F;
            this.DateOfPermit.HeaderText = "תאריך אישור";
            this.DateOfPermit.Name = "DateOfPermit";
            this.DateOfPermit.ReadOnly = true;
            // 
            // FixDateColumn
            // 
            this.FixDateColumn.FillWeight = 74.3513F;
            this.FixDateColumn.HeaderText = "תאריך תיקון";
            this.FixDateColumn.Name = "FixDateColumn";
            this.FixDateColumn.ReadOnly = true;
            // 
            // PermitNumberColumn
            // 
            this.PermitNumberColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PermitNumberColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.PermitNumberColumn.FillWeight = 73.81519F;
            this.PermitNumberColumn.HeaderText = "מצריך אישור";
            this.PermitNumberColumn.Name = "PermitNumberColumn";
            this.PermitNumberColumn.ReadOnly = true;
            this.PermitNumberColumn.Width = 95;
            // 
            // DefectInfo
            // 
            this.DefectInfo.FillWeight = 203.1544F;
            this.DefectInfo.HeaderText = "שם ליקוי";
            this.DefectInfo.Name = "DefectInfo";
            this.DefectInfo.ReadOnly = true;
            // 
            // DefectID
            // 
            this.DefectID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.DefectID.FillWeight = 74.34797F;
            this.DefectID.HeaderText = "קוד ליקוי";
            this.DefectID.Name = "DefectID";
            this.DefectID.ReadOnly = true;
            this.DefectID.Width = 77;
            // 
            // SavePermits
            // 
            this.SavePermits.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("SavePermits.BackgroundImage")));
            this.SavePermits.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SavePermits.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("SavePermits.ClickedHoverImage")));
            this.SavePermits.ClickedImage = ((System.Drawing.Image)(resources.GetObject("SavePermits.ClickedImage")));
            this.SavePermits.ClickState = false;
            this.SavePermits.FlatAppearance.BorderSize = 0;
            this.SavePermits.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SavePermits.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SavePermits.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.SavePermits.HoverImage = ((System.Drawing.Image)(resources.GetObject("SavePermits.HoverImage")));
            this.SavePermits.IdleImage = ((System.Drawing.Image)(resources.GetObject("SavePermits.IdleImage")));
            this.SavePermits.InactiveImage = global::RishuiClient.Properties.Resources.SaveAndExit_0003_Inactive;
            this.SavePermits.IsLocked = false;
            this.SavePermits.IsToggleable = false;
            this.SavePermits.Location = new System.Drawing.Point(38, 31);
            this.SavePermits.Name = "SavePermits";
            this.SavePermits.Size = new System.Drawing.Size(131, 51);
            this.SavePermits.TabIndex = 218;
            this.SavePermits.Text = "שמור";
            this.SavePermits.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.SavePermits.UseVisualStyleBackColor = true;
            this.SavePermits.Click += new System.EventHandler(this.SavePermits_Click);
            // 
            // EnterPermit
            // 
            this.EnterPermit.BackgroundImage = global::RishuiClient.Properties.Resources.TofesButton_Idle;
            this.EnterPermit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.EnterPermit.ClickedHoverImage = global::RishuiClient.Properties.Resources.TofesButton_Hover_Clicked;
            this.EnterPermit.ClickedImage = global::RishuiClient.Properties.Resources.TofesButton_Hover_Clicked;
            this.EnterPermit.ClickState = false;
            this.EnterPermit.FlatAppearance.BorderSize = 0;
            this.EnterPermit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EnterPermit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EnterPermit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.EnterPermit.HoverImage = global::RishuiClient.Properties.Resources.TofesButton_Hover;
            this.EnterPermit.IdleImage = global::RishuiClient.Properties.Resources.TofesButton_Idle;
            this.EnterPermit.InactiveImage = global::RishuiClient.Properties.Resources.TofesButton_Inactive;
            this.EnterPermit.IsLocked = false;
            this.EnterPermit.IsToggleable = false;
            this.EnterPermit.Location = new System.Drawing.Point(742, 50);
            this.EnterPermit.Name = "EnterPermit";
            this.EnterPermit.Size = new System.Drawing.Size(112, 50);
            this.EnterPermit.TabIndex = 216;
            this.EnterPermit.Text = "הכנס אישור";
            this.EnterPermit.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.EnterPermit.UseVisualStyleBackColor = true;
            this.EnterPermit.Click += new System.EventHandler(this.EnterPermit_Click);
            // 
            // WithdrawPermit
            // 
            this.WithdrawPermit.BackgroundImage = global::RishuiClient.Properties.Resources.SFI;
            this.WithdrawPermit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.WithdrawPermit.ClickedHoverImage = global::RishuiClient.Properties.Resources.SFHC;
            this.WithdrawPermit.ClickedImage = global::RishuiClient.Properties.Resources.SFC;
            this.WithdrawPermit.ClickState = false;
            this.WithdrawPermit.FlatAppearance.BorderSize = 0;
            this.WithdrawPermit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WithdrawPermit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WithdrawPermit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.WithdrawPermit.HoverImage = global::RishuiClient.Properties.Resources.SFH;
            this.WithdrawPermit.IdleImage = global::RishuiClient.Properties.Resources.SFI;
            this.WithdrawPermit.InactiveImage = ((System.Drawing.Image)(resources.GetObject("WithdrawPermit.InactiveImage")));
            this.WithdrawPermit.IsLocked = false;
            this.WithdrawPermit.IsToggleable = false;
            this.WithdrawPermit.Location = new System.Drawing.Point(623, 50);
            this.WithdrawPermit.Name = "WithdrawPermit";
            this.WithdrawPermit.Size = new System.Drawing.Size(112, 50);
            this.WithdrawPermit.TabIndex = 217;
            this.WithdrawPermit.Text = "הסר אישור";
            this.WithdrawPermit.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.WithdrawPermit.UseVisualStyleBackColor = true;
            this.WithdrawPermit.Click += new System.EventHandler(this.WithdrawPermit_Click);
            // 
            // defectFormBindingSource
            // 
            this.defectFormBindingSource.DataSource = typeof(RishuiClient.Forms.DefectForm);
            // 
            // DefectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1031, 606);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.EnteredPermitsGrid);
            this.Controls.Add(this.Label9);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DefectGrid);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DefectForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ליקויים";
            this.Load += new System.EventHandler(this.DefectForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.EnteredPermitsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefectGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.defectFormBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.TextBox PermitNumber;
        private System.Windows.Forms.BindingSource defectFormBindingSource;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private GUIUserControls.AdvancedButton EnterPermit;
        private GUIUserControls.AdvancedButton WithdrawPermit;
        private GUIUserControls.AdvancedButton SavePermits;
        private System.Windows.Forms.DataGridView EnteredPermitsGrid;
        private System.Windows.Forms.DataGridView DefectGrid;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker PermitDate;
        private System.Windows.Forms.DateTimePicker FixDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateOfPermit;
        private System.Windows.Forms.DataGridViewTextBoxColumn FixDateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn PermitNumberColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DefectInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn DefectID;
    }
}