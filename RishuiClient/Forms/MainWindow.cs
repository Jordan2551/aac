﻿using CommInf.Handlers;
using CommInf.Items;
using RishuiClient.Forms.Reports_Form;
using RishuiClient.Forms.TestCourse_Forms.Stations.Brakes_Station;
using RishuiClient.Forms.TestCourse_Forms.Stations.Smoke_Station;
using RishuiClient.Reports.ReceiveVehicle;
using RishuiCommDefines.TransferItems;
using RishuiCommDefines.TransferItems.MainWindowItems;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RishuiClient.Forms
{
    public partial class MainWindow : Form
    {
        #region Members

        #region User Info
        public struct UserInfo//Struct to hold all of the information about the logged in tester as well as the institute's information for use with the entire program
        {

            public string External_IP;

            //מידע משתמש
            public string testerName;
            public string testerPassword;
            public string rfid;
            public int userID;
            public int smokeID;
            public int tz;

            //הגבלות כלליות
            public int AllowStationRelease;
            public int AllowTechnicalChanges;
            public int AllowReportPrinting;
            public int AllowReCheck;
            public int AllowReportRePrinting;

            //הגבלות גישה
            public int AllowAccessToDefectWindow;
            public int AllowLicensePlateWindow;
            public int AllowAccessToOfficeWindow;
            public int AllowAdminPrivileges;

            //הגבלות עמדה
            public int AllowIDStation;
            public int AllowLightsStation;
            public int AllowPitStation;
            public int AllowSmokeStation;
            public int AllowBrakesStation;
            public int AllowWheelAlignmentStation;
            public int AllowCheckMechanisim;
            public int AllowTimeLimitAfterStationRelease;
            public int AllowDeviceStationSigning;


        }

        #endregion 

        public static BrakesTest BrakeTestFormInstance;//Object to make sure only one instance of SmokeTest is ever open

        #region Station Info

        public struct StationInfo
        {

            //All information regarding the station
            public int StationID;
            public int StationType;
            public int Arka_Days;

            public string LightsMachineType;
            public int LightsMachineComPort;
            public string LightsSettings;

            public string GasMachineType;
            public int GasMachineComPort;
            public string GasSettings;

            public string SmokeMachineType;
            public int SmokeMachineComPort;
            public string SmokeMeterType;

            public int RFIDPort;

            public int WorkStationNumber;
            public string ZeroVoltWeight;
            public string KiloPerVoltRMatmer;
            public string ZeroVoltRMatmer;
            public string BrakesMachineType;
            public string BrakeCheck;
            public string RunWayNum;
            public string JumpStation;
            public string DefineWorkLineStation;
            public string ReceiveMaha;
            public string MahaOutFolder;
            public string LoadCarColor;
            public string NewCarColor;
            public string RequestStationPassword;
            public string CashSettingsActive;
            public string PaymentEnd;
            public string PaymentFirst;
            public string TagCost;
            public string UseBinaCode;
            public string BinaMdbPath;
            public string AllowPrintEndRep;
            public string PasswordTimerOn;
            public string PasswordTimerInterval;
            public string DefectListColor;

        }

        #endregion 

        #region Server Info

        public struct ServerInfo
        {

            public string InstituteAddress;
            public string InstituteName;
            public string InstituteNumber;
            public string MisradRishuiMechozi;
            public int TestMonthsPeriod;
            public string Serial;
            public string DateOfLicense;
            public string PurchaseLicense;
            public string OnlinePassword;
            public string OnlineDataSupply;
            public string KodGorem;
            public string LicensePlateSerial;
            public string KniaSerial;
            public int OnlineUsage;
            public string OnlineAddress;

        }

        #endregion 

        public static UserInfo UserInformation = new UserInfo();
        public static StationInfo StationInformation = new StationInfo();
        public static ServerInfo ServerInformation = new ServerInfo();

        #endregion

        public MainWindow()//Takes a string representing the Tester's name that we are loading the program for
        {

            InitializeComponent();

            #region Mapping

            //Here you map all the handlers to the client's communication manager.

            //Example:
            Program.myCommManager.MapHandler(HandleMainWindowInfoResultITransferItem);
            Program.myCommManager.MapHandler(HandleMainWindowStationInfoResultTransferItem);
            Program.myCommManager.MapHandler(HandleMainWindowServerInfoResultTransferItem);

            #endregion
        }

        #region GUI Handlers


        private void MainWindow_Load(object sender, EventArgs e)
        {

            UserName.ReadOnly = true;
            InstituteName.ReadOnly = true;
            ExternalIP.ReadOnly = true;

            //Send the request for user, station and server settings & info 
            MainWindowUserInfoRequestTransferItem UserSettingsItem = new MainWindowUserInfoRequestTransferItem();
            MainWindowStationInfoRequestTransferItem StationSettingsItem = new MainWindowStationInfoRequestTransferItem();
            MainWindowServerInfoRequestTransferItem ServerSettingsItem = new MainWindowServerInfoRequestTransferItem();

            UserSettingsItem.TesterName = UserInformation.testerName;//Set the testername to the testername that was used to log in to the program so that we can get information 
            StationSettingsItem.StationNumber = 1;//FIND A WAY TO STORE STATION NUMBER LOCALLY

            Program.SendTransferItem(UserSettingsItem);
            Program.SendTransferItem(StationSettingsItem);
            Program.SendTransferItem(ServerSettingsItem);


        }

        #endregion

        #region TransferItemHandlers

        //An example of a TransferItem handler. We mapped this handler to handle it's type in the Form constructor,
        //under the 'Mappings' region.

        #region Getting User Settings & Info

        [HandleTypeDef(typeof(MainWindowUserInfoResultTransferItem))]
        private void HandleMainWindowInfoResultITransferItem(TransferItem tItem)
        {
        //    Casting the TransferItem into the specific type we want to handle.
            MainWindowUserInfoResultTransferItem UserInfo = (MainWindowUserInfoResultTransferItem)tItem;

            //

            //מידע משתמש
            UserInformation.testerName = UserInfo.testerName;
            UserInformation.testerPassword = UserInfo.testerPassword;
            UserInformation.rfid = UserInfo.rfid;
            UserInformation.userID = UserInfo.userID;
            UserInformation.smokeID = UserInfo.smokeID;
            UserInformation.tz = UserInfo.tz;

            //הגבלות כלליות
            UserInformation.AllowStationRelease = UserInfo.AllowStationRelease;
            UserInformation.AllowTechnicalChanges = UserInfo.AllowTechnicalChanges;
            UserInformation.AllowReportPrinting = UserInfo.AllowReportPrinting;
            UserInformation.AllowReCheck = UserInfo.AllowReCheck;
            UserInformation.AllowReportRePrinting = UserInfo.AllowReportRePrinting;

            //הגבלות גישה
            UserInformation.AllowAccessToDefectWindow = UserInfo.AllowAccessToDefectWindow;
            UserInformation.AllowLicensePlateWindow = UserInfo.AllowLicensePlateWindow;
            UserInformation.AllowAccessToOfficeWindow = UserInfo.AllowAccessToOfficeWindow;
            UserInformation.AllowAdminPrivileges = UserInfo.AllowAdminPrivileges;

            //הגבלות עמדה
            UserInformation.AllowIDStation = UserInfo.AllowIDStation;
            UserInformation.AllowLightsStation = UserInfo.AllowLightsStation;
            UserInformation.AllowPitStation = UserInfo.AllowPitStation;
            UserInformation.AllowSmokeStation = UserInfo.AllowSmokeStation;
            UserInformation.AllowBrakesStation = UserInfo.AllowBrakesStation;
            UserInformation.AllowWheelAlignmentStation = UserInfo.AllowWheelAlignmentStation;
            UserInformation.AllowCheckMechanisim = UserInfo.AllowCheckMechanisim;
            UserInformation.AllowTimeLimitAfterStationRelease = UserInfo.AllowTimeLimitAfterStationRelease;
            UserInformation.AllowDeviceStationSigning = UserInfo.AllowDeviceStationSigning;


            UserName.Text = UserInformation.testerName;
            //InstituteName.Text = UserInformation.InstituteName;
            ExternalIP.Text = UserInformation.External_IP;
            
        }

        #endregion 

        #region Getting Station Settings & Info

        [HandleTypeDef(typeof(MainWindowStationInfoResultTransferItem))]
        private void HandleMainWindowStationInfoResultTransferItem(TransferItem tItem)
        {

            MainWindowStationInfoResultTransferItem StationInfo = (MainWindowStationInfoResultTransferItem)tItem;

            StationInformation.StationID = StationInfo.StationID;
            StationInformation.StationType = StationInfo.StationType;
            StationInformation.Arka_Days = StationInfo.Arka_Days;

            StationInformation.LightsMachineType = StationInfo.LightsMachineType;
            StationInformation.LightsMachineComPort = StationInfo.LightsMachineComPort;
            StationInformation.LightsSettings = StationInfo.LightsSettings;

            StationInformation.GasMachineType = StationInfo.GasMachineType;
            StationInformation.GasMachineComPort = StationInfo.GasMachineComPort;
            StationInformation.GasSettings = StationInfo.GasSettings;

            StationInformation.SmokeMachineType = StationInfo.SmokeMachineType;
            StationInformation.SmokeMachineComPort = StationInfo.SmokeMachineComPort;
            StationInformation.SmokeMeterType = StationInfo.SmokeMeterType;

            StationInformation.RFIDPort = StationInfo.RFIDPort;

            StationInformation.WorkStationNumber = StationInfo.WorkStationNumber;
            StationInformation.ZeroVoltWeight = StationInfo.ZeroVoltWeight;
            StationInformation.KiloPerVoltRMatmer = StationInfo.KiloPerVoltRMatmer;

            StationInformation.ZeroVoltRMatmer = StationInfo.ZeroVoltRMatmer;
            StationInformation.BrakesMachineType = StationInfo.BrakesMachineType;
            StationInformation.BrakeCheck = StationInfo.BrakeCheck;
            StationInformation.RunWayNum = StationInfo.RunWayNum;
            StationInformation.JumpStation = StationInfo.JumpStation;
            StationInformation.DefineWorkLineStation = StationInfo.DefineWorkLineStation;
            StationInformation.ReceiveMaha = StationInfo.ReceiveMaha;
            StationInformation.MahaOutFolder = StationInfo.MahaOutFolder;
            StationInformation.LoadCarColor = StationInfo.LoadCarColor;
            StationInformation.NewCarColor = StationInfo.NewCarColor;
            StationInformation.RequestStationPassword = StationInfo.RequestStationPassword;
            StationInformation.CashSettingsActive = StationInfo.CashSettingsActive;
            StationInformation.PaymentEnd = StationInfo.PaymentEnd;
            StationInformation.PaymentFirst = StationInfo.PaymentFirst;
            StationInformation.TagCost = StationInfo.TagCost;
            StationInformation.UseBinaCode = StationInfo.UseBinaCode;
            StationInformation.BinaMdbPath = StationInfo.BinaMdbPath;
            StationInformation.AllowPrintEndRep = StationInfo.AllowPrintEndRep;
            StationInformation.PasswordTimerOn = StationInfo.PasswordTimerOn;
            StationInformation.PasswordTimerInterval = StationInfo.PasswordTimerInterval;
            StationInformation.DefectListColor = StationInfo.DefectListColor;

        }

        #endregion 

        #region Getting Server Settings & Info

        [HandleTypeDef(typeof(MainWindowServerInfoResultTransferItem))]
        private void HandleMainWindowServerInfoResultTransferItem(TransferItem tItem)
        {

            MainWindowServerInfoResultTransferItem ServerInfo = (MainWindowServerInfoResultTransferItem)tItem;

            ServerInformation.InstituteAddress = ServerInfo.InstituteAddress;
            ServerInformation.InstituteName = ServerInfo.InstituteName;
            ServerInformation.InstituteNumber = ServerInfo.InstituteNumber;
            ServerInformation.MisradRishuiMechozi = ServerInfo.MisradRishuiMechozi;
            ServerInformation.TestMonthsPeriod = ServerInfo.TestMonthsPeriod;
            ServerInformation.Serial = ServerInfo.Serial;
            ServerInformation.DateOfLicense = ServerInfo.DateOfLicense;
            ServerInformation.PurchaseLicense = ServerInfo.PurchaseLicense;
            ServerInformation.OnlinePassword = ServerInfo.OnlinePassword;
            ServerInformation.OnlineDataSupply = ServerInfo.OnlineDataSupply;
            ServerInformation.KodGorem = ServerInfo.KodGorem;
            ServerInformation.LicensePlateSerial = ServerInfo.LicensePlateSerial;
            ServerInformation.KniaSerial = ServerInfo.KniaSerial;
            ServerInformation.OnlineUsage = ServerInfo.OnlineUsage;
            ServerInformation.OnlineAddress = ServerInfo.OnlineAddress;


        }

        #endregion 

        #endregion

        #region Menu Buttons

        private void OfficeButton_Click(object sender, EventArgs e)
        {
            ReceiveVehicle RV = new ReceiveVehicle();
            RV.Show();
        }

        private void Settings_Click(object sender, EventArgs e)
        {
            new SettingsForm().Show();
        }

        private void TestCourseButon_Click(object sender, EventArgs e)
        {
            TestCourse TC = new TestCourse();

            TC.Show();

        }

        private void SwitchUser_Click(object sender, EventArgs e)
        {
            FormPointers.LoginFormPointer.Show();
        }

        #endregion 

        #region Get Set Functions

        public void ResetTesterInfo(string TesterName)//This function will get called whenever the TesterInformation needs to be updated. It sends a TI with the new tester name and gets the relevant data back and sets it!
        {

            //Send the request 
            MainWindowUserInfoRequestTransferItem InfoItem = new MainWindowUserInfoRequestTransferItem();

            InfoItem.TesterName = TesterName;//Set the testername to the testername that was used to log in to the program so that we can get information about them
            Program.SendTransferItem(InfoItem);

        }

        #endregion 

        private void ReportsForm_Click(object sender, EventArgs e)
        {
            new ReportsForm().Show();
        }

        private void advancedButton2_Click(object sender, EventArgs e)
        {
            /*
            //Check to see if device has been assigned for brake testing
            if (MainWindow.StationInformation.BrakesMachineType.Equals("לא מוגדר"))
            {
                MessageBox.Show("!מכשיר לבדיקה זו לא הוגדר. נא הגדר סוג מכשיר בלמים לעמדה זו", "שגיאה: מכשיר לא נמצא", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RtlReading);
            }

            else//If they both are assigned then procceed:
            {

                //If a smoke check has not been open yet then create a new instance of the SmokeTest class and pass in the desired machine and the commport and then start the check
                //OR if a smoketestform WAS created and EXITED(disposed) then recreate the instance again
                if (BrakeTestFormInstance == null || BrakeTestFormInstance.IsDisposed)
                {
                    BrakeTestFormInstance = new BrakesTest(MainWindow.StationInformation.BrakesMachineType);//Start a new instance and send the machine's name as well as it's com port
                    //BrakeTestFormInstance.StartCheck();
                }

                //If an instance exists then stop the check, set the comm port and desired machine(if the user desides to change them in the settings then it will allow to check with a different machine and port)
                //And finally start the check again
                //This occurs if a window of this form is already shown and someone keeps pressing the button
                else
                {

                    //BrakeTestFormInstance.StopCheck();
                   // BrakeTestFormInstance.StartCheck();

                }*/
                BrakeTestFormInstance = new BrakesTest(MainWindow.StationInformation.BrakesMachineType);//Start a new instance and send the machine's name as well as it's com port

                BrakeTestFormInstance.Show();
            //}

        }


    }
}
