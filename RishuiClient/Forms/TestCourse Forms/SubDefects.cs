﻿using CommInf.Handlers;
using RishuiClient.Forms.TestCourse_Forms.Stations.Pit_Station;
using RishuiClient.Types;
using RishuiClient.User_Controls.Test_Course;
using RishuiCommDefines.TransferItems.TestCourseItems;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RishuiClient.Forms.TestCourse_Forms
{
    public partial class SubDefects : Form //After a multi button has been pressed cancel on this form, turn it to canceled version icon
    {
        #region IObjects

        private List<CheckBox> SubDefectCheckBoxList = new List<CheckBox>();

        private readonly IDStation IDStationForm;
        private readonly PitStation PitStationForm;

        private int StationNumber;
        private int DefectID;//The ID of the button that started this form

        #endregion

        #region Constructor And Initial Setup

        public SubDefects(UserControl UC, int DefectID, int StationNumber)//Takes a reference of the User Control that is associated with this form, ID of the defect that triggered this form creation, and a Station Number
        {
            InitializeComponent();

            //Find out the type of the UC object. Once found cast it to the right type 
            if (UC.GetType() == typeof(IDStation)) { IDStationForm = UC as IDStation; }
            if (UC.GetType() == typeof(PitStation)) { PitStationForm = UC as PitStation; }
     
            this.DefectID = DefectID;
            this.StationNumber = StationNumber;

            //Add all the checkboxes to a list
            SubDefectCheckBoxList.Add(SubDefect1);
            SubDefectCheckBoxList.Add(SubDefect2);
            SubDefectCheckBoxList.Add(SubDefect3);
            SubDefectCheckBoxList.Add(SubDefect4);
            SubDefectCheckBoxList.Add(SubDefect5);
            SubDefectCheckBoxList.Add(SubDefect6);
            SubDefectCheckBoxList.Add(SubDefect7);
            SubDefectCheckBoxList.Add(SubDefect8);

            foreach (CheckBox SubDefectCheckBox in SubDefectCheckBoxList) { SubDefectCheckBox.Hide(); }//Run through the list so all the checkboxes are hidden

        }

      

        #region Mapping

        //Here you map all the handlers to the client's communication manager.

        //Example:
        //Program.myCommManager.MapHandler(HandleExampleTransferItem);

        #endregion
        public void InitSubDefects(List<string> SubDefectsList, string RemarkToAdd)//Dump the list of subdefects for the defect in and this function arranges the form correctly and the Remark for the defect
        {

            if (RemarkToAdd.Length > 1) { Remark.Text = RemarkToAdd; }
            else { Remark.Text = "לא קיימות הערות מיוחדות"; }

            for (int i = 0; i < SubDefectsList.Count; i++)
            {

                SubDefectCheckBoxList[i].Show();
                SubDefectCheckBoxList[i].Text = SubDefectsList[i];

            }

        }

        #endregion 

        #region GUI Handlers

        private void button1_Click(object sender, EventArgs e)//FUTURE HERE
        {
            this.Close();

            switch (StationNumber)//Find out which station has been associated with this form, and find the sub defect button that was clicked to get into this form, because we are exiting this form we are cancelling the selection of this button
            {

                case 1://ID

                    foreach (DefectButton Button in IDStation.ButtonList)
                    {

                        if (Button.BttnDefectID == this.DefectID)
                        {
                            Button.DefectBttn.ClickState = false;//Set the click state to false
                        }
                    }
                 break;

                case 3://Pit

                 foreach (DefectButton Button in PitStation.ButtonList)
                 {

                     if (Button.BttnDefectID == this.DefectID)
                     {
                         Button.DefectBttn.ClickState = false;//Set the click state to false
                     }
                 }
                 break;
                    //More stations here
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {

            List<CheckBox> RemoveIndexesFromSubDefectChckBoxList = new List<CheckBox>();//Holds the values to remove from SubDefectCheckBoxList
            string DefectString = "";//Holds the string for the final defect(example: "bad lights/ac leak/flat tires"
            
            //This loop checks which CheckeBoxes in our list that are enabled and NOT ticked. If the condition is met we add the value to remove from SubDefectChckBoxList 
            for(int i = 0; i < SubDefectCheckBoxList.Count; i++)
            {
                if (SubDefectCheckBoxList[i].Enabled && SubDefectCheckBoxList[i].Checked == false) { RemoveIndexesFromSubDefectChckBoxList.Add(SubDefectCheckBoxList[i]); }
            }

            //If all the CheckBoxes need to get removed(which means that none were checked then just prompt the user to select at least 1 checkbox)
            if (SubDefectCheckBoxList.Count == RemoveIndexesFromSubDefectChckBoxList.Count) { MessageBox.Show("נא לבחור לפחות תת סעיף אחד"); }

            else
            {

                foreach (CheckBox ValueToRemove in RemoveIndexesFromSubDefectChckBoxList) { SubDefectCheckBoxList.Remove(ValueToRemove); }//Remove the values from the CheckBoxList

                for (int i = 0; i < SubDefectCheckBoxList.Count; i++)//Loop through the new list of checkboxes(the ones that are enabled and ticked) 
                {

                    if (SubDefectCheckBoxList.Count == 1) { DefectString += SubDefectCheckBoxList[i].Text; }//If the list has only 1 checkbox ticked just add the text

                    else
                    {

                        //If the loop is at it's last count then don't add "/" after the last subdefect. Also the checkbox has to be enabled and checked!
                        if (i == SubDefectCheckBoxList.Count - 1) { DefectString += SubDefectCheckBoxList[i].Text; }

                        //Else just add the / after every sub defect
                        else { DefectString += SubDefectCheckBoxList[i].Text + "/"; }
                    }

                }

                switch(StationNumber)//Deals with finding which checkboxes were selected from this form and adding them to a SubDefectsSelected list(for the appropriate station)
                {

                #region ID Station

                case 1:

                IDStationForm.SetDefectDescriptionTextBoxInTestCourse(DefectString);//Send the final string over

                foreach (DefectButton AssociatedDefectButton in IDStation.ButtonList)//Find the button that triggered this form
                {

                    if (AssociatedDefectButton.BttnDefectID == DefectID)//Find the button associated with the creation of this form
                    {

                        foreach (CheckBox CheckBoxTicked in SubDefectCheckBoxList)
                        {

                            if (CheckBoxTicked.Checked)////Find the checkboxes that were ticked and add them to the SubDefectsSelected List
                            {
                                AssociatedDefectButton.SubDefectsSelected.Add(CheckBoxTicked.Text);//Add the sub defects that were selected from this form to the Sub defects selected list
                            }

                        }

                        #region ToolTip Related
                        //Set tooltip information to the selected sub defects from this form
                        IDStationForm.DefectInfoToolTipEnterButton.SetToolTip(AssociatedDefectButton.DefectBttn, null);
                        IDStationForm.DefectInfoToolTipSelectedDefects.SetToolTip(AssociatedDefectButton.DefectBttn, string.Join(string.Empty + "/", AssociatedDefectButton.SubDefectsSelected));

                        #endregion 

                    }

                }

                break;

                    #endregion

                #region Pit Station
                case 3:

                PitStationForm.SetDefectDescriptionTextBoxInTestCourse(DefectString);//Send the final string over

                foreach (DefectButton AssociatedDefectButton in PitStation.ButtonList)//Find the button that triggered this form
                {

                    if (AssociatedDefectButton.BttnDefectID == DefectID)//Find the button associated with the creation of this form
                    {

                        foreach (CheckBox CheckBoxTicked in SubDefectCheckBoxList)
                        {

                            if (CheckBoxTicked.Checked)////Find the checkboxes that were ticked and add them to the SubDefectsSelected List
                            {
                                AssociatedDefectButton.SubDefectsSelected.Add(CheckBoxTicked.Text);//Add the sub defects that were selected from this form to the Sub defects selected list
                            }

                        }

                        #region ToolTip Related
                        //Set tooltip information to the selected sub defects from this form
                        PitStationForm.DefectInfoToolTipEnterButton.SetToolTip(AssociatedDefectButton.DefectBttn, null);
                        PitStationForm.DefectInfoToolTipSelectedDefects.SetToolTip(AssociatedDefectButton.DefectBttn, string.Join(string.Empty + "/", AssociatedDefectButton.SubDefectsSelected));

                        #endregion
                    }

                }

                break;

                    #endregion

                }

                this.Close();
            }
        }
        #endregion 

        private void SubDefects_Load(object sender, EventArgs e)
        {

        }

        #region TransferItemHandlers

 

        #endregion
    }
}
