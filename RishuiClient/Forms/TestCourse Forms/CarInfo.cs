﻿using CommInf.Handlers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RishuiClient.Forms.TestCourse_Forms
{
    public partial class CarInfo : Form
    {
        #region Members


        #endregion

        public CarInfo()//Initalize constructor
        {
            InitializeComponent();

           
            //condition, num of attempts
            #region Mapping

            //Here you map all the handlers to the client's communication manager.

            //Example:
            //Program.myCommManager.MapHandler(HandleExampleTransferItem);

            #endregion
        }

        public void CarInfoSetData(int CarNumberInfo, string CarTypeInfo, string FuelTypeInfo, string TestTypeInfo, int CarYearInfo, string CarOwnerNameInfo, int NumberOfAttemptsInfo)//Setting the info condition numofattempts
        {

            //Fill the form's boxes
            CarNumber.Text = CarNumberInfo.ToString();
            CarType.Text = CarTypeInfo.ToString();
            FuelType.Text = FuelTypeInfo.ToString();
            TestType.Text = TestTypeInfo.ToString();
            CarYear.Text = CarYearInfo.ToString();
            CarOwnerName.Text = CarOwnerNameInfo.ToString();
            NumberOfAttempts.Text = NumberOfAttemptsInfo.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

  
        #region GUI Handlers

        //For the love of god, copy all your GUI callback functions into here. Keep it organized.

        //private void button_Click(object sender, EventArgs e)
        //{
        //    //Checking if the program succesfully connected.
        //    if(Program.myCommManager.IsConnected)
        //    {
        //        //Creating a new instance of a defined TransferItem
        //        LoginRequestTransferItem reqItem = new LoginRequestTransferItem()
        //            {
        //                Username = userNameTextBox.Text,
        //                Password = passwordTextBox.Text,
        //                SessionID = Program.SessionID
        //            };
        //
        //        //Sending the TransferItem to the server.
        //        Program.SendTransferItem(reqItem);
        //    }
        //    else
        //    {
        //        MessageBox.Show("Not connected dude..");
        //    }
        //}

        #endregion

        #region TransferItemHandlers

        //An example of a TransferItem handler. We mapped this handler to handle it's type in the Form constructor,
        //under the 'Mappings' region.

        //[HandleTypeDef(typeof(ExampleTransferItem))]
        //private void HandleExampleTransferItem(TransferItem tItem)
        //{
        //    Casting the TransferItem into the specific type we want to handle.
        //    ExampleTransferItem exItem = (ExampleTransferItem)tItem;

        //    Handler code goes here.

        //}

        #endregion
    }
}
