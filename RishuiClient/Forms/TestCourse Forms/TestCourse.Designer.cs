﻿namespace RishuiClient.Forms
{
    partial class TestCourse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            AnimatorNS.Animation animation1 = new AnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestCourse));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Animator = new AnimatorNS.Animator(this.components);
            this.InitialFormLoadingBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.DefectDataLoadingBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.FormsStackPanel = new RishuiClient.GUIUserControls.PanelStacker.StackPanel();
            this.TestCourseForm = new System.Windows.Forms.TabPage();
            this.LightsStationButton = new RishuiClient.GUIUserControls.AdvancedButton();
            this.DefectGridShowHide = new RishuiClient.GUIUserControls.AdvancedButton();
            this.DefectInfoGrid = new System.Windows.Forms.DataGridView();
            this.CarManufacturer = new System.Windows.Forms.DataGridViewImageColumn();
            this.EntryTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CarOwnerNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CarType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FuelType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TestAttempt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PitStationButton = new RishuiClient.GUIUserControls.AdvancedButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.advancedButton5 = new RishuiClient.GUIUserControls.AdvancedButton();
            this.label2 = new System.Windows.Forms.Label();
            this.advancedButton6 = new RishuiClient.GUIUserControls.AdvancedButton();
            this.OpenCashForm = new RishuiClient.GUIUserControls.AdvancedButton();
            this.WrittenDefects = new RishuiClient.GUIUserControls.AdvancedButton();
            this.Release = new RishuiClient.GUIUserControls.AdvancedButton();
            this.Sign = new RishuiClient.GUIUserControls.AdvancedButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CarNumberTextBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.DefectDescription = new System.Windows.Forms.RichTextBox();
            this.IDStationButton = new RishuiClient.GUIUserControls.AdvancedButton();
            this.BrakesStationButton = new RishuiClient.GUIUserControls.AdvancedButton();
            this.WheelAlignmentButton = new RishuiClient.GUIUserControls.AdvancedButton();
            this.SmokeStationButton = new RishuiClient.GUIUserControls.AdvancedButton();
            this.LoadingScreen = new System.Windows.Forms.TabPage();
            this.label9 = new System.Windows.Forms.Label();
            this.LoadingCircle = new MRG.Controls.UI.LoadingCircle();
            this.FormsStackPanel.SuspendLayout();
            this.TestCourseForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DefectInfoGrid)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.LoadingScreen.SuspendLayout();
            this.SuspendLayout();
            // 
            // Animator
            // 
            this.Animator.AnimationType = AnimatorNS.AnimationType.Transparent;
            this.Animator.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 1F;
            this.Animator.DefaultAnimation = animation1;
            this.Animator.MaxAnimationTime = 1000;
            // 
            // InitialFormLoadingBackgroundWorker
            // 
            this.InitialFormLoadingBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.InitialFormLoadingBackgroundWorker_DoWork);
            // 
            // DefectDataLoadingBackgroundWorker
            // 
            this.DefectDataLoadingBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.DefectDataLoadingBackgroundWorker_DoWork);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewImageColumn1.HeaderText = "תוצר רכב";
            this.dataGridViewImageColumn1.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn1.Image")));
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // FormsStackPanel
            // 
            this.FormsStackPanel.Controls.Add(this.TestCourseForm);
            this.FormsStackPanel.Controls.Add(this.LoadingScreen);
            this.Animator.SetDecoration(this.FormsStackPanel, AnimatorNS.DecorationType.None);
            this.FormsStackPanel.Location = new System.Drawing.Point(-4, -2);
            this.FormsStackPanel.Name = "FormsStackPanel";
            this.FormsStackPanel.SelectedIndex = 0;
            this.FormsStackPanel.Size = new System.Drawing.Size(1360, 750);
            this.FormsStackPanel.TabIndex = 20;
            // 
            // TestCourseForm
            // 
            this.TestCourseForm.Controls.Add(this.LightsStationButton);
            this.TestCourseForm.Controls.Add(this.DefectGridShowHide);
            this.TestCourseForm.Controls.Add(this.DefectInfoGrid);
            this.TestCourseForm.Controls.Add(this.PitStationButton);
            this.TestCourseForm.Controls.Add(this.groupBox2);
            this.TestCourseForm.Controls.Add(this.IDStationButton);
            this.TestCourseForm.Controls.Add(this.BrakesStationButton);
            this.TestCourseForm.Controls.Add(this.WheelAlignmentButton);
            this.TestCourseForm.Controls.Add(this.SmokeStationButton);
            this.Animator.SetDecoration(this.TestCourseForm, AnimatorNS.DecorationType.None);
            this.TestCourseForm.Location = new System.Drawing.Point(4, 22);
            this.TestCourseForm.Name = "TestCourseForm";
            this.TestCourseForm.Padding = new System.Windows.Forms.Padding(3);
            this.TestCourseForm.Size = new System.Drawing.Size(1352, 724);
            this.TestCourseForm.TabIndex = 0;
            this.TestCourseForm.Text = "Test Course Form";
            this.TestCourseForm.UseVisualStyleBackColor = true;
            // 
            // LightsStationButton
            // 
            this.LightsStationButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("LightsStationButton.BackgroundImage")));
            this.LightsStationButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LightsStationButton.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("LightsStationButton.ClickedHoverImage")));
            this.LightsStationButton.ClickedImage = ((System.Drawing.Image)(resources.GetObject("LightsStationButton.ClickedImage")));
            this.LightsStationButton.ClickState = false;
            this.Animator.SetDecoration(this.LightsStationButton, AnimatorNS.DecorationType.None);
            this.LightsStationButton.FlatAppearance.BorderSize = 0;
            this.LightsStationButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.LightsStationButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.LightsStationButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LightsStationButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold);
            this.LightsStationButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.LightsStationButton.HoverImage = ((System.Drawing.Image)(resources.GetObject("LightsStationButton.HoverImage")));
            this.LightsStationButton.IdleImage = ((System.Drawing.Image)(resources.GetObject("LightsStationButton.IdleImage")));
            this.LightsStationButton.InactiveImage = ((System.Drawing.Image)(resources.GetObject("LightsStationButton.InactiveImage")));
            this.LightsStationButton.IsLocked = false;
            this.LightsStationButton.IsToggleable = false;
            this.LightsStationButton.Location = new System.Drawing.Point(903, 6);
            this.LightsStationButton.Name = "LightsStationButton";
            this.LightsStationButton.Size = new System.Drawing.Size(216, 81);
            this.LightsStationButton.TabIndex = 17;
            this.LightsStationButton.Text = "אורות";
            this.LightsStationButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.LightsStationButton.UseVisualStyleBackColor = true;
            this.LightsStationButton.Click += new System.EventHandler(this.LightsStationButton_Click);
            // 
            // DefectGridShowHide
            // 
            this.DefectGridShowHide.BackgroundImage = global::RishuiClient.Properties.Resources.Right_Idle;
            this.DefectGridShowHide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.DefectGridShowHide.ClickedHoverImage = global::RishuiClient.Properties.Resources.Right_Hover_Clicked;
            this.DefectGridShowHide.ClickedImage = global::RishuiClient.Properties.Resources.Right_Clicked;
            this.DefectGridShowHide.ClickState = false;
            this.Animator.SetDecoration(this.DefectGridShowHide, AnimatorNS.DecorationType.None);
            this.DefectGridShowHide.FlatAppearance.BorderSize = 0;
            this.DefectGridShowHide.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.DefectGridShowHide.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.DefectGridShowHide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DefectGridShowHide.HoverImage = global::RishuiClient.Properties.Resources.Right_Hover;
            this.DefectGridShowHide.IdleImage = global::RishuiClient.Properties.Resources.Right_Idle;
            this.DefectGridShowHide.InactiveImage = null;
            this.DefectGridShowHide.IsLocked = false;
            this.DefectGridShowHide.IsToggleable = true;
            this.DefectGridShowHide.Location = new System.Drawing.Point(20, 89);
            this.DefectGridShowHide.Name = "DefectGridShowHide";
            this.DefectGridShowHide.Size = new System.Drawing.Size(36, 493);
            this.DefectGridShowHide.TabIndex = 19;
            this.DefectGridShowHide.UseVisualStyleBackColor = true;
            this.DefectGridShowHide.Click += new System.EventHandler(this.DefectGridShowHide_Click);
            // 
            // DefectInfoGrid
            // 
            this.DefectInfoGrid.AllowUserToAddRows = false;
            this.DefectInfoGrid.AllowUserToDeleteRows = false;
            this.DefectInfoGrid.AllowUserToResizeColumns = false;
            this.DefectInfoGrid.AllowUserToResizeRows = false;
            this.DefectInfoGrid.BackgroundColor = System.Drawing.Color.White;
            this.DefectInfoGrid.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Aharoni", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DefectInfoGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DefectInfoGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DefectInfoGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CarManufacturer,
            this.EntryTime,
            this.CarOwnerNumber,
            this.CarType,
            this.FuelType,
            this.TestAttempt});
            this.Animator.SetDecoration(this.DefectInfoGrid, AnimatorNS.DecorationType.None);
            this.DefectInfoGrid.EnableHeadersVisualStyles = false;
            this.DefectInfoGrid.GridColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DefectInfoGrid.Location = new System.Drawing.Point(57, 95);
            this.DefectInfoGrid.MultiSelect = false;
            this.DefectInfoGrid.Name = "DefectInfoGrid";
            this.DefectInfoGrid.ReadOnly = true;
            this.DefectInfoGrid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.DefectInfoGrid.RowHeadersVisible = false;
            this.DefectInfoGrid.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DefectInfoGrid.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Aharoni", 34F, System.Drawing.FontStyle.Bold);
            this.DefectInfoGrid.RowTemplate.Height = 45;
            this.DefectInfoGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DefectInfoGrid.Size = new System.Drawing.Size(1281, 484);
            this.DefectInfoGrid.TabIndex = 12;
            this.DefectInfoGrid.Visible = false;
            this.DefectInfoGrid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DefectInfoGrid_CellContentClick);
            // 
            // CarManufacturer
            // 
            this.CarManufacturer.HeaderText = "תוצר";
            this.CarManufacturer.Name = "CarManufacturer";
            this.CarManufacturer.ReadOnly = true;
            // 
            // EntryTime
            // 
            this.EntryTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.EntryTime.HeaderText = "שעת הזנה";
            this.EntryTime.Name = "EntryTime";
            this.EntryTime.ReadOnly = true;
            this.EntryTime.Width = 119;
            // 
            // CarOwnerNumber
            // 
            this.CarOwnerNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CarOwnerNumber.HeaderText = "מספר רכב";
            this.CarOwnerNumber.Name = "CarOwnerNumber";
            this.CarOwnerNumber.ReadOnly = true;
            // 
            // CarType
            // 
            this.CarType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CarType.HeaderText = "סוג רכב";
            this.CarType.Name = "CarType";
            this.CarType.ReadOnly = true;
            // 
            // FuelType
            // 
            this.FuelType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FuelType.HeaderText = "סוג דלק";
            this.FuelType.Name = "FuelType";
            this.FuelType.ReadOnly = true;
            // 
            // TestAttempt
            // 
            this.TestAttempt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TestAttempt.HeaderText = "מבחן מס\'";
            this.TestAttempt.Name = "TestAttempt";
            this.TestAttempt.ReadOnly = true;
            // 
            // PitStationButton
            // 
            this.PitStationButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PitStationButton.BackgroundImage")));
            this.PitStationButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PitStationButton.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("PitStationButton.ClickedHoverImage")));
            this.PitStationButton.ClickedImage = ((System.Drawing.Image)(resources.GetObject("PitStationButton.ClickedImage")));
            this.PitStationButton.ClickState = false;
            this.Animator.SetDecoration(this.PitStationButton, AnimatorNS.DecorationType.None);
            this.PitStationButton.FlatAppearance.BorderSize = 0;
            this.PitStationButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.PitStationButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.PitStationButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PitStationButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold);
            this.PitStationButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.PitStationButton.HoverImage = ((System.Drawing.Image)(resources.GetObject("PitStationButton.HoverImage")));
            this.PitStationButton.IdleImage = ((System.Drawing.Image)(resources.GetObject("PitStationButton.IdleImage")));
            this.PitStationButton.InactiveImage = ((System.Drawing.Image)(resources.GetObject("PitStationButton.InactiveImage")));
            this.PitStationButton.IsLocked = false;
            this.PitStationButton.IsToggleable = true;
            this.PitStationButton.Location = new System.Drawing.Point(682, 6);
            this.PitStationButton.Name = "PitStationButton";
            this.PitStationButton.Size = new System.Drawing.Size(216, 81);
            this.PitStationButton.TabIndex = 18;
            this.PitStationButton.Text = "בור";
            this.PitStationButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.PitStationButton.UseVisualStyleBackColor = true;
            this.PitStationButton.Click += new System.EventHandler(this.PitStationButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.advancedButton5);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.advancedButton6);
            this.groupBox2.Controls.Add(this.OpenCashForm);
            this.groupBox2.Controls.Add(this.WrittenDefects);
            this.groupBox2.Controls.Add(this.Release);
            this.groupBox2.Controls.Add(this.Sign);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.DefectDescription);
            this.Animator.SetDecoration(this.groupBox2, AnimatorNS.DecorationType.None);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.groupBox2.Location = new System.Drawing.Point(19, 574);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1320, 143);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            // 
            // advancedButton5
            // 
            this.advancedButton5.BackgroundImage = global::RishuiClient.Properties.Resources.TechnicalChanges_0004_Idle;
            this.advancedButton5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.advancedButton5.ClickedHoverImage = global::RishuiClient.Properties.Resources.TechnicalChanges_0001_Hover_Clicked;
            this.advancedButton5.ClickedImage = global::RishuiClient.Properties.Resources.TechnicalChanges_0000_Clicked;
            this.advancedButton5.ClickState = false;
            this.Animator.SetDecoration(this.advancedButton5, AnimatorNS.DecorationType.None);
            this.advancedButton5.FlatAppearance.BorderSize = 0;
            this.advancedButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.advancedButton5.Font = new System.Drawing.Font("Narkisim", 15.75F, System.Drawing.FontStyle.Bold);
            this.advancedButton5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.advancedButton5.HoverImage = global::RishuiClient.Properties.Resources.TechnicalChanges_0002_Hover;
            this.advancedButton5.IdleImage = global::RishuiClient.Properties.Resources.TechnicalChanges_0004_Idle;
            this.advancedButton5.InactiveImage = global::RishuiClient.Properties.Resources.TechnicalChanges_0003_Inactive;
            this.advancedButton5.IsLocked = false;
            this.advancedButton5.IsToggleable = false;
            this.advancedButton5.Location = new System.Drawing.Point(301, 37);
            this.advancedButton5.Name = "advancedButton5";
            this.advancedButton5.Size = new System.Drawing.Size(100, 100);
            this.advancedButton5.TabIndex = 136;
            this.advancedButton5.Text = "שינויים טכנים";
            this.advancedButton5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.advancedButton5.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.Animator.SetDecoration(this.label2, AnimatorNS.DecorationType.None);
            this.label2.Font = new System.Drawing.Font("Narkisim", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(737, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 28);
            this.label2.TabIndex = 128;
            this.label2.Text = "תיאור ליקוי";
            // 
            // advancedButton6
            // 
            this.advancedButton6.BackgroundImage = global::RishuiClient.Properties.Resources.PrintButton2_0004_Idle;
            this.advancedButton6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.advancedButton6.ClickedHoverImage = global::RishuiClient.Properties.Resources.PrintButton2_0001_Hover_Clicked;
            this.advancedButton6.ClickedImage = global::RishuiClient.Properties.Resources.PrintButton2_0000_Clicked;
            this.advancedButton6.ClickState = false;
            this.Animator.SetDecoration(this.advancedButton6, AnimatorNS.DecorationType.None);
            this.advancedButton6.FlatAppearance.BorderSize = 0;
            this.advancedButton6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.advancedButton6.Font = new System.Drawing.Font("Narkisim", 15.75F, System.Drawing.FontStyle.Bold);
            this.advancedButton6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.advancedButton6.HoverImage = global::RishuiClient.Properties.Resources.PrintButton2_0002_Hover;
            this.advancedButton6.IdleImage = global::RishuiClient.Properties.Resources.PrintButton2_0004_Idle;
            this.advancedButton6.InactiveImage = global::RishuiClient.Properties.Resources.PrintButton2_0003_Inactive;
            this.advancedButton6.IsLocked = false;
            this.advancedButton6.IsToggleable = false;
            this.advancedButton6.Location = new System.Drawing.Point(408, 37);
            this.advancedButton6.Name = "advancedButton6";
            this.advancedButton6.Size = new System.Drawing.Size(100, 100);
            this.advancedButton6.TabIndex = 135;
            this.advancedButton6.Text = "סיום";
            this.advancedButton6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.advancedButton6.UseVisualStyleBackColor = true;
            // 
            // OpenCashForm
            // 
            this.OpenCashForm.BackgroundImage = global::RishuiClient.Properties.Resources.Cash_0004_Idle;
            this.OpenCashForm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.OpenCashForm.ClickedHoverImage = global::RishuiClient.Properties.Resources.Cash_0001_Hover_Clicked;
            this.OpenCashForm.ClickedImage = global::RishuiClient.Properties.Resources.Cash_0000_Clicked;
            this.OpenCashForm.ClickState = false;
            this.Animator.SetDecoration(this.OpenCashForm, AnimatorNS.DecorationType.None);
            this.OpenCashForm.FlatAppearance.BorderSize = 0;
            this.OpenCashForm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OpenCashForm.Font = new System.Drawing.Font("Narkisim", 15.75F, System.Drawing.FontStyle.Bold);
            this.OpenCashForm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.OpenCashForm.HoverImage = global::RishuiClient.Properties.Resources.Cash_0002_Hover;
            this.OpenCashForm.IdleImage = global::RishuiClient.Properties.Resources.Cash_0004_Idle;
            this.OpenCashForm.InactiveImage = global::RishuiClient.Properties.Resources.Cash_0003_Inactive;
            this.OpenCashForm.IsLocked = false;
            this.OpenCashForm.IsToggleable = false;
            this.OpenCashForm.Location = new System.Drawing.Point(515, 37);
            this.OpenCashForm.Name = "OpenCashForm";
            this.OpenCashForm.Size = new System.Drawing.Size(100, 100);
            this.OpenCashForm.TabIndex = 134;
            this.OpenCashForm.Text = "תשלום";
            this.OpenCashForm.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.OpenCashForm.UseVisualStyleBackColor = true;
            // 
            // WrittenDefects
            // 
            this.WrittenDefects.BackgroundImage = global::RishuiClient.Properties.Resources.DefectsWritten_0004_Idle;
            this.WrittenDefects.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.WrittenDefects.ClickedHoverImage = global::RishuiClient.Properties.Resources.DefectsWritten_0001_Hover_Clicked;
            this.WrittenDefects.ClickedImage = global::RishuiClient.Properties.Resources.DefectsWritten_0000_Clicked;
            this.WrittenDefects.ClickState = false;
            this.Animator.SetDecoration(this.WrittenDefects, AnimatorNS.DecorationType.None);
            this.WrittenDefects.FlatAppearance.BorderSize = 0;
            this.WrittenDefects.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WrittenDefects.Font = new System.Drawing.Font("Narkisim", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WrittenDefects.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.WrittenDefects.HoverImage = global::RishuiClient.Properties.Resources.DefectsWritten_0003_Hover;
            this.WrittenDefects.IdleImage = global::RishuiClient.Properties.Resources.DefectsWritten_0004_Idle;
            this.WrittenDefects.InactiveImage = global::RishuiClient.Properties.Resources.DefectsWritten_0002_Inactive;
            this.WrittenDefects.IsLocked = false;
            this.WrittenDefects.IsToggleable = false;
            this.WrittenDefects.Location = new System.Drawing.Point(1000, 38);
            this.WrittenDefects.Name = "WrittenDefects";
            this.WrittenDefects.Size = new System.Drawing.Size(100, 100);
            this.WrittenDefects.TabIndex = 133;
            this.WrittenDefects.Text = "ליקויים רשומים";
            this.WrittenDefects.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.WrittenDefects.UseVisualStyleBackColor = true;
            // 
            // Release
            // 
            this.Release.BackgroundImage = global::RishuiClient.Properties.Resources.DefectStationRelease_0004_Idle;
            this.Release.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Release.ClickedHoverImage = global::RishuiClient.Properties.Resources.DefectStationRelease_0001_Hover_Clicked;
            this.Release.ClickedImage = global::RishuiClient.Properties.Resources.DefectStationRelease_0002_Clicked;
            this.Release.ClickState = false;
            this.Animator.SetDecoration(this.Release, AnimatorNS.DecorationType.None);
            this.Release.FlatAppearance.BorderSize = 0;
            this.Release.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Release.Font = new System.Drawing.Font("Narkisim", 15.75F, System.Drawing.FontStyle.Bold);
            this.Release.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Release.HoverImage = global::RishuiClient.Properties.Resources.DefectStationRelease_0000_Hover;
            this.Release.IdleImage = global::RishuiClient.Properties.Resources.DefectStationRelease_0004_Idle;
            this.Release.InactiveImage = global::RishuiClient.Properties.Resources.DefectStationRelease_0003_Inactive;
            this.Release.IsLocked = false;
            this.Release.IsToggleable = false;
            this.Release.Location = new System.Drawing.Point(1106, 38);
            this.Release.Name = "Release";
            this.Release.Size = new System.Drawing.Size(100, 100);
            this.Release.TabIndex = 132;
            this.Release.Text = "שחרור עמדה";
            this.Release.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Release.UseVisualStyleBackColor = true;
            this.Release.Click += new System.EventHandler(this.Release_Click);
            // 
            // Sign
            // 
            this.Sign.BackgroundImage = global::RishuiClient.Properties.Resources.DefectStationSign_0004_Idle;
            this.Sign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Sign.ClickedHoverImage = global::RishuiClient.Properties.Resources.DefectStationSign_0001_Hover_Clicked;
            this.Sign.ClickedImage = global::RishuiClient.Properties.Resources.DefectStationSign_0002_Clicked;
            this.Sign.ClickState = false;
            this.Animator.SetDecoration(this.Sign, AnimatorNS.DecorationType.None);
            this.Sign.FlatAppearance.BorderSize = 0;
            this.Sign.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Sign.Font = new System.Drawing.Font("Narkisim", 15.75F, System.Drawing.FontStyle.Bold);
            this.Sign.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Sign.HoverImage = global::RishuiClient.Properties.Resources.DefectStationSign_0000_Hover;
            this.Sign.IdleImage = global::RishuiClient.Properties.Resources.DefectStationSign_0004_Idle;
            this.Sign.InactiveImage = global::RishuiClient.Properties.Resources.DefectStationSign_0003_Inactive;
            this.Sign.IsLocked = false;
            this.Sign.IsToggleable = false;
            this.Sign.Location = new System.Drawing.Point(1212, 38);
            this.Sign.Name = "Sign";
            this.Sign.Size = new System.Drawing.Size(100, 100);
            this.Sign.TabIndex = 131;
            this.Sign.Text = "חתימת עמדה";
            this.Sign.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Sign.UseVisualStyleBackColor = true;
            this.Sign.Click += new System.EventHandler(this.Sign_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CarNumberTextBox);
            this.groupBox3.Controls.Add(this.label1);
            this.Animator.SetDecoration(this.groupBox3, AnimatorNS.DecorationType.None);
            this.groupBox3.Location = new System.Drawing.Point(6, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(289, 138);
            this.groupBox3.TabIndex = 126;
            this.groupBox3.TabStop = false;
            // 
            // CarNumberTextBox
            // 
            this.CarNumberTextBox.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.Animator.SetDecoration(this.CarNumberTextBox, AnimatorNS.DecorationType.None);
            this.CarNumberTextBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CarNumberTextBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CarNumberTextBox.Font = new System.Drawing.Font("Digital-7", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CarNumberTextBox.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.CarNumberTextBox.FormattingEnabled = true;
            this.CarNumberTextBox.Location = new System.Drawing.Point(6, 93);
            this.CarNumberTextBox.Name = "CarNumberTextBox";
            this.CarNumberTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CarNumberTextBox.Size = new System.Drawing.Size(274, 36);
            this.CarNumberTextBox.TabIndex = 127;
            this.CarNumberTextBox.SelectedIndexChanged += new System.EventHandler(this.CarNumberTextBox_SelectedIndexChanged);
            this.CarNumberTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CarNumberComboBox_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.Animator.SetDecoration(this.label1, AnimatorNS.DecorationType.None);
            this.label1.Font = new System.Drawing.Font("Narkisim", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(62, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 28);
            this.label1.TabIndex = 126;
            this.label1.Text = "קליטה ידנית";
            // 
            // DefectDescription
            // 
            this.Animator.SetDecoration(this.DefectDescription, AnimatorNS.DecorationType.None);
            this.DefectDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.DefectDescription.Location = new System.Drawing.Point(629, 44);
            this.DefectDescription.Name = "DefectDescription";
            this.DefectDescription.ReadOnly = true;
            this.DefectDescription.Size = new System.Drawing.Size(357, 90);
            this.DefectDescription.TabIndex = 129;
            this.DefectDescription.Text = "";
            // 
            // IDStationButton
            // 
            this.IDStationButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("IDStationButton.BackgroundImage")));
            this.IDStationButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.IDStationButton.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("IDStationButton.ClickedHoverImage")));
            this.IDStationButton.ClickedImage = ((System.Drawing.Image)(resources.GetObject("IDStationButton.ClickedImage")));
            this.IDStationButton.ClickState = false;
            this.Animator.SetDecoration(this.IDStationButton, AnimatorNS.DecorationType.None);
            this.IDStationButton.FlatAppearance.BorderSize = 0;
            this.IDStationButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.IDStationButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.IDStationButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.IDStationButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold);
            this.IDStationButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.IDStationButton.HoverImage = ((System.Drawing.Image)(resources.GetObject("IDStationButton.HoverImage")));
            this.IDStationButton.IdleImage = ((System.Drawing.Image)(resources.GetObject("IDStationButton.IdleImage")));
            this.IDStationButton.InactiveImage = ((System.Drawing.Image)(resources.GetObject("IDStationButton.InactiveImage")));
            this.IDStationButton.IsLocked = false;
            this.IDStationButton.IsToggleable = true;
            this.IDStationButton.Location = new System.Drawing.Point(1124, 6);
            this.IDStationButton.Name = "IDStationButton";
            this.IDStationButton.Size = new System.Drawing.Size(216, 81);
            this.IDStationButton.TabIndex = 16;
            this.IDStationButton.Text = "זיהוי";
            this.IDStationButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.IDStationButton.UseVisualStyleBackColor = true;
            this.IDStationButton.Click += new System.EventHandler(this.IDStationButton_Click);
            // 
            // BrakesStationButton
            // 
            this.BrakesStationButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BrakesStationButton.BackgroundImage")));
            this.BrakesStationButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BrakesStationButton.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("BrakesStationButton.ClickedHoverImage")));
            this.BrakesStationButton.ClickedImage = ((System.Drawing.Image)(resources.GetObject("BrakesStationButton.ClickedImage")));
            this.BrakesStationButton.ClickState = false;
            this.Animator.SetDecoration(this.BrakesStationButton, AnimatorNS.DecorationType.None);
            this.BrakesStationButton.FlatAppearance.BorderSize = 0;
            this.BrakesStationButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.BrakesStationButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.BrakesStationButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BrakesStationButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold);
            this.BrakesStationButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BrakesStationButton.HoverImage = ((System.Drawing.Image)(resources.GetObject("BrakesStationButton.HoverImage")));
            this.BrakesStationButton.IdleImage = ((System.Drawing.Image)(resources.GetObject("BrakesStationButton.IdleImage")));
            this.BrakesStationButton.InactiveImage = ((System.Drawing.Image)(resources.GetObject("BrakesStationButton.InactiveImage")));
            this.BrakesStationButton.IsLocked = false;
            this.BrakesStationButton.IsToggleable = false;
            this.BrakesStationButton.Location = new System.Drawing.Point(240, 6);
            this.BrakesStationButton.Name = "BrakesStationButton";
            this.BrakesStationButton.Size = new System.Drawing.Size(216, 81);
            this.BrakesStationButton.TabIndex = 13;
            this.BrakesStationButton.Text = "בלמים";
            this.BrakesStationButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BrakesStationButton.UseVisualStyleBackColor = true;
            // 
            // WheelAlignmentButton
            // 
            this.WheelAlignmentButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("WheelAlignmentButton.BackgroundImage")));
            this.WheelAlignmentButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.WheelAlignmentButton.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("WheelAlignmentButton.ClickedHoverImage")));
            this.WheelAlignmentButton.ClickedImage = ((System.Drawing.Image)(resources.GetObject("WheelAlignmentButton.ClickedImage")));
            this.WheelAlignmentButton.ClickState = false;
            this.Animator.SetDecoration(this.WheelAlignmentButton, AnimatorNS.DecorationType.None);
            this.WheelAlignmentButton.FlatAppearance.BorderSize = 0;
            this.WheelAlignmentButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.WheelAlignmentButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.WheelAlignmentButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WheelAlignmentButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold);
            this.WheelAlignmentButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.WheelAlignmentButton.HoverImage = ((System.Drawing.Image)(resources.GetObject("WheelAlignmentButton.HoverImage")));
            this.WheelAlignmentButton.IdleImage = ((System.Drawing.Image)(resources.GetObject("WheelAlignmentButton.IdleImage")));
            this.WheelAlignmentButton.InactiveImage = ((System.Drawing.Image)(resources.GetObject("WheelAlignmentButton.InactiveImage")));
            this.WheelAlignmentButton.IsLocked = false;
            this.WheelAlignmentButton.IsToggleable = false;
            this.WheelAlignmentButton.Location = new System.Drawing.Point(20, 6);
            this.WheelAlignmentButton.Name = "WheelAlignmentButton";
            this.WheelAlignmentButton.Size = new System.Drawing.Size(216, 81);
            this.WheelAlignmentButton.TabIndex = 15;
            this.WheelAlignmentButton.Text = "כיוון גלגלים";
            this.WheelAlignmentButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.WheelAlignmentButton.UseVisualStyleBackColor = true;
            // 
            // SmokeStationButton
            // 
            this.SmokeStationButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("SmokeStationButton.BackgroundImage")));
            this.SmokeStationButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SmokeStationButton.ClickedHoverImage = ((System.Drawing.Image)(resources.GetObject("SmokeStationButton.ClickedHoverImage")));
            this.SmokeStationButton.ClickedImage = ((System.Drawing.Image)(resources.GetObject("SmokeStationButton.ClickedImage")));
            this.SmokeStationButton.ClickState = false;
            this.Animator.SetDecoration(this.SmokeStationButton, AnimatorNS.DecorationType.None);
            this.SmokeStationButton.FlatAppearance.BorderSize = 0;
            this.SmokeStationButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.SmokeStationButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.SmokeStationButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SmokeStationButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold);
            this.SmokeStationButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.SmokeStationButton.HoverImage = ((System.Drawing.Image)(resources.GetObject("SmokeStationButton.HoverImage")));
            this.SmokeStationButton.IdleImage = ((System.Drawing.Image)(resources.GetObject("SmokeStationButton.IdleImage")));
            this.SmokeStationButton.InactiveImage = ((System.Drawing.Image)(resources.GetObject("SmokeStationButton.InactiveImage")));
            this.SmokeStationButton.IsLocked = false;
            this.SmokeStationButton.IsToggleable = false;
            this.SmokeStationButton.Location = new System.Drawing.Point(461, 6);
            this.SmokeStationButton.Name = "SmokeStationButton";
            this.SmokeStationButton.Size = new System.Drawing.Size(216, 81);
            this.SmokeStationButton.TabIndex = 14;
            this.SmokeStationButton.Text = "זיהום אוויר";
            this.SmokeStationButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.SmokeStationButton.UseVisualStyleBackColor = true;
            this.SmokeStationButton.Click += new System.EventHandler(this.SmokeStationButton_Click);
            // 
            // LoadingScreen
            // 
            this.LoadingScreen.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.LoadingScreen.Controls.Add(this.label9);
            this.LoadingScreen.Controls.Add(this.LoadingCircle);
            this.Animator.SetDecoration(this.LoadingScreen, AnimatorNS.DecorationType.None);
            this.LoadingScreen.Location = new System.Drawing.Point(4, 22);
            this.LoadingScreen.Name = "LoadingScreen";
            this.LoadingScreen.Padding = new System.Windows.Forms.Padding(3);
            this.LoadingScreen.Size = new System.Drawing.Size(1352, 724);
            this.LoadingScreen.TabIndex = 1;
            this.LoadingScreen.Text = "Loading Screen";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.Animator.SetDecoration(this.label9, AnimatorNS.DecorationType.None);
            this.label9.Font = new System.Drawing.Font("Aharoni", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.Location = new System.Drawing.Point(514, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(289, 63);
            this.label9.TabIndex = 5;
            this.label9.Text = "אנא המתן...";
            // 
            // LoadingCircle
            // 
            this.LoadingCircle.Active = true;
            this.LoadingCircle.Color = System.Drawing.Color.White;
            this.Animator.SetDecoration(this.LoadingCircle, AnimatorNS.DecorationType.None);
            this.LoadingCircle.InnerCircleRadius = 8;
            this.LoadingCircle.Location = new System.Drawing.Point(607, 224);
            this.LoadingCircle.Name = "LoadingCircle";
            this.LoadingCircle.NumberSpoke = 24;
            this.LoadingCircle.OuterCircleRadius = 9;
            this.LoadingCircle.RotationSpeed = 100;
            this.LoadingCircle.Size = new System.Drawing.Size(141, 103);
            this.LoadingCircle.SpokeThickness = 4;
            this.LoadingCircle.StylePreset = MRG.Controls.UI.LoadingCircle.StylePresets.IE7;
            this.LoadingCircle.TabIndex = 4;
            this.LoadingCircle.Text = "loadingCircle1";
            // 
            // TestCourse
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1350, 730);
            this.Controls.Add(this.FormsStackPanel);
            this.Animator.SetDecoration(this, AnimatorNS.DecorationType.None);
            this.Name = "TestCourse";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "מסלול רשוי";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TestCourse_FormClosed);
            this.Load += new System.EventHandler(this.TestCourse_Load);
            this.FormsStackPanel.ResumeLayout(false);
            this.TestCourseForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DefectInfoGrid)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.LoadingScreen.ResumeLayout(false);
            this.LoadingScreen.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private AnimatorNS.Animator Animator;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridView DefectInfoGrid;
        private System.Windows.Forms.DataGridViewImageColumn CarManufacturer;
        private System.Windows.Forms.DataGridViewTextBoxColumn EntryTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn CarOwnerNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn CarType;
        private System.Windows.Forms.DataGridViewTextBoxColumn FuelType;
        private System.Windows.Forms.DataGridViewTextBoxColumn TestAttempt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox DefectDescription;
        private GUIUserControls.AdvancedButton Release;
        private GUIUserControls.AdvancedButton Sign;
        private GUIUserControls.AdvancedButton advancedButton5;
        private GUIUserControls.AdvancedButton advancedButton6;
        private GUIUserControls.AdvancedButton OpenCashForm;
        private GUIUserControls.AdvancedButton WrittenDefects;
        private System.Windows.Forms.TabPage TestCourseForm;
        private System.Windows.Forms.TabPage LoadingScreen;
        private MRG.Controls.UI.LoadingCircle LoadingCircle;
        private System.Windows.Forms.Label label9;
        private System.ComponentModel.BackgroundWorker InitialFormLoadingBackgroundWorker;
        private System.ComponentModel.BackgroundWorker DefectDataLoadingBackgroundWorker;
        private GUIUserControls.PanelStacker.StackPanel FormsStackPanel;
        public GUIUserControls.AdvancedButton BrakesStationButton;
        public GUIUserControls.AdvancedButton SmokeStationButton;
        public GUIUserControls.AdvancedButton WheelAlignmentButton;
        public GUIUserControls.AdvancedButton IDStationButton;
        public GUIUserControls.AdvancedButton LightsStationButton;
        public GUIUserControls.AdvancedButton PitStationButton;
        public GUIUserControls.AdvancedButton DefectGridShowHide;
        public System.Windows.Forms.ComboBox CarNumberTextBox;
    }
}