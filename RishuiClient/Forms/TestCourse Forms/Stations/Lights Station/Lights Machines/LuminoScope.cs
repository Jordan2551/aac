﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RishuiClient.Forms.TestCourse_Forms.Stations.Lights_Station.Lights_Machines
{
    class LuminoScope
    {

        /// <summary>
        /// 
        /// This class is for communicating with the Luminoscope Light Machine. 
        /// 
        /// How it works: the machine communicates with the serial port when a user presses the right side lights check or left side. Upon communication, an array of bytes get returned
        /// This bytes array contains the following(important information) 
        /// 
        /// Index 0: STX(02h)
        /// Index 2, 3: KCD Value(we combine the strings of this hex to form the value)
        /// Index 4: SIDE(Left or Right side check)
        /// 
        /// Index 1: Function
        /// The function byte is taken and converted into a string that represents the function byte in binary
        /// 
        /// EACH BIT IN THE BINARY FUNCTION PLAYS AN IMPORTANT ROLE: see documentation for more information on this array
        /// 
        /// 
        /// </summary>
        /// 

        #region Members

        SerialPort LuminoScopeSerialPort;

        LightsStation LightsStationFormInstance;//Holds the instance of the SmokeTestForm so we can call a function in that class that retrieves data from here

        byte[] toAscii;//Contains all bytes returned by the serial port
        private string KCDValueHexString;//Stores the KCD values returned by the Luminoscope as a Hex string
        private double KCDValueDouble;//Stores the KCD values returned by the Luminoscope as a double 

        #endregion 

        #region LuminoScope Consts

        //Represents the ascii value that is found at the 4th index of the toAscii array(base 0)
        private const int RIGHT_SIDE = 82;
        private const int LEFT_SIDE = 76;

        #endregion 

        public LuminoScope(LightsStation LightsStationFormInstance)
        {

            this.LightsStationFormInstance = LightsStationFormInstance;

            //Serial port set-up
            LuminoScopeSerialPort = new SerialPort("COM" + MainWindow.StationInformation.LightsMachineComPort + "", 9600, Parity.None, 8, StopBits.One);
            LuminoScopeSerialPort.Handshake = Handshake.None;

            //Set datarec function for this serial port
            this.LuminoScopeSerialPort.DataReceived += LuminoScopeSerialPort_DataReceived;
            StartLightsCheck();//Start the lights check(opens the comm port)

        }

        #region Start/Stop Lights Check

        public void StartLightsCheck()
        {
            this.LuminoScopeSerialPort.Open();
        }

        public void StopLightsCheck()
        {
            this.LuminoScopeSerialPort.Close();
        }

        #endregion 

        #region Serial Port DataReceived

        private void LuminoScopeSerialPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {


            toAscii = ASCIIEncoding.ASCII.GetBytes(LuminoScopeSerialPort.ReadExisting());//Read all output from serial port and store into the byte array
            string inHex = BitConverter.ToString(toAscii);//Convert the Ascii byte array to Hex

            //Function in binary
            string BinaryFunction = "";
            string FunctionByteInHex = "";


            if (toAscii.Length > 6)//Make sure the bytes array has sufficient data to start reading
            {

                BinaryFunction = Convert.ToString(toAscii[1], 2);//Converts Index 1 of the bytes array to a binary string(see summary)
                FunctionByteInHex = Convert.ToString(toAscii[1], 16);//Convert the second byte of the array containing the response from the machine to a base 2 string(binary)


                if (FunctionByteInHex.Equals("1"))//When FunctionByteInHex is "1" that means a low light check which has been correctly aligned has been sent.
                {

                    if (toAscii[4] == LEFT_SIDE)
                    {

                        //Take the 2nd and 3rd indexes of the bytes array and add them together(for example: 1+2 = 12 NOT 3)
                        KCDValueHexString = string.Concat(Convert.ToString(toAscii[2], 16), Convert.ToString(toAscii[3], 16));

                        //Convert the hex string to a number of type double to get the KCD values
                        KCDValueDouble = Convert.ToInt64(KCDValueHexString, 16);

                        //Set the value of the appropriate light in the LightsStation form
                        this.LightsStationFormInstance.SetLightValue("Left Low", ConvertKCDToLuxLow(KCDValueDouble));

                    }

                    if (toAscii[4] == RIGHT_SIDE)
                    {

                        //Take the 2nd and 3rd indexes of the bytes array and add them together(for example: 1+2 = 12 NOT 3)
                        KCDValueHexString = string.Concat(Convert.ToString(toAscii[2], 16), Convert.ToString(toAscii[3], 16));

                        //Convert the hex string to a number of type double to get the KCD values
                        KCDValueDouble = Convert.ToInt64(KCDValueHexString, 16);

                        //Set the value of the appropriate light in the LightsStation form
                        this.LightsStationFormInstance.SetLightValue("Right Low", ConvertKCDToLuxLow(KCDValueDouble));

                    }

                }

                else
                {

                    if (BinaryFunction.Length > 0 && BinaryFunction.Equals("10"))//If the BinaryFunction array has length bigger than 0 and it contains the digits "10" in the array, then this is a high beam check
                    {

                        if (toAscii[4] == LEFT_SIDE)
                        {

                            //Take the 2nd and 3rd indexes of the bytes array and add them together(for example: 1+2 = 12 NOT 3)
                            KCDValueHexString = string.Concat(Convert.ToString(toAscii[2], 16), Convert.ToString(toAscii[3], 16));

                            //Convert the hex string to a number of type double to get the KCD values
                            KCDValueDouble = Convert.ToInt64(KCDValueHexString, 16);

                            //Set the value of the appropriate light in the LightsStation form
                            this.LightsStationFormInstance.SetLightValue("Left High", ConvertKCDToLuxHigh(KCDValueDouble));

                        }


                        if (toAscii[4] == RIGHT_SIDE)
                        {

                            //Take the 2nd and 3rd indexes of the bytes array and add them together(for example: 1+2 = 12 NOT 3)
                            KCDValueHexString = string.Concat(Convert.ToString(toAscii[2], 16), Convert.ToString(toAscii[3], 16));

                            //Convert the hex string to a number of type double to get the KCD values
                            KCDValueDouble = Convert.ToInt64(KCDValueHexString, 16);

                            //Set the value of the appropriate light in the LightsStation form
                            this.LightsStationFormInstance.SetLightValue("Right High", ConvertKCDToLuxHigh(KCDValueDouble));

                        }



                    }
                }
                
                //MACHINE NOT ALIGNED, AKA: NO LIGHTS ARE ON(THIS LIGHT FAILS)
                if (BinaryFunction.Length > 4 && BinaryFunction.Substring(BinaryFunction.Length - 3).Equals("001"))
                {
                    
                    if (toAscii[4] == LEFT_SIDE)
                    {
                        //Set the value of the appropriate light in the LightsStation form to 0(FAILED)
                        this.LightsStationFormInstance.SetLightValue("Right High", 0);
                    }

                    if (toAscii[4] == RIGHT_SIDE)
                    {
                        //Set the value of the appropriate light in the LightsStation form to 0(FAILED)
                        this.LightsStationFormInstance.SetLightValue("Right High", 0);
                    }                    

                }
                
            }
        }

        #endregion 

        #region KCD -> Lux(Low/High) Conversion

        //The Luminoscope returns the intensity value of the light it detects in KCD units, we need to convert them to Lux untis. 
        //Depending on the light check(low or high) we will have to use the suitable functions to convert the KCD units to Lux units

        //Converts KCD units to Lux (Low) units
        public double ConvertKCDToLuxLow(double Value) { return Math.Round((Value / 325), 1); }

        //Converts KCD units to Lux (High) units
        public double ConvertKCDToLuxHigh(double Value) { return Math.Round((Value * 125) / 10000, 1); }


        #endregion


    }


    
}
