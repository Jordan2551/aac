﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RishuiCommDefines.TransferItems.TestCourseItems;
using RishuiClient.Types;
using RishuiClient.GUIUserControls;
using RishuiClient.Types_and_Functions;
using RishuiCommDefines.TransferItems.TestCourseItems.Stations.Light_Station;
using CommInf.Handlers;
using CommInf.Items;
using System.IO.Ports;
using RishuiClient.Forms.TestCourse_Forms.Stations.Lights_Station.Lights_Machines;

namespace RishuiClient.Forms.TestCourse_Forms.Stations.Lights_Station
{
    public partial class LightsStation : UserControl
    {

        #region IObjects

        const int LIGHTS_STATION = 2;

        TableLayoutPanel DefectButtonLayout = new TableLayoutPanel();

        static LightsStation LightsTestFormInstance;//Object to make sure only one instance of LightsStation is ever open

        public static List<DefectButton> ButtonList = new List<DefectButton>();//A list that holds objects of type DefectButton which contains all the information about a button and its's defects
        List<LoadDefectInfoResultTransferItem> DefectsList;

        SubDefects SubDefectsForm;//Sub Defects Form control

        public Boolean IsStationLocked = true;//false = the station is not locked true = the station is locked

        #region Lights Machine Instances

        LuminoScope LuminoScopeInstance;

        #endregion

        #region Defect Button Image Consts

        public static readonly Image DEFECT_BUTTON_IDLE = RishuiClient.Properties.Resources.DefectButtonIdle;
        public static readonly Image DEFECT_BUTTON_CLICKED = RishuiClient.Properties.Resources.DefectButtonClicked;
        public static readonly Image DEFECT_BUTTON_HOVER = RishuiClient.Properties.Resources.DefectButtonHover;
        public static readonly Image DEFECT_BUTTON_HOVER_CLICKED = RishuiClient.Properties.Resources.DefectButtonHoverClicked;
        public static readonly Image DEFECT_BUTTON_CRITICAL_IDLE = RishuiClient.Properties.Resources.CriticalDefectIdle;
        public static readonly Image DEFECT_BUTTON_CRITICAL_CLICKED = RishuiClient.Properties.Resources.CriticalDefectClicked;
        public static readonly Image DEFECT_BUTTON_CRITICAL_HOVER = RishuiClient.Properties.Resources.CriticalDefectHover;
        public static readonly Image DEFECT_BUTTON_CRITICAL_HOVER_CLICKED = RishuiClient.Properties.Resources.CriticalDefectHoverClicked;

        #endregion

        #endregion

        #region Constructor

        private readonly TestCourse Form;//A reference to TestCourse that has not been set

        public LightsStation(List<LoadDefectInfoResultTransferItem> DefectsList, TestCourse Form)//Gets the DefectsList so we can use it here)
        {
            InitializeComponent();

            this.Form = Form;//We get a reference to the live TestCourse thrown in the constructor here so we can manipulate some of the "TestCourse" GUI
            this.DefectsList = DefectsList;

            DefectInfoToolTipSelectedDefects.OwnerDraw = true;//Tell the defect button tooltip to draw according to the draw event handler in this class(for changing the background and font)

            if (Program.myCommManager != null)
            {
                #region Mapping

                Program.myCommManager.MapHandler(HandleLoadLuxModsResultTransferItem);

                #endregion
            }

        }

        #endregion

        #region Defect Button Initial Set Up

        #region Set Defect Buttons for Station

        public void SetDefectButtonsForStation()//Gets all the defects for this station from the DefectsList and creates and adds a new Advanced button with the appropriate text, name and style for each defect found
        {

            ButtonList.Clear();//Reset the buttonlist so the defects won't stack upon station reload

            foreach (LoadDefectInfoResultTransferItem Defect in DefectsList)//For each defect in the defectlist
            {

                if (Defect.StationNumber == LIGHTS_STATION)//If this defect is for this station
                {

                    #region AdvancedButton Properties

                    AdvancedButton DefectButtonToAdd = new AdvancedButton();//This button will be added to the ButtonList 
                    DefectButtonToAdd.Size = new Size(96, 73);
                    DefectButtonToAdd.Font = new Font("Stencil", 20, FontStyle.Bold);

                    DefectButtonToAdd.IdleImage = Properties.Resources.DefectButtonIdle;
                    DefectButtonToAdd.ClickedImage = Properties.Resources.DefectButtonClicked;
                    DefectButtonToAdd.HoverImage = Properties.Resources.DefectButtonHover;
                    DefectButtonToAdd.ClickedHoverImage = Properties.Resources.DefectButtonHoverClicked;

                    DefectButtonToAdd.IsToggleable = true;
                    DefectButtonToAdd.IsLocked = true;//This function runs only once the program loads this sataion for the first time which means a car was never loaded, therfore lock all the button's images

                    //Set the text for the button
                    DefectButtonToAdd.Text = Defect.DefectID.ToString();
                    //Set the name for the button
                    DefectButtonToAdd.Name = "D" + Defect.DefectID.ToString();

                    #endregion

                    ButtonList.Add(new DefectButton(DefectButtonToAdd, false, Convert.ToInt32(DefectButtonToAdd.Text)));//Add the advanced button, a false click state for the button and the defect id that is bound to the button

                }

            }
        }

        #endregion

        #region Defect Button Table-Layout Placement

        public void InitializeDefectButtonLayout()//This function will set properties for the table layout, so we can place the defect buttons in a flexible way(allows for users to add their own custom defects)
        {

            //The location and size match the DefectGrids' because we want the buttons to be convered by the defectgird when a word serach for defects is executed
            DefectButtonLayout.AutoSize = true;
            DefectButtonLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            DefectButtonLayout.Location = new Point(297, 126);

            DefectButtonLayout.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;

            DefectButtonLayout.ColumnCount = 4;

            for (int i = 0; i < ButtonList.Count; i++)
            {

                DefectButtonLayout.Controls.Add(ButtonList[i].DefectBttn);//Add the appropriate button to the layout at the right column, row

                DefectButtonLayout.Controls[i].Margin = new Padding(1);

            }

            this.Controls.Add(DefectButtonLayout);//Add this control to the GUI

        }
        #endregion

        #region Defect Button Additional Properties and Final Setup

        private void LightsStation_Load(object sender, EventArgs e)
        {

            DisableContextMenuStrips();//Disable use of context menus when station loads without a car

            SetDefectButtonsForStation();//Gets all the defects relevant to this station. We will use this information to add a button for each defect

            //Here the rest of the properties for this list are filled

            for (int i = 0; i < ButtonList.Count; i++)//Go through the Defects list searching for defects that suit this StationNumber (ID = 1) and add additional properties to the DefectButton
            {

                for (int j = 0; j < DefectsList.Count; j++)//Second loop so we can compare each button in the list to all of the defects to find the right one(because the buttonList and defectlist are not in synchronized order!)
                {

                    if (DefectsList[j].StationNumber == LIGHTS_STATION && DefectsList[j].DefectID == Convert.ToInt32(ButtonList[i].DefectBttn.Text))//Set the defect of the button to the one from the DefectsList ONLY if the station number is 1 (ID) and the IDs match
                    {

                        ButtonList[i].Defect = DefectsList[j].DefectInfo;
                        ButtonList[i].Remark = DefectsList[j].Remark;
                        ButtonList[i].StationNumber = DefectsList[j].StationNumber;
                        ButtonList[i].RequiresPay = DefectsList[j].RequiresPay;
                        ButtonList[i].SubDefectsSelected = new List<string>();//Init the list

                        ButtonList[i].DefectBttn.MouseEnter += new System.EventHandler(ToolTipMouseEnterDefect);

                        if (ButtonList[i].Defect.Count > 1)//If the button has more than 1 string in the Defect List then set it as the sub defect click handler
                        {

                            ButtonList[i].DefectBttn.Click += new System.EventHandler(MultipleDefectClick);
                            ButtonList[i].DefectBttn.ForeColor = System.Drawing.Color.DarkOrchid;
                            ButtonList[i].DefectBttn.ContextMenuStrip = ContextMenuMultiDefects;

                        }


                        else//For single defect button
                        {

                            ButtonList[i].DefectBttn.Click += new System.EventHandler(SingleDefectClick);
                            ButtonList[i].DefectBttn.ForeColor = System.Drawing.Color.White;
                            ButtonList[i].DefectBttn.ContextMenuStrip = ContextMenuSingleDefect;

                        }

                        j = DefectsList.Count;//Exit second loop
                    }
                }

            }

            InitializeDefectButtonLayout();//Finally, after all the setup for the buttons, we call this function to load the buttons into the GUI

            LoadLuxModsRequestTransferItem LoadLuxModsRequest = new LoadLuxModsRequestTransferItem();
            LoadLuxModsRequest.StationID = 1;//FOR NOW

            //Request the Lux Mods saved for this station
            Program.SendTransferItem(LoadLuxModsRequest);

        }

        #endregion


        #endregion

        #region Defect Button Selection Handlers

        #region Defect Button Mouse Selection

        //Parameters: (the button itself, permit request, Dangerous defect, selected defects(defectinfo list) to set as  the tooltip for what was selected)
        public void SelectDefectButton(AdvancedButton ButtonToSelect, int PermitRequest, int DangerousDefect)//USED WHEN LOADING A CAR WITH DEFECTS FROM TEST COURSE - IT SELECTS THE BUTTON WITHOUT OPENING THE SUBDEFECTS FORM AND UPDATES THE BUTTONS TOOLTIP TO SHOW WHICH DEFECTS WERE SELECTED IN CASE OF A SUB DEFECT 
        {

            foreach (DefectButton Button in ButtonList)
            {

                if (Button.DefectBttn == ButtonToSelect)//If the button from the parameter = the button from the buttonlist iteration then
                {

                    //Set information
                    Button.DefectBttn.ClickState = true;
                    Button.Permit = PermitRequest;
                    Button.DangerousDefect = DangerousDefect;

                    EventArgs ArgForEvent = new EventArgs();//Event args object for marking/unmarking the defect as dangerous below

                    if (Button.SubDefectsSelected.Count == 0)
                    {
                        DefectInfoToolTipSelectedDefects.SetToolTip(Button.DefectBttn, Button.Defect[0]);//Set the selection tooltip for the button to be selected as the only string found in the Defect string list(means it's a button without sub defects)
                    }

                    else
                    {
                        DefectInfoToolTipSelectedDefects.SetToolTip(Button.DefectBttn, String.Join(string.Empty + "/", Button.SubDefectsSelected));//Set the selection tooltip for the button to be selected as the entire list of sub defects that were selected when the station was signed
                    }


                    if (Button.DangerousDefect == 1)//If the this defect is marked as dangerous then set that button as a dangerous defect
                    {
                        MarkDangerousDefect(Button.DefectBttn, ArgForEvent);//Call the function that marks the defect as a dangerous one
                    }

                    else//Change the image set to default if not marked as dangerous
                    {
                        UnmarkDangerousDefect(Button.DefectBttn, ArgForEvent);//Call the function that marks the defect as a dangerous one
                    }

                    //MAYBE DO SOMETHING WITH PERMIT LATER

                }

            }

        }

        private void SingleDefectClick(object sender, EventArgs e)//A defect with single entries has been selected
        {

            for (int i = 0; i < ButtonList.Count; i++)//See which button from the ButtonList has been selected 
            {

                if (ButtonList[i].DefectBttn == sender)
                {

                    if (IsStationLocked) { MessageBox.Show("נא שחרר עמדה"); }//If the station is locked don't let the user click any of the buttons!

                    else
                    {

                        if (ButtonList[i].DefectBttn.ClickState)
                        {
                            Form.SetDefectDescriptionText(ButtonList[i].Defect[0]);//This is a defect with 1 entry in it's list! (no sub defects)
                        }

                        else
                        {

                            EventArgs ArgForEvent = new EventArgs();//Event args object for marking/unmarking the defect as dangerous below

                            //Access the TestCourse live instance and set SetDefectDesc's text via a property we made
                            SetDefectDescriptionTextBoxInTestCourse("");//Button was deselected so set the description to empty

                            //Check if the button that was deselected was marked as a dangerous defect. If it was then call for the function that unmarks a dangerous defect
                            if (ButtonList[i].DangerousDefect == 1)
                            {
                                UnmarkDangerousDefect(ButtonList[i].DefectBttn, ArgForEvent);
                            }

                            if (ButtonList[i].Permit == 1)
                            {
                                PermitRequestCancel(ButtonList[i].DefectBttn, ArgForEvent);//Call for the function that unmarks the permit
                            }

                        }

                    }
                }

            }

        }

        private void MultipleDefectClick(object sender, EventArgs e)//A defect with multiple entries has been selected FIND WHICH CLASS EXECUTED THIS FUNCTION. IF IT'S FROM TESTCOURSE(AKA: LOADING A DEFECTIVE CAR THEN DON'T OPEN A SUB FORM AND JUST THE TOOLTIP FOR THE SELECTED DEFECTS) IF FROM HERE THEN PROCCEED AS NORMAL
        {

            for (int i = 0; i < ButtonList.Count; i++)//See which button from the ButtonList has been selected 
            {

                if (ButtonList[i].DefectBttn == sender)
                {

                    if (IsStationLocked) { MessageBox.Show("נא שחרר עמדה"); }//If the station is locked don't let the user click any of the buttons!

                    else
                    {

                        EventArgs ArgForEvent = new EventArgs();//Event args object for marking/unmarking the defect as dangerous below

                        if (ButtonList[i].DefectBttn.ClickState)
                        {

                            //Create a new SubDefects Form
                            SubDefectsForm = new SubDefects(this, Convert.ToInt32(ButtonList[i].DefectBttn.Text), LIGHTS_STATION);//Pass a reference to this form as well as the Defect ID associated with the button that started this event

                            //Check if the button that was selected was marked as a dangerous defect. If it was then call for the function that marks a dangerous defect
                            if (ButtonList[i].DangerousDefect == 1)
                            {
                                MarkDangerousDefect(ButtonList[i], ArgForEvent);
                            }

                            else
                            {
                                UnmarkDangerousDefect(ButtonList[i], ArgForEvent);
                            }

                            SubDefectsForm.InitSubDefects(ButtonList[i].Defect, ButtonList[i].Remark);//Add the sub defects to the form
                            SubDefectsForm.Show();

                        }

                        else
                        {

                            ButtonList[i].SubDefectsSelected.Clear();//This will clear the list in case a sub defect was selected from this button before and it got saved in this list so we don't keep those unnecessary defects
                            //ButtonList[i].DefectBttn.ClickState = false;

                            //Check if the button that was deselected was marked as a dangerous defect. If it was then call for the function that unmarks a dangerous defect
                            if (ButtonList[i].DangerousDefect == 1)
                            {
                                UnmarkDangerousDefect(ButtonList[i].DefectBttn, ArgForEvent);
                            }

                            //Do the same with Permit
                            if (ButtonList[i].Permit == 1)
                            {
                                PermitRequest(ButtonList[i].DefectBttn, ArgForEvent);
                            }

                            SetDefectDescriptionTextBoxInTestCourse("");//Button was deselected so set the description to empty

                        }
                    }
                }

            }

        }

        public static void ResetButtons()//Resets all the selection states of the buttons in this form to the default of false so that when for example we load a second vehicle number it won't mess up the button sleections, this also resets their color and selection image
        {

            EventArgs ArgForEvent = new EventArgs();//Parameter for clearing permits and dangerous defects below

            foreach (DefectButton Button in ButtonList)
            {

                Button.DefectBttn.IsLocked = false;//Unlock the image of the button so it can change when reseting
                Button.DefectBttn.ClickState = false;//Unclick the button to unselect it

                //Reset the permit request and dangerous defect values for the buttons and set it's image set to regular
                Button.DangerousDefect = 0;
                Button.Permit = 0;
                SetButtonImageSet(Button.DefectBttn, "Regular");

            }

        }

        #endregion


        public void SetDefectDescriptionTextBoxInTestCourse(string Text)//Sets the Defect Description box in TestCourse
        {
            Form.SetDefectDescriptionText(Text);
        }

        #endregion

        #region Context Strip Management

        #region Enable/Disable Context Strips

        public void EnableContextMenuStrips() { this.ContextMenuSingleDefect.Enabled = true; this.ContextMenuMultiDefects.Enabled = true; }
        public void DisableContextMenuStrips() { this.ContextMenuSingleDefect.Enabled = false; this.ContextMenuMultiDefects.Enabled = false; }

        #endregion

        private void בחרToolStripMenuItem_Click(object sender, EventArgs e)//Select/Deselect the button the context menu was opened for
        {

        }

        #region Permit Request Mark and Cancel
        private void PermitRequest(object sender, EventArgs e)//Select דרישת אישור תקינות from context strip
        {

        }

        private void PermitRequestCancel(object sender, EventArgs e)//Cancel a permit request
        {

        }

        #endregion

        #region Mark and Unmark Dangerous Defect
        private void MarkDangerousDefect(object sender, EventArgs e)//Mark Defect as dangerous applies for multiple and single defects
        {

        }

        private void UnmarkDangerousDefect(object sender, EventArgs e)//Mark Defect as dangerous applies for multiple and single defects
        {

        }

        #endregion

        #endregion

        #region ToolTip Management

        public void DefectInfoToolTipSelectedDefects_Draw(object sender, DrawToolTipEventArgs e)//Set the drawing properties for our defect selection tooltip
        {

            e.DrawBackground();
            e.DrawBorder();
            e.DrawText();

        }

        public void ToolTipMouseEnterDefect(object sender, EventArgs e)//Tooltip for hovering over a defect button that is either selected or unselected
        {

            //NOTE: DefectInfoToolTipSelectedDefects = the tooltip that deals with buttons that have been selected(indicated by a green highlight of tooltip text)
            //      DefectInfoToolTipEnterButton = the tooltip that deals with buttons that are not selected to indicate which defect it is(indicated by a grey highlight)
            //      Both of these tooltips are present upon mouse enter of a defect button.

            for (int i = 0; i < ButtonList.Count; i++)//See which button from the ButtonList has been entered
            {

                if (ButtonList[i].DefectBttn == sender)
                {
                    if (ButtonList[i].DefectBttn.ClickState == false)//If the button is not selected then set the tooltip to the button's defect info
                    {
                        //If it's a button with sub defects set the tooltip to all sub defects in that list
                        if (ButtonList[i].Defect.Count > 1)
                        {

                            DefectInfoToolTipEnterButton.SetToolTip(ButtonList[i].DefectBttn, string.Join(string.Empty + "/", ButtonList[i].Defect));
                            DefectInfoToolTipSelectedDefects.SetToolTip(ButtonList[i].DefectBttn, null);
                        }

                        //You know the drill
                        else
                        {
                            DefectInfoToolTipEnterButton.SetToolTip(ButtonList[i].DefectBttn, ButtonList[i].Defect[0]);
                            DefectInfoToolTipSelectedDefects.SetToolTip(ButtonList[i].DefectBttn, null);
                        }
                    }

                    else//If it is selected then hide it
                    {

                        //NOTE: We do not do this section for multiple defects because doing so will result in showing the selected tooltip(green one) like all of the sub defects have been selected. The SubDefects class does the work for that
                        if (ButtonList[i].Defect.Count == 1)
                        {

                            DefectInfoToolTipSelectedDefects.SetToolTip(ButtonList[i].DefectBttn, ButtonList[i].Defect[0]);
                            DefectInfoToolTipEnterButton.SetToolTip(ButtonList[i].DefectBttn, null);

                        }

                    }
                }

            }
        }
        #endregion

        #region Button Graphics Related Functions

        public static void ChangeButtonBackGroundImage(Button ButtonToChange, Image ImageToSet) { ButtonToChange.BackgroundImage = ImageToSet; }

        public static void SetButtonImageSet(AdvancedButton ButtonToChange, string ImageSet)//Takes the  button and a string representing the set of the button images
        {

            //The button that was passed to this function will have it's image set changed according to what "ImageSet" was passed

            if (ImageSet.Equals("Regular"))
            {

                ButtonToChange.IdleImage = DEFECT_BUTTON_IDLE;
                ButtonToChange.ClickedImage = DEFECT_BUTTON_CLICKED;
                ButtonToChange.HoverImage = DEFECT_BUTTON_HOVER;
                ButtonToChange.ClickedHoverImage = DEFECT_BUTTON_HOVER_CLICKED;

            }

            if (ImageSet.Equals("Critical"))
            {

                //ButtonToChange.IdleImage = DEFECT_BUTTON_CRITICAL_IDLE;
                ButtonToChange.ClickedImage = DEFECT_BUTTON_CRITICAL_CLICKED;
                ButtonToChange.HoverImage = DEFECT_BUTTON_CRITICAL_HOVER;
                ButtonToChange.ClickedHoverImage = DEFECT_BUTTON_CRITICAL_HOVER_CLICKED;

            }

        }

        #endregion

        private void StartLightsTest_Click(object sender, EventArgs e)//Clicking the button to start a new lights test
        {

            //Check to see if a port for this station's check and a device have been assigned in the Machine Settings as well as if the port even exists
            if (MainWindow.StationInformation.LightsMachineType.Equals("לא מוגדר") || MainWindow.StationInformation.LightsMachineComPort == 0 || PortCheck() == false)
            {
                MessageBox.Show("!פורט או מכשיר לבדיקה זו לא הוגדרו/נמצאו. נא הגדר פורט וסוג מכשיר לבדיקה זו", "שגיאה: פורט או מכשיר", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RtlReading);
            }

            else//If they both are assigned and the com port assigned has been found then begin the lights check
            {

                //Disable pressing this button because a check has already started
                this.StartLightsTest.Enabled = false;

                //DISABLE THE ABILITY TO SWITCH STATATIONS BECAUSE THE LIGHTS MACHINE IS COMMUNICATING WITH THE FORM(VERY IMPORTANT FOR AVOIDING BUGS!)
                foreach (StationButton StationBttn in Form.StationButtonList)
                {
                    StationBttn.StationButtonInForm.Enabled = false;
                }

                //Disable Car Number loading 
                Form.DefectGridShowHide.Enabled = false;
                Form.CarNumberTextBox.Enabled = false;

                #region Light Bulb Default Color and Blinking

                this.HLR.Color = Color.White;
                this.HLL.Color = Color.White;
                this.LLR.Color = Color.White;
                this.LLL.Color = Color.White;

                this.HLR.Blink(500);
                this.HLL.Blink(500);
                this.LLR.Blink(500);
                this.LLL.Blink(500);

                #endregion

                #region Reset Labels

                this.CheckStatusLabel.Text = "בדיקת אורות פעילה";
                this.CheckStatusLabel.BackColor = Color.MediumSeaGreen;

                this.HLRLabel.ForeColor = Color.Gray;
                this.HLLLabel.ForeColor = Color.Gray;
                this.LLRLabel.ForeColor = Color.Gray;
                this.LLLLabel.ForeColor = Color.Gray;

                this.HLRLuxValue.Text = "0";
                this.HLLLuxValue.Text = "0";
                this.LLRLuxValue.Text = "0";
                this.LLLLuxValue.Text = "0";


                #endregion

                //Start the lights check according to the lights machine chosen in the settings
                switch (MainWindow.StationInformation.LightsMachineType)
                {

                    #region LuminoScope

                    case "LuminoScope":

                        //If an instance of this class has been started before, then close the com port for the machine
                        if (this.LuminoScopeInstance != null) { LuminoScopeInstance.StopLightsCheck(); }

                        this.LuminoScopeInstance = new LuminoScope(this);

                        LuminoScopeInstance.StartLightsCheck();

                        break;

                }

                    #endregion


            }

        }

        public void StopLightsTest()//Stop the lights test
        {

            //stop all device communication

            #region Labels Stop

            this.CheckStatusLabel.Text = "בדיקת אורות לא פעילה";
            this.CheckStatusLabel.BackColor = Color.Gray;

            this.MachineCommunicationBulb.Color = Color.Red;
            this.MachineCommunicationLabel.ForeColor = Color.Maroon;

            #endregion

            #region Light Bulbs Stop

            this.HLR.Blink(0);
            this.HLL.Blink(0);
            this.LLR.Blink(0);
            this.LLL.Blink(0);

            this.HLR.On = true;
            this.HLL.On = true;
            this.LLR.On = true;
            this.LLL.On = true;

            #endregion

            //ENABLE THE ABILITY TO SWITCH STATATIONS BECAUSE THE CHECK HAS STOPPED
            foreach (StationButton StationBttn in Form.StationButtonList)
            {
                StationBttn.StationButtonInForm.Enabled = true;
            }

            //Enable Car Number loading 
            Form.DefectGridShowHide.Enabled = true;
            Form.CarNumberTextBox.Enabled = true;

            //Unclick technician, manual check buttons
            if (this.ManualLightsCheckShowHideButton.ClickState == true) { this.ManualLightsCheckShowHideButton.PerformClick(); }
            if (this.TechnicianToolsShowHideButton.ClickState == true) { this.TechnicianToolsShowHideButton.PerformClick(); }

        }

        private void NumberTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {

            //These textboxes may only contain positive/negative, whole/decimal numbers
            if (UniversalFunctions.IsCharacterANumberWithNegativeAndDecimal(e)) { e.Handled = true; }

        }

        private void SetValuesManualLightsCheckButton_Click(object sender, EventArgs e)
        {

            //Sets the values of the light check manually

            //High Light Right
            try
            {
                //Try to convert the manual value entered by the user to an int. If the conversion is successful then set the value of that light's Textbox to the manually entered one.
                //We also check to see if the value for the lights is enough to pass. if it is, then set the bulb representing the light to green, else, red.
                this.HLRLuxValue.Text = (Convert.ToDouble(HLRManualTextBox.Text) + Convert.ToDouble(LuxModHLR.Text)).ToString();

                if (Convert.ToDouble(HLRLuxValue.Text) >= 16)
                {

                    this.HLR.Color = Color.Lime;
                    this.HLR.Blink(0);
                    this.HLR.On = true;
                    this.HLRLabel.ForeColor = Color.MediumSeaGreen;
                }

                else
                {

                    this.HLR.Color = Color.Red;
                    this.HLR.Blink(0);
                    this.HLR.On = true;
                    this.HLRLabel.ForeColor = Color.Red;

                }

            }

            catch
            {
                MessageBox.Show("יש להזין ערך תקין לימין גבוה", "בדיקת אורות", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
            }

            //High Light Left
            try
            {

                this.HLLLuxValue.Text = (Convert.ToDouble(HLLManualTextBox.Text) + Convert.ToDouble(LuxModHLL.Text)).ToString();

                if (Convert.ToDouble(HLLLuxValue.Text) >= 16)
                {

                    this.HLL.Color = Color.Lime;
                    this.HLL.Blink(0);
                    this.HLL.On = true;
                    this.HLLLabel.ForeColor = Color.MediumSeaGreen;

                }

                else
                {

                    this.HLL.Color = Color.Red;
                    this.HLL.Blink(0);
                    this.HLL.On = true;
                    this.HLLLabel.ForeColor = Color.Red;

                }

            }

            catch
            {
                MessageBox.Show("יש להזין ערך תקין לשמאל גבוה", "בדיקת אורות", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
            }

            //Low Light Right
            try
            {

                this.LLRLuxValue.Text = (Convert.ToDouble(LLRManualTextBox.Text) + Convert.ToDouble(LuxModLLR.Text)).ToString();

                if (Convert.ToDouble(LLRLuxValue.Text) > 0)
                {

                    this.LLR.Color = Color.Lime;
                    this.LLR.Blink(0);
                    this.LLR.On = true;
                    this.LLRLabel.ForeColor = Color.MediumSeaGreen;

                }

                else
                {

                    this.LLR.Color = Color.Red;
                    this.LLR.Blink(0);
                    this.LLR.On = true;
                    this.LLRLabel.ForeColor = Color.Red;

                }

            }

            catch
            {
                MessageBox.Show("יש להזין ערך תקין לימין נמוך", "בדיקת אורות", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
            }

            //Low Light Left
            try
            {

                this.LLLLuxValue.Text = (Convert.ToDouble(LLLManualTextBox.Text) + Convert.ToDouble(LuxModLLL.Text)).ToString();


                if (Convert.ToDouble(LLLLuxValue.Text) > 0)
                {

                    this.LLL.Color = Color.Lime;
                    this.LLL.Blink(0);
                    this.LLL.On = true;
                    this.LLLLabel.ForeColor = Color.MediumSeaGreen;

                }

                else
                {

                    this.LLL.Color = Color.Red;
                    this.LLL.Blink(0);
                    this.LLL.On = true;
                    this.LLLLabel.ForeColor = Color.Red;

                }

            }

            catch
            {
                MessageBox.Show("יש להזין ערך תקין לשמאל נמוך", "בדיקת אורות", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
            }


        }

        private void LightLuxValue_TextChanged(object sender, EventArgs e)
        {

        }

        #region Show/Hide ManualLightsCheck & TechnicianTools

        private void ManualLightsCheckShowHideButton_Click(object sender, EventArgs e)
        {
            this.ManualLightsCheckGroupBox.Visible = !this.ManualLightsCheckGroupBox.Visible;
        }

        private void TechnicianToolsShowHideButton_Click(object sender, EventArgs e)
        {
            this.TechnicianToolsGroupBox.Visible = !this.TechnicianToolsGroupBox.Visible;
        }

        #endregion

        #region KCD -> Lux(Low/High) Conversion

        //The Luminoscope returns the intensity value of the light it detects in KCD units, we need to convert them to Lux untis. 
        //Depending on the light check(low or high) we will have to use the suitable functions to convert the KCD units to Lux units

        //Converts KCD units to Lux (Low) units
        public double ConvertKCDToLuxLow(double Value) { return Math.Round((Value / 325), 1); }

        //Converts KCD units to Lux (High) units
        public double ConvertKCDToLuxHigh(double Value) { return Math.Round((Value * 125) / 10000, 1); }


        #endregion

        #region Load Lux Mods Result

        [HandleTypeDef(typeof(LoadLuxModsResultTransferItem))]
        private void HandleLoadLuxModsResultTransferItem(TransferItem tItem)
        {

            //Set the lux mods according to what was in the DB for this particular station

            LoadLuxModsResultTransferItem LuxModsResult = (LoadLuxModsResultTransferItem)tItem;

            this.LuxModHLR.Text = LuxModsResult.LuxModHLR.ToString();
            this.LuxModHLL.Text = LuxModsResult.LuxModHLL.ToString();
            this.LuxModLLR.Text = LuxModsResult.LuxModLLR.ToString();
            this.LuxModLLL.Text = LuxModsResult.LuxModLLL.ToString();

        }

        #endregion

        private void LuxModHLR_Leave(object sender, EventArgs e)
        {

            TextBox SenderAsTextBox = sender as TextBox;

            if (SenderAsTextBox.Text.Length == 0) { SenderAsTextBox.Text = "0"; }

        }

        private void SaveTechnicianSettingsButton_Click(object sender, EventArgs e)
        {

            //Saves lux value mods that are entered manually

            SaveLuxModsRequestTransferItem SaveLightsModsRequest = new SaveLuxModsRequestTransferItem();

            try
            {

                SaveLightsModsRequest.LuxModHLR = Convert.ToDouble(this.LuxModHLR.Text);
                SaveLightsModsRequest.LuxModHLL = Convert.ToDouble(this.LuxModHLL.Text);
                SaveLightsModsRequest.LuxModLLR = Convert.ToDouble(this.LuxModLLR.Text);
                SaveLightsModsRequest.LuxModLLL = Convert.ToDouble(this.LuxModLLL.Text);
                SaveLightsModsRequest.StationID = 1;//FOR NOW

                Program.SendTransferItem(SaveLightsModsRequest);

                MessageBox.Show("נתונים נשמרו בהצלחה");

            }

            catch
            {
                MessageBox.Show("יש להזין ערכים תקינים", "בדיקת אורות", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
            }

        }

        #region Com Port Checker

        public static bool PortCheck()//Function to check that the port for this device is connected and working to avoid crashes and bugs
        {

            string[] CommPortNames = SerialPort.GetPortNames();//List of strings that represents the name of the comm ports currently in use

            foreach (string ComPortName in CommPortNames)//Loop through each comm port name
            {

                if (ComPortName.Equals("COM" + MainWindow.StationInformation.LightsMachineComPort))//If the comm port name from the string list = the comm port that was passed for this function to compare
                {
                    return true;//Port was found! Return true to allow further operation with this class
                }

            }

            return false;//Port was not found, return false

        }

        #endregion


        /// <summary>
        /// 
        /// Sets the Lux values of the specified light side
        /// 
        /// <example> Side = "Left Low" LuxValue = "2.5"</example>
        /// 
        /// </summary>
        /// <param name="Side"></param>
        /// <param name="LuxValue"></param>
        /// 
        public void SetLightValue(string Side, double LuxValue)
        {

            switch (Side)
            {

                #region Right High

                case "Right High":

                    //Set the text of the textbox representing this light to the value from the machine + mod of value
                    this.HLRLuxValue.Text = (LuxValue + Convert.ToDouble(LuxModHLR.Text)).ToString();

                    //Value passed
                    if (LuxValue + Convert.ToDouble(LuxModHLR.Text) >= 16)
                    {

                        this.HLR.Color = Color.Lime;
                        this.HLR.Blink(0);
                        this.HLR.On = true;
                        this.HLRLabel.ForeColor = Color.MediumSeaGreen;

                    }

                    //Value failed
                    else
                    {

                        this.HLR.Color = Color.Red;
                        this.HLR.Blink(0);
                        this.HLR.On = true;
                        this.HLRLabel.ForeColor = Color.Red;

                    }

                    break;

                #endregion

                #region Left High

                case "Left High":

                    //Set the text of the textbox representing this light to the value from the machine + mod of value
                    this.HLLLuxValue.Text = (LuxValue + Convert.ToDouble(LuxModHLL.Text)).ToString();

                    //Value passed
                    if (LuxValue + Convert.ToDouble(LuxModHLL.Text) >= 16)
                    {

                        this.HLL.Color = Color.Lime;
                        this.HLL.Blink(0);
                        this.HLL.On = true;
                        this.HLLLabel.ForeColor = Color.MediumSeaGreen;

                    }

                    //Value failed
                    else
                    {

                        this.HLL.Color = Color.Red;
                        this.HLL.Blink(0);
                        this.HLL.On = true;
                        this.HLLLabel.ForeColor = Color.Red;

                    }

                    break;

                #endregion

                #region Right Low

                case "Right Low":

                    //Set the text of the textbox representing this light to the value from the machine + mod of value
                    this.LLRLuxValue.Text = (LuxValue + Convert.ToDouble(LuxModLLR.Text)).ToString();

                    //Value passed
                    if (LuxValue + Convert.ToDouble(LuxModLLR.Text) >= 1)
                    {

                        this.LLR.Color = Color.Lime;
                        this.LLR.Blink(0);
                        this.LLR.On = true;
                        this.LLRLabel.ForeColor = Color.MediumSeaGreen;

                    }

                    //Value failed
                    else
                    {

                        this.LLR.Color = Color.Red;
                        this.LLR.Blink(0);
                        this.LLR.On = true;
                        this.LLRLabel.ForeColor = Color.Red;

                    }

                    break;

                #endregion

                #region Left Low

                case "Left Low":

                    //Set the text of the textbox representing this light to the value from the machine + mod of value
                    this.LLLLuxValue.Text = (LuxValue + Convert.ToDouble(LuxModLLL.Text)).ToString();

                    //Value passed
                    if (LuxValue + Convert.ToDouble(LuxModLLL.Text) >= 1)
                    {

                        this.LLL.Color = Color.Lime;
                        this.LLL.Blink(0);
                        this.LLL.On = true;
                        this.LLLLabel.ForeColor = Color.MediumSeaGreen;

                    }

                    //Value failed
                    else
                    {

                        this.LLL.Color = Color.Red;
                        this.LLL.Blink(0);
                        this.LLL.On = true;
                        this.LLLLabel.ForeColor = Color.Red;

                    }

                    break;

                #endregion

            }
        }

    }
}
