﻿using CommInf.Handlers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RishuiCommDefines.TransferItems.ErrorItems;
using CommInf.Items;
using RishuiCommDefines.TransferItems.UserSettingsItems;
using RishuiClient.Forms;
using RishuiClient.Forms.TestCourse_Forms;
using RishuiCommDefines.TransferItems.TestCourseItems;
using RishuiClient.Types;
using RishuiClient.GUIUserControls;

namespace RishuiClient.User_Controls.Test_Course
{
    public partial class IDStation : UserControl
    {
        #region IObjects

        const int ID_STATION = 1;

        TableLayoutPanel DefectButtonLayout = new TableLayoutPanel();

        public static List<DefectButton> ButtonList = new List<DefectButton>();//A list that holds objects of type DefectButton which contains all the information about a button and its's defects
        List<LoadDefectInfoResultTransferItem> DefectsList;

        SubDefects SubDefectsForm;//Sub Defects Form control

        public Boolean IsStationLocked = true;//false = the station is not locked true = the station is locked

        #region Defect Button Image Consts

        public static readonly Image DEFECT_BUTTON_IDLE = RishuiClient.Properties.Resources.DefectButtonIdle;
        public static readonly Image DEFECT_BUTTON_CLICKED = RishuiClient.Properties.Resources.DefectButtonClicked;
        public static readonly Image DEFECT_BUTTON_HOVER = RishuiClient.Properties.Resources.DefectButtonHover;
        public static readonly Image DEFECT_BUTTON_HOVER_CLICKED = RishuiClient.Properties.Resources.DefectButtonHoverClicked;
        public static readonly Image DEFECT_BUTTON_CRITICAL_IDLE = RishuiClient.Properties.Resources.CriticalDefectIdle;
        public static readonly Image DEFECT_BUTTON_CRITICAL_CLICKED = RishuiClient.Properties.Resources.CriticalDefectClicked;
        public static readonly Image DEFECT_BUTTON_CRITICAL_HOVER = RishuiClient.Properties.Resources.CriticalDefectHover;
        public static readonly Image DEFECT_BUTTON_CRITICAL_HOVER_CLICKED = RishuiClient.Properties.Resources.CriticalDefectHoverClicked;

        #endregion

        #endregion 

        #region Constructor

        private readonly TestCourse TestCourseFormInstance;//A reference to TestCourse that has not been set

        public IDStation(List<LoadDefectInfoResultTransferItem> DefectsList, TestCourse TestCourseFormInstance)//Gets the DefectsList so we can use it here)
        {
            InitializeComponent();

            this.TestCourseFormInstance = TestCourseFormInstance;//We get a reference to the live TestCourse thrown in the constructor here so we can manipulate some of the "TestCourse" GUI
            this.DefectsList = DefectsList;
            this.DefectGrid.Hide();

            DefectInfoToolTipSelectedDefects.OwnerDraw = true;//Tell the defect button tooltip to draw according to the draw event handler in this class(for changing the background and font)

            if (Program.myCommManager != null)
            {
                #region Mapping
                #endregion
            }

        }

        #endregion

        #region Defect Button Initial Set Up

        #region Set Defect Buttons for Station

        public void SetDefectButtonsForStation()//Gets all the defects for this station from the DefectsList and creates and adds a new Advanced button with the appropriate text, name and style for each defect found
        {

            ButtonList.Clear();//Reset the buttonlist so the defects won't stack upon station reload

            foreach (LoadDefectInfoResultTransferItem Defect in DefectsList)//For each defect in the defectlist
            {

                if (Defect.StationNumber == ID_STATION)//If this defect is for this station
                {

                    #region AdvancedButton Properties

                    AdvancedButton DefectButtonToAdd = new AdvancedButton();//This button will be added to the ButtonList 
                    DefectButtonToAdd.Size = new Size(96, 73);
                    DefectButtonToAdd.Font = new Font("Stencil", 20, FontStyle.Bold);

                    DefectButtonToAdd.IdleImage = Properties.Resources.DefectButtonIdle;
                    DefectButtonToAdd.ClickedImage = Properties.Resources.DefectButtonClicked;
                    DefectButtonToAdd.HoverImage = Properties.Resources.DefectButtonHover;
                    DefectButtonToAdd.ClickedHoverImage = Properties.Resources.DefectButtonHoverClicked;

                    DefectButtonToAdd.IsToggleable = true;
                    DefectButtonToAdd.IsLocked = true;//This function runs only once the program loads this sataion for the first time which means a car was never loaded, therfore lock all the button's images

                    //Set the text for the button
                    DefectButtonToAdd.Text = Defect.DefectID.ToString();
                    //Set the name for the button
                    DefectButtonToAdd.Name = "D" + Defect.DefectID.ToString();

                    #endregion

                    ButtonList.Add(new DefectButton(DefectButtonToAdd, false, Convert.ToInt32(DefectButtonToAdd.Text)));//Add the advanced button, a false click state for the button and the defect id that is bound to the button

                }

            }
        }

        #endregion 

        #region Defect Button Table-Layout Placement

        public void InitializeDefectButtonLayout()//This function will set properties for the table layout, so we can place the defect buttons in a flexible way(allows for users to add their own custom defects)
        {

            //The location and size match the DefectGrids' because we want the buttons to be convered by the defectgird when a word serach for defects is executed
            DefectButtonLayout.Location = DefectGrid.Location;
            DefectButtonLayout.AutoSize = true;
            DefectButtonLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;

            DefectButtonLayout.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;

            DefectButtonLayout.ColumnCount = 13;

            for (int i = 0; i < ButtonList.Count; i++)
            {
                DefectButtonLayout.Controls.Add(ButtonList[i].DefectBttn);//Add the appropriate button to the layout at the right column, row
                
                DefectButtonLayout.Controls[i].Margin = new Padding(1);

            }

            this.Controls.Add(DefectButtonLayout);//Add this control to the GUI

        }
        #endregion 

        #region Defect Button Additional Properties and Final Setup

        private void IDStation_Load(object sender, EventArgs e)
        {

            DisableContextMenuStrips();//Disable use of context menus when station loads without a car

            SetDefectButtonsForStation();//Gets all the defects relevant to this station. We will use this information to add a button for each defect
            //Here the rest of the properties for this list are filled

            for (int i = 0; i < ButtonList.Count; i++)//Go through the Defects list searching for defects that suit this StationNumber (ID = 1) and add additional properties to the DefectButton
            {

                for (int j = 0; j < DefectsList.Count; j++)//Second loop so we can compare each button in the list to all of the defects to find the right one(because the buttonList and defectlist are not in synchronized order!)
                {

                    if (DefectsList[j].StationNumber == ID_STATION && DefectsList[j].DefectID == Convert.ToInt32(ButtonList[i].DefectBttn.Text))//Set the defect of the button to the one from the DefectsList ONLY if the station number is 1 (ID) and the IDs match
                    {

                        ButtonList[i].Defect = DefectsList[j].DefectInfo;
                        ButtonList[i].Remark = DefectsList[j].Remark;
                        ButtonList[i].StationNumber = DefectsList[j].StationNumber;
                        ButtonList[i].RequiresPay = DefectsList[j].RequiresPay;
                        ButtonList[i].SubDefectsSelected = new List<string>();//Init the list

                        ButtonList[i].DefectBttn.MouseEnter += new System.EventHandler(ToolTipMouseEnterDefect);

                        if (ButtonList[i].Defect.Count > 1)//If the button has more than 1 string in the Defect List then set it as the sub defect click handler
                        {

                            ButtonList[i].DefectBttn.Click += new System.EventHandler(MultipleDefectClick);
                            ButtonList[i].DefectBttn.ForeColor = System.Drawing.Color.DarkOrchid;
                            ButtonList[i].DefectBttn.ContextMenuStrip = ContextMenuMultiDefects;

                        }


                        else//For single defect button
                        {

                            ButtonList[i].DefectBttn.Click += new System.EventHandler(SingleDefectClick);
                            ButtonList[i].DefectBttn.ForeColor = System.Drawing.Color.White;
                            ButtonList[i].DefectBttn.ContextMenuStrip = ContextMenuSingleDefect;

                        }

                        j = DefectsList.Count;//Exit second loop
                    }
                }

            }

            InitializeDefectButtonLayout();//Finally, after all the setup for the buttons, we call this function to load the buttons into the GUI

        }

            #endregion 


        #endregion

        #region Defect Button Selection Handlers

        #region Defect Button Mouse Selection

        //Parameters: (the button itself, permit request, Dangerous defect, selected defects(defectinfo list) to set as  the tooltip for what was selected)
        public void SelectDefectButton(AdvancedButton ButtonToSelect, int PermitRequest, int DangerousDefect)//USED WHEN LOADING A CAR WITH DEFECTS FROM TEST COURSE - IT SELECTS THE BUTTON WITHOUT OPENING THE SUBDEFECTS FORM AND UPDATES THE BUTTONS TOOLTIP TO SHOW WHICH DEFECTS WERE SELECTED IN CASE OF A SUB DEFECT 
        {
            
            foreach(DefectButton Button in ButtonList)
            {

                if (Button.DefectBttn == ButtonToSelect)//If the button from the parameter = the button from the buttonlist iteration then
                {

                    //Set information
                    Button.DefectBttn.ClickState = true;
                    Button.Permit = PermitRequest;
                    Button.DangerousDefect = DangerousDefect;

                    EventArgs ArgForEvent = new EventArgs();//Event args object for marking/unmarking the defect as dangerous below

                    if (Button.SubDefectsSelected.Count == 0)
                    {
                        DefectInfoToolTipSelectedDefects.SetToolTip(Button.DefectBttn, Button.Defect[0]);//Set the selection tooltip for the button to be selected as the only string found in the Defect string list(means it's a button without sub defects)
                    }

                    else
                    {
                        DefectInfoToolTipSelectedDefects.SetToolTip(Button.DefectBttn, String.Join(string.Empty + "/", Button.SubDefectsSelected));//Set the selection tooltip for the button to be selected as the entire list of sub defects that were selected when the station was signed
                    }


                    if (Button.DangerousDefect == 1)//If the this defect is marked as dangerous then set that button as a dangerous defect
                    {
                        MarkDangerousDefect(Button.DefectBttn, ArgForEvent);//Call the function that marks the defect as a dangerous one
                    }

                    else//Change the image set to default if not marked as dangerous
                    {
                        UnmarkDangerousDefect(Button.DefectBttn, ArgForEvent);//Call the function that marks the defect as a dangerous one
                    }

                    //MAYBE DO SOMETHING WITH PERMIT LATER

                }

            }

        }

        private void SingleDefectClick(object sender, EventArgs e)//A defect with single entries has been selected
        {

            for (int i = 0; i < ButtonList.Count; i++)//See which button from the ButtonList has been selected 
            {

                if (ButtonList[i].DefectBttn == sender)
                {

                    if (IsStationLocked) { MessageBox.Show("נא שחרר עמדה"); }//If the station is locked don't let the user click any of the buttons!

                    else
                    {

                        if (ButtonList[i].DefectBttn.ClickState)
                        {
                            TestCourseFormInstance.SetDefectDescriptionText(ButtonList[i].Defect[0]);//This is a defect with 1 entry in it's list! (no sub defects)
                        }

                        else
                        {

                            EventArgs ArgForEvent = new EventArgs();//Event args object for marking/unmarking the defect as dangerous below

                            //Access the TestCourse live instance and set SetDefectDesc's text via a property we made
                            SetDefectDescriptionTextBoxInTestCourse("");//Button was deselected so set the description to empty
                            
                            //Check if the button that was deselected was marked as a dangerous defect. If it was then call for the function that unmarks a dangerous defect
                            if (ButtonList[i].DangerousDefect == 1)
                            {
                                UnmarkDangerousDefect(ButtonList[i].DefectBttn, ArgForEvent);
                            }

                            if (ButtonList[i].Permit == 1)
                            {
                                PermitRequestCancel(ButtonList[i].DefectBttn, ArgForEvent);//Call for the function that unmarks the permit
                            }

                        }

                    }
                }

            }

        }

        private void MultipleDefectClick(object sender, EventArgs e)//A defect with multiple entries has been selected FIND WHICH CLASS EXECUTED THIS FUNCTION. IF IT'S FROM TESTCOURSE(AKA: LOADING A DEFECTIVE CAR THEN DON'T OPEN A SUB FORM AND JUST THE TOOLTIP FOR THE SELECTED DEFECTS) IF FROM HERE THEN PROCCEED AS NORMAL
        {

            for (int i = 0; i < ButtonList.Count; i++)//See which button from the ButtonList has been selected 
            {

                if (ButtonList[i].DefectBttn == sender)
                {

                    if (IsStationLocked) { MessageBox.Show("נא שחרר עמדה"); }//If the station is locked don't let the user click any of the buttons!

                    else
                    {

                        EventArgs ArgForEvent = new EventArgs();//Event args object for marking/unmarking the defect as dangerous below

                        if (ButtonList[i].DefectBttn.ClickState)
                        {

                            //Create a new SubDefects Form
                            SubDefectsForm = new SubDefects(this, Convert.ToInt32(ButtonList[i].DefectBttn.Text), ID_STATION);//Pass a reference to this form as well as the Defect ID associated with the button that started this event

                            //Check if the button that was selected was marked as a dangerous defect. If it was then call for the function that marks a dangerous defect
                            if (ButtonList[i].DangerousDefect == 1)
                            {
                                MarkDangerousDefect(ButtonList[i], ArgForEvent);
                            }

                            else
                            {
                                UnmarkDangerousDefect(ButtonList[i], ArgForEvent);
                            }

                            SubDefectsForm.InitSubDefects(ButtonList[i].Defect, ButtonList[i].Remark);//Add the sub defects to the form
                            SubDefectsForm.Show();

                        }

                        else
                        {

                            ButtonList[i].SubDefectsSelected.Clear();//This will clear the list in case a sub defect was selected from this button before and it got saved in this list so we don't keep those unnecessary defects
                            //ButtonList[i].DefectBttn.ClickState = false;

                            //Check if the button that was deselected was marked as a dangerous defect. If it was then call for the function that unmarks a dangerous defect
                            if (ButtonList[i].DangerousDefect == 1)
                            {
                                UnmarkDangerousDefect(ButtonList[i].DefectBttn, ArgForEvent);
                            }

                            //Do the same with Permit
                            if (ButtonList[i].Permit == 1)
                            {
                                PermitRequest(ButtonList[i].DefectBttn, ArgForEvent);
                            }

                            SetDefectDescriptionTextBoxInTestCourse("");//Button was deselected so set the description to empty

                        }
                    }
                }

            }
            
        }

        public static void ResetButtons()//Resets all the selection states of the buttons in this form to the default of false so that when for example we load a second vehicle number it won't mess up the button sleections, this also resets their color and selection image
        {

            EventArgs ArgForEvent = new EventArgs();//Parameter for clearing permits and dangerous defects below

            foreach(DefectButton Button in ButtonList)
            {

                Button.DefectBttn.IsLocked = false;//Unlock the image of the button so it can change when reseting
                Button.DefectBttn.ClickState = false;//Unclick the button to unselect it

                //Reset the permit request and dangerous defect values for the buttons and set it's image set to regular
                Button.DangerousDefect = 0;
                Button.Permit = 0;
                SetButtonImageSet(Button.DefectBttn, "Regular");

            }

        }

        #endregion 

        #region Defect Search By Words
        private void DefectSearch_TextChanged(object sender, EventArgs e)
        {

            DefectGrid.Rows.Clear();//Start off by clearing the grid so it doesn't stack up with irrelevant information

            if (DefectSearch.Text.Length == 0) { DefectGrid.Hide(); }//If the TextBox is empty just hide the DefectGrid

            else//Serach for the string
            {

                DefectGrid.Show();

                foreach (DefectButton Button in ButtonList)
                {

                    string SearchIn = string.Empty;//String that will store the possible search text of a button's defect ID + it's defect contents

                    if (Button.Defect.Count > 1) { SearchIn = string.Join(Button.DefectBttn.Text + "", Button.Defect); }//Use the join function if it's a defect with sub defects

                    else{ SearchIn = Button.DefectBttn.Text + Button.Defect[0]; }//Just add them together if no sub defects


                    if (SearchIn.Contains(DefectSearch.Text))//See if the big string contains the text that was put in the box
                    {

                        DefectGrid.Rows.Add(Button.BttnDefectID, string.Join(String.Empty + "/", Button.Defect));

                    }

                }

            }
        }


        private void DefectGrid_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            string CellValue = DefectGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();//The value of the cell with the ID or Description that the event was started for
            //string DefectDescriptionValue = DefectGrid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();//The value of the cell with the ID that the event was started for

            if (e.RowIndex > -1)//Check that the first row (the names for columns) is not selected
            {

                foreach (DefectButton ButtonToSelect in ButtonList)
                {

                    string SearchString = string.Join(ButtonToSelect.BttnDefectID + "/", ButtonToSelect.Defect);//The string we want to compare to find the right defect button to select. It contains the ID of the defect as well as it's defect info so that everything is searchable

                    if (CellValue.Equals(ButtonToSelect.DefectBttn.Text) || CellValue.Equals(String.Join(string.Empty + "/", ButtonToSelect.Defect)))//Just check if the defect number in the grid cell = the one on the button the
                    {

                        DefectGrid.Rows.Clear();//Clean the Grid
                        DefectGrid.Hide();//Hide it so we can see the buttons again

                        ButtonToSelect.DefectBttn.PerformClick();//Tell the button to get clicked for us, and therfore will execute the correct Click function for it(to whatever the button was set to(either multiple selection or single selection click event)

                    }

                }
            }
        }

        #endregion


        public void SetDefectDescriptionTextBoxInTestCourse(string Text)//Sets the Defect Description box in TestCourse
        {
            TestCourseFormInstance.SetDefectDescriptionText(Text);
        }

        #endregion


        #region Context Strip Management

        #region Enable/Disable Context Strips

        public void EnableContextMenuStrips() { this.ContextMenuSingleDefect.Enabled = true; this.ContextMenuMultiDefects.Enabled = true; }
        public void DisableContextMenuStrips() { this.ContextMenuSingleDefect.Enabled = false; this.ContextMenuMultiDefects.Enabled = false; }

        #endregion 

        private void בחרToolStripMenuItem_Click(object sender, EventArgs e)//Select/Deselect the button the context menu was opened for
        {

            //Make sure the sender is a ToolStripMenuItem
            ToolStripMenuItem myItem = sender as ToolStripMenuItem;

            if (myItem != null)
            {
                //Get the ContextMenuString (owner of the ToolsStripMenuItem)
                ContextMenuStrip theStrip = myItem.Owner as ContextMenuStrip;

                if (theStrip != null)
                {
                    //The SourceControl is the control that opened the contextmenustrip.
                    //In my case it could be a linkLabel
                    Button ClickedButton = theStrip.SourceControl as Button;//Cast the context menu strip's SourceControl as a button(the owner of the context strip in this case)

                    foreach (DefectButton Button in ButtonList)
                    {

                        if (ClickedButton == Button.DefectBttn)
                        {
                            Button.DefectBttn.PerformClick();
                        }

                    }

                }
            }
        }

        #region Permit Request Mark and Cancel
        private void PermitRequest(object sender, EventArgs e)//Select דרישת אישור תקינות from context strip
        {

            //Make sure the sender is a ToolStripMenuItem
            ToolStripMenuItem myItem = sender as ToolStripMenuItem;

            if (myItem != null)
            {
                //Get the ContextMenuString (owner of the ToolsStripMenuItem)
                ContextMenuStrip theStrip = myItem.Owner as ContextMenuStrip;

                if (theStrip != null)
                {
                    //The SourceControl is the control that opened the contextmenustrip.
                    //In my case it could be a linkLabel
                    Button ClickedButton = theStrip.SourceControl as Button;//Cast the context menu strip's SourceControl as a button(the owner of the context strip in this case)

                    foreach (DefectButton Button in ButtonList)
                    {

                        if (ClickedButton == Button.DefectBttn)
                        {

                            if (Button.DefectBttn.ClickState) { Button.Permit = 1; Button.DefectBttn.ForeColor = Color.Gold; }//If the button that was selected is in a clicked state then set permit value to true. Also change text of the button to gold to indicate a permit was requested

                            else { MessageBox.Show("יש לבחור ליקוי כדי לבקש אישור תקינות"); }//Else prompt the user that they must select a defect first

                        }

                    }

                }
            } 

        }

        private void PermitRequestCancel(object sender, EventArgs e)//Cancel a permit request
        {

            //Make sure the sender is a ToolStripMenuItem(When using the ToolStripMenu to click the function) OR an AdvancedButton(when wanting to call this function because of deselecting a defect, which also deselects the permit)
            ToolStripMenuItem ToolStripItem = sender as ToolStripMenuItem;
            AdvancedButton ButtonItem = sender as AdvancedButton;

            #region Calling this Function from ToolStripMenu

            if (ToolStripItem != null)
            {
                //Get the ContextMenuString (owner of the ToolsStripMenuItem)
                ContextMenuStrip theStrip = ToolStripItem.Owner as ContextMenuStrip;

                if (theStrip != null)
                {

                    //The SourceControl is the control that opened the contextmenustrip.
                    //In my case it could be a linkLabel
                    Button ClickedButton = theStrip.SourceControl as Button;//Cast the context menu strip's SourceControl as a button(the owner of the context strip in this case)

                    foreach (DefectButton Button in ButtonList)
                    {

                        if (ClickedButton == Button.DefectBttn)
                        {

                            //Check if that button has sub defects or not and change the color of it's text according to what it was originally
                            if (Button.Defect.Count > 1)
                            {
                                Button.DefectBttn.ForeColor = Color.DarkOrchid;
                            }

                            else
                            {
                                Button.DefectBttn.ForeColor = Color.White;
                            }

                            Button.Permit = 0;
                        }

                    }

                }
            }

            #endregion 

            #region Calling this Function from Deselecting a Button with Permit Requested

            if (ButtonItem != null)
            {

                foreach (DefectButton Button in ButtonList)
                {

                    if (ButtonItem == Button.DefectBttn)
                    {

                        //Check if that button has sub defects or not and change the color of it's text according to what it was originally
                        if (Button.Defect.Count > 1)
                        {
                            Button.DefectBttn.ForeColor = Color.DarkOrchid;
                        }

                        else
                        {
                            Button.DefectBttn.ForeColor = Color.White;
                        }

                        Button.Permit = 0;
                    }

                }
                
            }

            #endregion 

        }

        #endregion 

        //FIND A SOLUTION SO THAT WHEN ONE IS ENABLED THE OTHER IS DISABLED
        #region Mark and Unmark Dangerous Defect
        private void MarkDangerousDefect(object sender, EventArgs e)//Mark Defect as dangerous applies for multiple and single defects
        {

            ToolStripMenuItem ToolStripItem = sender as ToolStripMenuItem;

            if (ToolStripItem != null)
            {
                //Get the ContextMenuString (owner of the ToolsStripMenuItem)
                ContextMenuStrip theStrip = ToolStripItem.Owner as ContextMenuStrip;

                if (theStrip != null)
                {
                    //The SourceControl is the control that opened the contextmenustrip.
                    Button ClickedButton = theStrip.SourceControl as Button;//Cast the context menu strip's SourceControl as a button(the owner of the context strip in this case)

                    foreach (DefectButton Button in ButtonList)
                    {

                        if (ClickedButton == Button.DefectBttn)
                        {

                            if (Button.DefectBttn.ClickState)//Only allow a button that has been selected to be marked as a dangerous defect
                            {
                               
                                Button.DangerousDefect = 1;

                                SetButtonImageSet(Button.DefectBttn, "Critical");

                            }

                            else { MessageBox.Show("יש לבחור ליקוי על מנת לסמנו כמסוכן"); }
                        }
                    }
                }

            }
        }

        private void UnmarkDangerousDefect(object sender, EventArgs e)//Mark Defect as dangerous applies for multiple and single defects
        {

            //Make sure the sender is a ToolStripMenuItem(When using the ToolStripMenu to click the function) OR an AdvancedButton(when wanting to call this function because of deselecting a defect, which also deselects the permit)
            ToolStripMenuItem ToolStripItem = sender as ToolStripMenuItem;
            AdvancedButton ButtonItem = sender as AdvancedButton;

            #region Calling this Function from ToolStripMenu

            if (ToolStripItem != null)
            {
                //Get the ContextMenuString (owner of the ToolsStripMenuItem)
                ContextMenuStrip theStrip = ToolStripItem.Owner as ContextMenuStrip;
            
                if (theStrip != null)
                {
                    //The SourceControl is the control that opened the contextmenustrip.
                    Button ClickedButton = theStrip.SourceControl as Button;//Cast the context menu strip's SourceControl as a button(the owner of the context strip in this case)
            
                    foreach (DefectButton Button in ButtonList)
                    {
            
                        if (ClickedButton == Button.DefectBttn)
                        {
            
                            Button.DangerousDefect = 0;
                            SetButtonImageSet(Button.DefectBttn, "Regular");
            
                        }
            
                    }
            
                }
            }

            #endregion 

            #region Calling this Function from Deselecting a Button with DangerousDefect

            if (ButtonItem != null)
            {

                foreach (DefectButton Button in ButtonList)
                {

                    if (ButtonItem == Button.DefectBttn)
                    {

                        Button.DangerousDefect = 0;
                        SetButtonImageSet(Button.DefectBttn, "Regular");

                    }

                }

            }

            #endregion 

        }

        #endregion

        #endregion


        #region ToolTip Management

        public void DefectInfoToolTipSelectedDefects_Draw(object sender, DrawToolTipEventArgs e)//Set the drawing properties for our defect selection tooltip
        {

            e.DrawBackground();
            e.DrawBorder();
            e.DrawText();

        }


        public void ToolTipMouseEnterDefect(object sender, EventArgs e)//Tooltip for hovering over a defect button that is either selected or unselected
        {
            
            //NOTE: DefectInfoToolTipSelectedDefects = the tooltip that deals with buttons that have been selected(indicated by a green highlight of tooltip text)
            //      DefectInfoToolTipEnterButton = the tooltip that deals with buttons that are not selected to indicate which defect it is(indicated by a grey highlight)
            //      Both of these tooltips are present upon mouse enter of a defect button.

            for (int i = 0; i < ButtonList.Count; i++)//See which button from the ButtonList has been entered
            {

                if (ButtonList[i].DefectBttn == sender)
                {
                    if (ButtonList[i].DefectBttn.ClickState == false)//If the button is not selected then set the tooltip to the button's defect info
                    {
                        //If it's a button with sub defects set the tooltip to all sub defects in that list
                        if (ButtonList[i].Defect.Count > 1)
                        {

                            DefectInfoToolTipEnterButton.SetToolTip(ButtonList[i].DefectBttn, string.Join(string.Empty + "/", ButtonList[i].Defect));
                            DefectInfoToolTipSelectedDefects.SetToolTip(ButtonList[i].DefectBttn, null);

                        }
                        
                        //You know the drill
                        else 
                        { 
                            DefectInfoToolTipEnterButton.SetToolTip(ButtonList[i].DefectBttn, ButtonList[i].Defect[0]);
                            DefectInfoToolTipSelectedDefects.SetToolTip(ButtonList[i].DefectBttn, null);
                        }
                    }

                    else//If it is selected then hide it
                    {

                        //NOTE: We do not do this section for multiple defects because doing so will result in showing the selected tooltip(green one) like all of the sub defects have been selected. The SubDefects class does the work for that
                        if (ButtonList[i].Defect.Count == 1)
                        {

                            DefectInfoToolTipSelectedDefects.SetToolTip(ButtonList[i].DefectBttn, ButtonList[i].Defect[0]);
                            DefectInfoToolTipEnterButton.SetToolTip(ButtonList[i].DefectBttn, null);
                  
                        }

                    }
                }

            }
        }

        #endregion


        #region Button Graphics Related Functions

        public static void SetButtonImageSet(AdvancedButton ButtonToChange, string ImageSet)//Takes the  button and a string representing the set of the button images
        {

            //The button that was passed to this function will have it's image set changed according to what "ImageSet" was passed

            if (ImageSet.Equals("Regular"))
            {

                ButtonToChange.IdleImage = DEFECT_BUTTON_IDLE;
                ButtonToChange.ClickedImage = DEFECT_BUTTON_CLICKED;
                ButtonToChange.HoverImage = DEFECT_BUTTON_HOVER;
                ButtonToChange.ClickedHoverImage = DEFECT_BUTTON_HOVER_CLICKED;

            }

            if (ImageSet.Equals("Critical"))
            {

                //ButtonToChange.IdleImage = DEFECT_BUTTON_CRITICAL_IDLE;
                ButtonToChange.ClickedImage = DEFECT_BUTTON_CRITICAL_CLICKED;
                ButtonToChange.HoverImage = DEFECT_BUTTON_CRITICAL_HOVER;
                ButtonToChange.ClickedHoverImage = DEFECT_BUTTON_CRITICAL_HOVER_CLICKED;

            }

        }

        #endregion


        #region Abandoned Functions
        public bool ContainsString(string SourceString, string SearchInSource)//This function will return true if a string appears in another string(used for searching Defect strings with Defect by words text box)
        {

            int EqualChars = 0;//Holds the amount of characters that are equal(SearchInSource to SourceString) If the value of EqualsChars = the num of chars in the SearchInSource string that means that all characters are present in the SourceString

            for (int i = 0; i < SourceString.Length; i++)//Loop through the whole source string
            {

                for (int j = 0; j < SearchInSource.Length; j++)//Compare each char in the SearchInSource string to the ones in the SourceString
                {

                    if (SearchInSource[j].Equals(SourceString[i]) && SearchInSource.Length == 1) { return true; }//IN CASE OF 1 CHAR IN SEARCHINSOURCE: If that char is equal to somewhere in SourceString then return true!

                    else if (SearchInSource[j].Equals(SourceString[i]) && SearchInSource.Length > 1)//If a character in SearchInSource is equal to one in SourceString we increment EqualChars 
                    {

                        if (i == SourceString.Length - 1) { return true; }//If we are at the last index of the SourceString and we found an equal character then just return true

                        else
                        {

                            //Now that We found the first char that was equal, We must make another loop to make sure that every char after the one found to be equal is also equal to the next char in the SourceString!
                            j++;//Increment to next SearchInSource char
                            i++;//Increment to next SourceString char

                            while (SearchInSource[j].Equals(SourceString[i]))//Comparing the next char for each of the strings, for this function to return true this loop has to run every time which means all chars after the first found char are equal
                            {

                                if (j == SearchInSource.Length - 1 || i == SourceString.Length - 1) { }//If either index is at the last index of their char array then don't increment!

                                else
                                {

                                    j++;
                                    i++;

                                }

                                EqualChars++;
                            }

                        }

                    }

                }
            }


            if (EqualChars == SearchInSource.Length - 1) { return true; }//If the value of EqualsChars = the length of SearchInSource(aka: all the characters from SearchInSource were found) we return true

            return false;
        }

        #endregion 



    } 
}
