﻿namespace RishuiClient.User_Controls.Test_Course
{
    partial class IDStation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ContextMenuSingleDefect = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.בחרToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EditSingle = new System.Windows.Forms.ToolStripMenuItem();
            this.MarkDangerousSingle = new System.Windows.Forms.ToolStripMenuItem();
            this.ביטולסימוןליקויכמסוכןToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.דרישתאישורתקינותToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ביטולדרישתאישורתקינותToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenuMultiDefects = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.בחרלקויToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EditMulti = new System.Windows.Forms.ToolStripMenuItem();
            this.MarkDangerousMulti = new System.Windows.Forms.ToolStripMenuItem();
            this.ביטולסימוןליקויכמסוכןToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.DefectInfoToolTipSelectedDefects = new System.Windows.Forms.ToolTip(this.components);
            this.DefectInfoToolTipEnterButton = new System.Windows.Forms.ToolTip(this.components);
            this.DefectSearch = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.DefectGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.דרישתאישורתקינותToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ביטולדרישתאישורתקינותToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenuSingleDefect.SuspendLayout();
            this.ContextMenuMultiDefects.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DefectGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // ContextMenuSingleDefect
            // 
            this.ContextMenuSingleDefect.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.בחרToolStripMenuItem,
            this.EditSingle,
            this.MarkDangerousSingle,
            this.ביטולסימוןליקויכמסוכןToolStripMenuItem,
            this.דרישתאישורתקינותToolStripMenuItem,
            this.ביטולדרישתאישורתקינותToolStripMenuItem});
            this.ContextMenuSingleDefect.Name = "ContextMenuMultiDefects";
            this.ContextMenuSingleDefect.Size = new System.Drawing.Size(213, 136);
            // 
            // בחרToolStripMenuItem
            // 
            this.בחרToolStripMenuItem.Name = "בחרToolStripMenuItem";
            this.בחרToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.בחרToolStripMenuItem.Text = "בחירה/ביטול";
            this.בחרToolStripMenuItem.Click += new System.EventHandler(this.בחרToolStripMenuItem_Click);
            // 
            // EditSingle
            // 
            this.EditSingle.Name = "EditSingle";
            this.EditSingle.Size = new System.Drawing.Size(212, 22);
            this.EditSingle.Text = "עריכת מלל";
            // 
            // MarkDangerousSingle
            // 
            this.MarkDangerousSingle.Name = "MarkDangerousSingle";
            this.MarkDangerousSingle.Size = new System.Drawing.Size(212, 22);
            this.MarkDangerousSingle.Text = "סימון ליקוי כמסוכן";
            this.MarkDangerousSingle.Click += new System.EventHandler(this.MarkDangerousDefect);
            // 
            // ביטולסימוןליקויכמסוכןToolStripMenuItem
            // 
            this.ביטולסימוןליקויכמסוכןToolStripMenuItem.Name = "ביטולסימוןליקויכמסוכןToolStripMenuItem";
            this.ביטולסימוןליקויכמסוכןToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.ביטולסימוןליקויכמסוכןToolStripMenuItem.Text = "ביטול סימון ליקוי כמסוכן";
            this.ביטולסימוןליקויכמסוכןToolStripMenuItem.Click += new System.EventHandler(this.UnmarkDangerousDefect);
            // 
            // דרישתאישורתקינותToolStripMenuItem
            // 
            this.דרישתאישורתקינותToolStripMenuItem.Name = "דרישתאישורתקינותToolStripMenuItem";
            this.דרישתאישורתקינותToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.דרישתאישורתקינותToolStripMenuItem.Text = "דרישת אישור תקינות";
            this.דרישתאישורתקינותToolStripMenuItem.Click += new System.EventHandler(this.PermitRequest);
            // 
            // ביטולדרישתאישורתקינותToolStripMenuItem
            // 
            this.ביטולדרישתאישורתקינותToolStripMenuItem.Name = "ביטולדרישתאישורתקינותToolStripMenuItem";
            this.ביטולדרישתאישורתקינותToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.ביטולדרישתאישורתקינותToolStripMenuItem.Text = "ביטול דרישת אישור תקינות";
            this.ביטולדרישתאישורתקינותToolStripMenuItem.Click += new System.EventHandler(this.PermitRequestCancel);
            // 
            // ContextMenuMultiDefects
            // 
            this.ContextMenuMultiDefects.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.בחרלקויToolStripMenuItem,
            this.EditMulti,
            this.MarkDangerousMulti,
            this.ביטולסימוןליקויכמסוכןToolStripMenuItem1,
            this.דרישתאישורתקינותToolStripMenuItem1,
            this.ביטולדרישתאישורתקינותToolStripMenuItem1});
            this.ContextMenuMultiDefects.Name = "ContextMenuMultiDefects";
            this.ContextMenuMultiDefects.Size = new System.Drawing.Size(213, 158);
            // 
            // בחרלקויToolStripMenuItem
            // 
            this.בחרלקויToolStripMenuItem.Name = "בחרלקויToolStripMenuItem";
            this.בחרלקויToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.בחרלקויToolStripMenuItem.Text = "בחירה/ביטול";
            this.בחרלקויToolStripMenuItem.Click += new System.EventHandler(this.בחרToolStripMenuItem_Click);
            // 
            // EditMulti
            // 
            this.EditMulti.Name = "EditMulti";
            this.EditMulti.Size = new System.Drawing.Size(196, 22);
            this.EditMulti.Text = "עריכת מלל";
            // 
            // MarkDangerousMulti
            // 
            this.MarkDangerousMulti.Name = "MarkDangerousMulti";
            this.MarkDangerousMulti.Size = new System.Drawing.Size(196, 22);
            this.MarkDangerousMulti.Text = "סימון ליקוי כמסוכן";
            this.MarkDangerousMulti.Click += new System.EventHandler(this.MarkDangerousDefect);
            // 
            // ביטולסימוןליקויכמסוכןToolStripMenuItem1
            // 
            this.ביטולסימוןליקויכמסוכןToolStripMenuItem1.Name = "ביטולסימוןליקויכמסוכןToolStripMenuItem1";
            this.ביטולסימוןליקויכמסוכןToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.ביטולסימוןליקויכמסוכןToolStripMenuItem1.Text = "ביטול סימון ליקוי כמסוכן";
            this.ביטולסימוןליקויכמסוכןToolStripMenuItem1.Click += new System.EventHandler(this.UnmarkDangerousDefect);
            // 
            // DefectInfoToolTipSelectedDefects
            // 
            this.DefectInfoToolTipSelectedDefects.AutomaticDelay = 0;
            this.DefectInfoToolTipSelectedDefects.BackColor = System.Drawing.Color.LimeGreen;
            this.DefectInfoToolTipSelectedDefects.ShowAlways = true;
            this.DefectInfoToolTipSelectedDefects.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.DefectInfoToolTipSelectedDefects_Draw);
            // 
            // DefectInfoToolTipEnterButton
            // 
            this.DefectInfoToolTipEnterButton.BackColor = System.Drawing.Color.White;
            // 
            // DefectSearch
            // 
            this.DefectSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DefectSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.DefectSearch.Location = new System.Drawing.Point(3, 3);
            this.DefectSearch.Name = "DefectSearch";
            this.DefectSearch.Size = new System.Drawing.Size(865, 29);
            this.DefectSearch.TabIndex = 68;
            this.DefectSearch.TextChanged += new System.EventHandler(this.DefectSearch_TextChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(867, 2);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(224, 29);
            this.label1.TabIndex = 69;
            this.label1.Text = "חיפוש ליקויים במלל:";
            // 
            // DefectGrid
            // 
            this.DefectGrid.AllowUserToAddRows = false;
            this.DefectGrid.AllowUserToDeleteRows = false;
            this.DefectGrid.AllowUserToResizeColumns = false;
            this.DefectGrid.AllowUserToResizeRows = false;
            this.DefectGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DefectGrid.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DefectGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.DefectGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DefectGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.DefectGrid.Location = new System.Drawing.Point(0, 35);
            this.DefectGrid.Name = "DefectGrid";
            this.DefectGrid.ReadOnly = true;
            this.DefectGrid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DefectGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.DefectGrid.RowHeadersVisible = false;
            this.DefectGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DefectGrid.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.DefectGrid.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DefectGrid.Size = new System.Drawing.Size(1094, 460);
            this.DefectGrid.TabIndex = 86;
            this.DefectGrid.Visible = false;
            this.DefectGrid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DefectGrid_CellContentDoubleClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn1.HeaderText = "מס\' ליקוי";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 101;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "פירוט ליקוי";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // דרישתאישורתקינותToolStripMenuItem1
            // 
            this.דרישתאישורתקינותToolStripMenuItem1.Name = "דרישתאישורתקינותToolStripMenuItem1";
            this.דרישתאישורתקינותToolStripMenuItem1.Size = new System.Drawing.Size(212, 22);
            this.דרישתאישורתקינותToolStripMenuItem1.Text = "דרישת אישור תקינות";
            this.דרישתאישורתקינותToolStripMenuItem1.Click += new System.EventHandler(this.PermitRequest);
            // 
            // ביטולדרישתאישורתקינותToolStripMenuItem1
            // 
            this.ביטולדרישתאישורתקינותToolStripMenuItem1.Name = "ביטולדרישתאישורתקינותToolStripMenuItem1";
            this.ביטולדרישתאישורתקינותToolStripMenuItem1.Size = new System.Drawing.Size(212, 22);
            this.ביטולדרישתאישורתקינותToolStripMenuItem1.Text = "ביטול דרישת אישור תקינות";
            this.ביטולדרישתאישורתקינותToolStripMenuItem1.Click += new System.EventHandler(this.PermitRequestCancel);
            // 
            // IDStation
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.AutoScrollMinSize = new System.Drawing.Size(10, 10);
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.DefectGrid);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DefectSearch);
            this.Name = "IDStation";
            this.Size = new System.Drawing.Size(1131, 517);
            this.Load += new System.EventHandler(this.IDStation_Load);
            this.ContextMenuSingleDefect.ResumeLayout(false);
            this.ContextMenuMultiDefects.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DefectGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem EditMulti;
        private System.Windows.Forms.ToolStripMenuItem MarkDangerousMulti;
        private System.Windows.Forms.ToolStripMenuItem EditSingle;
        private System.Windows.Forms.ToolStripMenuItem MarkDangerousSingle;
        private System.Windows.Forms.TextBox DefectSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem דרישתאישורתקינותToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ביטולסימוןליקויכמסוכןToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ביטולסימוןליקויכמסוכןToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ביטולדרישתאישורתקינותToolStripMenuItem;
        private System.Windows.Forms.DataGridView DefectGrid;
        public System.Windows.Forms.ToolTip DefectInfoToolTipSelectedDefects;
        public System.Windows.Forms.ToolTip DefectInfoToolTipEnterButton;
        private System.Windows.Forms.ToolStripMenuItem בחרToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem בחרלקויToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        public System.Windows.Forms.ContextMenuStrip ContextMenuMultiDefects;
        public System.Windows.Forms.ContextMenuStrip ContextMenuSingleDefect;
        private System.Windows.Forms.ToolStripMenuItem דרישתאישורתקינותToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ביטולדרישתאישורתקינותToolStripMenuItem1;
    }
}
