﻿using CommInf.Handlers;
using CommInf.Items;
using RishuiClient.Forms.TestCourse_Forms;
using RishuiClient.Forms.TestCourse_Forms.Stations.Lights_Station;
using RishuiClient.Forms.TestCourse_Forms.Stations.Pit_Station;
using RishuiClient.Forms.TestCourse_Forms.Stations.Smoke_Station;
using RishuiClient.GUIUserControls;
using RishuiClient.Types;
using RishuiClient.User_Controls.Test_Course;
using RishuiCommDefines.TransferItems.SharedDataItems;
using RishuiCommDefines.TransferItems.TestCourseItems;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RishuiClient.Forms
{

    public partial class TestCourse : Form
    {
        
        #region Consts

        #region Button Image Consts

        #region Station Button Image Consts

        private readonly Image STATION_BUTTON_PASSED_IDLE = RishuiClient.Properties.Resources.SPI;
        private readonly Image STATION_BUTTON_PASSED_CLICKED = RishuiClient.Properties.Resources.SPC;
        private readonly Image STATION_BUTTON_PASSED_HOVER = RishuiClient.Properties.Resources.SPH;
        private readonly Image STATION_BUTTON_PASSED_HOVER_CLICKED = RishuiClient.Properties.Resources.SPHC;
        
        private readonly Image STATION_BUTTON_FAILED_IDLE = RishuiClient.Properties.Resources.SFI;
        private readonly Image STATION_BUTTON_FAILED_CLICKED = RishuiClient.Properties.Resources.SFC;
        private readonly Image STATION_BUTTON_FAILED_HOVER = RishuiClient.Properties.Resources.SFH;
        private readonly Image STATION_BUTTON_FAILED_HOVER_CLICKED = RishuiClient.Properties.Resources.SFHC;
        
        private readonly Image STATION_BUTTON_RELEASED_IDLE = RishuiClient.Properties.Resources.SUI;
        private readonly Image STATION_BUTTON_RELEASED_CLICKED = RishuiClient.Properties.Resources.SUC;
        private readonly Image STATION_BUTTON_RELEASED_HOVER = RishuiClient.Properties.Resources.SUH;
        private readonly Image STATION_BUTTON_RELEASED_HOVER_CLICKED = RishuiClient.Properties.Resources.SUHC;

        #endregion

        #region DefectGridShowHide Button Image Consts

        private readonly Image DEFECTGRIDSHOWHIDE_LEFT_IDLE = RishuiClient.Properties.Resources.Left_Idle;
        private readonly Image DEFECTGRIDSHOWHIDE_LEFT_CLICKED = RishuiClient.Properties.Resources.Left_Clicked;
        private readonly Image DEFECTGRIDSHOWHIDE_LEFT_HOVER = RishuiClient.Properties.Resources.Left_Hover;
        private readonly Image DEFECTGRIDSHOWHIDE_LEFT_HOVER_CLICKED = RishuiClient.Properties.Resources.Left_Hover_Clicked;

        private readonly Image DEFECTGRIDSHOWHIDE_RIGHT_IDLE = RishuiClient.Properties.Resources.Right_Idle;
        private readonly Image DEFECTGRIDSHOWHIDE_RIGHT_CLICKED = RishuiClient.Properties.Resources.Right_Clicked;
        private readonly Image DEFECTGRIDSHOWHIDE_RIGHT_HOVER = RishuiClient.Properties.Resources.Right_Hover;
        private readonly Image DEFECTGRIDSHOWHIDE_RIGHT_HOVER_CLICKED = RishuiClient.Properties.Resources.Right_Hover_Clicked;

        #endregion

        #endregion 

        #region Station Consts

        private const int ID_STATION = 1;
        private const int LIGHTS_STATION = 2;
        private const int PIT_STATION = 3;
        private const int SMOKE_STATION = 4;
        private const int BRAKES_STATION = 5;
        private const int WHEEL_ALIGNMENT_STATION = 6;

        #endregion 

        #endregion

        public static string DefectDescriptionText;//A universal string that will be used by all the user controls associated with this form to change the DefectDescriptionTB

        List<Image> ManufacturerSymbolList = new List<Image>();//This list stores all images from the manufacturer symbol resource folder

        #region IObjects

        List<Carmanufacturer> CarmanufacturerList = new List<Carmanufacturer>();//Holds all manu IDs and their appropriate images

        List<LoadDefectInfoResultTransferItem> DefectsList;//List of all the defects
        public static List<SignStationRequestTransferItem> DefectsSelectedList = new List<SignStationRequestTransferItem>();//List of all the defects that were selected from any of the stations(one user control at a time)

        public List<StationButton> StationButtonList = new List<StationButton>();//Holds all the station buttons and their click states

        CarInfo CarInfoForm = new CarInfo();//An instance of the CarInfo Form. Constructor takes the list of possible defects

        int CarNumber;//Holds the number of the car that is associated with loading defects for

        #region Station User Control Definitions

        List<UserControl> StationControlList = new List<UserControl>();//Holds all the Station's user controls in this list 

        IDStation IDStationControl;
        PitStation PitStationControl;
        SmokeStation SmokeStationControl;
        LightsStation LightsStationControl;

        int SelectedStation;//Holds an int value representing which station has been selected. This is faster than looping through the station buttons list and finding which button was clicked!

        #endregion 

        LoadDefectsForCarResultTransferItem DefectiveCar;//Holds information regarding a requested car's defects;

        #endregion

        #region Constructor
        public TestCourse()
        {
            InitializeComponent();

            #region FIX THIS 
            IDStationButton.IsToggleable = IDStationButton.IsToggleable;
            IDStationButton.ClickState = IDStationButton.ClickState;

            PitStationButton.IsToggleable = PitStationButton.IsToggleable;
            PitStationButton.ClickState = PitStationButton.ClickState;

            #endregion 

            #region Mapping

            Program.myCommManager.MapHandler(HandleLoadCarNumbersWithDefectsResultTransferItem);
            Program.myCommManager.MapHandler(HandleLoadDefectsForCarResultTransferItem);
            Program.myCommManager.MapHandler(HandleLoadDefectInfoResultTransferItem);
            Program.myCommManager.MapHandler(HandleSignStationResultTransferItem);

            #endregion
        }

        #endregion 

        #region Form Initialization
        private void TestCourse_Load(object sender, EventArgs e)
        {

            this.FormsStackPanel.SelectedIndex = 1;//Start by showing the loading screen

            #region Loading Circle Properties

            LoadingCircle.NumberSpoke = 100;
            LoadingCircle.OuterCircleRadius = 35;
            LoadingCircle.InnerCircleRadius = 30;
            LoadingCircle.RotationSpeed = 15;
            LoadingCircle.SpokeThickness = 12;

            #endregion

            #region Load Resource Images(Car Manufacturer Logos)

            ResourceManager rm = ManufacturerLogos.ResourceManager;

            ResourceSet rs = rm.GetResourceSet(CultureInfo.CurrentCulture, true, true);
            var ordered = rs.OfType<DictionaryEntry>().OrderBy(val => val.Key.ToString());


            if (ordered != null)
            {

                var images =
                  from entry in ordered
                  where entry.Value is Image
                  select entry.Value;

                foreach (Image img in images)
                {
                    ManufacturerSymbolList.Add(img);
                }
            }

            #endregion 

            #region Station User Control Init

            //Start the background worker that loads the initial data requires for this form(on a separate thread)
            this.InitialFormLoadingBackgroundWorker.RunWorkerAsync(); 

            //Add the station control buttons to list
            StationButtonList.Add(new StationButton(IDStationButton, 1, "Released"));
            StationButtonList.Add(new StationButton(LightsStationButton, 2, "Released"));
            StationButtonList.Add(new StationButton(PitStationButton, 3, "Released"));
            StationButtonList.Add(new StationButton(SmokeStationButton, 4, "Released"));
            StationButtonList.Add(new StationButton(BrakesStationButton, 5, "Released"));
            StationButtonList.Add(new StationButton(WheelAlignmentButton, 6, "Released"));

            #endregion 

            //Set the order of the DefectInfoGrid
            DefectInfoGrid.Sort(DefectInfoGrid.Columns[2], ListSortDirection.Ascending);

            this.Sign.Enabled = false;//Change sign button to locked
            this.Release.Enabled = false; //Change release button to locked

            

        }

        #endregion 

        #region GUI Handlers

        #region Station Button Click

        #region Station Control Init

        //Sets up everything about the station when it loads; draws the control, selects the defect buttons etc

        #region ID Station

        private void InitIDStation()
        {

            DrawUserControl(IDStationControl, 60, 90, 1320, 490);

            //Weird bug: when user control gets drawn this grid moves to the right for some reason, this fixes it
            DefectInfoGrid.Location = new Point(48, 89);

            if (DefectiveCar != null)//If the information about the defective car has been loaded then we can proceed to load all the info about this car on the station's UC
            {

                IDStation.ResetButtons();//Reset the buttons that switching stations doesn't cause unsigned buttons to be selected!

                SelectedStation = ID_STATION;//Change selected station to 1(ID station)

                #region Sign - Release Set Up

                if (DefectiveCar.IDStationData.IsStationClosed == 0)
                {

                    this.Sign.Enabled = false;//Sign to lock
                    this.Release.Enabled = true;//Release to unlocked

                    IDStationControl.IsStationLocked = true;
                    IDStationControl.DisableContextMenuStrips();

                }

                else
                {

                    this.Sign.Enabled = true;//Sign to unlock
                    this.Release.Enabled = false;//Release to locked

                    IDStationControl.IsStationLocked = false;
                    IDStationControl.EnableContextMenuStrips();

                }

                #endregion

                foreach (StationButton Button in StationButtonList)
                {

                    if (Button.StationNumber == ID_STATION)//Find the button that belongs to the this station
                    {

                        //If the station is open then set the button to a released clicked state
                        if (DefectiveCar.IDStationData.IsStationClosed == 1)
                        {
                            Button.StationButtonImageType = "Released";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                            ChangeStationButtonImage(Button, "Released", true); 
                        }

                        else//The station is closed we need to either set the button to a passed clicked or a fail clicked state depending on "StationFailed"
                        {

                            if (DefectiveCar.IDStationData.StationFailed == 1)
                            {
                                Button.StationButtonImageType = "Passed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                                ChangeStationButtonImage(Button, "Passed", true);
                            }
                            else
                            {
                                Button.StationButtonImageType = "Failed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                                ChangeStationButtonImage(Button, "Failed", true);
                            }
                        }
                    }

                }

                //This loop will compare every Defect ID in a Station's ButtonList to the DefectiveCar DefectList entries(second loop) in order to determine which defects need to be pressed as we load the car
                for (int i = 0; i < IDStation.ButtonList.Count; i++)
                {

                    for (int j = 0; j < DefectiveCar.IDStationData.DefectList.Count; j++)
                    {

                        if (IDStation.ButtonList[i].BttnDefectID == DefectiveCar.IDStationData.DefectList[j].DefectID)////If the defect ID in the button object = to the defect ID from the Defective car then press it
                        {
                            //Call the function to set up a selected button. Parameters: (the button itself, permit request, Dangerous defect, selected defects(defectinfo list))
                            IDStationControl.SelectDefectButton(IDStation.ButtonList[i].DefectBttn, DefectiveCar.IDStationData.DefectList[j].PermitRequest, DefectiveCar.IDStationData.DefectList[j].DangerousDefect);

                        }

                    }
                }

                //Loop through each button in this station's buttonlist and lock all of the controls to the current image, that way the user can't change the image state of the button when it's locked

                if (DefectiveCar.IDStationData.IsStationClosed == 0)
                {
                    foreach (DefectButton Button in IDStation.ButtonList)
                    {
                        Button.DefectBttn.IsLocked = true;
                    }
                }

                else
                {

                    foreach (DefectButton Button in IDStation.ButtonList)
                    {
                        Button.DefectBttn.IsLocked = false;
                    }

                }

            }

            else//If the car's defect data is not gotten yet(a car has not been loaded and a station has been selected)
            {

                foreach (StationButton Button in StationButtonList)
                {

                    if (Button.StationNumber == ID_STATION)//Find the button that belongs to the this station
                    {
                        //This will make sure that when no car is loaded that the station buttons still act like they would(only one station will show a selected button state at a time)
                        this.SelectedStation = ID_STATION;
                        ChangeStationButtonImage(Button, "Released", true);

                    }
                }
            }
        }

        #endregion 

        #region Pit Station
        private void InitPitStation()
        {

            DrawUserControl(PitStationControl, 60, 90, 1320, 490);

            //Weird bug: when user control gets drawn this grid moves to the right for some reason, this fixes it
            DefectInfoGrid.Location = new Point(48, 89);

            if (DefectiveCar != null)//If the information about the defective car has been loaded then we can proceed to load all the info about this car on the station's UC
            {

                PitStation.ResetButtons();//Reset the buttons that switching stations doesn't cause unsigned buttons to be selected!

                SelectedStation = PIT_STATION;//Change selected station to 1(ID station)

                #region Sign - Release Set Up

                if (DefectiveCar.PitStationData.IsStationClosed == 0)
                {

                    this.Sign.Enabled = false;//Sign to locked
                    this.Release.Enabled = true;//Release to unlocked

                    PitStationControl.IsStationLocked = true;
                    PitStationControl.DisableContextMenuStrips();

                }

                else
                {

                    this.Sign.Enabled = true;//Sign to unlocked
                    this.Release.Enabled = false;//Release to locked

                    PitStationControl.IsStationLocked = false;
                    PitStationControl.EnableContextMenuStrips();

                }

                #endregion

                foreach (StationButton Button in StationButtonList)
                {

                    if (Button.StationNumber == PIT_STATION)//Find the button that belongs to the this station
                    {

                        //If the station is open then set the button to a released clicked state
                        if (DefectiveCar.PitStationData.IsStationClosed == 1)
                        {
                            Button.StationButtonImageType = "Released";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                            ChangeStationButtonImage(Button, "Released", true);
                        }

                        else//The station is closed we need to either set the button to a passed clicked or a fail clicked state depending on "StationFailed"
                        {

                            if (DefectiveCar.PitStationData.StationFailed == 1)
                            {
                                Button.StationButtonImageType = "Passed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                                ChangeStationButtonImage(Button, "Passed", true);
                            }
                            else
                            {
                                Button.StationButtonImageType = "Failed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                                ChangeStationButtonImage(Button, "Failed", true);
                            }
                        }
                    }

                }

                //This loop will compare every Defect ID in a Station's ButtonList to the DefectiveCar DefectList entries(second loop) in order to determine which defects need to be pressed as we load the car
                for (int i = 0; i < PitStation.ButtonList.Count; i++)
                {

                    for (int j = 0; j < DefectiveCar.PitStationData.DefectList.Count; j++)
                    {

                        if (PitStation.ButtonList[i].BttnDefectID == DefectiveCar.PitStationData.DefectList[j].DefectID)////If the defect ID in the button object = to the defect ID from the Defective car then press it
                        {
                            //Call the function to set up a selected button. Parameters: (the button itself, permit request, Dangerous defect, selected defects(defectinfo list))
                            PitStationControl.SelectDefectButton(PitStation.ButtonList[i].DefectBttn, DefectiveCar.PitStationData.DefectList[j].PermitRequest, DefectiveCar.PitStationData.DefectList[j].DangerousDefect);

                        }

                    }
                }

                //Loop through each button in this station's buttonlist and lock all of the controls to the current image, that way the user can't change the image state of the button when it's locked

                if (DefectiveCar.PitStationData.IsStationClosed == 0)
                {
                    foreach (DefectButton Button in PitStation.ButtonList)
                    {
                        Button.DefectBttn.IsLocked = true;
                    }
                }

                else
                {

                    foreach (DefectButton Button in PitStation.ButtonList)
                    {
                        Button.DefectBttn.IsLocked = false;
                    }

                }

            }

            else//If the car's defect data is not gotten yet(a car has not been loaded and a station has been selected)
            {

                foreach (StationButton Button in StationButtonList)
                {

                    if (Button.StationNumber == PIT_STATION)//Find the button that belongs to the this station
                    {
                        //This will make sure that when no car is loaded that the station buttons still act like they would
                        this.SelectedStation = PIT_STATION;
                        ChangeStationButtonImage(Button, "Released", true);

                    }
                }
            }
        }

        #endregion 

        #region Smoke Station
        private void InitSmokeStation()
        {

            DrawUserControl(SmokeStationControl, 60, 90, 1320, 490);

            SmokeStationControl.StartSmokeTest.Enabled = false;//Disable the option to start a smoke test if no car has been loaded yet.

           /* //Weird bug: when user control gets drawn this grid moves to the right for some reason, this fixes it
            DefectInfoGrid.Location = new Point(48, 89);
            */
            if (DefectiveCar != null)//If the information about the defective car has been loaded then we can proceed to load all the info about this car on the station's UC
            {

                SmokeStation.ResetButtons();//Reset the buttons that switching stations doesn't cause unsigned buttons to be selected!

                SelectedStation = SMOKE_STATION;

                #region Sign - Release Set Up

                if (DefectiveCar.SmokeStationData.IsStationClosed == 0)
                {

                    this.Sign.Enabled = false;//Sign to locked
                    this.Release.Enabled = true;//Release to unlocked

                    SmokeStationControl.IsStationLocked = true;
                    SmokeStationControl.StartSmokeTest.Enabled = false;//Disable the option to start a smoke test in the smoke test station if the station is locked
                    SmokeStationControl.DisableContextMenuStrips();

                }

                else
                {

                    this.Sign.Enabled = true;//Sign to unlocked
                    this.Release.Enabled = false;//Release to locked

                    SmokeStationControl.IsStationLocked = false;
                    SmokeStationControl.StartSmokeTest.Enabled = true;//Enable the option to start a smoke test in the smoke test station if the station is locked
                    SmokeStationControl.EnableContextMenuStrips();

                }

                #endregion

                foreach (StationButton Button in StationButtonList)
                {

                    if (Button.StationNumber == SMOKE_STATION)//Find the button that belongs to the this station
                    {

                        //If the station is open then set the button to a released clicked state
                        if (DefectiveCar.SmokeStationData.IsStationClosed == 1)
                        {
                            Button.StationButtonImageType = "Released";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                            ChangeStationButtonImage(Button, "Released", true);
                        }

                        else//The station is closed we need to either set the button to a passed clicked or a fail clicked state depending on "StationFailed"
                        {

                            if (DefectiveCar.SmokeStationData.StationFailed == 1)
                            {
                                Button.StationButtonImageType = "Passed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                                ChangeStationButtonImage(Button, "Passed", true);
                            }
                            else
                            {
                                Button.StationButtonImageType = "Failed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                                ChangeStationButtonImage(Button, "Failed", true);
                            }
                        }
                    }

                }

                //This loop will compare every Defect ID in a Station's ButtonList to the DefectiveCar DefectList entries(second loop) in order to determine which defects need to be pressed as we load the car
                for (int i = 0; i < SmokeStation.ButtonList.Count; i++)
                {

                    for (int j = 0; j < DefectiveCar.SmokeStationData.DefectList.Count; j++)
                    {

                        if (SmokeStation.ButtonList[i].BttnDefectID == DefectiveCar.SmokeStationData.DefectList[j].DefectID)////If the defect ID in the button object = to the defect ID from the Defective car then press it
                        {
                            //Call the function to set up a selected button. Parameters: (the button itself, permit request, Dangerous defect, selected defects(defectinfo list))
                            SmokeStationControl.SelectDefectButton(SmokeStation.ButtonList[i].DefectBttn, DefectiveCar.SmokeStationData.DefectList[j].PermitRequest, DefectiveCar.SmokeStationData.DefectList[j].DangerousDefect);

                        }

                    }
                }

                //Loop through each button in this station's buttonlist and lock all of the controls to the current image, that way the user can't change the image state of the button when it's locked

                if (DefectiveCar.SmokeStationData.IsStationClosed == 0)
                {
                    foreach (DefectButton Button in SmokeStation.ButtonList)
                    {
                        Button.DefectBttn.IsLocked = true;
                    }


                }

                else
                {

                    foreach (DefectButton Button in SmokeStation.ButtonList)
                    {
                        Button.DefectBttn.IsLocked = false;
                    }

                }

            }

            else//If the car's defect data is not gotten yet(a car has not been loaded and a station has been selected)
            {

                foreach (StationButton Button in StationButtonList)
                {

                    if (Button.StationNumber == SMOKE_STATION)//Find the button that belongs to the this station
                    {
                        //This will make sure that when no car is loaded that the station buttons still act like they would
                        this.SelectedStation = SMOKE_STATION;
                        ChangeStationButtonImage(Button, "Released", true);

                    }
                }
            }
        }

        #endregion 

        #region Lights Station
        private void InitLightsStation()
        {

            DrawUserControl(LightsStationControl, 60, 90, 1320, 490);

            LightsStationControl.StartLightsTest.Enabled = false;//Disable the option to start a smoke test if no car has been loaded yet.
            LightsStationControl.ManualLightsCheckShowHideButton.Enabled = false;
            LightsStationControl.TechnicianToolsShowHideButton.Enabled = false;

            /* //Weird bug: when user control gets drawn this grid moves to the right for some reason, this fixes it
             DefectInfoGrid.Location = new Point(48, 89);
             */
            if (DefectiveCar != null)//If the information about the defective car has been loaded then we can proceed to load all the info about this car on the station's UC
            {

                LightsStation.ResetButtons();//Reset the buttons that switching stations doesn't cause unsigned buttons to be selected!

                SelectedStation = LIGHTS_STATION;

                #region Sign - Release Set Up

                if (DefectiveCar.LightsStationData.IsStationClosed == 0)
                {

                    this.Sign.Enabled = false;//Sign to locked
                    this.Release.Enabled = true;//Release to unlocked

                    LightsStationControl.IsStationLocked = true;
                    LightsStationControl.StartLightsTest.Enabled = false;//Disable the option to start a smoke test in the smoke test station if the station is locked
                    LightsStationControl.ManualLightsCheckShowHideButton.Enabled = false;
                    LightsStationControl.TechnicianToolsShowHideButton.Enabled = false;
                    LightsStationControl.DisableContextMenuStrips();

                }

                else
                {

                    this.Sign.Enabled = true;//Sign to unlocked
                    this.Release.Enabled = false;//Release to locked

                    LightsStationControl.IsStationLocked = false;
                    LightsStationControl.StartLightsTest.Enabled = true;//Enable the option to start a smoke test in the smoke test station if the station is locked
                    LightsStationControl.ManualLightsCheckShowHideButton.Enabled = true;
                    LightsStationControl.TechnicianToolsShowHideButton.Enabled = true;
                    LightsStationControl.EnableContextMenuStrips();

                }

                #endregion

                foreach (StationButton Button in StationButtonList)
                {

                    if (Button.StationNumber == LIGHTS_STATION)//Find the button that belongs to the this station
                    {

                        //If the station is open then set the button to a released clicked state
                        if (DefectiveCar.LightsStationData.IsStationClosed == 1)
                        {
                            Button.StationButtonImageType = "Released";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                            ChangeStationButtonImage(Button, "Released", true);
                        }

                        else//The station is closed we need to either set the button to a passed clicked or a fail clicked state depending on "StationFailed"
                        {

                            if (DefectiveCar.LightsStationData.StationFailed == 1)
                            {
                                Button.StationButtonImageType = "Passed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                                ChangeStationButtonImage(Button, "Passed", true);
                            }
                            else
                            {
                                Button.StationButtonImageType = "Failed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                                ChangeStationButtonImage(Button, "Failed", true);
                            }
                        }
                    }

                }

                //This loop will compare every Defect ID in a Station's ButtonList to the DefectiveCar DefectList entries(second loop) in order to determine which defects need to be pressed as we load the car
                for (int i = 0; i < LightsStation.ButtonList.Count; i++)
                {

                    for (int j = 0; j < DefectiveCar.LightsStationData.DefectList.Count; j++)
                    {

                        if (LightsStation.ButtonList[i].BttnDefectID == DefectiveCar.LightsStationData.DefectList[j].DefectID)////If the defect ID in the button object = to the defect ID from the Defective car then press it
                        {
                            //Call the function to set up a selected button. Parameters: (the button itself, permit request, Dangerous defect, selected defects(defectinfo list))
                            LightsStationControl.SelectDefectButton(LightsStation.ButtonList[i].DefectBttn, DefectiveCar.LightsStationData.DefectList[j].PermitRequest, DefectiveCar.LightsStationData.DefectList[j].DangerousDefect);

                        }

                    }
                }

                //Loop through each button in this station's buttonlist and lock all of the controls to the current image, that way the user can't change the image state of the button when it's locked

                if (DefectiveCar.LightsStationData.IsStationClosed == 0)
                {
                    foreach (DefectButton Button in LightsStation.ButtonList)
                    {
                        Button.DefectBttn.IsLocked = true;
                    }


                }

                else
                {

                    foreach (DefectButton Button in LightsStation.ButtonList)
                    {
                        Button.DefectBttn.IsLocked = false;
                    }

                }

            }

            else//If the car's defect data is not gotten yet(a car has not been loaded and a station has been selected)
            {

                foreach (StationButton Button in StationButtonList)
                {

                    if (Button.StationNumber == LIGHTS_STATION)//Find the button that belongs to the this station
                    {
                        //This will make sure that when no car is loaded that the station buttons still act like they would
                        this.SelectedStation = LIGHTS_STATION;
                        ChangeStationButtonImage(Button, "Released", true);

                    }
                }
            }
        }

        #endregion 

   

        #endregion

        private void IDStationButton_Click(object sender, EventArgs e) { InitIDStation(); }


        private void PitStationButton_Click(object sender, EventArgs e) { InitPitStation(); }

        private void SmokeStationButton_Click(object sender, EventArgs e) { InitSmokeStation(); }

        private void LightsStationButton_Click(object sender, EventArgs e) { InitLightsStation(); }


        private void CarNumberComboBox_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {

                //this.FormsStackPanel.SelectedIndex = 1;//Show the loading screen

                //Start the background worker that loads the defect data for this form
                //this.DefectDataLoadingBackgroundWorker.RunWorkerAsync();

                LoadDefectsForCarRequestTransferItem RequestLoadDefects = new LoadDefectsForCarRequestTransferItem();//Make a requesto load the defects for that specific car number
                RequestLoadDefects.CarNumber = this.CarNumber;
                Program.SendTransferItem(RequestLoadDefects);

            }
        }


        #endregion
      
        private void CarInfo_Click(object sender, EventArgs e)
        {
            CarInfoForm.Show();
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            FormPointers.MainWindowFormPointer.Show();
            this.Close();

        }

        private void Sign_Click(object sender, EventArgs e)//When the sign button is pressed FOR ID STATION ONLY SO FAR
        {

            #region Sign Station - General Station Information

            SignStationRequestTransferItem SignStation = new SignStationRequestTransferItem();

            SignStation.CarCheckID = DefectiveCar.CarCheckID;//Get the CarCheckID from the DefectiveCar object so we know where in the table to save it(done by server side request)
            SignStation.StationSignDate = DateTime.Now.ToString();//Get the DateTime when the station was signed
            SignStation.IsStationClosed = 0;//The station is locked if it has reached this point in code

            //TesterInformation is a static object in the MainWindow form that contains all the info about the tester that signed into the program
            SignStation.TesterNameOfSignedStation = MainWindow.UserInformation.testerName;
            SignStation.TesterIDOfSignedStation = MainWindow.UserInformation.userID;
            //SignStation.TimeExpiredReleaseStation....Figure it out in abit
                
            #endregion 

            #region ID Station - Station Button Image Set-Up & Defect List Information

            //Checks which station is currently selected and signs the information appropriate to that station!

            #region ID Station Defect List Information

            if (SelectedStation == ID_STATION)
            {

                SignStation.StationFailed = 1;//Set the StationFailed to PASS because the station can't fail until the if(Button.Clickstate) runs AT LEAST once. If it doesn't run then the station has passed!
                DefectiveCar.IDStationData.StationFailed = 1;//Update this info for DefectiveCar as well!

                SignStation.StationNumber = 1;//Set the stationNumber to the selectedStation's number
                IDStationControl.IsStationLocked = true;//Lock the station!
                IDStationControl.DisableContextMenuStrips();//Disable the use of context menus when this station is locked

                #region Sign - Release Button Set Up

                DefectiveCar.IDStationData.IsStationClosed = 0;//Update station closed status to true(station is signed)

                this.Sign.Enabled = false;
                this.Release.Enabled = true;

                #endregion 
                
                foreach (DefectButton Button in IDStation.ButtonList)
                {

                    if (Button.DefectBttn.ClickState)//Search through the ButtonList to see if a button's click state = true if it is we get all of it's information and send it away to the server
                    {

                        SignStation.StationFailed = 0;//If at least 1 button in the station is selected then the station has FAILED. If this IF statement never runs that means there are no defects to select, AKA: station has passed
                        DefectiveCar.IDStationData.StationFailed = 0;//Update this info for DefectiveCar as well!

                        SignStation.DefectList.Add(new EmdaDataInJsonStationDefectInfo());//Make a new defect entry in the SignStation object

                        if (Button.SubDefectsSelected.Count == 0) { SignStation.DefectList[SignStation.DefectList.Count - 1].DefectInfo = Button.Defect[0]; }//For defects without sub defects: just add the button's defect list (which has 1 entry)

                        //For defects WITH sub defects: combine all the entries from the SubDefectsSelected list to the DefectInfo string
                        else
                        {
                            //If 1 sub defect is selected we can't use the Join function! Only in a SubDefectsSelected that has a bigger count than 1
                            if (Button.SubDefectsSelected.Count == 1) { SignStation.DefectList[SignStation.DefectList.Count - 1].DefectInfo = Button.SubDefectsSelected[0]; }

                            else if (Button.SubDefectsSelected.Count > 1) { SignStation.DefectList[SignStation.DefectList.Count - 1].DefectInfo = string.Join(string.Empty + "/", Button.SubDefectsSelected); }

                        }

                        SignStation.DefectList[SignStation.DefectList.Count - 1].DefectID = Convert.ToInt32(Button.DefectBttn.Text);
                        SignStation.DefectList[SignStation.DefectList.Count - 1].PermitRequest = Button.Permit;
                        SignStation.DefectList[SignStation.DefectList.Count - 1].RequiresPay = Button.RequiresPay;
                        SignStation.DefectList[SignStation.DefectList.Count - 1].DefectSignDateTime = DateTime.Now.ToString();
                        SignStation.DefectList[SignStation.DefectList.Count - 1].DangerousDefect = Button.DangerousDefect;
                        SignStation.DefectList[SignStation.DefectList.Count - 1].TesterNameOfSignedDefect = MainWindow.UserInformation.testerName;
                        SignStation.DefectList[SignStation.DefectList.Count - 1].TesterIDOfSignedDefect = MainWindow.UserInformation.userID;
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].Transmitted = Button.DefectBttn.; WIP
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].DefectFixed  = Button.DefectBttn; WIP
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].PermitNumber = Button.DefectBttn; WIP
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].PermitDate = Button.DefectBttn; WIP
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].FixDate = Button.DefectBttn; WIP
                        
                    }

                }


                /*
                 * When a car is loaded the defects from the DB are selected in the station. THE PROBLEM: If 1 user signs for example: 4 defects and then another user logs in and signs 5 for example
                   will cause the program to sign all of the defects on the person that recently signed the station which is incorrect because the first user signed the 4 out of 5, not the second
                 * 
                 * SOLUTION: This double For compares the defects that were loaded from the DB for this station, to the defects that have been selected after the "Sign" Button has been pressed
                 * If a button with the same ID is found in both of these lists that means that defect was signed by some other user already so the info from the SignStation's defectlist at that index
                 * is set to the old info from the DefectiveCar.station.defectlist in order to keep that info unchanged!
                */

                for (int i = 0; i < DefectiveCar.IDStationData.DefectList.Count; i++)
                {

                    for (int j = 0; j < SignStation.DefectList.Count; j++)
                    {

                        if (DefectiveCar.IDStationData.DefectList[i].DefectID == SignStation.DefectList[j].DefectID)////If the defect ID in the button object = to the defect ID from the Defective car then press it
                        {

                            //SignStation.DefectList[j].Transmitted = DefectiveCar.IDStationData.DefectList[i].Transmitted;
                            //SignStation.DefectList[j].DefectFixed = DefectiveCar.IDStationData.DefectList[i].DefectFixed;
                            //SignStation.DefectList[j].PermitRequest = DefectiveCar.IDStationData.DefectList[i].PermitRequest;
                            //SignStation.DefectList[j].PermitNumber = DefectiveCar.IDStationData.DefectList[i].PermitNumber;
                            //SignStation.DefectList[j].FixDate = DefectiveCar.IDStationData.DefectList[i].FixDate;
                            SignStation.DefectList[j].DefectInfo = DefectiveCar.IDStationData.DefectList[i].DefectInfo;
                            SignStation.DefectList[j].PermitDate = DefectiveCar.IDStationData.DefectList[i].PermitDate;
                            SignStation.DefectList[j].RequiresPay = DefectiveCar.IDStationData.DefectList[i].RequiresPay;
                            SignStation.DefectList[j].TesterIDOfSignedDefect = DefectiveCar.IDStationData.DefectList[i].TesterIDOfSignedDefect;
                            SignStation.DefectList[j].TesterNameOfSignedDefect = DefectiveCar.IDStationData.DefectList[i].TesterNameOfSignedDefect;
                            SignStation.DefectList[j].DefectSignDateTime = DefectiveCar.IDStationData.DefectList[i].DefectSignDateTime;

                        }

                    }
                }

                
                #endregion 

                #region ID Station Button Image Set Up

                foreach (StationButton Button in StationButtonList)
                {

                    if (SignStation.StationFailed == 1 && Button.StationNumber == ID_STATION)//if the station has passed and the station button that was selected by the loop is this station
                    {
                        Button.StationButtonImageType = "Passed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                        ChangeStationButtonImage(Button, "Passed", true);//Change the button to a passed, clicked sign station
                    }

                    else if (SignStation.StationFailed == 0 && Button.StationNumber == ID_STATION)
                    {
                        Button.StationButtonImageType = "Failed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                        ChangeStationButtonImage(Button, "Failed", true);//Change the button to a failed, clicked sign station
                    }
                }

                //Loop through the button list of this station and lock all the buttons so they can't be pressed
                foreach (DefectButton Button in IDStation.ButtonList) { Button.DefectBttn.IsLocked = true; }

                #endregion

                DefectiveCar.IDStationData.DefectList = new List<testhing.Table_Objects.Emdadata_Objects.EmdaDataInJsonStationDefectInfo>();

                //This loop updates the DefectiveCar object with the buttons that have been selected from this Station so that we don't have to request the updated data from the database every time a user switches stations. Instead the DefectiveCar object is updated with the selected buttons info
                for(int i = 0; i < SignStation.DefectList.Count; i++)
                {

                    DefectiveCar.IDStationData.DefectList.Add(new testhing.Table_Objects.Emdadata_Objects.EmdaDataInJsonStationDefectInfo());

                    //Update DefectiveCar Data after the station has been signed to reflect the updated defect selections
                    DefectiveCar.IDStationData.DefectList[i].DefectID = SignStation.DefectList[i].DefectID;
                    DefectiveCar.IDStationData.DefectList[i].DefectInfo = SignStation.DefectList[i].DefectInfo;
                    DefectiveCar.IDStationData.DefectList[i].Transmitted = SignStation.DefectList[i].Transmitted;
                    DefectiveCar.IDStationData.DefectList[i].DefectFixed = SignStation.DefectList[i].DefectFixed;
                    DefectiveCar.IDStationData.DefectList[i].PermitRequest = SignStation.DefectList[i].PermitRequest;
                    DefectiveCar.IDStationData.DefectList[i].PermitNumber = SignStation.DefectList[i].PermitNumber;
                    DefectiveCar.IDStationData.DefectList[i].PermitDate = SignStation.DefectList[i].PermitDate;
                    DefectiveCar.IDStationData.DefectList[i].RequiresPay = SignStation.DefectList[i].RequiresPay;
                    DefectiveCar.IDStationData.DefectList[i].FixDate = SignStation.DefectList[i].FixDate;
                    DefectiveCar.IDStationData.DefectList[i].TesterIDOfSignedDefect = SignStation.DefectList[i].TesterIDOfSignedDefect;
                    DefectiveCar.IDStationData.DefectList[i].TesterNameOfSignedDefect = SignStation.DefectList[i].TesterNameOfSignedDefect;
                    DefectiveCar.IDStationData.DefectList[i].DefectSignDateTime = SignStation.DefectList[i].DefectSignDateTime;
                    DefectiveCar.IDStationData.DefectList[i].DangerousDefect = SignStation.DefectList[i].DangerousDefect;

                }

            }

            #endregion 

            #region Pit Station - Station Button Image Set-Up & Defect List Information

            //Checks which station is currently selected and signs the information appropriate to that station!

            #region Pit Station Defect List Information

            if (SelectedStation == PIT_STATION)
            {

                SignStation.StationFailed = 1;//Set the StationFailed to PASS because the station can't fail until the if(Button.Clickstate) runs AT LEAST once. If it doesn't run then the station has passed!
                DefectiveCar.PitStationData.StationFailed = 1;//Update this info for DefectiveCar as well!

                SignStation.StationNumber = PIT_STATION;//Set the stationNumber to the selectedStation's number
                PitStationControl.IsStationLocked = true;//Lock the station!
                PitStationControl.DisableContextMenuStrips();//Disable the use of context menus when this station is locked

                #region Sign - Release Button Set Up

                DefectiveCar.PitStationData.IsStationClosed = 0;//Update station closed status to true(station is signed)

                this.Sign.Enabled = false;
                this.Release.Enabled = true;

                #endregion

                foreach (DefectButton Button in PitStation.ButtonList)
                {

                    if (Button.DefectBttn.ClickState)//Search through the ButtonList to see if a button's click state = true if it is we get all of it's information and send it away to the server
                    {

                        SignStation.StationFailed = 0;//If at least 1 button in the station is selected then the station has FAILED. If this IF statement never runs that means there are no defects to select, AKA: station has passed
                        DefectiveCar.PitStationData.StationFailed = 0;//Update this info for DefectiveCar as well!

                        SignStation.DefectList.Add(new EmdaDataInJsonStationDefectInfo());//Make a new defect entry in the SignStation object

                        if (Button.SubDefectsSelected.Count == 0) { SignStation.DefectList[SignStation.DefectList.Count - 1].DefectInfo = Button.Defect[0]; }//For defects without sub defects: just add the button's defect list (which has 1 entry)

                        //For defects WITH sub defects: combine all the entries from the SubDefectsSelected list to the DefectInfo string
                        else
                        {
                            //If 1 sub defect is selected we can't use the Join function! Only in a SubDefectsSelected that has a bigger count than 1
                            if (Button.SubDefectsSelected.Count == 1) { SignStation.DefectList[SignStation.DefectList.Count - 1].DefectInfo = Button.SubDefectsSelected[0]; }

                            else if (Button.SubDefectsSelected.Count > 1) { SignStation.DefectList[SignStation.DefectList.Count - 1].DefectInfo = string.Join(string.Empty + "/", Button.SubDefectsSelected); }

                        }

                        SignStation.DefectList[SignStation.DefectList.Count - 1].DefectID = Convert.ToInt32(Button.DefectBttn.Text);
                        SignStation.DefectList[SignStation.DefectList.Count - 1].PermitRequest = Button.Permit;
                        SignStation.DefectList[SignStation.DefectList.Count - 1].RequiresPay = Button.RequiresPay;
                        SignStation.DefectList[SignStation.DefectList.Count - 1].DefectSignDateTime = DateTime.Now.ToString();
                        SignStation.DefectList[SignStation.DefectList.Count - 1].DangerousDefect = Button.DangerousDefect;
                        SignStation.DefectList[SignStation.DefectList.Count - 1].TesterNameOfSignedDefect = MainWindow.UserInformation.testerName;
                        SignStation.DefectList[SignStation.DefectList.Count - 1].TesterIDOfSignedDefect = MainWindow.UserInformation.userID;
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].Transmitted = Button.DefectBttn.; WIP
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].DefectFixed  = Button.DefectBttn; WIP
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].PermitNumber = Button.DefectBttn; WIP
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].PermitDate = Button.DefectBttn; WIP
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].FixDate = Button.DefectBttn; WIP

                    }

                }


                /*
                 * When a car is loaded the defects from the DB are selected in the station. THE PROBLEM: If 1 user signs for example: 4 defects and then another user logs in and signs 5 for example
                   will cause the program to sign all of the defects on the person that recently signed the station which is incorrect because the first user signed the 4 out of 5, not the second
                 * 
                 * SOLUTION: This double For compares the defects that were loaded from the DB for this station, to the defects that have been selected after the "Sign" Button has been pressed
                 * If a button with the same ID is found in both of these lists that means that defect was signed by some other user already so the info from the SignStation's defectlist at that index
                 * is set to the old info from the DefectiveCar.station.defectlist in order to keep that info unchanged!
                */

                for (int i = 0; i < DefectiveCar.PitStationData.DefectList.Count; i++)
                {

                    for (int j = 0; j < SignStation.DefectList.Count; j++)
                    {

                        if (DefectiveCar.PitStationData.DefectList[i].DefectID == SignStation.DefectList[j].DefectID)////If the defect ID in the button object = to the defect ID from the Defective car then press it
                        {

                            //SignStation.DefectList[j].Transmitted = DefectiveCar.PitStationData.DefectList[i].Transmitted;
                            //SignStation.DefectList[j].DefectFixed = DefectiveCar.PitStationData.DefectList[i].DefectFixed;
                            //SignStation.DefectList[j].PermitRequest = DefectiveCar.PitStationData.DefectList[i].PermitRequest;
                            //SignStation.DefectList[j].PermitNumber = DefectiveCar.PitStationData.DefectList[i].PermitNumber;
                            //SignStation.DefectList[j].FixDate = DefectiveCar.PitStationData.DefectList[i].FixDate;
                            SignStation.DefectList[j].DefectInfo = DefectiveCar.PitStationData.DefectList[i].DefectInfo;
                            SignStation.DefectList[j].PermitDate = DefectiveCar.PitStationData.DefectList[i].PermitDate;
                            SignStation.DefectList[j].RequiresPay = DefectiveCar.PitStationData.DefectList[i].RequiresPay;
                            SignStation.DefectList[j].TesterIDOfSignedDefect = DefectiveCar.PitStationData.DefectList[i].TesterIDOfSignedDefect;
                            SignStation.DefectList[j].TesterNameOfSignedDefect = DefectiveCar.PitStationData.DefectList[i].TesterNameOfSignedDefect;
                            SignStation.DefectList[j].DefectSignDateTime = DefectiveCar.PitStationData.DefectList[i].DefectSignDateTime;

                        }

                    }
                }


            #endregion

                #region Pit Station Button Image Set Up

                foreach (StationButton Button in StationButtonList)
                {

                    if (SignStation.StationFailed == 1 && Button.StationNumber == PIT_STATION)//if the station has passed and the station button that was selected by the loop is this station
                    {
                        Button.StationButtonImageType = "Passed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                        ChangeStationButtonImage(Button, "Passed", true);//Change the button to a passed, clicked sign station
                    }

                    else if (SignStation.StationFailed == 0 && Button.StationNumber == PIT_STATION)
                    {
                        Button.StationButtonImageType = "Failed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                        ChangeStationButtonImage(Button, "Failed", true);//Change the button to a failed, clicked sign station
                    }
                }

                //Loop through the button list of this station and lock all the buttons so they can't be pressed
                foreach (DefectButton Button in PitStation.ButtonList) { Button.DefectBttn.IsLocked = true; }

                #endregion

                DefectiveCar.PitStationData.DefectList = new List<testhing.Table_Objects.Emdadata_Objects.EmdaDataInJsonStationDefectInfo>();

                //This loop updates the DefectiveCar object with the buttons that have been selected from this Station so that we don't have to request the updated data from the database every time a user switches stations. Instead the DefectiveCar object is updated with the selected buttons info
                for (int i = 0; i < SignStation.DefectList.Count; i++)
                {

                    DefectiveCar.PitStationData.DefectList.Add(new testhing.Table_Objects.Emdadata_Objects.EmdaDataInJsonStationDefectInfo());

                    //Update DefectiveCar Data after the station has been signed to reflect the updated defect selections
                    DefectiveCar.PitStationData.DefectList[i].DefectID = SignStation.DefectList[i].DefectID;
                    DefectiveCar.PitStationData.DefectList[i].DefectInfo = SignStation.DefectList[i].DefectInfo;
                    DefectiveCar.PitStationData.DefectList[i].Transmitted = SignStation.DefectList[i].Transmitted;
                    DefectiveCar.PitStationData.DefectList[i].DefectFixed = SignStation.DefectList[i].DefectFixed;
                    DefectiveCar.PitStationData.DefectList[i].PermitRequest = SignStation.DefectList[i].PermitRequest;
                    DefectiveCar.PitStationData.DefectList[i].PermitNumber = SignStation.DefectList[i].PermitNumber;
                    DefectiveCar.PitStationData.DefectList[i].PermitDate = SignStation.DefectList[i].PermitDate;
                    DefectiveCar.PitStationData.DefectList[i].RequiresPay = SignStation.DefectList[i].RequiresPay;
                    DefectiveCar.PitStationData.DefectList[i].FixDate = SignStation.DefectList[i].FixDate;
                    DefectiveCar.PitStationData.DefectList[i].TesterIDOfSignedDefect = SignStation.DefectList[i].TesterIDOfSignedDefect;
                    DefectiveCar.PitStationData.DefectList[i].TesterNameOfSignedDefect = SignStation.DefectList[i].TesterNameOfSignedDefect;
                    DefectiveCar.PitStationData.DefectList[i].DefectSignDateTime = SignStation.DefectList[i].DefectSignDateTime;
                    DefectiveCar.PitStationData.DefectList[i].DangerousDefect = SignStation.DefectList[i].DangerousDefect;

                }

            }

            #endregion 

            #region Smoke Station - Station Button Image Set-Up & Defect List Information

            //Checks which station is currently selected and signs the information appropriate to that station!

            #region Smoke Station Defect List Information

            if (SelectedStation == SMOKE_STATION)
            {

                SignStation.StationFailed = 1;//Set the StationFailed to PASS because the station can't fail until the if(Button.Clickstate) runs AT LEAST once. If it doesn't run then the station has passed!
                DefectiveCar.SmokeStationData.StationFailed = 1;//Update this info for DefectiveCar as well!

                SignStation.StationNumber = SMOKE_STATION;//Set the stationNumber to the selectedStation's number
                SmokeStationControl.IsStationLocked = true;//Lock the station!
                SmokeStationControl.DisableContextMenuStrips();//Disable the use of context menus when this station is locked

                #region Sign - Release Button Set Up
                DefectiveCar.SmokeStationData.IsStationClosed = 0;//Update station closed status to true(station is signed)

                this.Sign.Enabled = false;
                this.Release.Enabled = true;

                #endregion

                SmokeStationControl.StartSmokeTest.Enabled = false;//DISABLED THE OPTION TO STRAT A SMOKE TEST WHEN THE STATION IS SIGNED

                foreach (DefectButton Button in SmokeStation.ButtonList)
                {

                    if (Button.DefectBttn.ClickState)//Search through the ButtonList to see if a button's click state = true if it is we get all of it's information and send it away to the server
                    {

                        SignStation.StationFailed = 0;//If at least 1 button in the station is selected then the station has FAILED. If this IF statement never runs that means there are no defects to select, AKA: station has passed
                        DefectiveCar.SmokeStationData.StationFailed = 0;//Update this info for DefectiveCar as well!

                        SignStation.DefectList.Add(new EmdaDataInJsonStationDefectInfo());//Make a new defect entry in the SignStation object

                        if (Button.SubDefectsSelected.Count == 0) { SignStation.DefectList[SignStation.DefectList.Count - 1].DefectInfo = Button.Defect[0]; }//For defects without sub defects: just add the button's defect list (which has 1 entry)

                        //For defects WITH sub defects: combine all the entries from the SubDefectsSelected list to the DefectInfo string
                        else
                        {
                            //If 1 sub defect is selected we can't use the Join function! Only in a SubDefectsSelected that has a bigger count than 1
                            if (Button.SubDefectsSelected.Count == 1) { SignStation.DefectList[SignStation.DefectList.Count - 1].DefectInfo = Button.SubDefectsSelected[0]; }

                            else if (Button.SubDefectsSelected.Count > 1) { SignStation.DefectList[SignStation.DefectList.Count - 1].DefectInfo = string.Join(string.Empty + "/", Button.SubDefectsSelected); }

                        }

                        SignStation.DefectList[SignStation.DefectList.Count - 1].DefectID = Convert.ToInt32(Button.DefectBttn.Text);
                        SignStation.DefectList[SignStation.DefectList.Count - 1].PermitRequest = Button.Permit;
                        SignStation.DefectList[SignStation.DefectList.Count - 1].RequiresPay = Button.RequiresPay;
                        SignStation.DefectList[SignStation.DefectList.Count - 1].DefectSignDateTime = DateTime.Now.ToString();
                        SignStation.DefectList[SignStation.DefectList.Count - 1].DangerousDefect = Button.DangerousDefect;
                        SignStation.DefectList[SignStation.DefectList.Count - 1].TesterNameOfSignedDefect = MainWindow.UserInformation.testerName;
                        SignStation.DefectList[SignStation.DefectList.Count - 1].TesterIDOfSignedDefect = MainWindow.UserInformation.userID;
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].Transmitted = Button.DefectBttn.; WIP
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].DefectFixed  = Button.DefectBttn; WIP
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].PermitNumber = Button.DefectBttn; WIP
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].PermitDate = Button.DefectBttn; WIP
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].FixDate = Button.DefectBttn; WIP

                    }

                }


                /*
                 * When a car is loaded the defects from the DB are selected in the station. THE PROBLEM: If 1 user signs for example: 4 defects and then another user logs in and signs 5 for example
                   will cause the program to sign all of the defects on the person that recently signed the station which is incorrect because the first user signed the 4 out of 5, not the second
                 * 
                 * SOLUTION: This double For compares the defects that were loaded from the DB for this station, to the defects that have been selected after the "Sign" Button has been pressed
                 * If a button with the same ID is found in both of these lists that means that defect was signed by some other user already so the info from the SignStation's defectlist at that index
                 * is set to the old info from the DefectiveCar.station.defectlist in order to keep that info unchanged!
                */

                for (int i = 0; i < DefectiveCar.SmokeStationData.DefectList.Count; i++)
                {

                    for (int j = 0; j < SignStation.DefectList.Count; j++)
                    {

                        if (DefectiveCar.SmokeStationData.DefectList[i].DefectID == SignStation.DefectList[j].DefectID)////If the defect ID in the button object = to the defect ID from the Defective car then press it
                        {

                            //SignStation.DefectList[j].Transmitted = DefectiveCar.SmokeStationData.DefectList[i].Transmitted;
                            //SignStation.DefectList[j].DefectFixed = DefectiveCar.SmokeStationData.DefectList[i].DefectFixed;
                            //SignStation.DefectList[j].PermitRequest = DefectiveCar.SmokeStationData.DefectList[i].PermitRequest;
                            //SignStation.DefectList[j].PermitNumber = DefectiveCar.SmokeStationData.DefectList[i].PermitNumber;
                            //SignStation.DefectList[j].FixDate = DefectiveCar.SmokeStationData.DefectList[i].FixDate;
                            SignStation.DefectList[j].DefectInfo = DefectiveCar.SmokeStationData.DefectList[i].DefectInfo;
                            SignStation.DefectList[j].PermitDate = DefectiveCar.SmokeStationData.DefectList[i].PermitDate;
                            SignStation.DefectList[j].RequiresPay = DefectiveCar.SmokeStationData.DefectList[i].RequiresPay;
                            SignStation.DefectList[j].TesterIDOfSignedDefect = DefectiveCar.SmokeStationData.DefectList[i].TesterIDOfSignedDefect;
                            SignStation.DefectList[j].TesterNameOfSignedDefect = DefectiveCar.SmokeStationData.DefectList[i].TesterNameOfSignedDefect;
                            SignStation.DefectList[j].DefectSignDateTime = DefectiveCar.SmokeStationData.DefectList[i].DefectSignDateTime;

                        }

                    }
                }


            #endregion

                #region Smoke Station Button Image Set Up

                foreach (StationButton Button in StationButtonList)
                {

                    if (SignStation.StationFailed == 1 && Button.StationNumber == SMOKE_STATION)//if the station has passed and the station button that was selected by the loop is this station
                    {
                        Button.StationButtonImageType = "Passed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                        ChangeStationButtonImage(Button, "Passed", true);//Change the button to a passed, clicked sign station
                    }

                    else if (SignStation.StationFailed == 0 && Button.StationNumber == SMOKE_STATION)
                    {
                        Button.StationButtonImageType = "Failed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                        ChangeStationButtonImage(Button, "Failed", true);//Change the button to a failed, clicked sign station
                    }
                }

                //Loop through the button list of this station and lock all the buttons so they can't be pressed
                foreach (DefectButton Button in SmokeStation.ButtonList) { Button.DefectBttn.IsLocked = true; }

                #endregion

                DefectiveCar.SmokeStationData.DefectList = new List<testhing.Table_Objects.Emdadata_Objects.EmdaDataInJsonStationDefectInfo>();

                //This loop updates the DefectiveCar object with the buttons that have been selected from this Station so that we don't have to request the updated data from the database every time a user switches stations. Instead the DefectiveCar object is updated with the selected buttons info
                for (int i = 0; i < SignStation.DefectList.Count; i++)
                {

                    DefectiveCar.SmokeStationData.DefectList.Add(new testhing.Table_Objects.Emdadata_Objects.EmdaDataInJsonStationDefectInfo());

                    //Update DefectiveCar Data after the station has been signed to reflect the updated defect selections
                    DefectiveCar.SmokeStationData.DefectList[i].DefectID = SignStation.DefectList[i].DefectID;
                    DefectiveCar.SmokeStationData.DefectList[i].DefectInfo = SignStation.DefectList[i].DefectInfo;
                    DefectiveCar.SmokeStationData.DefectList[i].Transmitted = SignStation.DefectList[i].Transmitted;
                    DefectiveCar.SmokeStationData.DefectList[i].DefectFixed = SignStation.DefectList[i].DefectFixed;
                    DefectiveCar.SmokeStationData.DefectList[i].PermitRequest = SignStation.DefectList[i].PermitRequest;
                    DefectiveCar.SmokeStationData.DefectList[i].PermitNumber = SignStation.DefectList[i].PermitNumber;
                    DefectiveCar.SmokeStationData.DefectList[i].PermitDate = SignStation.DefectList[i].PermitDate;
                    DefectiveCar.SmokeStationData.DefectList[i].RequiresPay = SignStation.DefectList[i].RequiresPay;
                    DefectiveCar.SmokeStationData.DefectList[i].FixDate = SignStation.DefectList[i].FixDate;
                    DefectiveCar.SmokeStationData.DefectList[i].TesterIDOfSignedDefect = SignStation.DefectList[i].TesterIDOfSignedDefect;
                    DefectiveCar.SmokeStationData.DefectList[i].TesterNameOfSignedDefect = SignStation.DefectList[i].TesterNameOfSignedDefect;
                    DefectiveCar.SmokeStationData.DefectList[i].DefectSignDateTime = SignStation.DefectList[i].DefectSignDateTime;
                    DefectiveCar.SmokeStationData.DefectList[i].DangerousDefect = SignStation.DefectList[i].DangerousDefect;

                }

            }

            #endregion 

            #region Lights Station - Station Button Image Set-Up & Defect List Information

            //Checks which station is currently selected and signs the information appropriate to that station!

            #region Lights Station Defect List Information

            if (SelectedStation == LIGHTS_STATION)
            {

                SignStation.StationFailed = 1;//Set the StationFailed to PASS because the station can't fail until the if(Button.Clickstate) runs AT LEAST once. If it doesn't run then the station has passed!
                DefectiveCar.LightsStationData.StationFailed = 1;//Update this info for DefectiveCar as well!

                SignStation.StationNumber = LIGHTS_STATION;//Set the stationNumber to the selectedStation's number
                LightsStationControl.IsStationLocked = true;//Lock the station!
                LightsStationControl.DisableContextMenuStrips();//Disable the use of context menus when this station is locked

                #region Sign - Release Button Set Up
                DefectiveCar.LightsStationData.IsStationClosed = 0;//Update station closed status to true(station is signed)

                this.Sign.Enabled = false;
                this.Release.Enabled = true;

                #endregion

                //DISABLE AN ONGOING LIGHTTEST WHEN SIGNING THE STATION!
                LightsStationControl.StopLightsTest();

                //DISABLED THE OPTION TO STRAT A LIGHT TEST WHEN THE STATION IS SIGNED AS WELL AS ANY USER INTERACTION WITH THIS CONTROL
                LightsStationControl.StartLightsTest.Enabled = false;
                LightsStationControl.ManualLightsCheckShowHideButton.Enabled = false;
                LightsStationControl.TechnicianToolsShowHideButton.Enabled = false;

                foreach (DefectButton Button in LightsStation.ButtonList)
                {

                    if (Button.DefectBttn.ClickState)//Search through the ButtonList to see if a button's click state = true if it is we get all of it's information and send it away to the server
                    {

                        SignStation.StationFailed = 0;//If at least 1 button in the station is selected then the station has FAILED. If this IF statement never runs that means there are no defects to select, AKA: station has passed
                        DefectiveCar.LightsStationData.StationFailed = 0;//Update this info for DefectiveCar as well!

                        SignStation.DefectList.Add(new EmdaDataInJsonStationDefectInfo());//Make a new defect entry in the SignStation object

                        if (Button.SubDefectsSelected.Count == 0) { SignStation.DefectList[SignStation.DefectList.Count - 1].DefectInfo = Button.Defect[0]; }//For defects without sub defects: just add the button's defect list (which has 1 entry)

                        //For defects WITH sub defects: combine all the entries from the SubDefectsSelected list to the DefectInfo string
                        else
                        {
                            //If 1 sub defect is selected we can't use the Join function! Only in a SubDefectsSelected that has a bigger count than 1
                            if (Button.SubDefectsSelected.Count == 1) { SignStation.DefectList[SignStation.DefectList.Count - 1].DefectInfo = Button.SubDefectsSelected[0]; }

                            else if (Button.SubDefectsSelected.Count > 1) { SignStation.DefectList[SignStation.DefectList.Count - 1].DefectInfo = string.Join(string.Empty + "/", Button.SubDefectsSelected); }

                        }

                        SignStation.DefectList[SignStation.DefectList.Count - 1].DefectID = Convert.ToInt32(Button.DefectBttn.Text);
                        SignStation.DefectList[SignStation.DefectList.Count - 1].PermitRequest = Button.Permit;
                        SignStation.DefectList[SignStation.DefectList.Count - 1].RequiresPay = Button.RequiresPay;
                        SignStation.DefectList[SignStation.DefectList.Count - 1].DefectSignDateTime = DateTime.Now.ToString();
                        SignStation.DefectList[SignStation.DefectList.Count - 1].DangerousDefect = Button.DangerousDefect;
                        SignStation.DefectList[SignStation.DefectList.Count - 1].TesterNameOfSignedDefect = MainWindow.UserInformation.testerName;
                        SignStation.DefectList[SignStation.DefectList.Count - 1].TesterIDOfSignedDefect = MainWindow.UserInformation.userID;
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].Transmitted = Button.DefectBttn.; WIP
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].DefectFixed  = Button.DefectBttn; WIP
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].PermitNumber = Button.DefectBttn; WIP
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].PermitDate = Button.DefectBttn; WIP
                        //SignStation.DefectList[SignStation.DefectList.Count - 1].FixDate = Button.DefectBttn; WIP

                    }

                }


                /*
                 * When a car is loaded the defects from the DB are selected in the station. THE PROBLEM: If 1 user signs for example: 4 defects and then another user logs in and signs 5 for example
                   will cause the program to sign all of the defects on the person that recently signed the station which is incorrect because the first user signed the 4 out of 5, not the second
                 * 
                 * SOLUTION: This double For compares the defects that were loaded from the DB for this station, to the defects that have been selected after the "Sign" Button has been pressed
                 * If a button with the same ID is found in both of these lists that means that defect was signed by some other user already so the info from the SignStation's defectlist at that index
                 * is set to the old info from the DefectiveCar.station.defectlist in order to keep that info unchanged!
                */

                for (int i = 0; i < DefectiveCar.LightsStationData.DefectList.Count; i++)
                {

                    for (int j = 0; j < SignStation.DefectList.Count; j++)
                    {

                        if (DefectiveCar.LightsStationData.DefectList[i].DefectID == SignStation.DefectList[j].DefectID)////If the defect ID in the button object = to the defect ID from the Defective car then press it
                        {

                            //SignStation.DefectList[j].Transmitted = DefectiveCar.SmokeStationData.DefectList[i].Transmitted;
                            //SignStation.DefectList[j].DefectFixed = DefectiveCar.SmokeStationData.DefectList[i].DefectFixed;
                            //SignStation.DefectList[j].PermitRequest = DefectiveCar.SmokeStationData.DefectList[i].PermitRequest;
                            //SignStation.DefectList[j].PermitNumber = DefectiveCar.SmokeStationData.DefectList[i].PermitNumber;
                            //SignStation.DefectList[j].FixDate = DefectiveCar.SmokeStationData.DefectList[i].FixDate;
                            SignStation.DefectList[j].DefectInfo = DefectiveCar.LightsStationData.DefectList[i].DefectInfo;
                            SignStation.DefectList[j].PermitDate = DefectiveCar.LightsStationData.DefectList[i].PermitDate;
                            SignStation.DefectList[j].RequiresPay = DefectiveCar.LightsStationData.DefectList[i].RequiresPay;
                            SignStation.DefectList[j].TesterIDOfSignedDefect = DefectiveCar.LightsStationData.DefectList[i].TesterIDOfSignedDefect;
                            SignStation.DefectList[j].TesterNameOfSignedDefect = DefectiveCar.LightsStationData.DefectList[i].TesterNameOfSignedDefect;
                            SignStation.DefectList[j].DefectSignDateTime = DefectiveCar.LightsStationData.DefectList[i].DefectSignDateTime;

                        }

                    }
                }


            #endregion

                #region Lights Station Button Image Set Up

                foreach (StationButton Button in StationButtonList)
                {

                    if (SignStation.StationFailed == 1 && Button.StationNumber == LIGHTS_STATION)//if the station has passed and the station button that was selected by the loop is this station
                    {
                        Button.StationButtonImageType = "Passed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                        ChangeStationButtonImage(Button, "Passed", true);//Change the button to a passed, clicked sign station
                    }

                    else if (SignStation.StationFailed == 0 && Button.StationNumber == LIGHTS_STATION)
                    {
                        Button.StationButtonImageType = "Failed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                        ChangeStationButtonImage(Button, "Failed", true);//Change the button to a failed, clicked sign station
                    }
                }

                //Loop through the button list of this station and lock all the buttons so they can't be pressed
                foreach (DefectButton Button in LightsStation.ButtonList) { Button.DefectBttn.IsLocked = true; }

                #endregion

                DefectiveCar.LightsStationData.DefectList = new List<testhing.Table_Objects.Emdadata_Objects.EmdaDataInJsonStationDefectInfo>();

                //This loop updates the DefectiveCar object with the buttons that have been selected from this Station so that we don't have to request the updated data from the database every time a user switches stations. Instead the DefectiveCar object is updated with the selected buttons info
                for (int i = 0; i < SignStation.DefectList.Count; i++)
                {

                    DefectiveCar.LightsStationData.DefectList.Add(new testhing.Table_Objects.Emdadata_Objects.EmdaDataInJsonStationDefectInfo());

                    //Update DefectiveCar Data after the station has been signed to reflect the updated defect selections
                    DefectiveCar.LightsStationData.DefectList[i].DefectID = SignStation.DefectList[i].DefectID;
                    DefectiveCar.LightsStationData.DefectList[i].DefectInfo = SignStation.DefectList[i].DefectInfo;
                    DefectiveCar.LightsStationData.DefectList[i].Transmitted = SignStation.DefectList[i].Transmitted;
                    DefectiveCar.LightsStationData.DefectList[i].DefectFixed = SignStation.DefectList[i].DefectFixed;
                    DefectiveCar.LightsStationData.DefectList[i].PermitRequest = SignStation.DefectList[i].PermitRequest;
                    DefectiveCar.LightsStationData.DefectList[i].PermitNumber = SignStation.DefectList[i].PermitNumber;
                    DefectiveCar.LightsStationData.DefectList[i].PermitDate = SignStation.DefectList[i].PermitDate;
                    DefectiveCar.LightsStationData.DefectList[i].RequiresPay = SignStation.DefectList[i].RequiresPay;
                    DefectiveCar.LightsStationData.DefectList[i].FixDate = SignStation.DefectList[i].FixDate;
                    DefectiveCar.LightsStationData.DefectList[i].TesterIDOfSignedDefect = SignStation.DefectList[i].TesterIDOfSignedDefect;
                    DefectiveCar.LightsStationData.DefectList[i].TesterNameOfSignedDefect = SignStation.DefectList[i].TesterNameOfSignedDefect;
                    DefectiveCar.LightsStationData.DefectList[i].DefectSignDateTime = SignStation.DefectList[i].DefectSignDateTime;
                    DefectiveCar.LightsStationData.DefectList[i].DangerousDefect = SignStation.DefectList[i].DangerousDefect;

                }

            }

            #endregion 

            Program.SendTransferItem(SignStation);//Send the item to the server

           }

        private void Release_Click(object sender, EventArgs e)
        {
            //This item takes the station number we would like to release
            ReleaseStationRequestTransferItem ReleaseStation = new ReleaseStationRequestTransferItem();

            #region ID

            if (SelectedStation == ID_STATION)
            {

                #region Sign & Release Button Management

                this.Sign.Enabled = true;//Unlock Sign button 
                this.Release.Enabled = false;//Lock the release button after releasing

                DefectiveCar.IDStationData.IsStationClosed = 1;//Set the station to open after clicking release

                IDStationControl.IsStationLocked = false;//Unlock the IDStation
                IDStationControl.EnableContextMenuStrips();//Enable the use of context menus when station is unlocked

                ReleaseStation.StationToRelease = ID_STATION;//Tell the server to release this station

                #endregion

                #region Station Button Management

                //Change the button and stationlock according to which station is selected
                foreach (StationButton Button in StationButtonList)
                {
                    //If the button's station number = 1(id station) AND the selected station = 1(id station) that means that the id station is selected and we have found the button that represents the id station

                    if (Button.StationNumber == ID_STATION && SelectedStation == ID_STATION)//ID
                    {

                        Button.StationButtonImageType = "Released";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                        ChangeStationButtonImage(Button, "Released", true);//Change it to released, clicked

                    }
                }

                //Loop through the button list of this station and unlock all the buttons so they can be pressed
                foreach (DefectButton Button in IDStation.ButtonList)
                {

                    Button.DefectBttn.IsLocked = false;

                }

                #endregion

            }

            #endregion

            #region Pit

            if (SelectedStation == PIT_STATION)
            {

                #region Sign & Release Button Management

                this.Sign.Enabled = true;//Unlock Sign button 
                this.Release.Enabled = false;//Lock the release button after releasing

                DefectiveCar.PitStationData.IsStationClosed = 1;//Set the station to open after clicking release

                PitStationControl.IsStationLocked = false;//Unlock the IDStation
                PitStationControl.EnableContextMenuStrips();//Enable the use of context menus when station is unlocked

                ReleaseStation.StationToRelease = PIT_STATION;//Tell the server to release this station

                #endregion

                #region Station Button Management

                //Change the button and stationlock according to which station is selected
                foreach (StationButton Button in StationButtonList)
                {

                    if (Button.StationNumber == PIT_STATION && SelectedStation == PIT_STATION)
                    {

                        Button.StationButtonImageType = "Released";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                        ChangeStationButtonImage(Button, "Released", true);//Change it to released, clicked

                    }
                }

                //Loop through the button list of this station and unlock all the buttons so they can be pressed
                foreach (DefectButton Button in PitStation.ButtonList)
                {

                    Button.DefectBttn.IsLocked = false;

                }

                #endregion

            }

            #endregion

            #region Smoke

            if (SelectedStation == SMOKE_STATION)
            {

                #region Sign & Release Button Management

                this.Sign.Enabled = true;//Unlock Sign button 
                this.Release.Enabled = false;//Lock the release button after releasing

                DefectiveCar.SmokeStationData.IsStationClosed = 1;//Set the station to open after clicking release

                SmokeStationControl.IsStationLocked = false;//Unlock the IDStation
                SmokeStationControl.EnableContextMenuStrips();//Enable the use of context menus when station is unlocked

                ReleaseStation.StationToRelease = SMOKE_STATION;//Tell the server to release this station

                #endregion

                #region Station Button Management

                SmokeStationControl.StartSmokeTest.Enabled = true;//ENABLE THE OPTION TO STRAT A SMOKE TEST WHEN THE STATION IS SIGNED

                //Change the button and stationlock according to which station is selected
                foreach (StationButton Button in StationButtonList)
                {

                    if (Button.StationNumber == SMOKE_STATION && SelectedStation == SMOKE_STATION)
                    {

                        Button.StationButtonImageType = "Released";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                        ChangeStationButtonImage(Button, "Released", true);//Change it to released, clicked

                    }
                }

                //Loop through the button list of this station and unlock all the buttons so they can be pressed
                foreach (DefectButton Button in SmokeStation.ButtonList)
                {

                    Button.DefectBttn.IsLocked = false;

                }

                #endregion

            }

            ReleaseStation.CarCheckID = DefectiveCar.CarCheckID;//To know which car to release
            Program.SendTransferItem(ReleaseStation);
        
            #endregion

            #region Lights

            if (SelectedStation == LIGHTS_STATION)
            {

                #region Sign & Release Button Management

                this.Sign.Enabled = true;//Unlock Sign button 
                this.Release.Enabled = false;//Lock the release button after releasing

                DefectiveCar.LightsStationData.IsStationClosed = 1;//Set the station to open after clicking release

                LightsStationControl.IsStationLocked = false;//Unlock the IDStation
                LightsStationControl.EnableContextMenuStrips();//Enable the use of context menus when station is unlocked

                ReleaseStation.StationToRelease = LIGHTS_STATION;//Tell the server to release this station

                #endregion

                #region Station Button Management

                LightsStationControl.StartLightsTest.Enabled = true;//ENABLE THE OPTION TO STRAT A LIGHTS TEST WHEN THE STATION IS SIGNED
                LightsStationControl.ManualLightsCheckShowHideButton.Enabled = true;
                LightsStationControl.TechnicianToolsShowHideButton.Enabled = true;

                //Change the button and stationlock according to which station is selected
                foreach (StationButton Button in StationButtonList)
                {

                    if (Button.StationNumber == LIGHTS_STATION && SelectedStation == LIGHTS_STATION)
                    {

                        Button.StationButtonImageType = "Released";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                        ChangeStationButtonImage(Button, "Released", true);//Change it to released, clicked

                    }
                }

                //Loop through the button list of this station and unlock all the buttons so they can be pressed
                foreach (DefectButton Button in LightsStation.ButtonList)
                {

                    Button.DefectBttn.IsLocked = false;

                }

                #endregion

            }

            ReleaseStation.CarCheckID = DefectiveCar.CarCheckID;//To know which car to release
            Program.SendTransferItem(ReleaseStation);

            #endregion

        }
            
            

        #endregion 

        #region TransferItemHandlers

        #region Load Defect Info Table Data

        [HandleTypeDef(typeof(LoadDefectInfoResultTransferItem))]
        private void HandleLoadDefectInfoResultTransferItem(TransferItem tItem)
        {
      
            LoadDefectInfoResultTransferItem LoadedDefectsList = (LoadDefectInfoResultTransferItem)tItem;//This object contains the data about EVERY defect
            DefectsList = LoadedDefectsList.Defects;//For use outside of this function

        }

        #endregion

        #region Load Car Numbers With Defects Handler

        //Threading
        private delegate void CarNumbersWithDefectsDelegate(TransferItem tItem);

        [HandleTypeDef(typeof(LoadCarNumbersWithDefectsResultTransferItem))]
        private void HandleLoadCarNumbersWithDefectsResultTransferItem(TransferItem tItem)
        {

            if (this.InvokeRequired)//Threading
            {

                CarNumbersWithDefectsDelegate CNWDDelegate = new CarNumbersWithDefectsDelegate(HandleLoadCarNumbersWithDefectsResultTransferItem);//Threading
                BeginInvoke(CNWDDelegate, tItem);//Threading

            }

            else
            {

                LoadCarNumbersWithDefectsResultTransferItem DefectiveCarsFound = (LoadCarNumbersWithDefectsResultTransferItem)tItem;

                for (int i = 0; i < DefectiveCarsFound.CarManuFacturers.Count; i++)
                {

                    //Check if the symbol list has not reached it's end. If it has not then just add the name + image from the imagelist
                    if (i <= ManufacturerSymbolList.Count - 1) { CarmanufacturerList.Add(new Carmanufacturer(DefectiveCarsFound.CarManuFacturers[i], ManufacturerSymbolList[i])); }

                    //If it has then just add the name and a specific symbol
                    else { CarmanufacturerList.Add(new Carmanufacturer(DefectiveCarsFound.CarManuFacturers[i], ManufacturerSymbolList[1])); }

                }

                Image SymbolForGrid = ManufacturerSymbolList[ManufacturerSymbolList.Count - 1];//Image to set for the DefectGrid's image cells. If the image of this manufacturer can't be found it will be set to a default image(the value here will stay if the if statement that assigns a manu image to the cell won't execute once)

                foreach (LoadCarNumbersWithDefectsResultTransferItem Car in DefectiveCarsFound.DefectiveCarInfo)//for each defective car that is found in the list of defective cars located in our DefectiveCarsFound object
                {

                    CarNumberTextBox.Items.Add(Car.DefectiveCarNumber);//Add the car number of the defective car to the CarNumber box so we can select it to find out more info in the CarInfo form

                    for (int i = 0; i < CarmanufacturerList.Count; i++)//Loop through the car manufacturer list
                    {

                        if (CarmanufacturerList[i].ManufacturerID == Car.CarManufacturerID)//Check if the manufacturer from the manufacturer list's id = the id of the defective car's car manufacturer
                        {

                            SymbolForGrid = CarmanufacturerList[i].ManufacturerImage;//If it is then set that person's carmanufacturer cell in the gridview to the image of the appropriate ar manufacturer

                            i = CarmanufacturerList.Count;//exit loop
                        }

                    }

                    DefectInfoGrid.Rows.Add(SymbolForGrid, Car.LastCheckTime, Car.DefectiveCarNumber, Car.CarType, Car.FuelType, Car.AttemptNumber);

                }

                //Show the Test Course Form and hide the Loading Screen
                this.FormsStackPanel.SelectedIndex = 0;
            }

        }

        #endregion 

        #region Load Defects for Car Number

        //Threading
        private delegate void DefectsForCarDelegate(TransferItem tItem);

        [HandleTypeDef(typeof(LoadDefectsForCarResultTransferItem))]
        private void HandleLoadDefectsForCarResultTransferItem(TransferItem tItem)
        {

            /*if(this.InvokeRequired)//Threading
            {

                DefectsForCarDelegate DFCDelegate = new DefectsForCarDelegate(HandleLoadDefectsForCarResultTransferItem);//Threading
                BeginInvoke(DFCDelegate, tItem);//Threading

            }*/

            LoadDefectsForCarResultTransferItem DefectiveCar = (LoadDefectsForCarResultTransferItem)tItem;
            this.DefectiveCar = DefectiveCar;

            //Set the info about the defective car in the CarInfo form
            CarInfoForm.CarInfoSetData(DefectiveCar.CarNumber, DefectiveCar.CarType, DefectiveCar.FuelType, DefectiveCar.TestType, DefectiveCar.CarYear, DefectiveCar.CarOwnerName, DefectiveCar.NumberOfAttempts);
            //This sets up the station's images initially to know if they should be released or passed or failed.
            #region Station Button Image set-up
            
            foreach (StationButton StationBttn in StationButtonList)//Loop through the station button list and find the station we want based on it's number
            {

                #region ID Station

                if (StationBttn.StationNumber == ID_STATION)//ID Station
                {

                    this.SelectedStation = ID_STATION;//Set the selected station to this one

                    //If this station is still open then change the icon to released clicked
                    if (DefectiveCar.IDStationData.IsStationClosed == 1) 
                    {

                        StationBttn.StationButtonImageType = "Released";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                        ChangeStationButtonImage(StationBttn, "Released", true);

                    }

                    else//The station is closed; decide between fail and pass clicked icons
                    {

                        //If the station has passed set the station button icon correctly
                        if (DefectiveCar.IDStationData.StationFailed == 0)
                        {
                            StationBttn.StationButtonImageType = "Failed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                            ChangeStationButtonImage(StationBttn, "Failed", true);//CALL THIS FUNCTION TO SET ALL OTHER STATION BUTTONS TO UNCLICKED STATE AND SET THIS ONE TO A CLICKED STATE
                        }

                        else
                        {
                            StationBttn.StationButtonImageType = "Passed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                            ChangeStationButtonImage(StationBttn, "Passed", true);//CALL THIS FUNCTION TO SET ALL OTHER STATION BUTTONS TO UNCLICKED STATE AND SET THIS ONE TO A CLICKED STATE
                        }

                    }

                }

                #endregion 

                #region Pit Station

                if (StationBttn.StationNumber == PIT_STATION)//Pit Station
                {

                    //If this station is still open then change the icon to released clicked
                    if (DefectiveCar.PitStationData.IsStationClosed == 1)
                    {
                        StationBttn.StationButtonImageType = "Released";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                        ChangeStationButtonImage(StationBttn, "Released", true);
                    }

                    else
                    {

                        //If the station has failed set the station button icon correctly. NOT USING THE "CHANGESTATIONBUTTONIMAGE" FUNCTION
                        if (DefectiveCar.PitStationData.StationFailed == 0)
                        {
                            //  StationBttn.StationButtonImageType = "Failed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                            //ChangeStationButtonImage(StationBttn, "Failed", true);//CALL THIS FUNCTION TO SET ALL OTHER STATION BUTTONS TO UNCLICKED STATE AND SET THIS ONE TO A CLICKED STATE
                            StationBttn.StationButtonImageType = "Failed";
                            ChangeStationButtonImage(StationBttn, "Failed", false);//CALL THIS FUNCTION TO SET ALL OTHER STATION BUTTONS TO UNCLICKED STATE AND SET THIS ONE TO A CLICKED STATE

                        }

                        else
                        {

                            StationBttn.StationButtonImageType = "Passed";
                            ChangeStationButtonImage(StationBttn, "Passed", false);//CALL THIS FUNCTION TO SET ALL OTHER STATION BUTTONS TO UNCLICKED STATE AND SET THIS ONE TO A CLICKED STATE

                        }

                    }
                }

                #endregion 

                #region Smoke Station

                if (StationBttn.StationNumber == SMOKE_STATION)//Smoke Station
                {

                    //If this station is still open then change the icon to released clicked
                    if (DefectiveCar.SmokeStationData.IsStationClosed == 1)
                    {
                        StationBttn.StationButtonImageType = "Released";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                        ChangeStationButtonImage(StationBttn, "Released", true);
                    }

                    else
                    {

                        //If the station has failed set the station button icon correctly. NOT USING THE "CHANGESTATIONBUTTONIMAGE" FUNCTION
                        if (DefectiveCar.SmokeStationData.StationFailed == 0)
                        {
                            //  StationBttn.StationButtonImageType = "Failed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                            //ChangeStationButtonImage(StationBttn, "Failed", true);//CALL THIS FUNCTION TO SET ALL OTHER STATION BUTTONS TO UNCLICKED STATE AND SET THIS ONE TO A CLICKED STATE
                            StationBttn.StationButtonImageType = "Failed";
                            ChangeStationButtonImage(StationBttn, "Failed", false);//CALL THIS FUNCTION TO SET ALL OTHER STATION BUTTONS TO UNCLICKED STATE AND SET THIS ONE TO A CLICKED STATE

                        }

                        else
                        {

                            StationBttn.StationButtonImageType = "Passed";
                            ChangeStationButtonImage(StationBttn, "Passed", false);//CALL THIS FUNCTION TO SET ALL OTHER STATION BUTTONS TO UNCLICKED STATE AND SET THIS ONE TO A CLICKED STATE

                        }

                    }
                }

                #endregion 

                #region Lights Station

                if (StationBttn.StationNumber == LIGHTS_STATION)
                {

                    //If this station is still open then change the icon to released clicked
                    if (DefectiveCar.LightsStationData.IsStationClosed == 1)
                    {
                        StationBttn.StationButtonImageType = "Released";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                        ChangeStationButtonImage(StationBttn, "Released", true);
                    }

                    else
                    {

                        //If the station has failed set the station button icon correctly. NOT USING THE "CHANGESTATIONBUTTONIMAGE" FUNCTION
                        if (DefectiveCar.LightsStationData.StationFailed == 0)
                        {
                            //  StationBttn.StationButtonImageType = "Failed";//Very important!!! Set the image set string before calling the functions to change the image of this button!
                            //ChangeStationButtonImage(StationBttn, "Failed", true);//CALL THIS FUNCTION TO SET ALL OTHER STATION BUTTONS TO UNCLICKED STATE AND SET THIS ONE TO A CLICKED STATE
                            StationBttn.StationButtonImageType = "Failed";
                            ChangeStationButtonImage(StationBttn, "Failed", false);//CALL THIS FUNCTION TO SET ALL OTHER STATION BUTTONS TO UNCLICKED STATE AND SET THIS ONE TO A CLICKED STATE

                        }

                        else
                        {

                            StationBttn.StationButtonImageType = "Passed";
                            ChangeStationButtonImage(StationBttn, "Passed", false);//CALL THIS FUNCTION TO SET ALL OTHER STATION BUTTONS TO UNCLICKED STATE AND SET THIS ONE TO A CLICKED STATE

                        }

                    }
                }

                #endregion 

            }
            
            #endregion
            
            //IDStation.ResetButtons();//Reset all the buttons of the ID stations to their initial state. This is done so when a new vehicle is loaded we don't end up stacking the selected buttons of the new vehicle with the old one
            InitIDStation();//A loaded car ALWAYS starts at the ID Station

        }



        #endregion 

        #region Sign Station Result Handler

        [HandleTypeDef(typeof(SignStationResultTransferItem))]
        private void HandleSignStationResultTransferItem(TransferItem tItem)
        {

            SignStationResultTransferItem SignStationResponse = (SignStationResultTransferItem)tItem;

            MessageBox.Show(SignStationResponse.ResponseMessage);

        }

        #endregion


        #endregion

        #region Extra Functions

        private delegate void DrawUserControlDelegate(UserControl ControlToDraw, int LocationX, int LocationY, int SizeX, int SizeY);//Threading

        public void DrawUserControl(UserControl ControlToDraw, int LocationX, int LocationY, int SizeX, int SizeY)//A function to draw a user control at a specific location and at a specific size
        {
            /*
            if (this.InvokeRequired)//Threading
            {

                DrawUserControlDelegate DUCDelegate = new DrawUserControlDelegate(DrawUserControl);//Threading
                BeginInvoke(DUCDelegate, ControlToDraw, LocationX, LocationY, SizeX, SizeY);//Threading

            }
            */
            //else
            //{

                if (!(this.Controls.Contains(ControlToDraw)))//Before drawing the control we check to see if this control exists or not. If it doesnt then draw it if it does then just show it again.
                {

                    ControlToDraw.Location = new System.Drawing.Point(LocationX, LocationY);
                    ControlToDraw.Size = new System.Drawing.Size(SizeX, SizeY);
                    this.FormsStackPanel.TabPages["TestCourseForm"].Controls.Add(ControlToDraw);

                    foreach (UserControl UserControlsToHide in StationControlList)//Disable the visible station's user controls so we can draw the new one 
                    {

                        UserControlsToHide.Hide();//Hide all user controls in this list

                    }

                    ControlToDraw.Show();//Display the right control

                }

                else//If this control has been drawn before then just display it again(memory convserve)
                {

                    if (ControlToDraw.Visible) { }//If this control is visible right(which means it's the present control at the moment) then do nothing because there is no need to redraw it

                    else//A different control is being requested to be drawn
                    {
                        foreach (UserControl UserControlsToHide in StationControlList)//Disable the visible station's user controls so we can draw the new one 
                        {

                            UserControlsToHide.Hide();//Hide all user controls in this list

                        }

                        ControlToDraw.Show();//Display the right control

                    }
                }

           // }
             
        }



        public void SetDefectDescriptionText(string Text){ DefectDescription.Text = Text; }
        public string GetDefectDescriptionText() { return DefectDescription.Text; }

        #endregion 

        #region Button Graphics Related Functions

        //Function that will set a button to it's appropriate image state
        public void ChangeStationButtonImage(StationButton ButtonToChange, string StationButtonImageSet, Boolean AsClickedImage)
        {
            /*Parameter info:
             
             * StationButton - for getting the station button and it's current image set
             * StationButtonImageSet - for setting the button to this requested image set
             * AsClickedImage - Is this button going to be the clicked button of the station or not
             */

              foreach(StationButton Button in StationButtonList)//Loop through the station button list
              {

                  if (ButtonToChange.StationButtonInForm.Equals(Button.StationButtonInForm) && AsClickedImage)//If the looped through button is the one that was passed in the function then set it's clickstate to true and it's image appropriately
                  {

                      //Set the image via this function
                      SetButtonImageSet(ButtonToChange.StationButtonInForm, ButtonToChange.StationButtonImageType);

                      //Set it to a true clickstate
                      Button.StationButtonInForm.ClickState = true;

                  }

                  else//If this is a different button(not the one that is going to be selected)
                  {

                      //Set the image via this function
                      SetButtonImageSet(ButtonToChange.StationButtonInForm, ButtonToChange.StationButtonImageType);

                      //Set it to a false clickstate
                      Button.StationButtonInForm.ClickState = false;

                  }

              }

         
        }

        public void SetButtonImageSet(AdvancedButton ButtonToChange, string ImageSet)//Takes the station button and a string representing the set of the button images
        {

            //The button that was passed to this function will have it's image set changed according to what "ImageSet" was passed

            #region Station Button Image Sets

            if (ImageSet.Equals("Passed"))
             {

                 ButtonToChange.IdleImage = STATION_BUTTON_PASSED_IDLE;
                 ButtonToChange.ClickedImage = STATION_BUTTON_PASSED_CLICKED;
                 ButtonToChange.HoverImage = STATION_BUTTON_PASSED_HOVER;
                 ButtonToChange.ClickedHoverImage = STATION_BUTTON_PASSED_HOVER_CLICKED;

             }

             if (ImageSet.Equals("Failed"))
             {

                 ButtonToChange.IdleImage = STATION_BUTTON_FAILED_IDLE;
                 ButtonToChange.ClickedImage = STATION_BUTTON_FAILED_CLICKED;
                 ButtonToChange.HoverImage = STATION_BUTTON_FAILED_HOVER;
                 ButtonToChange.ClickedHoverImage = STATION_BUTTON_FAILED_HOVER_CLICKED;
                
             }

             if (ImageSet.Equals("Released"))
             {

                 ButtonToChange.IdleImage = STATION_BUTTON_RELEASED_IDLE;
                 ButtonToChange.ClickedImage = STATION_BUTTON_RELEASED_CLICKED;
                 ButtonToChange.HoverImage = STATION_BUTTON_RELEASED_HOVER;
                 ButtonToChange.ClickedHoverImage = STATION_BUTTON_RELEASED_HOVER_CLICKED;

             }

            #endregion 

            #region DefectGridShowHide Button Image Sets

             if (ImageSet.Equals("Left"))
             {

                 ButtonToChange.IdleImage = DEFECTGRIDSHOWHIDE_LEFT_IDLE;
                 ButtonToChange.ClickedImage = DEFECTGRIDSHOWHIDE_LEFT_CLICKED;
                 ButtonToChange.HoverImage = DEFECTGRIDSHOWHIDE_LEFT_HOVER;
                 ButtonToChange.ClickedHoverImage = DEFECTGRIDSHOWHIDE_LEFT_HOVER_CLICKED;

             }

             if (ImageSet.Equals("Right"))
             {

                 ButtonToChange.IdleImage = DEFECTGRIDSHOWHIDE_RIGHT_IDLE; ;
                 ButtonToChange.ClickedImage = DEFECTGRIDSHOWHIDE_RIGHT_CLICKED;
                 ButtonToChange.HoverImage = DEFECTGRIDSHOWHIDE_RIGHT_HOVER;
                 ButtonToChange.ClickedHoverImage = DEFECTGRIDSHOWHIDE_RIGHT_HOVER_CLICKED;

             }
            #endregion 

        }

        #endregion

        #region Defect Grid Table Related

        private void DefectGridShowHide_Click(object sender, EventArgs e)
        {

            if(DefectGridShowHide.ClickState)
            {


                SetButtonImageSet(DefectGridShowHide, "Left");

                //Animate the defect grid 
                Animator.ShowSync(DefectInfoGrid);

                DefectGridShowHide.Size = new Size(36, 501);
                DefectGridShowHide.Location = new Point(4, 90);

            }

            else
            {

                //Set the defectgrid's image set
                SetButtonImageSet(DefectGridShowHide, "Right");

                DefectInfoGrid.Location = new Point(48, 89);

                Animator.HideSync(DefectInfoGrid);

            }
        }

        private void DefectInfoGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex == -1) { }

            else
            {

                int IndexToSelect;

                //Searches for the car number value that was selected from the table, inside of the CarNumber box. Once that is found the index is returned so that the CarNumber box reflects the selection of the number from the table. This is needed because if someone arranges the table differently then the index of the item won't stay the same
                IndexToSelect = CarNumberTextBox.Items.IndexOf(DefectInfoGrid[2, e.RowIndex].Value);

                CarNumberTextBox.SelectedIndex = IndexToSelect;//Set the selected item in the CarNumber box to reflect the chosen item from the DefectInfoGrid

                //Send a KeyDown (with enter key pressed) to simulate pressing the enter key on the CarNumberComboBox, which sends a request to the server for getting defect info for the requested car number
                KeyEventArgs KeyEvent = new KeyEventArgs(Keys.Enter);
                CarNumberComboBox_KeyDown(this, KeyEvent);
                

                DefectGridShowHide.PerformClick();

            }

        }


        private void InitialFormLoadingBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            
            //Backgroundworker for loading: RequestSharedData, RequestDefectInfo, RequestDefectCarNumbers

            SharedDataRequestTransferItem RequestSharedData = new SharedDataRequestTransferItem();//Make shared data load request to server(this data is for CarInfo)
            LoadDefectInfoRequestTransferItem RequestDefectInfo = new LoadDefectInfoRequestTransferItem();//Load all the defects from the DefectInfo table so we can use them in the form
            LoadCarNumbersWithDefectsRequestTransferItem RequestDefectCarNumbers = new LoadCarNumbersWithDefectsRequestTransferItem();//Make a request to load all car numbers of cars with defects
            Program.SendTransferItem(RequestSharedData);
            Program.SendTransferItem(RequestDefectCarNumbers);
            Program.SendTransferItem(RequestDefectInfo);

            //Init the station controls(all initial data for this form that was requested above has loaded)
            IDStationControl = new User_Controls.Test_Course.IDStation(DefectsList, this);//Pass the DefectList and a reference to this classes form to IDStation so it can 1. Bind the defects to the buttons and send a defectList back and 2. Manipulate the DefectDescription.Text from this form
            PitStationControl = new RishuiClient.Forms.TestCourse_Forms.Stations.Pit_Station.PitStation(DefectsList, this);
            SmokeStationControl = new RishuiClient.Forms.TestCourse_Forms.Stations.Smoke_Station.SmokeStation(DefectsList, this);
            LightsStationControl = new LightsStation(DefectsList, this);

            //Add station controls to list
            StationControlList.Add(IDStationControl);
            StationControlList.Add(PitStationControl);
            StationControlList.Add(SmokeStationControl);
            StationControlList.Add(LightsStationControl);

        }

        private void DefectDataLoadingBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            /*
            //Backgroundworker for loading defect data. This is alot of data so threading this is essential!

            LoadDefectsForCarRequestTransferItem RequestLoadDefects = new LoadDefectsForCarRequestTransferItem();//Make a requesto load the defects for that specific car number
            //RequestLoadDefects.CarNumber = Convert.ToInt32(CarNumber.Text);
            RequestLoadDefects.CarNumber = this.CarNumber;
            Program.SendTransferItem(RequestLoadDefects);
            */
        }

        private void CarNumberTextBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.CarNumber = Convert.ToInt32(this.CarNumberTextBox.SelectedItem.ToString());//Set the car number local variable to the one selected to in the CarNumberComboBox
        }

        private void TestCourse_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Stop any possible ongoing check with a machine. disables machine communication to avoid bugs
            LightsStationControl.StopLightsTest();
        }

    }

        #endregion 


    #region StationButton Type

    public class StationButton//A type that will make it easy to keep track of which station button is pressed, it holds the station's number, the button, it's clickstate and the curren't button's background image set as a string
        //The imagetype string will be able to take 3 values: Passed, Failed and Released
    {

        public AdvancedButton StationButtonInForm { get; set; }
        public string StationButtonImageType { get; set; }
        public int StationNumber { get; set; }

        public StationButton(AdvancedButton StationButtonInForm, int StationNumber, string StationButtonImageType)
        {

            this.StationButtonInForm = StationButtonInForm;
            this.StationNumber = StationNumber;
            this.StationButtonImageType = StationButtonImageType;

        }

    }
        #endregion 

    #region Car Manufacturer Type
    //A type that holds a car manufacturer ID and image(used for the DefectGrid car manufacturer symbols)
    public class Carmanufacturer
    {

        public int ManufacturerID { get; set; }
        public Image ManufacturerImage { get; set; }


        public Carmanufacturer(int ManufacturerID, Image ManufacturerImage)
        {

            this.ManufacturerID = ManufacturerID;
            this.ManufacturerImage = ManufacturerImage;

        }
    }

    #endregion 

}
