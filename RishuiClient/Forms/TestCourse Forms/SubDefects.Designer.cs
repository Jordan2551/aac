﻿namespace RishuiClient.Forms.TestCourse_Forms
{
    partial class SubDefects
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OKButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.Remark = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SubDefect2 = new System.Windows.Forms.CheckBox();
            this.SubDefect1 = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SubDefect3 = new System.Windows.Forms.CheckBox();
            this.SubDefect6 = new System.Windows.Forms.CheckBox();
            this.SubDefect5 = new System.Windows.Forms.CheckBox();
            this.SubDefect7 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.SubDefect8 = new System.Windows.Forms.CheckBox();
            this.SubDefect4 = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // OKButton
            // 
            this.OKButton.BackColor = System.Drawing.Color.Transparent;
            this.OKButton.BackgroundImage = global::RishuiClient.Properties.Resources.OK;
            this.OKButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.OKButton.FlatAppearance.BorderSize = 0;
            this.OKButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.OKButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.OKButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OKButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OKButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.OKButton.Location = new System.Drawing.Point(705, 444);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(129, 112);
            this.OKButton.TabIndex = 13;
            this.OKButton.UseVisualStyleBackColor = false;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::RishuiClient.Properties.Resources.notepad_vectoricon;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(12, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(288, 434);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImage = global::RishuiClient.Properties.Resources.BackButton;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(570, 444);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 112);
            this.button1.TabIndex = 14;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Remark
            // 
            this.Remark.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Remark.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Remark.Location = new System.Drawing.Point(11, 28);
            this.Remark.Name = "Remark";
            this.Remark.ReadOnly = true;
            this.Remark.Size = new System.Drawing.Size(751, 159);
            this.Remark.TabIndex = 0;
            this.Remark.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(624, -7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 31);
            this.label1.TabIndex = 2;
            this.label1.Text = "פירוט הליקוי";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Remark);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(306, 251);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox1.Size = new System.Drawing.Size(771, 193);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // SubDefect2
            // 
            this.SubDefect2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SubDefect2.AutoSize = true;
            this.SubDefect2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubDefect2.ForeColor = System.Drawing.Color.DarkOrchid;
            this.SubDefect2.Location = new System.Drawing.Point(508, 92);
            this.SubDefect2.Name = "SubDefect2";
            this.SubDefect2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SubDefect2.Size = new System.Drawing.Size(254, 33);
            this.SubDefect2.TabIndex = 5;
            this.SubDefect2.Text = "ליקוי יותר ארוך מהקודם";
            this.SubDefect2.UseVisualStyleBackColor = true;
            // 
            // SubDefect1
            // 
            this.SubDefect1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SubDefect1.AutoSize = true;
            this.SubDefect1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubDefect1.ForeColor = System.Drawing.Color.DarkOrchid;
            this.SubDefect1.Location = new System.Drawing.Point(586, 37);
            this.SubDefect1.Name = "SubDefect1";
            this.SubDefect1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SubDefect1.Size = new System.Drawing.Size(176, 33);
            this.SubDefect1.TabIndex = 4;
            this.SubDefect1.Text = "ליקוי קצת ארוך";
            this.SubDefect1.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(493, -8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(299, 31);
            this.label2.TabIndex = 3;
            this.label2.Text = "בחירת סעיפי משנה לליקוי";
            // 
            // SubDefect3
            // 
            this.SubDefect3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SubDefect3.AutoSize = true;
            this.SubDefect3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubDefect3.ForeColor = System.Drawing.Color.DarkOrchid;
            this.SubDefect3.Location = new System.Drawing.Point(471, 146);
            this.SubDefect3.Name = "SubDefect3";
            this.SubDefect3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SubDefect3.Size = new System.Drawing.Size(291, 33);
            this.SubDefect3.TabIndex = 6;
            this.SubDefect3.Text = "ליקוי מאוד מאוד מאוד  ארוך";
            this.SubDefect3.UseVisualStyleBackColor = true;
            // 
            // SubDefect6
            // 
            this.SubDefect6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SubDefect6.AutoSize = true;
            this.SubDefect6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubDefect6.ForeColor = System.Drawing.Color.DarkOrchid;
            this.SubDefect6.Location = new System.Drawing.Point(115, 92);
            this.SubDefect6.Name = "SubDefect6";
            this.SubDefect6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SubDefect6.Size = new System.Drawing.Size(254, 33);
            this.SubDefect6.TabIndex = 8;
            this.SubDefect6.Text = "ליקוי יותר ארוך מהקודם";
            this.SubDefect6.UseVisualStyleBackColor = true;
            // 
            // SubDefect5
            // 
            this.SubDefect5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SubDefect5.AutoSize = true;
            this.SubDefect5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubDefect5.ForeColor = System.Drawing.Color.DarkOrchid;
            this.SubDefect5.Location = new System.Drawing.Point(193, 37);
            this.SubDefect5.Name = "SubDefect5";
            this.SubDefect5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SubDefect5.Size = new System.Drawing.Size(176, 33);
            this.SubDefect5.TabIndex = 7;
            this.SubDefect5.Text = "ליקוי קצת ארוך";
            this.SubDefect5.UseVisualStyleBackColor = true;
            // 
            // SubDefect7
            // 
            this.SubDefect7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SubDefect7.AutoSize = true;
            this.SubDefect7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubDefect7.ForeColor = System.Drawing.Color.DarkOrchid;
            this.SubDefect7.Location = new System.Drawing.Point(78, 146);
            this.SubDefect7.Name = "SubDefect7";
            this.SubDefect7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SubDefect7.Size = new System.Drawing.Size(291, 33);
            this.SubDefect7.TabIndex = 9;
            this.SubDefect7.Text = "ליקוי מאוד מאוד מאוד  ארוך";
            this.SubDefect7.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.SubDefect8);
            this.groupBox2.Controls.Add(this.SubDefect4);
            this.groupBox2.Controls.Add(this.SubDefect7);
            this.groupBox2.Controls.Add(this.SubDefect5);
            this.groupBox2.Controls.Add(this.SubDefect6);
            this.groupBox2.Controls.Add(this.SubDefect3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.SubDefect1);
            this.groupBox2.Controls.Add(this.SubDefect2);
            this.groupBox2.Location = new System.Drawing.Point(306, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(771, 241);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            // 
            // SubDefect8
            // 
            this.SubDefect8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SubDefect8.AutoSize = true;
            this.SubDefect8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubDefect8.ForeColor = System.Drawing.Color.DarkOrchid;
            this.SubDefect8.Location = new System.Drawing.Point(34, 199);
            this.SubDefect8.Name = "SubDefect8";
            this.SubDefect8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SubDefect8.Size = new System.Drawing.Size(335, 33);
            this.SubDefect8.TabIndex = 11;
            this.SubDefect8.Text = "ליקוי מאוד  מאוד מאוד הכי  ארוך";
            this.SubDefect8.UseVisualStyleBackColor = true;
            // 
            // SubDefect4
            // 
            this.SubDefect4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SubDefect4.AutoSize = true;
            this.SubDefect4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubDefect4.ForeColor = System.Drawing.Color.DarkOrchid;
            this.SubDefect4.Location = new System.Drawing.Point(433, 199);
            this.SubDefect4.Name = "SubDefect4";
            this.SubDefect4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SubDefect4.Size = new System.Drawing.Size(329, 33);
            this.SubDefect4.TabIndex = 10;
            this.SubDefect4.Text = "ליקוי מאוד מאוד מאוד הכי  ארוך";
            this.SubDefect4.UseVisualStyleBackColor = true;
            // 
            // SubDefects
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1089, 560);
            this.ControlBox = false;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximumSize = new System.Drawing.Size(1105, 598);
            this.MinimumSize = new System.Drawing.Size(1105, 598);
            this.Name = "SubDefects";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "בחירת ליקוי";
            this.Load += new System.EventHandler(this.SubDefects_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox Remark;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox SubDefect2;
        private System.Windows.Forms.CheckBox SubDefect1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox SubDefect3;
        private System.Windows.Forms.CheckBox SubDefect6;
        private System.Windows.Forms.CheckBox SubDefect5;
        private System.Windows.Forms.CheckBox SubDefect7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox SubDefect8;
        private System.Windows.Forms.CheckBox SubDefect4;
    }
}