﻿namespace VBTCSCodeConversion
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.signIn = new System.Windows.Forms.Button();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.IP = new System.Windows.Forms.TextBox();
            this.PW = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // signIn
            // 
            this.signIn.Location = new System.Drawing.Point(168, 185);
            this.signIn.Name = "signIn";
            this.signIn.Size = new System.Drawing.Size(124, 62);
            this.signIn.TabIndex = 15;
            this.signIn.Text = "Log-In";
            this.signIn.UseVisualStyleBackColor = true;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label2.Location = new System.Drawing.Point(85, 141);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(61, 13);
            this.Label2.TabIndex = 14;
            this.Label2.Text = "Tag(WIP)";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label3.Location = new System.Drawing.Point(73, 103);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(73, 15);
            this.Label3.TabIndex = 13;
            this.Label3.Text = "Password:";
            // 
            // IP
            // 
            this.IP.Location = new System.Drawing.Point(168, 141);
            this.IP.Name = "IP";
            this.IP.Size = new System.Drawing.Size(124, 20);
            this.IP.TabIndex = 12;
            // 
            // PW
            // 
            this.PW.Location = new System.Drawing.Point(168, 102);
            this.PW.Name = "PW";
            this.PW.Size = new System.Drawing.Size(124, 20);
            this.PW.TabIndex = 11;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label1.Location = new System.Drawing.Point(123, 23);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(169, 25);
            this.Label1.TabIndex = 10;
            this.Label1.Text = "Institute Log-In";
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(364, 271);
            this.Controls.Add(this.signIn);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.IP);
            this.Controls.Add(this.PW);
            this.Controls.Add(this.Label1);
            this.Name = "Login";
            this.Text = "Login";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button signIn;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox IP;
        internal System.Windows.Forms.TextBox PW;
        internal System.Windows.Forms.Label Label1;
    }
}