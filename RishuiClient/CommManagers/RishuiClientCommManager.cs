﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommInf.Managers;
using RishuiCommDefines.Managers;
using CommInf.Handlers;
using CommInf.Items;
using System.Collections.ObjectModel;
using CommInf.Items.SessionItems;
using RishuiCommDefines.TransferItems.ErrorItems;
using System.Windows.Forms;

namespace RishuiClient.CommManagers
{
    class RishuiClientCommManager : VoidCommManager
    {
        public int SessionID;
        public bool IsConnected { get; private set; }

        public RishuiClientCommManager(): base()
        {
            SessionID = 0;
            IsConnected = false;
        }

        protected override void MapHandlers()
        {
            MapHandler(HandleSessionAcceptTransferItem);
            MapHandler(HandleErrorTransferItem);
        }

        [HandleTypeDef(typeof(SessionAcceptTransferItem))]
        private void HandleSessionAcceptTransferItem(TransferItem tItem)
        {
            IsConnected = true;
            SessionID = (tItem as SessionAcceptTransferItem).SessionID;
        }

        [HandleTypeDef(typeof(ErrorTransferItem))]
        private void HandleErrorTransferItem(TransferItem tItem)
        {
            MessageBox.Show((tItem as ErrorTransferItem).ErrorDescription);
        }
    }
}
