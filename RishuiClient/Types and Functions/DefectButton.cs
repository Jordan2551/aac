﻿using CommInf.Items;
using RishuiClient.GUIUserControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace RishuiClient.Types
{
    public class DefectButton : SessionTransferItem//This type allows defect to button binding
    {


        public DefectButton(AdvancedButton DefectBttn, Boolean ClickState, int BttnDefectID)
        {

            this.DefectBttn = DefectBttn;
            DefectBttn.ClickState = ClickState;
            this.BttnDefectID = BttnDefectID;

        }

        public AdvancedButton DefectBttn { get; set; }
        public int BttnDefectID { get; set; }//Because buttons are private, we are unable to get their.Text property to know which defect ID is referenced. So we just set this property to that
        public string Remark { get; set; }
        public List<string> Defect { get; set; }//Contains the list of defects for the button. 1 entry = defect. more than 1 = subdefects (NOT PART OF THE CONSTRUCTOR, THIS IS SET MANUALLY)
        public List<string> SubDefectsSelected { get; set; }//This list only has entries if the defect has sub defects. It holds all the sub defects that were ticked from the checkboxes in the SubDefects form
        public int StationNumber { get; set; }
        public int Permit { get; set; }
        public int RequiresPay { get; set; }
        public int DangerousDefect { get; set; }

    }
}
