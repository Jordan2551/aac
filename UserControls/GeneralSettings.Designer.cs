﻿namespace UserControls
{
    partial class GeneralSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sway = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.lockTime = new System.Windows.Forms.NumericUpDown();
            this.numOfConnectedScreens = new System.Windows.Forms.NumericUpDown();
            this.licenseNum = new System.Windows.Forms.NumericUpDown();
            this.saveButton = new System.Windows.Forms.Button();
            this.mandatorySignStationSign = new System.Windows.Forms.CheckBox();
            this.passwordBetweenStands = new System.Windows.Forms.CheckBox();
            this.printReportEndOfStation = new System.Windows.Forms.CheckBox();
            this.lockStand = new System.Windows.Forms.CheckBox();
            this.Label11 = new System.Windows.Forms.Label();
            this.Label10 = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.instituteName = new System.Windows.Forms.TextBox();
            this.instituteAddress = new System.Windows.Forms.TextBox();
            this.enableMultipleScreens = new System.Windows.Forms.CheckBox();
            this.printReportUponEndOfInspection = new System.Windows.Forms.CheckBox();
            this.workStation = new System.Windows.Forms.ComboBox();
            this.courseNum = new System.Windows.Forms.ComboBox();
            this.region = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.sway)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lockTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOfConnectedScreens)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.licenseNum)).BeginInit();
            this.SuspendLayout();
            // 
            // sway
            // 
            this.sway.Location = new System.Drawing.Point(163, 302);
            this.sway.Name = "sway";
            this.sway.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.sway.Size = new System.Drawing.Size(82, 20);
            this.sway.TabIndex = 178;
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.Location = new System.Drawing.Point(176, 362);
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericUpDown4.Size = new System.Drawing.Size(128, 20);
            this.numericUpDown4.TabIndex = 177;
            // 
            // lockTime
            // 
            this.lockTime.Location = new System.Drawing.Point(273, 198);
            this.lockTime.Name = "lockTime";
            this.lockTime.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lockTime.Size = new System.Drawing.Size(100, 20);
            this.lockTime.TabIndex = 176;
            // 
            // numOfConnectedScreens
            // 
            this.numOfConnectedScreens.Location = new System.Drawing.Point(291, 302);
            this.numOfConnectedScreens.Name = "numOfConnectedScreens";
            this.numOfConnectedScreens.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numOfConnectedScreens.Size = new System.Drawing.Size(82, 20);
            this.numOfConnectedScreens.TabIndex = 175;
            // 
            // licenseNum
            // 
            this.licenseNum.Location = new System.Drawing.Point(683, 99);
            this.licenseNum.Name = "licenseNum";
            this.licenseNum.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.licenseNum.Size = new System.Drawing.Size(189, 20);
            this.licenseNum.TabIndex = 174;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(63, 400);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(81, 51);
            this.saveButton.TabIndex = 173;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            // 
            // mandatorySignStationSign
            // 
            this.mandatorySignStationSign.AutoSize = true;
            this.mandatorySignStationSign.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.mandatorySignStationSign.Location = new System.Drawing.Point(782, 363);
            this.mandatorySignStationSign.Name = "mandatorySignStationSign";
            this.mandatorySignStationSign.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.mandatorySignStationSign.Size = new System.Drawing.Size(193, 19);
            this.mandatorySignStationSign.TabIndex = 172;
            this.mandatorySignStationSign.Text = "חובת חתימה של עמדת עבודה";
            this.mandatorySignStationSign.UseVisualStyleBackColor = true;
            // 
            // passwordBetweenStands
            // 
            this.passwordBetweenStands.AutoSize = true;
            this.passwordBetweenStands.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.passwordBetweenStands.Location = new System.Drawing.Point(333, 48);
            this.passwordBetweenStands.Name = "passwordBetweenStands";
            this.passwordBetweenStands.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.passwordBetweenStands.Size = new System.Drawing.Size(164, 19);
            this.passwordBetweenStands.TabIndex = 171;
            this.passwordBetweenStands.Text = "בקשת סיסמא בין עמדות";
            this.passwordBetweenStands.UseVisualStyleBackColor = true;
            // 
            // printReportEndOfStation
            // 
            this.printReportEndOfStation.AutoSize = true;
            this.printReportEndOfStation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.printReportEndOfStation.Location = new System.Drawing.Point(311, 98);
            this.printReportEndOfStation.Name = "printReportEndOfStation";
            this.printReportEndOfStation.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.printReportEndOfStation.Size = new System.Drawing.Size(189, 19);
            this.printReportEndOfStation.TabIndex = 170;
            this.printReportEndOfStation.Text = "הדפסה של דו\"ח סיום בעמדה";
            this.printReportEndOfStation.UseVisualStyleBackColor = true;
            // 
            // lockStand
            // 
            this.lockStand.AutoSize = true;
            this.lockStand.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lockStand.Location = new System.Drawing.Point(404, 142);
            this.lockStand.Name = "lockStand";
            this.lockStand.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lockStand.Size = new System.Drawing.Size(96, 19);
            this.lockStand.TabIndex = 169;
            this.lockStand.Text = "נעילת עמדה";
            this.lockStand.UseVisualStyleBackColor = true;
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label11.Location = new System.Drawing.Point(914, 99);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(67, 15);
            this.Label11.TabIndex = 168;
            this.Label11.Text = "מס\' רשיון:";
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label10.Location = new System.Drawing.Point(914, 156);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(84, 15);
            this.Label10.TabIndex = 167;
            this.Label10.Text = "כתובת המכון:";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label8.Location = new System.Drawing.Point(310, 362);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(193, 15);
            this.Label8.TabIndex = 166;
            this.Label8.Text = "מס\' רשיון מטביע לוחיות מורשה:";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label7.Location = new System.Drawing.Point(379, 303);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(124, 60);
            this.Label7.TabIndex = 165;
            this.Label7.Text = "מס\' מסכים מחוברים:\r\n\r\n\r\n\r\n";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label6.Location = new System.Drawing.Point(251, 302);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(40, 15);
            this.Label6.TabIndex = 164;
            this.Label6.Text = "סטיה:";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label5.Location = new System.Drawing.Point(390, 198);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(113, 15);
            this.Label5.TabIndex = 163;
            this.Label5.Text = "זמן נעילה(שניות):";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label4.Location = new System.Drawing.Point(914, 322);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(68, 15);
            this.Label4.TabIndex = 162;
            this.Label4.Text = "מסלול מס\'";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label3.Location = new System.Drawing.Point(914, 266);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(138, 15);
            this.Label3.TabIndex = 161;
            this.Label3.Text = "עמדת עבודה של תחנה:";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label2.Location = new System.Drawing.Point(914, 217);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(80, 15);
            this.Label2.TabIndex = 160;
            this.Label2.Text = "תחום שיפוט:";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Label1.Location = new System.Drawing.Point(914, 52);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(58, 15);
            this.Label1.TabIndex = 159;
            this.Label1.Text = "שם מכון:";
            // 
            // instituteName
            // 
            this.instituteName.Location = new System.Drawing.Point(683, 52);
            this.instituteName.Name = "instituteName";
            this.instituteName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.instituteName.Size = new System.Drawing.Size(189, 20);
            this.instituteName.TabIndex = 156;
            // 
            // instituteAddress
            // 
            this.instituteAddress.Location = new System.Drawing.Point(683, 156);
            this.instituteAddress.Name = "instituteAddress";
            this.instituteAddress.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.instituteAddress.Size = new System.Drawing.Size(189, 20);
            this.instituteAddress.TabIndex = 152;
            // 
            // enableMultipleScreens
            // 
            this.enableMultipleScreens.AutoSize = true;
            this.enableMultipleScreens.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.enableMultipleScreens.Location = new System.Drawing.Point(356, 247);
            this.enableMultipleScreens.Name = "enableMultipleScreens";
            this.enableMultipleScreens.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.enableMultipleScreens.Size = new System.Drawing.Size(144, 19);
            this.enableMultipleScreens.TabIndex = 158;
            this.enableMultipleScreens.Text = "הפעלת ריבוי מסכים:";
            this.enableMultipleScreens.UseVisualStyleBackColor = true;
            // 
            // printReportUponEndOfInspection
            // 
            this.printReportUponEndOfInspection.AutoSize = true;
            this.printReportUponEndOfInspection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.printReportUponEndOfInspection.Location = new System.Drawing.Point(778, 400);
            this.printReportUponEndOfInspection.Name = "printReportUponEndOfInspection";
            this.printReportUponEndOfInspection.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.printReportUponEndOfInspection.Size = new System.Drawing.Size(201, 19);
            this.printReportUponEndOfInspection.TabIndex = 157;
            this.printReportUponEndOfInspection.Text = "הדפס טופס סיום בתום הבדיקה";
            this.printReportUponEndOfInspection.UseVisualStyleBackColor = true;
            // 
            // workStation
            // 
            this.workStation.FormattingEnabled = true;
            this.workStation.Items.AddRange(new object[] {
            "זיהוי",
            "אורות",
            "בור",
            "זיהום אוויר",
            "בלמים",
            "כוון גלגלים"});
            this.workStation.Location = new System.Drawing.Point(683, 266);
            this.workStation.Name = "workStation";
            this.workStation.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.workStation.Size = new System.Drawing.Size(189, 21);
            this.workStation.TabIndex = 155;
            // 
            // courseNum
            // 
            this.courseNum.FormattingEnabled = true;
            this.courseNum.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7"});
            this.courseNum.Location = new System.Drawing.Point(683, 316);
            this.courseNum.Name = "courseNum";
            this.courseNum.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.courseNum.Size = new System.Drawing.Size(189, 21);
            this.courseNum.TabIndex = 154;
            // 
            // region
            // 
            this.region.FormattingEnabled = true;
            this.region.Location = new System.Drawing.Point(683, 217);
            this.region.Name = "region";
            this.region.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.region.Size = new System.Drawing.Size(189, 21);
            this.region.TabIndex = 153;
            // 
            // GeneralSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Controls.Add(this.sway);
            this.Controls.Add(this.numericUpDown4);
            this.Controls.Add(this.lockTime);
            this.Controls.Add(this.numOfConnectedScreens);
            this.Controls.Add(this.licenseNum);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.mandatorySignStationSign);
            this.Controls.Add(this.passwordBetweenStands);
            this.Controls.Add(this.printReportEndOfStation);
            this.Controls.Add(this.lockStand);
            this.Controls.Add(this.Label11);
            this.Controls.Add(this.Label10);
            this.Controls.Add(this.Label8);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.instituteName);
            this.Controls.Add(this.instituteAddress);
            this.Controls.Add(this.enableMultipleScreens);
            this.Controls.Add(this.printReportUponEndOfInspection);
            this.Controls.Add(this.workStation);
            this.Controls.Add(this.courseNum);
            this.Controls.Add(this.region);
            this.Name = "GeneralSettings";
            this.Size = new System.Drawing.Size(1119, 467);
            this.Load += new System.EventHandler(this.GeneralSettings_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.sway)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lockTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numOfConnectedScreens)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.licenseNum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown sway;
        private System.Windows.Forms.NumericUpDown numericUpDown4;
        private System.Windows.Forms.NumericUpDown lockTime;
        private System.Windows.Forms.NumericUpDown numOfConnectedScreens;
        private System.Windows.Forms.NumericUpDown licenseNum;
        internal System.Windows.Forms.Button saveButton;
        internal System.Windows.Forms.CheckBox mandatorySignStationSign;
        internal System.Windows.Forms.CheckBox passwordBetweenStands;
        internal System.Windows.Forms.CheckBox printReportEndOfStation;
        internal System.Windows.Forms.CheckBox lockStand;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox instituteName;
        internal System.Windows.Forms.TextBox instituteAddress;
        internal System.Windows.Forms.CheckBox enableMultipleScreens;
        internal System.Windows.Forms.CheckBox printReportUponEndOfInspection;
        internal System.Windows.Forms.ComboBox workStation;
        internal System.Windows.Forms.ComboBox courseNum;
        internal System.Windows.Forms.ComboBox region;
    }
}
