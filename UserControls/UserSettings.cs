﻿using CommInf.Handlers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RishuiClient
{
    public partial class UserSettings : UserControl
    {
        public UserSettings()
        {
            InitializeComponent();
        }

            #region Mapping

            //Here you map all the handlers to the client's communication manager.

            //Example:
            //Program.myCommManager.MapHandler(HandleExampleTransferItem);

            #endregion
        }

        #region GUI Handlers

        //For the love of god, copy all your GUI callback functions into here. Keep it organized.

        //private void button_Click(object sender, EventArgs e)
        //{
        //    //Checking if the program succesfully connected.
        //    if(Program.myCommManager.IsConnected)
        //    {
        //        //Creating a new instance of a defined TransferItem
        //        LoginRequestTransferItem reqItem = new LoginRequestTransferItem()
        //            {
        //                Username = userNameTextBox.Text,
        //                Password = passwordTextBox.Text,
        //                SessionID = Program.SessionID
        //            };
        //
        //        //Sending the TransferItem to the server.
        //        Program.SendTransferItem(reqItem);
        //    }
        //    else
        //    {
        //        MessageBox.Show("Not connected dude..");
        //    }
        //}

        #endregion

        #region TransferItemHandlers

        //An example of a TransferItem handler. We mapped this handler to handle it's type in the Form constructor,
        //under the 'Mappings' region.

        //[HandleTypeDef(typeof(ExampleTransferItem))]
        //private void HandleExampleTransferItem(TransferItem tItem)
        //{
        //    Casting the TransferItem into the specific type we want to handle.
        //    ExampleTransferItem exItem = (ExampleTransferItem)tItem;

        //    Handler code goes here.

        //}

        #endregion


        /*loadDB loadSettings = new loadDB();

        private void testerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Set the User Settings options and boxes accordingly

            testerName.Text = loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).testerName;
            testerPassword.Text = loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).testerPassword;
            rfid.Text = loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).rfid;
            //CHANGES!
            userID.Text = loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userID.ToString();
            smokeID.Text = loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).smokeID.ToString();
            tz.Text = loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).tz.ToString();

            //Ifs for checkboxes in user settings. If enabled then (value is 1 for true or 0 for false) then execute/don't execute the option. When we load the checkbox settings we check for an integer from the DB for each of the checkboxes. Then we set the state of the checkboxes accordingly from the integer value. When we save the state of the checkboxes we can't actually do that without an integer so we set the struct values accordingly to then save back in  the database
            userOfficeCHK.Checked = (loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userOfficeCHK == 1);
            userReleaseStation.Checked = (loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userReleaseStation == 1);
            userTechniCHK.Checked = (loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userTechniCHK == 1);
            userReportEnable.Checked = (loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userReportEnable == 1);
            userRepeatCHK.Checked = (loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userRepeatCHK == 1);
            userReprintCHK.Checked = (loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userReprintCHK == 1);
            userAllowSignatureWithoutDeviceResults.Checked = (loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userAllowSignatureWithoutDeviceResults == 1);
            userLicensePlateModule.Checked = (loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userLicensePlateModule == 1);
            userAdminCHK.Checked = (loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userAdminCHK == 1);
            userOrotCHK.Checked = (loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userOrotCHK == 1);
            userZihuiCHK.Checked = (loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userZihuiCHK == 1);
            userBorCHK.Checked = (loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userBorCHK == 1);
            userZhumCHK.Checked = (loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userZhumCHK == 1);
            userBlamimCHK.Checked = (loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userBlamimCHK == 1);
            userGalgalimCHK.Checked = (loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userGalgalimCHK == 1);
            changeRPMCHK.Checked = (loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).changeRPMCHK == 1);
            isReleaseEmdaTimeLimitchk.Checked = (loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).isReleaseEmdaTimeLimitchk == 1);

        }


        private void userSetting_CheckedChanged(object sender, EventArgs e)
        {
            //User Settings enable/disable

            if (userSetting.Checked == false)
            {
                userOfficeCHK.Enabled = false;
                userReleaseStation.Enabled = false;
                userTechniCHK.Enabled = false;
                userReportEnable.Enabled = false;
                userRepeatCHK.Enabled = false;
                userReprintCHK.Enabled = false;
                userAllowSignatureWithoutDeviceResults.Enabled = false;
                userLicensePlateModule.Enabled = false;
                userAdminCHK.Enabled = false;
                userOrotCHK.Enabled = false;
                userZihuiCHK.Enabled = false;
                userBorCHK.Enabled = false;
                userZhumCHK.Enabled = false;
                userBlamimCHK.Enabled = false;
                userGalgalimCHK.Enabled = false;
                changeRPMCHK.Enabled = false;
                isReleaseEmdaTimeLimitchk.Enabled = false;

                testerName.Enabled = false;
                testerPassword.Enabled = false;
                rfid.Enabled = false;
                userID.Enabled = false;
                smokeID.Enabled = false;
                tz.Enabled = false;


            }
            else
            {
                userOfficeCHK.Enabled = true;
                userReleaseStation.Enabled = true;
                userTechniCHK.Enabled = true;
                userReportEnable.Enabled = true;
                userRepeatCHK.Enabled = true;
                userReprintCHK.Enabled = true;
                userAllowSignatureWithoutDeviceResults.Enabled = true;
                userLicensePlateModule.Enabled = true;
                userAdminCHK.Enabled = true;
                userOrotCHK.Enabled = true;
                userZihuiCHK.Enabled = true;
                userBorCHK.Enabled = true;
                userZhumCHK.Enabled = true;
                userBlamimCHK.Enabled = true;
                userGalgalimCHK.Enabled = true;
                changeRPMCHK.Enabled = true;
                isReleaseEmdaTimeLimitchk.Enabled = true;

                testerName.Enabled = true;
                testerPassword.Enabled = true;
                rfid.Enabled = true;
                userID.Enabled = true;
                smokeID.Enabled = true;
                tz.Enabled = true;

            }

        }


        private void addUser_Click(object sender, EventArgs e)
        {
            MySqlConnection sqlConnection = new MySqlConnection();
            string query = "select * from moshech_aac.generalsettings";
            MySqlDataReader reader = default(MySqlDataReader);
            MySqlCommand command = default(MySqlCommand);


            try
            {
                //Check if the fields are full or someone's just pressing the "Add" on nothing
                if (testerName.Text == null | testerPassword.Text == null | userID.Text == null | tz.Text == null)
                {

                    MessageBox.Show("מידע חסר");

                    return;

                }

                sqlConnection.ConnectionString = "server=74.50.87.131;userid=moshech_Admin;password=aac1234;database=moshech_aac";
                sqlConnection.Open();

                query = "select * from moshech_aac.usersettings";
                command = new MySqlCommand(query, sqlConnection);
                reader = command.ExecuteReader();


                while (reader.Read())
                {
                    //Check for duplicate information
                    if (testerName.Text == reader.GetString("testerName") | userID.Text.Equals(reader.GetInt32("userID")) | tz.Text.Equals(reader.GetInt32("tz")) | smokeID.Text.Equals(reader.GetInt32("smokeID")))
                    {

                        MessageBox.Show("מידע כפול נמצא");

                        sqlConnection.Close();
                        sqlConnection.Dispose();

                        return;

                    }

                }

                reader.Close();
                //Close the reader so that we can set it to a new Command below

                //query = "insert into moshech_aac.usersettings(testerName, testerPassword, rfid, userID, smokeID, tz, userOfficeCHK, userReleaseStation, userTechniCHK, userReportEnable, userRepeatCHK ,userReprintCHK , userAllowSignatureWithoutDeviceResults, userLicensePlateModule, userAdminCHK, userOrotCHK, userZihuiCHK, userBorCHK, userZhumCHK, userBlamimCHK, userGalgalimCHK,changeRPMCHK ,isReleaseEmdaTimeLimitchk ) values('" & testerName.Text & "','" & testerPassword.Text & "','" & rfid.Text & "','" & userID.Text & "','" & smokeID.Text & "','" & tz.Text & "','" & userOfficeCHK & "','" & userReleaseStation & "','" & userTechniCHK & "','" & userReportEnable & "','" & userRepeatCHK & "','" & userReprintCHK & "','" & userAllowSignatureWithoutDeviceResults & "','" & userLicensePlateModule & "','" & userAdminCHK & "','" & userOrotCHK & "','" & userZihuiCHK & "','" & userLicensePlateModule & "','" & userBorCHK & "','" & userZhumCHK & "','" & userBlamimCHK & "','" & userGalgalimCHK & "','" & changeRPMCHK & "','" & isReleaseEmdaTimeLimitchk & "')"
                command = new MySqlCommand(query, sqlConnection);
                reader = command.ExecuteReader();


            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);


            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                UpdateTesterList();

            }


        }


        private void saveUser_Click(object sender, EventArgs e)
        {
            MySqlConnection sqlConnection = new MySqlConnection();
            string query = "select * from moshech_aac.generalsettings";
            MySqlDataReader reader = default(MySqlDataReader);
            MySqlCommand command = default(MySqlCommand);

            sqlConnection.ConnectionString = "server=74.50.87.131;userid=moshech_Admin;password=aac1234;database=moshech_aac";


            try
            {
                sqlConnection.Open();

                command = new MySqlCommand(query, sqlConnection);
                reader = command.ExecuteReader();

                reader.Read();

                //Check if fields are full so we can save them. 
                //Check for duplicate information
                if (testerName.Text == null | testerPassword.Text == null | rfid.Text == null | userID.Text == null | smokeID.Text == null | tz.Text == null)
                {

                    MessageBox.Show("בבקשה מלא את כל הסעיפים");

                    sqlConnection.Close();
                    sqlConnection.Dispose();

                    return;

                }

                //Ifs for saving the settings. The checkboxes are checked to see if they have been selected or not. If they have been then we set the state as 1 for true or 0 for false. These changes are called in the query FIX IT ASSIGNMENT PROB
                loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userOfficeCHK = System.Convert.ToInt32(userOfficeCHK.Checked);
                loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userReleaseStation = System.Convert.ToInt32(userReleaseStation.Checked);
                loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userTechniCHK = System.Convert.ToInt32(userTechniCHK.Checked);
                loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userReportEnable = System.Convert.ToInt32(userReportEnable.Checked);
                loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userRepeatCHK = System.Convert.ToInt32(userRepeatCHK.Checked);
                loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userReprintCHK = System.Convert.ToInt32(userReprintCHK.Checked);
                loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userAllowSignatureWithoutDeviceResults = System.Convert.ToInt32(userAllowSignatureWithoutDeviceResults.Checked);
                loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userAdminCHK = System.Convert.ToInt32(userAdminCHK.Checked);
                loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userOrotCHK = System.Convert.ToInt32(userOrotCHK.Checked);
                loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userZihuiCHK = System.Convert.ToInt32(userZihuiCHK.Checked);
                loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userBorCHK = System.Convert.ToInt32(userBorCHK.Checked);
                loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userZhumCHK = System.Convert.ToInt32(userZhumCHK.Checked);
                loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userBlamimCHK = System.Convert.ToInt32(userBlamimCHK.Checked);
                loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userGalgalimCHK = System.Convert.ToInt32(userGalgalimCHK.Checked);
                loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).changeRPMCHK = System.Convert.ToInt32(changeRPMCHK.Checked);
                loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).isReleaseEmdaTimeLimitchk = System.Convert.ToInt32(isReleaseEmdaTimeLimitchk.Checked);
                loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userAllowSignatureWithoutDeviceResults = System.Convert.ToInt32(userAllowSignatureWithoutDeviceResults.Checked);

                //add rest!

                reader.Close();

                query = "update moshech_aac.usersettings set testerName =N'" + testerName.Text +
                    "', testerPassword ='" + testerPassword.Text +
                    "', rfid ='" + rfid.Text +
                    "', userID ='" + userID.Text +
                    "', smokeID ='" + smokeID.Text +
                    "', tz ='" + tz.Text +
                    "', userOfficeCHK ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userOfficeCHK +
                    "', userReleaseStation ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userReleaseStation +
                    "', userTechniCHK ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userTechniCHK +
                    "', userReportEnable ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userReportEnable +
                    "', userRepeatCHK ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userRepeatCHK +
                    "', userReprintCHK ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userReprintCHK +
                    "', userAllowSignatureWithoutDeviceResults ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userAllowSignatureWithoutDeviceResults +
                    "', userLicensePlateModule ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userLicensePlateModule +
                    "', userAdminCHK ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userAdminCHK +
                    "', userOrotCHK ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userOrotCHK +
                    "', userZihuiCHK ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userZihuiCHK +
                    "', userBorCHK ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userBorCHK +
                    "', userZhumCHK ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userZhumCHK +
                    "', userBlamimCHK ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userBlamimCHK +
                    "', userGalgalimCHK ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).userGalgalimCHK +
                    "', changeRPMCHK ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).changeRPMCHK +
                    "', isReleaseEmdaTimeLimitchk ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).isReleaseEmdaTimeLimitchk +
                    "' where ID ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).ID + "'";

                command = new MySqlCommand(query, sqlConnection);
                reader = command.ExecuteReader();

            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);


            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
                UpdateTesterList();

            }

        }


        private void deleteUser_Click(object sender, EventArgs e)
        {
            MySqlConnection sqlConnection = new MySqlConnection();
            string query = "select * from moshech_aac.generalsettings";
            MySqlDataReader reader = default(MySqlDataReader);
            MySqlCommand command = default(MySqlCommand);


            try
            {

                if (testerList.SelectedIndex >= 0)
                {
                    sqlConnection.ConnectionString = "server=74.50.87.131;userid=moshech_Admin;password=aac1234;database=moshech_aac";
                    sqlConnection.Open();

                    int deleteUserAtIndex = testerList.SelectedIndex;

                    query = "DELETE from usersettings where ID ='" + loadDB.loadUserSettings.ElementAt(testerList.SelectedIndex).ID + "'";
                    command = new MySqlCommand(query, sqlConnection);
                    reader = command.ExecuteReader();


                }
                else
                {
                    MessageBox.Show("Must select tester to delete");

                }

                UpdateTesterList();

            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);


            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();

            }

        }

        //Updates the tester list
        private void UpdateTesterList()
        {

            testerList.Items.Clear();
            //Clear testerList
            loadDB.loadUserSettings.Clear();
            //Clear the tester list object so that recalling this function won't stack the tester

            loadSettings.setUserSettings();


            for (int i = 0; i <= loadDB.loadUserSettings.Count - 1; i++)
            {
                testerList.Items.Add(loadDB.loadUserSettings.ElementAt(i).testerName);
            }
        }*/
        }
    


