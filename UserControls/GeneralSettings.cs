﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommInf.Handlers;

namespace UserControls
{
    public partial class GeneralSettings : UserControl
    {
        public GeneralSettings()
        {
            InitializeComponent();
        }


#region crap
        //private void GeneralSettings_Load(object sender, EventArgs e)
       // {


            /*loadSettings.setGeneralSettings();
            //Loads the General Settings

            //Set the General Settings options and boxes accordingly
            instituteName.Text = loadDB.loadGeneralSetting.instName;
            licenseNum.Value = loadDB.loadGeneralSetting.licenseNum;
            instituteAddress.Text = loadDB.loadGeneralSetting.instAddress;
            region.Text = loadDB.loadGeneralSetting.region;
            workStation.SelectedIndex = loadDB.loadGeneralSetting.workstation;
            courseNum.Text = loadDB.loadGeneralSetting.courseNum + 1.ToString();
            lockTime.Value = loadDB.loadGeneralSetting.lockTime;
            numOfConnectedScreens.Value = loadDB.loadGeneralSetting.numOfConnectedScreens;
            sway.Value = loadDB.loadGeneralSetting.sway;

            //Ifs for checkboxes in general settings. If enabled then (value is 1 for true or 0 for false) then execute/don't execute the option
            enableMultipleScreens.Checked = (loadDB.loadGeneralSetting.enableMultipleScreens == 1);
            passwordBetweenStands.Checked = (loadDB.loadGeneralSetting.passwordBetweenStands == 1);
            lockStand.Checked = (loadDB.loadGeneralSetting.lockStand == 1);
            printReportEndOfStation.Checked = (loadDB.loadGeneralSetting.printReportEndOfStation == 1);
            printReportUponEndOfInspection.Checked = (loadDB.loadGeneralSetting.printReportUponEndOfInspection == 1);
            mandatorySignStationSign.Checked = (loadDB.loadGeneralSetting.mandatoryStationSign == 1);


        }



        private void saveButton_Click(object sender, EventArgs e)
        {
            MySqlConnection sqlConnection = new MySqlConnection();
            string query = "select * from moshech_aac.generalsettings";
            MySqlDataReader reader = default(MySqlDataReader);
            MySqlCommand command = default(MySqlCommand);

            sqlConnection.ConnectionString = "server=74.50.87.131;userid=moshech_Admin;password=aac1234;database=moshech_aac;";


            try
            {
                sqlConnection.Open();

                command = new MySqlCommand(query, sqlConnection);
                reader = command.ExecuteReader();

                reader.Read();

                //Check if fields are full so we can save them. 
                //Check for duplicate information
                if (instituteName.Text == null | licenseNum.Text == null | instituteAddress.Text == null | region.Text == null | instituteName.Text == null | lockTime.Text == null | numOfConnectedScreens.Text == null | sway.Text == null)
                {

                    MessageBox.Show("בבקשה מלא את כל הסעיפים");

                    sqlConnection.Close();
                    sqlConnection.Dispose();

                    return;

                }

                //Ifs for saving the settings. The checkboxes are checked to see if they have been selected or not. If they have been then we set the state as 1 for true or 0 for false. These changes are called in the query
                loadDB.loadGeneralSetting.enableMultipleScreens = System.Convert.ToInt32(enableMultipleScreens.Checked);
                loadDB.loadGeneralSetting.passwordBetweenStands = System.Convert.ToInt32(passwordBetweenStands.Checked);
                loadDB.loadGeneralSetting.lockStand = System.Convert.ToInt32(lockStand.Checked);
                loadDB.loadGeneralSetting.printReportEndOfStation = System.Convert.ToInt32(printReportEndOfStation.Checked);
                loadDB.loadGeneralSetting.printReportUponEndOfInspection = System.Convert.ToInt32(printReportUponEndOfInspection.Checked);
                loadDB.loadGeneralSetting.mandatoryStationSign = System.Convert.ToInt32(mandatorySignStationSign.Checked);
                loadDB.loadGeneralSetting.enableMultipleScreens = System.Convert.ToInt32(enableMultipleScreens.Checked);
                loadDB.loadGeneralSetting.enableMultipleScreens = System.Convert.ToInt32(enableMultipleScreens.Checked);

                reader.Close();

                query = "update moshech_aac.generalsettings set instName ='" + instituteName.Text +
                    "', licenseNum ='" + Convert.ToInt32(licenseNum.Text) + 
                    "', instAddress ='" + instituteAddress.Text + 
                    "', region ='" + region.Text +
                    "', enableMultipleScreens ='" + loadDB.loadGeneralSetting.enableMultipleScreens + 
                    "', numOfConnectedScreens ='" + numOfConnectedScreens.Text + 
                    "', workStation ='" + workStation.SelectedIndex + 
                    "', mandatoryStationSign ='" + loadDB.loadGeneralSetting.mandatoryStationSign + 
                    "', courseNum ='" + courseNum.SelectedIndex + 
                    "', passwordRequestBetweenStations ='" + loadDB.loadGeneralSetting.passwordBetweenStands + 
                    "', printReportUponEndOfInspection ='" + loadDB.loadGeneralSetting.printReportUponEndOfInspection + 
                    "', lockStation ='" + loadDB.loadGeneralSetting.lockStand + 
                    "', printReportEndOfStation ='" + loadDB.loadGeneralSetting.printReportEndOfStation + 
                    "', lockTime ='" + lockTime.Text + "', sway ='" + sway.Text + 
                    "' where ID ='" + loadDB.loadGeneralSetting.ID + "'";

                command = new MySqlCommand(query, sqlConnection);
                reader = command.ExecuteReader();

                loadSettings.setGeneralSettings();

            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);


            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();

            }

        */
#endregion
            #region Mapping

            Program.myCommManager.MapHandler(HandleGeneralSettingsResultTransferItem);
       
            #endregion
        

        #region GUI Handlers

        private void GeneralSettings_Load_1(object sender, EventArgs e)
        {



        }

        #endregion

        #region TransferItemHandlers

        //An example of a TransferItem handler. We mapped this handler to handle it's type in the Form constructor,
        //under the 'Mappings' region.

        [HandleTypeDef(typeof(GeneralSettingsResultTransferItem))]
        private void HandleGeneralSettingsResultTransferItem(TransferItem tItem)
        {

            GeneralSettingsResultTransferItem InfoItem = (GeneralSettingsResultTransferItem)tItem;
            

           

        }

        #endregion

    }

}
