﻿using CommInf.Handlers;
using CommInf.Items;
using CommInf.Items.SessionItems;
using CommInf.Managers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustForTesting.TestClasses
{
    class MySessionCommManager : SessionCommManager
    {
        public MySessionCommManager() : base()
        {
        }
        protected override void MapHandlers()
        {
            MapHandler(HandleLoginParametersTransferItem);
        }

        [HandleTypeDef(typeof(MyLoginParametersSessionTransferObject))]
        private Collection<TransferItem> HandleLoginParametersTransferItem(TransferItem tItem)
        {
            if((tItem as MyLoginParametersSessionTransferObject).Username == "CHEEZE")
            {
                return new SuccessSessionTransferItem() { Result = true, SessionID = this.SessionID};
            }
            else
            {
                return new SuccessSessionTransferItem() { Result = false, SessionID = this.SessionID };
            }
        }
    }
}
