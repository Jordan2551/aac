﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustForTesting.TestClasses
{
    class SuccessSessionTransferItem : SessionTransferItem
    {
        public SuccessSessionTransferItem()
            : base()
        {
        }

        public bool Result { get; set; }
    }
}
