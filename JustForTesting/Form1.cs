﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CommInf.Items;
using CommInf.Handlers;
using JustForTesting.TestClasses;
using CommInf.Items.SessionItems;
using System.Collections.ObjectModel;


namespace JustForTesting
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MyServerCommManager serverCommManager = new MyServerCommManager();
            uint sessionID;
            Collection<TransferItem> res = serverCommManager.HandleTransferItem(new SessionRequestTransferItem());
            if(res[0] is SessionAcceptTransferItem)
            {
                sessionID = (res[0] as SessionAcceptTransferItem).SessionID;
                res = serverCommManager.HandleTransferItem(new MyLoginParametersSessionTransferObject() { Username = "CHEEZE", Password = "CHEEZE", SessionID = sessionID });
                MessageBox.Show(res[0].SerializeWithJSon());
            }
        }
    }
}
