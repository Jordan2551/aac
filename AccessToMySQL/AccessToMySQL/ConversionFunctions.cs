﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;

public class ConversionFunctions
{     

        //Query Examples:
        /*
         SELECT table_name, column_name, data_type, data_length
         FROM USER_TAB_COLUMNS
         WHERE table_name = 'MYTABLE'
        */

        const string AccessConnectString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\\aac2.mdb;" + "Jet OLEDB:Database Password=AAC86342;";
        OleDbConnection Connection = new OleDbConnection();
        OleDbDataReader AccessDataReader;
        OleDbCommand Command = new OleDbCommand();
        string Query;

        //These lists will store the column names for both tables so that we could say that AccessColumnNames(0) = MySQLColumnNames(0) for the column spots
        List<string> AccessColumnNames = new List<string>();
        List<int> MySQLColumnNames = new List<int>();


        public void GetColumnNames()
        {

         try
            {
                Connection.ConnectionString = (AccessConnectString);
                Query = "SELECT column_name FROM USER_TAB_COLUMNS  WHERE table_name = 'ActiveCarFile'";
                Connection = new OleDbConnection(AccessConnectString);
                Command = new OleDbCommand(Query, Connection);

                Connection.Open();

                AccessDataReader = Command.ExecuteReader();
                int i = 0;
                while (AccessDataReader.Read())
                {
                    AccessColumnNames.Add(AccessDataReader.GetString(i).ToString());
                    Console.WriteLine(AccessColumnNames.IndexOf(i).ToString());
                    i++;
                }
            }

            catch (Exception e) { Console.Write(e); Console.Read(); }

            finally
            {
                AccessDataReader.Close();
                Connection.Close();
          }
        }

        public void ActiveCarFileConversion()
        {
            try
            {
                Connection.ConnectionString = (AccessConnectString);
                Query = "SELECT * FROM ActiveCarFile";
                Connection = new OleDbConnection(AccessConnectString);
                Command = new OleDbCommand(Query, Connection);

                Connection.Open();

                AccessDataReader = Command.ExecuteReader();
                
                while (AccessDataReader.Read())
                {

                    Console.Write((AccessDataReader["CarBringLastName"].ToString()));
                    Console.WriteLine("");
                }
            }

            catch (Exception e) { Console.Write(e); Console.Read(); }

            finally
            {
                AccessDataReader.Close();
                Connection.Close();
          }
       }
    }


