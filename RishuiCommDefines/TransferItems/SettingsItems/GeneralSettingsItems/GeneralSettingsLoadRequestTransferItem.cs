﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RishuiCommDefines.TransferItems.GeneralSettingsItems
{
    public class GeneralSettingsLoadRequestTransferItem : SessionTransferItem
    {
        public string SignInID { get; set; }
    }
}
