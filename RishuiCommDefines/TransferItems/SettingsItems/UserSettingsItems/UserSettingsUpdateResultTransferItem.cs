﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RishuiCommDefines.TransferItems.UserSettingsItems
{
    public class UserSettingsUpdateResultTransferItem : SessionTransferItem
    {
        public string StatusMessage { get; set; }
    }
}
