﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RishuiCommDefines.TransferItems.UserSettingsItems
{
    public class UserSettingsDeleteUserRequestTransferItem : SessionTransferItem
    {

        public string UserToDelete { get; set; }

    }
}
