﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testhing.Table_Objects.Emdadata_Objects;

namespace RishuiCommDefines.TransferItems.TestCourseItems
{
    public class LoadDefectsForCarResultTransferItem : SessionTransferItem
    {

        //CarInfo Form Information
        public int CarCheckID { get; set; }
        public int CarNumber { get; set; }
        public string CarType { get; set; }
        public string FuelType { get; set; }
        public string TestType { get; set; }
        public int Condition { get; set; }
        public int CarYear { get; set; }
        public string CarOwnerName { get; set; }
        public int NumberOfAttempts { get; set; }

        //These objects get the data for the corresponding station(in Json) for the Emdadata table
        public EmdaDataInJsonGeneralStationInfo IDStationData = new EmdaDataInJsonGeneralStationInfo();
        public EmdaDataInJsonGeneralStationInfo LightsStationData = new EmdaDataInJsonGeneralStationInfo();
        public EmdaDataInJsonGeneralStationInfo PitStationData = new EmdaDataInJsonGeneralStationInfo();
        public EmdaDataInJsonGeneralStationInfo SmokeStationData = new EmdaDataInJsonGeneralStationInfo();
        public EmdaDataInJsonGeneralStationInfo BrakesStationData = new EmdaDataInJsonGeneralStationInfo();
        public EmdaDataInJsonGeneralStationInfo WheelAlignmentStationData = new EmdaDataInJsonGeneralStationInfo();


    }
}
