﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RishuiCommDefines.TransferItems.TestCourseItems
{
    public class LoadDefectInfoResultTransferItem : SessionTransferItem
    {

        public List<LoadDefectInfoResultTransferItem> Defects;//Stores all the defects 

        public int DefectID { get; set; }

        public List<string> DefectInfo;//List containing the defect's description. If the list has more than 1 index (aka a "/" was found in the description) then the defect has sub defects!
        public string Remark { get; set; }
        public int StationNumber { get; set; }

        //0 = no 1 = yes
        public int Permit { get; set; }
        public int RequiresPay { get; set; }

    }
}
