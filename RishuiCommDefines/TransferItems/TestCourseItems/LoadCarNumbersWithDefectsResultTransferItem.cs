﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RishuiCommDefines.TransferItems.TestCourseItems
{
    public class LoadCarNumbersWithDefectsResultTransferItem : SessionTransferItem
    {

        public List<LoadCarNumbersWithDefectsResultTransferItem> DefectiveCarInfo = new List<LoadCarNumbersWithDefectsResultTransferItem>();//List that stores objects of this type(the data below)

        //This list stores all the IDs of all the car manufacturers. The purpose of this is to display the icon of the car manufacturer that has made the defective car. These ints will go into a type that will arrange the ID of the manu to it's icon in the TestCourse Class
        public List<int> CarManuFacturers = new List<int>();
        //

        public int DefectiveCarNumber { get; set; }//Stores a car's number
        public string LastCheckTime { get; set; }
        public string CarType { get; set; }
        public string FuelType { get; set; }
        public int AttemptNumber { get; set; }

        public int CarManufacturerID { get; set; }



    }
}
