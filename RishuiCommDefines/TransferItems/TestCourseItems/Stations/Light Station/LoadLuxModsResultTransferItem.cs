﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RishuiCommDefines.TransferItems.TestCourseItems.Stations.Light_Station
{
    public class LoadLuxModsResultTransferItem : SessionTransferItem
    {

        public double LuxModHLR;
        public double LuxModHLL;
        public double LuxModLLR;
        public double LuxModLLL;

    }
}
