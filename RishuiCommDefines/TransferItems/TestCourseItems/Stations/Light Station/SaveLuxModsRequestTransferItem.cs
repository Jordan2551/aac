﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RishuiCommDefines.TransferItems.TestCourseItems.Stations.Light_Station
{
    public class SaveLuxModsRequestTransferItem : SessionTransferItem
    {

        public int StationID;//The ID representing the station, to know where in the DB to save the values below

        public double LuxModHLR;
        public double LuxModHLL;
        public double LuxModLLR;
        public double LuxModLLL;

    }

}
