﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RishuiCommDefines.TransferItems.TestCourseItems
{
    public class SignStationResultTransferItem : SessionTransferItem
    {
        public string ResponseMessage { get; set; }

    }
}
