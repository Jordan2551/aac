﻿using CommInf.Items;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RishuiCommDefines.TransferItems.TestCourseItems
{
    public class SignStationRequestTransferItem : SessionTransferItem
    {
        //MIGHT HAVE TO DEAL WITH REQUIRESPAY!

        //The general information about the station that was signed
        public int CarCheckID { get; set; }//The CarCheckID so we know which field in the table to sign

        //public List<string> SelectedDefectInfo;//This list will store the strings that were selected from a defect with Sub Defects. For example: from the defect "Bad lights/AC Leak/Bad Engine" only 2 items were chosen then it will fill this list with only 2 items. OR if it's a Defect without sub defects it just adds 1 entry to this list

        public int TesterIDOfSignedStation { get; set; }
        public int StationNumber { get; set; }
        public string TesterNameOfSignedStation { get; set; }
        public int StationFailed { get; set; }
        public string StationSignDate { get; set; }
        public int IsStationClosed { get; set; }
        public string TimeExpiredReleaseStation { get; set; }

        public List<EmdaDataInJsonStationDefectInfo> DefectList = new List<EmdaDataInJsonStationDefectInfo>();//This list contains a defect in every entry

        }

        public class EmdaDataInJsonStationDefectInfo//Contains the info per defect for every station
        {
            public int DefectID { get; set; }
            public string DefectInfo { get; set; }
            public int Transmitted { get; set; }
            public int DefectFixed { get; set; }
            public int PermitRequest { get; set; }
            public int PermitNumber { get; set; }
            public string PermitDate { get; set; }
            public int RequiresPay { get; set; }
            public string FixDate { get; set; }
            public int TesterIDOfSignedDefect { get; set; }
            public string TesterNameOfSignedDefect { get; set; }
            public string DefectSignDateTime { get; set; }
            public int DangerousDefect { get; set; }
        }
     
    }

