﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RishuiCommDefines.TransferItems.TestCourseItems
{
    public class LoadDefectsForCarRequestTransferItem : SessionTransferItem
    {
        public int CarNumber { get; set; }//Send over the car number that was selected that was shown in the CarNumber combo box so we can get it's defects.

    }
}
