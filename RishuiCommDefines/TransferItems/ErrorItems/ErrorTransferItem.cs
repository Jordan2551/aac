﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RishuiCommDefines.TransferItems.ErrorItems
{
    public class ErrorTransferItem : TransferItem
    {
        public string ErrorDescription { get; set; }
    }
}
