﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RishuiCommDefines.TransferItems.ReceiveVehicleItems.InitialPermitsForReceiveingVehicleItems
{
    public class InputVehiclePermitsRequestTransferItem : SessionTransferItem
    {

        //OFFLINE PROPERTIES
        public int CarNumber;

        //ONLINE PROPERTIES
        public int TestType;//0 = UNSPECIFIED 1 = ANNUALTEST 2 = DISQUALIFICATIONTEST --Convert to type: "TestType"--
        public DateTime TestStartDate;//תחילת מבחן
        public DateTime ReTestDate;//תחילת מבחן חוזר
        public List<GarageApproval> NeededPermitList;//List of type that contains information regarding sending online requested garage approvals. Also used for saving these permits offline in the DB

        //CarData Initializing Properties
        public int CarOwnerID;
        public int PriviledgedID;
        public int KodGorem;
        public int PakidID;
        public string PakidName;
        public int InstituteID;
        public string CarDataPassword;

    }

    public class GarageApproval
    {

        //Type that contains information regarding sending online requested garage approvals

        public int GarrageApprovalID;//Certified garage license number
        public int PermitCode;//ID to represent the permit we are talking about. For example: אישור בלמים = 1
        public DateTime CheckDate;//The date of which the vehicle was FIRST checked(before the PermitDate)
        public DateTime PermitIssueDate;//The date of which the permit was given to the client
        public string PermitName;//String represntation of the permit's name

    }

}
