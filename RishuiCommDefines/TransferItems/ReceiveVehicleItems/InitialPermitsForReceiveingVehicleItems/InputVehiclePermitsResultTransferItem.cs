﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RishuiCommDefines.TransferItems.ReceiveVehicleItems.InitialPermitsForReceiveingVehicleItems
{
    public class InputVehiclePermitsResultTransferItem : SessionTransferItem
    {
        public string Response;//if the permits have been saved or not
        public Boolean IsResponseAnError;

        public string VehiclePermitListInJSon;//Save all the permits and their info as a Json string to store in the DB later

    }
}
