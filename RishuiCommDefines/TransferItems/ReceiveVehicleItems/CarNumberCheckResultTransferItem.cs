﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RishuiCommDefines.TransferItems.ReceiveVehicleItems
{
    public class CarNumberCheckResultTransferItem : SessionTransferItem
    {

        public string TestResult;//String that represents the result of the test. "New" = new test(doesn't exist in db or online). "Passed" = a car that exists and has passed the test. "Failed" a car that exists and has failed a test

    }
}
