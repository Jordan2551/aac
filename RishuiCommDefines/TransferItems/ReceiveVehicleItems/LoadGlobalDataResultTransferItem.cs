﻿using CommInf.Items;
using RishuiCommDefines.TransferItems.SharedDataItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RishuiCommDefines.TransferItems.ReceiveVehicleItems
{
    public class LoadGlobalDataResultTransferItem : SessionTransferItem
    {
        public Dictionary<int, string> CarManufacturerNames = new Dictionary<int, string>();//List that takes a key(manufacturer ID) and returns the manufacturer name
        public List<int> CarManuOrder = new List<int>();//List to keep the order of the car manufacturers(see ReceiveVehicle for more info)
        public Dictionary<int, List<string>> CarModelDictionary = new Dictionary<int, List<string>>();
        public Dictionary<string, string> CarModelIDNameDictionary= new Dictionary<string, string>();//Dictionary that takes every model ID and string name
        public Dictionary<int, string> CarColorDictionary = new Dictionary<int, string>();//Dictionary for storing the car colors(color ID, color description)
        public Dictionary<int, string> FuelTypesDictionary = new Dictionary<int, string>();//Dictionary for storing the fuel types(fuel ID, fuel description)
        public Dictionary<int, string> TestTypesDictionary = new Dictionary<int, string>();
        public Dictionary<int, string> VehicleTypeDictionary = new Dictionary<int, string>();//Dictionary of all vehicle types
        public string ErrorMessage { get; set; }
        
        //public Dictionary<int, List<string>> EngineModelDictionary = new Dictionary<int, List<string>>();//Give this dictionary a car model and get a list full of the engine models for it

    }
}
