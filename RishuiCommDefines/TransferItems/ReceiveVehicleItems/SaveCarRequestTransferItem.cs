﻿using CommInf.Items;
using RishuiCommDefines.TransferItems.ReceiveVehicleItems.InitialPermitsForReceiveingVehicleItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RishuiCommDefines.TransferItems.ReceiveVehicleItems
{
    public class SaveCarRequestTransferItem : SessionTransferItem
    {
        public int CarNumber ;
        public int CarCheckID ;
        public int CarModelID ;
        public int CarManufacturerID ;
        public string CarModel ;
        public string CarMaker ;
        public int FuelType ;
        public int TotalWeight ;
        public string CarShilda ;
        public int CarKm ;
        public int CarKmStatus ;//0 = תקין 1 = לא תקין 3 = אין מד 4 = אין מד מהירות לסוג רכב
        public string EngineNumber ;
        public string EngineModel ;
        public int EngineVolume ;
        public string CarFrontTire ;
        public string CarRearTire ;
        public int CarColor ;//Can be negative!!!
        public int NumOfSeren ;
        public int VavGrira ;
        public int Awd ;
        public int CarOwnerID ;
        public string CarOwnerName ;
        public string LicenseCode ;
        public string Phone1 ;
        public string Phone2 ;
        public string OpenTestDateTime ;
        public int CarYear ;
        public int CarMonth ;
        public int PriviledgeEnable ;
        public int PriviledgedID ;
        public string PriviledgedName ;
        public string TesterName ;
        public int TestType ;
        public int CarType ;
        public int LastStation ;
        public int IsRecOnline ;//0 = Online 1 = Offline
        public string CloseTestDateTime ;//0 = Online 1 = Offline
        public int Transmitted ;
        public int CarLBL ;
        public int CarCheckFinal ;
        public double IncMaxLambda;
        public double IncMinLambda;
        public double IncMaxCo;
        public int IncMaxRpm;
        public int IncMinRpm;
        public double IdleMaxCo;
        public int OilTempMax;
        public int IdleSpeedMax;
        public int IdleSpeedMin;
        public double MaxK;
        public int IdleSpeedMaxHC;
        public int NoLoadSpeedMin;
        public int NoLoadSpeedMax;

        public string InitialVehiclePermitListInJSon;//Contains all initial permits entered by the user as well as dates(if entered) in Json


    }
}
