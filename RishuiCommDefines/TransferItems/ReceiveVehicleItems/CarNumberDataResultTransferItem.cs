﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RishuiCommDefines.TransferItems.ReceiveVehicleItems
{
    public class CarNumberDataResultTransferItem : SessionTransferItem
    {

        public int CarCheckID { get; set; }
        public int CarNumber { get; set; }
        public int CarModelID { get; set; }
        public string CarModel { get; set; }//String representation of car model
        public int CarMakerID { get; set; }
        public string CarMaker { get; set; }//String representation of car maker
        public int FuelType { get; set; }
        public int TotalWeight { get; set; }
        public string CarShilda { get; set; }
        public int CarKm { get; set; }
        public int CarKmStatus { get; set; }//0 = תקין 1 = לא תקין 3 = אין מד 4 = אין מד מהירות לסוג רכב
        public string EngineNumber { get; set; }
        public string EngineModel { get; set; }
        public int EngineVolume { get; set; }
        public string CarFrontTire { get; set; }
        public string CarRearTire { get; set; }
        public int CarColor { get; set; }//Can be negative!!!
        public int NumOfSeren { get; set; }
        public int VavGrira { get; set; }
        public int Awd { get; set; }
        public int CarOwnerID { get; set; }
        public string CarOwnerName { get; set; }
        public string LicenseCode { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string OpenTestDateTime { get; set; }
        public string CarYear { get; set; }
        public string PriviledgedID { get; set; }
        public string PriviledgedName { get; set; }
        public string TesterName { get; set; }
        public int TestType { get; set; }
        public int LastStation { get; set; }

        public int CarCheckFinal;

        public int VehicleType;

        //CARDATA SPECIFIC 
        public List<CarDataNeededPermit> CarDataNeededPermitList;//List of initial permits required by Car Data
        public CarDataResponse CarDataResponse;

    }

    public class CarDataResponse//A type that holds the request response message from CarData as well as if that response is an error message or not
    {
        public string ResponseMessage;
        public Boolean IsResponseError;
    }

    public class CarDataNeededPermit//A type that holds needed approvals returned by CarData
    {

        public int Code;
        public string Description;
        public bool GarageApprovalNeeded;//Is approval needed from a certified garage?
        public bool ApprovalDateNeeded;//Is date needed for the approval?

    }



}
