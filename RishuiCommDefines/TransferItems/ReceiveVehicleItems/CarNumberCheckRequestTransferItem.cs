﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RishuiCommDefines.TransferItems.ReceiveVehicleItems
{
    public class CarNumberCheckRequestTransferItem : SessionTransferItem
    {

        public int CarNumber;//Car number to check for in the DB
    }
}
