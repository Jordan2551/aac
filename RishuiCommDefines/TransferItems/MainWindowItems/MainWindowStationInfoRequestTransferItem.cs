﻿using CommInf.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RishuiCommDefines.TransferItems.MainWindowItems
{
    public class MainWindowStationInfoRequestTransferItem : SessionTransferItem
    {

        public int StationNumber;//The station number the data should be loaded for

    }
}
