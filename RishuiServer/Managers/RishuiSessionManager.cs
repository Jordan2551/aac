﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CommInf.Managers;
using CommInf.Handlers;
using RishuiCommDefines.Managers;
using System.Collections.ObjectModel;
using CommInf.Items;
using MySql.Data.MySqlClient;
using System.Net;
using System.Text.RegularExpressions;
using RishuiCommDefines.TransferItems.ErrorItems;
using RishuiCommDefines.TransferItems.MainWindowItems;
using RishuiCommDefines.TransferItems;
using RishuiCommDefines.TransferItems.GeneralSettingsItems;
using RishuiCommDefines.TransferItems.UserSettingsItems;
using RishuiCommDefines.TransferItems.ReceiveVehicleItems;
using Newtonsoft.Json;
using testhing.Table_Objects.Emdadata_Objects;
using RishuiCommDefines.TransferItems.DefectWindowItems;
using RishuiCommDefines.TransferItems.TestCourseItems;
using RishuiCommDefines.TransferItems.SharedDataItems;
using RishuiCommDefines.TransferItems.SettingsItems.DefectViewAndEditItems;
using RishuiCommDefines.TransferItems.ReceiveVehicleItems.Report_Printing;
using System.Text;
using RishuiCommDefines.TransferItems.ReportsFormItems.Car_Test_List_ReportItems;
using System.Globalization;
using RishuiCommDefines.TransferItems.ReportsFormItems;
using RishuiCommDefines.TransferItems.ReportsFormItems.ReportItems.TestsForPeriodItems;
using RishuiCommDefines.TransferItems.SettingsItems.MachineSettingsItems;
using RishuiServer.CDWS;
using RishuiCommDefines.TransferItems.SettingsItems.PrinterAndReportSettings;
using RishuiCommDefines.TransferItems.ReceiveVehicleItems.InitialPermitsForReceiveingVehicleItems;
using RishuiCommDefines.TransferItems.TestCourseItems.Stations.Light_Station;

namespace RishuiServer.Managers
{
    public class RishuiSessionManager : SessionCommManager
    {

        //mySQL Objects
        private const String mySQLConnectionString = "server=localhost;userid=root;password=admin;database=moshech_aac;CharSet=utf8;";
        private static MySqlConnection mySQLDBConnection = new MySqlConnection(mySQLConnectionString);
        private static MySqlDataReader mySQLReader;
        private static MySqlCommand mySQLCommand;
        private static string query;

        //CarData Objects
        CDWS.CarDataLogin CarDataLogin = new CDWS.CarDataLogin();
        CDWS.ID CarDataID = new CDWS.ID();
        CDWS.Vehicle CarDataVehicle = new CDWS.Vehicle();

        #region Members

        //Any session-specific member is defined here.

        #region Log-In Define

        private string Password;
        private static string External_IP;
        //private string Tag_ID;//WIP

        #endregion

        #region Main Window Define


        #endregion


        #endregion

        #region Shared Handler Data
        //This is all data that is useful to more than 1 handler
        private Dictionary<int, string> FuelTypesDictionary = new Dictionary<int, string>();
        private Dictionary<int, string> TestTypeDictionary = new Dictionary<int, string>();
        private Dictionary<int, string> VehicleTypeDictionary = new Dictionary<int, string>();

        #endregion

        public RishuiSessionManager()
            : base()
        {

            #region CarData Object Initialize

            //fill CarDataLogin object
            CarDataLogin.centerId = 40950;
            CarDataLogin.pass = "tudrep2f";

            //fill ID object
            CarDataID.kodGorem = 112;
            CarDataID.tzPakid = 204860548;
            CarDataID.shemPakid = "test";

            #endregion

            //Member initialization! Remember to initialize them!

            #region Log-In Init

            Password = "";
            External_IP = "";
        }
            #endregion


        protected override void MapHandlers()
        {

            #region Map Handlers

            //Every handler defined in the 'Handlers' section needs to be mapped here!!
            MapHandler(HandleSharedDataRequestTransferItem);
            MapHandler(HandleLoginRequestTransferItem);
            MapHandler(HandleMainWindowUserInfoRequestTransferItem);
            MapHandler(HandleGeneralSettingsLoadRequestTransferItem);
            MapHandler(HandleGeneralSettingsSaveRequestTransferItem);
            MapHandler(HandleUserSettingsLoadRequestTransferItem);
            MapHandler(HandleUserSettingsSaveRequestTransferItem);
            MapHandler(HandleUserSettingsAddRequestTransferItem);
            MapHandler(HandleUserSettingsDeleteUserRequestTransferItem);
            MapHandler(HandleCarNumberCheckRequestTransferItem);
            MapHandler(HandleDefectWindowLoadRequestTransferItem);
            MapHandler(HandleDefectWindowSavePermitsRequestTransferItem);
            MapHandler(HandleLoadGlobalDataRequestTransferItem);
            MapHandler(HandleSaveCarRequestTransferItem);
            MapHandler(HandleLoadDefectsForCarRequestTransferItem);
            MapHandler(HandleLoadCarNumbersWithDefectsRequestTransferItem);
            MapHandler(HandleLoadDefectInfoRequestTransferItem);
            MapHandler(HandleSignStationRequestTransferItem);
            MapHandler(HandleReleaseStationRequestTransferItem);
            MapHandler(HandleSaveReceiveVehicleReportRequestTransferItem);
            MapHandler(HandleDefectInfoRequestTransferItem);
            MapHandler(HandleReportRequestTransferItem);
            MapHandler(HandleReportsFormDataGrid0DataRequestTransferItem);
            MapHandler(HandleReportTypesLoadRequestTransferItem);
            MapHandler(HandlePrintReportForCarNumberRequestTransferItem);
            MapHandler(HandleUpdateFirstCopyRequestTransferItem);
            MapHandler(HandleUpdateDateTimeOfFirstCopyRequestTransferItem);
            MapHandler(HandleMachineSettingsSaveRequestTransferItem);
            MapHandler(HandleMainWindowStationInfoRequestTransferItem);
            MapHandler(HandleMainWindowServerInfoResultTransferItem);
            MapHandler(HandleSaveReportAndPrinterSettingsRequestTransferItem);
            MapHandler(HandleLoadReportAndPrinterSettingsRequestTransferItem);
            MapHandler(HandleEngineModelsForCarModelRequestTransferItem);
            MapHandler(HandleSaveInitialPermitsRequestTransferItem);
            MapHandler(HandleCarNumberDataRequestTransferItem);
            MapHandler(HandleSaveLuxModsRequestTransferItem);
            MapHandler(HandleLoadLuxModsRequestTransferItem);

            #endregion

        }

        #region Handlers

        //All handlers are defined here.
        #region Shared Handler Data

        //This handler will get all the information that is shared by different handlers. For example, both ReceiveVehicle and CarInfo Forms share TestType, FuelType and VehicleType so they will make a request to load up the data over here to then get it
        //THIS HANDLER DOES NOT RETURN ANYTHING. IT'S FOR SETTING DATA LOCALLY ONLY
        [HandleTypeDef(typeof(SharedDataRequestTransferItem))]
        private Collection<TransferItem> HandleSharedDataRequestTransferItem(TransferItem tItem)
        {

            try
            {

                mySQLDBConnection.Open();

                #region Load Vehicle Types

                query = "select * from moshech_aac.vehicletypes";//Read where we have a unique ID
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                while (mySQLReader.Read())
                {
                    this.VehicleTypeDictionary.Add(mySQLReader.GetInt32("ID"), mySQLReader.GetString("CodeName") + " " + mySQLReader.GetString("VehicleType"));
                }

                mySQLReader.Close();

                #endregion

                #region Load Fuel Types

                query = "select * from moshech_aac.fueltype";//Read where we have a unique ID
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                while (mySQLReader.Read())
                {
                    this.FuelTypesDictionary.Add(mySQLReader.GetInt32("ID"), mySQLReader.GetString("FuelType"));
                }

                mySQLReader.Close();

                #endregion

                #region Load Test Types

                query = "select * from moshech_aac.testtype";//Read where we have a unique ID
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                while (mySQLReader.Read())
                {
                    this.TestTypeDictionary.Add(mySQLReader.GetInt32("ID"), mySQLReader.GetString("TestType"));
                }

                mySQLReader.Close();

                #endregion

            }

            catch { }

            mySQLDBConnection.Close();

            return (null);
        }


        #endregion

        #region Login Form Handlers

        #region Login Request Handler

        //This first attribute sets the type of TransferItem that the handler gets.
        [HandleTypeDef(typeof(LoginRequestTransferItem))]
        private Collection<TransferItem> HandleLoginRequestTransferItem(TransferItem tItem)
        {
            //Casting to the specific type.
            //Item that got sent here
            LoginRequestTransferItem reqItem = (LoginRequestTransferItem)tItem;//Why casting
            //User code.
            //Item to transfer
            LoginResultTransferItem LoginResult = new LoginResultTransferItem();

            try
            {

                mySQLDBConnection.Open();
                query = "select * from moshech_aac.usersettings";
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();


                #region Check if RFID or Regualr Passwords Match from TI to vs DB

                while (mySQLReader.Read())
                {

                    LoginResult.TesterName = mySQLReader.GetString("testerName");//Sends the testername over as well

                    if ((reqItem.Password.Equals(mySQLReader.GetString("testerPassword"))))//ADD EXTERNAL IP IN FUTURE
                    {
                        //Log in granted
                        LoginResult.LoginResult = true;
                        SessionID = this.SessionID;
                        mySQLDBConnection.Close();
                        return (LoginResult);

                    }


                    if ((reqItem.RFID.Length > 0 && reqItem.RFID.Equals(mySQLReader.GetString("rfid"))))//ADD EXTERNAL IP IN FUTURE
                    {
                        //Log in granted
                        LoginResult.LoginResult = true;
                        SessionID = this.SessionID;
                        mySQLDBConnection.Close();
                        return (LoginResult);

                    }

                }

                #endregion
            }

            catch (Exception) { }

            finally
            {
                mySQLDBConnection.Close();
            }
            //Log in failed
            LoginResult.LoginResult = false;
            SessionID = this.SessionID;
            return (LoginResult);
        }

        #endregion

        #endregion

        #region Main Window Form Handlers

        #region Main Window Handler

        #region Load User Settings & Info

        //Loads data that is specific to the logged in tester.
        [HandleTypeDef(typeof(MainWindowUserInfoRequestTransferItem))]//So far only returns ext IP
        private Collection<TransferItem> HandleMainWindowUserInfoRequestTransferItem(TransferItem tItem)
        {

            MainWindowUserInfoRequestTransferItem RequestedTester = (MainWindowUserInfoRequestTransferItem)tItem;
            MainWindowUserInfoResultTransferItem UserInfo = new MainWindowUserInfoResultTransferItem();

            try
            {

                mySQLDBConnection.Open();

                //Info from user settings
                query = "select * from moshech_aac.usersettings where TesterName like '%" + RequestedTester.TesterName + "%'";

                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                mySQLReader.Read();

                //מידע משתמש
                UserInfo.testerName = mySQLReader.GetString("testerName");
                UserInfo.testerPassword = mySQLReader.GetString("testerPassword");
                UserInfo.rfid = mySQLReader.GetString("rfid");
                UserInfo.userID = mySQLReader.GetInt32("userID");
                UserInfo.smokeID = mySQLReader.GetInt16("smokeID");
                UserInfo.tz = mySQLReader.GetInt32("tz");
                UserInfo.External_IP = mySQLReader.GetString("External_IP");

                //הגבלות כלליות
                UserInfo.AllowStationRelease = mySQLReader.GetInt32("AllowStationRelease");
                UserInfo.AllowTechnicalChanges = mySQLReader.GetInt16("AllowTechnicalChanges");
                UserInfo.AllowReportPrinting = mySQLReader.GetInt16("AllowReportPrinting");
                UserInfo.AllowReCheck = mySQLReader.GetInt16("AllowReCheck");
                UserInfo.AllowReportRePrinting = mySQLReader.GetInt16("AllowReportRePrinting");

                //הגבלות גישה
                UserInfo.AllowAccessToDefectWindow = mySQLReader.GetInt16("AllowAccessToDefectWindow");
                UserInfo.AllowLicensePlateWindow = mySQLReader.GetInt16("AllowLicensePlateWindow");
                UserInfo.AllowAccessToOfficeWindow = mySQLReader.GetInt16("AllowAccessToOfficeWindow");
                UserInfo.AllowAdminPrivileges = mySQLReader.GetInt32("AllowAdminPrivileges");

                //הגבלות עמדה
                UserInfo.AllowIDStation = mySQLReader.GetInt32("AllowIDStation");
                UserInfo.AllowLightsStation = mySQLReader.GetInt32("AllowLightsStation");
                UserInfo.AllowPitStation = mySQLReader.GetInt32("AllowPitStation");
                UserInfo.AllowSmokeStation = mySQLReader.GetInt32("AllowSmokeStation");
                UserInfo.AllowBrakesStation = mySQLReader.GetInt32("AllowBrakesStation");
                UserInfo.AllowWheelAlignmentStation = mySQLReader.GetInt32("AllowWheelAlignmentStation");
                UserInfo.AllowCheckMechanisim = mySQLReader.GetInt32("AllowCheckMechanisim");
                UserInfo.AllowTimeLimitAfterStationRelease = mySQLReader.GetInt32("AllowTimeLimitAfterStationRelease");
                UserInfo.AllowDeviceStationSigning = mySQLReader.GetInt32("AllowDeviceStationSigning");

                mySQLReader.Close();

            }

            catch (Exception) { }

            finally
            {
                mySQLDBConnection.Close();
            }

            SessionID = this.SessionID;

            return (UserInfo);

        }

        #endregion

        #region Load Station Settings & Info

        //Loads data associated with the station(such as machine types, ports, etc)
        [HandleTypeDef(typeof(MainWindowStationInfoRequestTransferItem))]
        private Collection<TransferItem> HandleMainWindowStationInfoRequestTransferItem(TransferItem tItem)//Change this to ONLY user specific data and add a new transfer item for the station data
        {

            MainWindowStationInfoRequestTransferItem RequestedStation = (MainWindowStationInfoRequestTransferItem)tItem;
            MainWindowStationInfoResultTransferItem StationInfo = new MainWindowStationInfoResultTransferItem();

            try
            {

                mySQLDBConnection.Open();

                #region Institute Specific Data
                query = "select * from moshech_aac.station_settings WHERE StationID = " + RequestedStation.StationNumber + "";

                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                mySQLReader.Read();

                //Read all fields from the station settings db table

                //KEEP FILLING OUT THE STATION INFORMATION THEN CHANGE THE WAY THE MAIN WINDOW CALLS INFORMATION FOR USER SETTINGS AND STATION SETTING SEPARATELY!! THEN CONTINUE ON WITH THE MACHINE SETTINGS WINDOW REGARDING THESE SETTINGS
                StationInfo.StationID = mySQLReader.GetInt32("StationID");
                StationInfo.StationType = mySQLReader.GetInt32("StationType");
                StationInfo.Arka_Days = mySQLReader.GetInt32("Arka_Days");

                StationInfo.LightsMachineType = mySQLReader.GetString("LightsMachineType");
                StationInfo.LightsMachineComPort = mySQLReader.GetInt32("LightsMachineComPort");
                StationInfo.LightsSettings = mySQLReader.GetString("LightsSettings");

                StationInfo.GasMachineType = mySQLReader.GetString("GasMachineType");
                StationInfo.GasMachineComPort = mySQLReader.GetInt32("GasMachineComPort");
                StationInfo.GasSettings = mySQLReader.GetString("GasSettings");

                StationInfo.SmokeMachineType = mySQLReader.GetString("SmokeMachineType");
                StationInfo.SmokeMachineComPort = mySQLReader.GetInt32("SmokeMachineComPort");
                StationInfo.SmokeMeterType = mySQLReader.GetString("SmokeMeterType");

                StationInfo.RFIDPort = mySQLReader.GetInt32("RFIDPort");

                StationInfo.WorkStationNumber = mySQLReader.GetInt32("WorkStationNumber");
                StationInfo.ZeroVoltWeight = mySQLReader.GetString("ZeroVoltWeight");
                StationInfo.KiloPerVoltRMatmer = mySQLReader.GetString("KiloPerVoltRMatmer");
                StationInfo.ZeroVoltRMatmer = mySQLReader.GetString("ZeroVoltRMatmer");
                StationInfo.BrakesMachineType = mySQLReader.GetString("BrakesMachineType");
                StationInfo.BrakeCheck = mySQLReader.GetString("BrakeCheck");
                StationInfo.RunWayNum = mySQLReader.GetString("RunWayNum");
                StationInfo.JumpStation = mySQLReader.GetString("JumpStation");
                StationInfo.DefineWorkLineStation = mySQLReader.GetString("DefineWorkLineStation");
                StationInfo.ReceiveMaha = mySQLReader.GetString("ReceiveMaha");
                StationInfo.MahaOutFolder = mySQLReader.GetString("MahaOutFolder");
                StationInfo.LoadCarColor = mySQLReader.GetString("LoadCarColor");
                StationInfo.NewCarColor = mySQLReader.GetString("NewCarColor");
                StationInfo.RequestStationPassword = mySQLReader.GetString("RequestStationPassword");
                StationInfo.CashSettingsActive = mySQLReader.GetString("CashSettingsActive");
                StationInfo.PaymentEnd = mySQLReader.GetString("PaymentEnd");
                StationInfo.PaymentFirst = mySQLReader.GetString("PaymentFirst");
                StationInfo.TagCost = mySQLReader.GetString("TagCost");
                StationInfo.UseBinaCode = mySQLReader.GetString("UseBinaCode");
                StationInfo.BinaMdbPath = mySQLReader.GetString("BinaMdbPath");
                StationInfo.AllowPrintEndRep = mySQLReader.GetString("AllowPrintEndRep");
                StationInfo.PasswordTimerOn = mySQLReader.GetString("PasswordTimerOn");
                StationInfo.PasswordTimerInterval = mySQLReader.GetString("PasswordTimerInterval");
                StationInfo.DefectListColor = mySQLReader.GetString("DefectListColor");


                mySQLReader.Close();

                #endregion

            }

            catch (Exception) { }

            finally
            {
                mySQLDBConnection.Close();
            }

            SessionID = this.SessionID;

            return (StationInfo);

        }

        #endregion

        #region Load Server Settings & Info

        //Loads data associated with the server(such as institute name, address, id, LP etc)
        [HandleTypeDef(typeof(MainWindowServerInfoRequestTransferItem))]
        private Collection<TransferItem> HandleMainWindowServerInfoResultTransferItem(TransferItem tItem)
        {

            MainWindowServerInfoResultTransferItem ServerInfo = new MainWindowServerInfoResultTransferItem();

            try
            {

                mySQLDBConnection.Open();

                #region Institute Specific Data
                query = "select * from moshech_aac.server_settings";

                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                mySQLReader.Read();

                ServerInfo.InstituteAddress = mySQLReader.GetString("InstituteAddress");
                ServerInfo.InstituteName = mySQLReader.GetString("InstituteName");
                ServerInfo.InstituteNumber = mySQLReader.GetString("InstituteNumber");
                ServerInfo.MisradRishuiMechozi = mySQLReader.GetString("MisradRishuiMechozi");
                ServerInfo.TestMonthsPeriod = mySQLReader.GetInt32("TestMonthsPeriod");
                ServerInfo.Serial = mySQLReader.GetString("Serial");
                ServerInfo.DateOfLicense = mySQLReader.GetString("DateOfLicense");
                ServerInfo.PurchaseLicense = mySQLReader.GetString("PurchaseLicense");
                ServerInfo.OnlinePassword = mySQLReader.GetString("OnlinePassword");
                ServerInfo.OnlineDataSupply = mySQLReader.GetString("OnlineDataSupply");
                ServerInfo.KodGorem = mySQLReader.GetString("KodGorem");
                ServerInfo.LicensePlateSerial = mySQLReader.GetString("LicensePlateSerial");
                ServerInfo.KniaSerial = mySQLReader.GetString("KniaSerial");
                ServerInfo.OnlineUsage = mySQLReader.GetInt32("OnlineUsage");
                ServerInfo.OnlineAddress = mySQLReader.GetString("OnlineAddress");

                mySQLReader.Close();

                #endregion

            }

            catch (Exception) { }

            finally
            {
                mySQLDBConnection.Close();
            }

            SessionID = this.SessionID;

            return (ServerInfo);

        }

        #endregion

        #endregion

        #region Settings Handlers

        #region General Settings User Control Handlers

        #region General Settings Load Handler

        [HandleTypeDef(typeof(GeneralSettingsLoadRequestTransferItem))]//So far only returns ext IP
        private Collection<TransferItem> HandleGeneralSettingsLoadRequestTransferItem(TransferItem tItem)
        {

            GeneralSettingsLoadResultTransferItem LoadItem = new GeneralSettingsLoadResultTransferItem();

            try
            {

                mySQLDBConnection.Open();
                query = "select * from moshech_aac.generalsettings";
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                while (mySQLReader.Read())
                {

                    LoadItem.ID = mySQLReader.GetInt32("ID");
                    LoadItem.InstituteName = mySQLReader.GetString("instName");
                    LoadItem.LicenseNumber = mySQLReader.GetInt32("licenseNum");
                    LoadItem.InstituteAddress = mySQLReader.GetString("instAddress");
                    LoadItem.InstituteRegion = mySQLReader.GetString("region");
                    LoadItem.enableMultipleScreens = mySQLReader.GetInt16("enableMultipleScreens");
                    LoadItem.numOfConnectedScreens = mySQLReader.GetInt16("numOfConnectedScreens");
                    LoadItem.WorkStation = mySQLReader.GetInt32("workStation");
                    LoadItem.mandatoryStationSign = mySQLReader.GetInt16("mandatoryStationSign");
                    LoadItem.CourseNumber = mySQLReader.GetInt16("courseNum");
                    LoadItem.passwordBetweenStands = mySQLReader.GetInt16("passwordRequestBetweenStations");
                    LoadItem.printReportUponEndOfInspection = mySQLReader.GetInt16("prIntReportUponEndOfInspection");
                    LoadItem.LockStand = mySQLReader.GetInt16("lockStand");
                    LoadItem.printReportEndOfStation = mySQLReader.GetInt16("prIntReportEndOfStation");
                    LoadItem.lockTime = mySQLReader.GetInt32("lockTime");
                    LoadItem.ScreenSway = mySQLReader.GetInt32("sway");
                }
            }

            catch (Exception) { }

            finally
            {
                mySQLDBConnection.Close();
            }
            SessionID = this.SessionID;
            return (LoadItem);
        }

        #endregion

        #region General Settings Save Handler

        [HandleTypeDef(typeof(GeneralSettingsSaveRequestTransferItem))]//So far only returns ext IP
        private Collection<TransferItem> HandleGeneralSettingsSaveRequestTransferItem(TransferItem tItem)
        {

            GeneralSettingsSaveRequestTransferItem SaveItem = (GeneralSettingsSaveRequestTransferItem)tItem;

            GeneralSettingsSaveResultTransferItem ConfirmSave = new GeneralSettingsSaveResultTransferItem();

            try
            {

                mySQLDBConnection.Open();

                //Ifs for saving the settings. The checkboxes are checked to see if they have been selected or not. If they have been then we set the state as 1 for true or 0 for false. These changes are called in the query
                query = "update moshech_aac.generalsettings set instName ='" + SaveItem.InstituteName +
                    "', licenseNum ='" + Convert.ToInt32(SaveItem.licenseNum) +
                    "', instAddress ='" + SaveItem.instAddress +
                    "', region ='" + SaveItem.InstituteRegion +
                    "', enableMultipleScreens ='" + SaveItem.enableMultipleScreens +
                    "', numOfConnectedScreens ='" + SaveItem.numOfConnectedScreens +
                    "', workStation ='" + SaveItem.WorkStation +
                    "', mandatoryStationSign ='" + SaveItem.mandatoryStationSign +
                    "', courseNum ='" + SaveItem.CourseNumber +
                    "', passwordRequestBetweenStations ='" + SaveItem.passwordBetweenStands +
                    "', prIntReportUponEndOfInspection ='" + SaveItem.printReportUponEndOfInspection +
                    "', lockStand ='" + SaveItem.LockStation +
                    "', prIntReportEndOfStation ='" + SaveItem.printReportEndOfStation +
                    "', lockTime ='" + SaveItem.lockTime + "', sway ='" + SaveItem.ScreenSway +
                    "' where ID ='" + SaveItem.ID + "'";

                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

            }

            catch (Exception) { }

            finally
            {
                mySQLDBConnection.Close();
            }

            SessionID = this.SessionID;
            return (ConfirmSave);
        }

        #endregion

        #endregion

        #region User Settings User Control Handlers

        #region User Settings Load Handler

        [HandleTypeDef(typeof(UserSettingsLoadRequestTransferItem))]//So far only returns ext IP
        private Collection<TransferItem> HandleUserSettingsLoadRequestTransferItem(TransferItem tItem)
        {
            List<UserSettingsLoadResultTransferItem> BochenSettingsCopy = new List<UserSettingsLoadResultTransferItem>();

            UserSettingsLoadResultTransferItem LoadBochenSettings = new UserSettingsLoadResultTransferItem();
            //LoadBochenSettings.
            try
            {

                mySQLDBConnection.Open();
                query = "select * from moshech_aac.usersettings";
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                //IMPORTANT: We make an instance of the UserSettingsLoadResultTransferItem class so we can set one user setting at a time. We then add the instance to the list which is inside the same class
                //The list is static so we just pass the instance Into the list, then for the next user settings we reset the instance but the list stays the same as it is static!
                while (mySQLReader.Read())
                {

                    //מידע משתמש
                    LoadBochenSettings.ID = mySQLReader.GetInt32("ID");
                    LoadBochenSettings.testerName = mySQLReader.GetString("testerName");
                    LoadBochenSettings.testerPassword = mySQLReader.GetString("testerPassword");
                    LoadBochenSettings.rfid = mySQLReader.GetString("rfid");
                    LoadBochenSettings.userID = mySQLReader.GetInt32("userID");
                    LoadBochenSettings.smokeID = mySQLReader.GetInt16("smokeID");
                    LoadBochenSettings.tz = mySQLReader.GetInt32("tz");

                    //הגבלות כלליות
                    LoadBochenSettings.AllowStationRelease = mySQLReader.GetInt32("AllowStationRelease");
                    LoadBochenSettings.AllowTechnicalChanges = mySQLReader.GetInt16("AllowTechnicalChanges");
                    LoadBochenSettings.AllowReportPrinting = mySQLReader.GetInt16("AllowReportPrinting");
                    LoadBochenSettings.AllowReCheck = mySQLReader.GetInt16("AllowReCheck");
                    LoadBochenSettings.AllowReportRePrinting = mySQLReader.GetInt16("AllowReportRePrinting");

                    //הגבלות גישה
                    LoadBochenSettings.AllowAccessToDefectWindow = mySQLReader.GetInt16("AllowAccessToDefectWindow");
                    LoadBochenSettings.AllowLicensePlateWindow = mySQLReader.GetInt16("AllowLicensePlateWindow");
                    LoadBochenSettings.AllowAccessToOfficeWindow = mySQLReader.GetInt16("AllowAccessToOfficeWindow");
                    LoadBochenSettings.AllowAdminPrivileges = mySQLReader.GetInt32("AllowAdminPrivileges");

                    //הגבלות עמדה
                    LoadBochenSettings.AllowIDStation = mySQLReader.GetInt32("AllowIDStation");
                    LoadBochenSettings.AllowLightsStation = mySQLReader.GetInt32("AllowLightsStation");
                    LoadBochenSettings.AllowPitStation = mySQLReader.GetInt32("AllowPitStation");
                    LoadBochenSettings.AllowSmokeStation = mySQLReader.GetInt32("AllowSmokeStation");
                    LoadBochenSettings.AllowBrakesStation = mySQLReader.GetInt32("AllowBrakesStation");
                    LoadBochenSettings.AllowWheelAlignmentStation = mySQLReader.GetInt32("AllowWheelAlignmentStation");
                    LoadBochenSettings.AllowCheckMechanisim = mySQLReader.GetInt32("AllowCheckMechanisim");
                    LoadBochenSettings.AllowTimeLimitAfterStationRelease = mySQLReader.GetInt32("AllowTimeLimitAfterStationRelease");
                    LoadBochenSettings.AllowDeviceStationSigning = mySQLReader.GetInt32("AllowDeviceStationSigning");

                    BochenSettingsCopy.Add(LoadBochenSettings);
                    LoadBochenSettings = new UserSettingsLoadResultTransferItem();

                }
            }

            catch (Exception) { }

            finally
            {
                mySQLDBConnection.Close();
            }
            SessionID = this.SessionID;
            //UserSettingsLoadResultTransferItem.BochenSettingsList = BochenSettingsCopy;
            LoadBochenSettings.BochenSettingsList = BochenSettingsCopy;
            return (LoadBochenSettings);
        }


        #endregion

        #region User Settings Update Handler

        [HandleTypeDef(typeof(UserSettingsUpdateRequestTransferItem))]
        private Collection<TransferItem> HandleUserSettingsSaveRequestTransferItem(TransferItem tItem)
        {

            UserSettingsUpdateRequestTransferItem BochenSettingsToSave = (UserSettingsUpdateRequestTransferItem)tItem;
            UserSettingsUpdateResultTransferItem BochenReturnSaveState = new UserSettingsUpdateResultTransferItem();

            try
            {

                mySQLDBConnection.Open();

                query = "update moshech_aac.usersettings set testerName ='" + BochenSettingsToSave.testerName +
                    "', testerPassword ='" + BochenSettingsToSave.testerPassword +
                    "', rfid ='" + BochenSettingsToSave.rfid +
                    "', userID ='" + BochenSettingsToSave.userID +
                    "', smokeID ='" + BochenSettingsToSave.smokeID +
                    "', tz ='" + BochenSettingsToSave.tz +
                    "', AllowStationRelease ='" + BochenSettingsToSave.AllowStationRelease +
                    "', AllowTechnicalChanges ='" + BochenSettingsToSave.AllowTechnicalChanges +
                    "', AllowReportPrinting ='" + BochenSettingsToSave.AllowReportPrinting +
                    "', AllowReCheck ='" + BochenSettingsToSave.AllowReCheck +
                    "', AllowReportRePrinting ='" + BochenSettingsToSave.AllowReportRePrinting +
                    "', AllowAccessToDefectWindow ='" + BochenSettingsToSave.AllowAccessToDefectWindow +
                    "', AllowLicensePlateWindow ='" + BochenSettingsToSave.AllowLicensePlateWindow +
                    "', AllowAccessToOfficeWindow ='" + BochenSettingsToSave.AllowAccessToOfficeWindow +
                    "', AllowAdminPrivileges ='" + BochenSettingsToSave.AllowAdminPrivileges +
                    "', AllowIDStation ='" + BochenSettingsToSave.AllowIDStation +
                    "', AllowLightsStation ='" + BochenSettingsToSave.AllowLightsStation +
                    "', AllowPitStation ='" + BochenSettingsToSave.AllowPitStation +
                    "', AllowSmokeStation ='" + BochenSettingsToSave.AllowSmokeStation +
                    "', AllowBrakesStation ='" + BochenSettingsToSave.AllowBrakesStation +
                    "', AllowWheelAlignmentStation ='" + BochenSettingsToSave.AllowWheelAlignmentStation +
                    "', AllowCheckMechanisim ='" + BochenSettingsToSave.AllowCheckMechanisim +
                    "', AllowTimeLimitAfterStationRelease ='" + BochenSettingsToSave.AllowTimeLimitAfterStationRelease +
                    "', AllowDeviceStationSigning ='" + BochenSettingsToSave.AllowDeviceStationSigning +
                    "' where ID ='" + BochenSettingsToSave.ID + "'";

                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();
                mySQLReader.Close();

            }

            catch (Exception) { }

            finally
            {
                mySQLDBConnection.Close();
            }
            SessionID = this.SessionID;
            BochenReturnSaveState.StatusMessage = "בוחן עודכן בהצלחה";
            return (BochenReturnSaveState);
        }


        #endregion

        #region User Settings Add Handler

        [HandleTypeDef(typeof(UserSettingsAddRequestTransferItem))]//So far only returns ext IP
        private Collection<TransferItem> HandleUserSettingsAddRequestTransferItem(TransferItem tItem)
        {

            UserSettingsAddRequestTransferItem AddBochen = (UserSettingsAddRequestTransferItem)tItem;
            UserSettingsAddResultTransferItem AddStatus = new UserSettingsAddResultTransferItem();

            try
            {

                mySQLDBConnection.Open();

                //Insert new user to table
                query = "insert Into moshech_aac.usersettings(testerName, testerPassword, rfid, userID, smokeID, tz, External_IP, AllowStationRelease, AllowTechnicalChanges, AllowReportPrinting, AllowReCheck, AllowReportRePrinting, AllowAccessToDefectWindow , AllowLicensePlateWindow, AllowAccessToOfficeWindow, AllowAdminPrivileges, AllowIDStation, AllowLightsStation, AllowPitStation, AllowSmokeStation, AllowBrakesStation, AllowWheelAlignmentStation ,AllowCheckMechanisim, AllowTimeLimitAfterStationRelease, AllowDeviceStationSigning) values('"
                    + AddBochen.testerName + "','"
                    + AddBochen.testerPassword + "','"
                    + AddBochen.rfid + "','"
                    + AddBochen.userID + "','"
                    + AddBochen.smokeID + "','"
                    + AddBochen.tz + "','"
                    + GetExternalIP() + "','"
                    + AddBochen.AllowStationRelease + "','"
                    + AddBochen.AllowTechnicalChanges + "','"
                    + AddBochen.AllowReportPrinting + "','"
                    + AddBochen.AllowReCheck + "','"
                    + AddBochen.AllowReportRePrinting + "','"
                    + AddBochen.AllowAccessToDefectWindow + "','"
                    + AddBochen.AllowLicensePlateWindow + "','"
                    + AddBochen.AllowAccessToOfficeWindow + "','"
                    + AddBochen.AllowAdminPrivileges + "','"
                    + AddBochen.AllowIDStation + "','"
                    + AddBochen.AllowLightsStation + "','"
                    + AddBochen.AllowPitStation + "','"
                    + AddBochen.AllowSmokeStation + "','"
                    + AddBochen.AllowBrakesStation + "','"
                    + AddBochen.AllowWheelAlignmentStation + "','"
                    + AddBochen.AllowCheckMechanisim + "','"
                    + AddBochen.AllowTimeLimitAfterStationRelease + "','"
                    + AddBochen.AllowDeviceStationSigning + "')";


                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();
                mySQLReader.Close();

            }

            catch (Exception) { }

            finally
            {
                mySQLDBConnection.Close();
            }

            SessionID = this.SessionID;
            AddStatus.StatusMessage = "בוחן הוסף בהצלחה";
            return (AddStatus);
        }

        #endregion

        #region User Settings Delete Handler

        [HandleTypeDef(typeof(UserSettingsDeleteUserRequestTransferItem))]//So far only returns ext IP
        private Collection<TransferItem> HandleUserSettingsDeleteUserRequestTransferItem(TransferItem tItem)
        {

            UserSettingsDeleteUserRequestTransferItem DeleteUser = (UserSettingsDeleteUserRequestTransferItem)tItem;

            try
            {

                mySQLDBConnection.Open();

                query = "delete from usersettings where testerName like '%" + DeleteUser.UserToDelete + "%'";

                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();
                mySQLReader.Close();


            }

            catch (Exception) { }

            finally
            {
                mySQLDBConnection.Close();
            }

            SessionID = this.SessionID;
            return (null);

        }

        #endregion

        #endregion

        #region Machine Settings User Control Handlers

        #region Save Machine Settings

        [HandleTypeDef(typeof(MachineSettingsSaveRequestTransferItem))]
        private Collection<TransferItem> HandleMachineSettingsSaveRequestTransferItem(TransferItem tItem)
        {

            MachineSettingsSaveRequestTransferItem SaveMachineSettings = (MachineSettingsSaveRequestTransferItem)tItem;

            try
            {

                mySQLDBConnection.Open();
                //Update station settings
                query = "Update moshech_aac.station_settings set GasMachineType = '" + SaveMachineSettings.GasMachineType +
                    "', GasMachineComPort ='" + SaveMachineSettings.GasMachineComPort +
                    "', SmokeMachineType ='" + SaveMachineSettings.SmokeMachineType +
                    "', SmokeMachineComPort ='" + SaveMachineSettings.SmokeMachineComPort +
                    "', LightsMachineType ='" + SaveMachineSettings.LightsMachineType +
                    "', LightsMachineComPort ='" + SaveMachineSettings.LightsMachineComPort +
                    "', BrakesMachineType ='" + SaveMachineSettings.BrakesMachineType +
                    "', RFIDPort ='" + SaveMachineSettings.RFIDPort +
                    "'  where StationID = '" + SaveMachineSettings.StationID + "'";

                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

            }

            catch (Exception) { }

            finally
            {
                mySQLDBConnection.Close();
            }

            SessionID = this.SessionID;

            return (null);

        }

        #endregion

        #endregion

        #region Defect Edit & Defect View User Control Handlers

        #region Defect View Request Defects Handler

        [HandleTypeDef(typeof(DefectInfoRequestTransferItem))]
        private Collection<TransferItem> HandleDefectInfoRequestTransferItem(TransferItem tItem)
        {

            //THIS IS ALL LIKE LOADING THE DEFECT DATA FOR TESTCOURSE, CHECK THAT FOR MORE INFORMATION

            DefectInfoResultTransferItem DefectData = new DefectInfoResultTransferItem();//Object to store all the info for a defect

            List<DefectInfoResultTransferItem> DefectsListCopy = new List<DefectInfoResultTransferItem>();//A list that contains items of type LoadDefectInfoResultTI, each entry represents a defect. The same list inside DefectData will set itself = to this list because each new object entry would reset the list inside the DefectData object as well.

            mySQLDBConnection.Open();


            //Some of this information is taken from the shared dictionaries in this class. This is data we use in both ReceiveVehicle and here for example

            query = "select * from moshech_aac.defectinfo";
            mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
            mySQLReader = mySQLCommand.ExecuteReader();

            while (mySQLReader.Read())
            {

                DefectData.DefectID = mySQLReader.GetInt32("DefectID");
                DefectData.DefectInfo = mySQLReader.GetString("DefectInfo").Split('/').ToList();//Split up the DefectInfo string into seperate strings if there is a "/" present
                DefectData.Remark = mySQLReader.GetString("Remark");
                DefectData.Permit = mySQLReader.GetInt32("Permit");
                DefectData.StationNumber = mySQLReader.GetInt32("StationNumber");

                DefectsListCopy.Add(DefectData);

                DefectData = new DefectInfoResultTransferItem();

            }

            mySQLReader.Close();
            mySQLDBConnection.Close();

            DefectData.Defects = DefectsListCopy;//Set the list of defects inside DefectData to this one so we can transfer the data over
            SessionID = this.SessionID;

            return (DefectData);
        }

        #endregion

        #endregion

        #region Defect Edit Save Defects Handler

        #endregion

        #region Printer & Report Settings Handlers

        //---REPORT TYPES(NAMES) ARE LOADED FROM "Reports Form Handlers' - Load Reports Form Report Types" ---

        #region Save Report Print Settings

        [HandleTypeDef(typeof(SaveReportAndPrinterSettingsRequestTransferItem))]
        private Collection<TransferItem> HandleSaveReportAndPrinterSettingsRequestTransferItem(TransferItem tItem)
        {

            SaveReportAndPrinterSettingsRequestTransferItem PrintAndReportSettingsInfo = (SaveReportAndPrinterSettingsRequestTransferItem)tItem;
            SaveReportAndPrinterSettingsResultTransferItem SaveReportPrintResult = new SaveReportAndPrinterSettingsResultTransferItem();//Response item so that the user know the save was ok

            try
            {

                mySQLDBConnection.Open();

                //Save the print and report settings. Based on the station id and report ID of the report wanted to be updated with new settings
                query = "update moshech_aac.printer_and_report_settings set PrinterDeviceName ='" + PrintAndReportSettingsInfo.PrinterDeviceName.Replace("\\", @"\\\\") +
                    "', PaperSourceName ='" + PrintAndReportSettingsInfo.PaperSourceName +
                    "', NumberOfCopies ='" + PrintAndReportSettingsInfo.NumberOfCopies +
                    "', OnlyDisplayReportOnScreen ='" + Convert.ToInt32(PrintAndReportSettingsInfo.OnlyDisplayReportOnScreen) +
                    "', SpaceFromTop ='" + PrintAndReportSettingsInfo.SpaceFromTop +
                    "' where StationID ='" + PrintAndReportSettingsInfo.StationID + "' and ReportID ='" + PrintAndReportSettingsInfo.ReportID + "'";

                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();
                mySQLReader.Read();

            }

            catch (Exception) { }

            finally
            {
                mySQLDBConnection.Close();
            }

            SessionID = this.SessionID;
            return (SaveReportPrintResult);
        }

        #endregion

        #region Load Report Print Settings

        [HandleTypeDef(typeof(LoadReportAndPrinterSettingsRequestTransferItem))]
        private Collection<TransferItem> HandleLoadReportAndPrinterSettingsRequestTransferItem(TransferItem tItem)
        {

            LoadReportAndPrinterSettingsResultTransferItem PrintAndReportSettingsInfo = new LoadReportAndPrinterSettingsResultTransferItem();
            LoadReportAndPrinterSettingsResultTransferItem PrintAndReportSettingsInfoCopy = new LoadReportAndPrinterSettingsResultTransferItem();//Holds the values for the report print settings temporarily

            try
            {

                mySQLDBConnection.Open();

                query = "select * from moshech_aac.printer_and_report_settings";

                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                while (mySQLReader.Read())
                {

                    PrintAndReportSettingsInfo = new LoadReportAndPrinterSettingsResultTransferItem();//Reset the object 

                    //Add the report print properties to the object initiated above
                    PrintAndReportSettingsInfo.ReportID = mySQLReader.GetInt32("ReportID");
                    PrintAndReportSettingsInfo.ReportName = mySQLReader.GetString("ReportName");
                    PrintAndReportSettingsInfo.PrinterDeviceName = mySQLReader.GetString("PrinterDeviceName");
                    PrintAndReportSettingsInfo.PaperSourceName = mySQLReader.GetString("PaperSourceName");
                    PrintAndReportSettingsInfo.NumberOfCopies = mySQLReader.GetInt32("NumberOfCopies");
                    PrintAndReportSettingsInfo.OnlyDisplayReportOnScreen = Convert.ToBoolean(mySQLReader.GetInt32("OnlyDisplayReportOnScreen"));
                    PrintAndReportSettingsInfo.SpaceFromTop = mySQLReader.GetInt32("SpaceFromTop");

                    //Add that object to the list of print and report settings
                    PrintAndReportSettingsInfoCopy.ReportSettingsList.Add(PrintAndReportSettingsInfo);

                }

                PrintAndReportSettingsInfo = PrintAndReportSettingsInfoCopy;//Set the actual object we want the information for = to the temporary object of that same type

            }

            catch (Exception) { }

            finally
            {
                mySQLDBConnection.Close();
            }

            SessionID = this.SessionID;
            return (PrintAndReportSettingsInfo);
        }

        #endregion

        #endregion


        #endregion

        #region Reports Form Handlers

        #region Load Reports Form Report Types

        [HandleTypeDef(typeof(ReportTypesLoadRequestTransferItem))]
        private Collection<TransferItem> HandleReportTypesLoadRequestTransferItem(TransferItem tItem)
        {

            ReportTypesLoadResultTransferItem ReportTypesData = new ReportTypesLoadResultTransferItem();

            //This list holds the data for an entry in the report_types table
            List<ReportTypesLoadResultTransferItem> ReportTypesDataCopy = new List<ReportTypesLoadResultTransferItem>();//The db data gets filled in this object first, then we set the "ReportTypesData"'s "ReportTypesDataList" = to this list 

            try
            {

                ReportTypesLoadResultTransferItem ReportTypeToAdd;//Object to add to the ReportTypesDataCopy list

                mySQLDBConnection.Open();

                query = "SELECT * FROM moshech_aac.report_types";
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                while (mySQLReader.Read())
                {

                    ReportTypeToAdd = new ReportTypesLoadResultTransferItem();
                    ReportTypeToAdd.FilterOptions = new List<string>();//Init the list

                    ReportTypeToAdd.ReportID = mySQLReader.GetInt32("ReportID");
                    ReportTypeToAdd.ReportName = mySQLReader.GetString("ReportName");
                    ReportTypeToAdd.DataGridAssociationID = mySQLReader.GetInt32("DataGridAssociationID");
                    ReportTypeToAdd.InReportsForm = mySQLReader.GetInt32("InReportsForm");

                    //The reports that have more than 1 filter option have their FilterOptions string separated by "/" characters, therfore if a report like this is involved
                    //Then we need to split every filter option into a separate list element
                    if (mySQLReader.GetString("FilterOptions").Contains("/")) { ReportTypeToAdd.FilterOptions = mySQLReader.GetString("FilterOptions").Split('/').ToList(); }

                    //If this is a report with 1 or NO filter options then splitting will cause an error, therfore just adding it as the first index(0) is the solution
                    else { ReportTypeToAdd.FilterOptions.Add(mySQLReader.GetString("FilterOptions")); }

                    ReportTypesDataCopy.Add(ReportTypeToAdd);

                }

                mySQLReader.Close();

            }

            catch (MySqlException) { }

            finally
            {
                mySQLDBConnection.Close();

                ReportTypesData.ReportTypesDataList = ReportTypesDataCopy;

            }

            SessionID = this.SessionID;
            return (ReportTypesData);
        }

        #endregion

        #region Load Reports Form Table

        //Data for the "ReportInfoTable" for the specified date range

        [HandleTypeDef(typeof(ReportsFormDataGrid0DataRequestTransferItem))]
        private Collection<TransferItem> HandleReportsFormDataGrid0DataRequestTransferItem(TransferItem tItem)
        {

            #region Data Grid Data Objects

            #region DataGrid 0

            ReportsFormDataGrid0DataRequestTransferItem InfoForReport = (ReportsFormDataGrid0DataRequestTransferItem)tItem;
            ReportsFormDataGrid0DataResultTransferItem DataGrid0Data = new ReportsFormDataGrid0DataResultTransferItem();

            #endregion

            #endregion

            //This list holds the data for a grid entry
            List<ReportsFormDataGrid0DataResultTransferItem> DataGrid0DataCopy = new List<ReportsFormDataGrid0DataResultTransferItem>();

            switch (InfoForReport.RequestedReport)
            {

                //Switch statement based on ReportName string. The reports that share the same GridData, also share the same information, so the data returned by the transfer item is the same for the reports
                //That are grouped together below. This also means that they can all be ordered the same way. For example: the reports in the first group of switch cases all have the same data(but different report files) but they 
                //Share the same DataGrid(DataGrid0) so they can be ordered the same way(CarCheckID, LastCheckDate, CarNumber, CarType....etc)
                case "בדיקות רישוי לפי תאריכים":
                case "בדיקות הורדה מהכביש רשימה מפורטת":
                case "פירוט לפי סוג מבחן":

                    try
                    {

                        #region Receivevehicle Report Info

                        mySQLDBConnection.Open();

                        //DateTime conversion for the "From" & "To" date strings
                        DateTime FromDateInDateTime = Convert.ToDateTime(InfoForReport.FromDate);
                        DateTime ToDateInDateTime = Convert.ToDateTime(InfoForReport.ToDate);

                        //Settings the strings = to a "yyyy-MM-dd" format version of them
                        InfoForReport.FromDate = FromDateInDateTime.ToString("yyyy-MM-dd");
                        InfoForReport.ToDate = ToDateInDateTime.ToString("yyyy-MM-dd");

                        //Get the info for this date range

                        if (InfoForReport.RequestedReport.Equals("בדיקות הורדה מהכביש רשימה מפורטת"))//If the the report is תעודת תקינות - בדיקות הורדה מהכביש רשימה מפורטת then we need to search for a specific TestType in ReceiveVehicle for that
                        {
                            query = "SELECT * from moshech_aac.receivevehicle WHERE TestType = " + 1 + " AND LastCheckDate >= '" + InfoForReport.FromDate + "' AND LastCheckDate <= '" + InfoForReport.ToDate + "'";
                        }

                        else//If this is פירוט לפי סוג מבחן או בדיקות רישוי לפי תאריכים then all of the TestTypes are relevant 
                        {
                            query = "SELECT * from moshech_aac.receivevehicle WHERE LastCheckDate >= '" + InfoForReport.FromDate + "' AND LastCheckDate <= '" + InfoForReport.ToDate + "'";
                        }

                        mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                        mySQLReader = mySQLCommand.ExecuteReader();

                        while (mySQLReader.Read())//Loop through all the results and add them to the DataGrid0DataCopy object
                        {

                            DataGrid0DataCopy.Add(new ReportsFormDataGrid0DataResultTransferItem());

                            DateTime LastCheckDateInDateTime = Convert.ToDateTime(mySQLReader.GetString("LastCheckDate"));

                            //Report Information from receivevehicle
                            DataGrid0DataCopy[DataGrid0DataCopy.Count - 1].CarCheckID = mySQLReader.GetInt32("CarCheckID");
                            DataGrid0DataCopy[DataGrid0DataCopy.Count - 1].CarNumber = mySQLReader.GetInt32("CarNumber");
                            DataGrid0DataCopy[DataGrid0DataCopy.Count - 1].FuelTypeID = mySQLReader.GetInt32("FuelType");
                            DataGrid0DataCopy[DataGrid0DataCopy.Count - 1].LastCheckDate = LastCheckDateInDateTime.ToString("dd-MM-yyyy");//Insert into db as the date format that the user would want to see
                            DataGrid0DataCopy[DataGrid0DataCopy.Count - 1].CarTypeID = mySQLReader.GetString("CarType");
                            DataGrid0DataCopy[DataGrid0DataCopy.Count - 1].TestTypeID = mySQLReader.GetInt32("TestType");
                            DataGrid0DataCopy[DataGrid0DataCopy.Count - 1].CarOwnerID = mySQLReader.GetString("CarOwnerID");
                            DataGrid0DataCopy[DataGrid0DataCopy.Count - 1].CarCheckFinal = mySQLReader.GetInt32("CarCheckFinal");
                            DataGrid0DataCopy[DataGrid0DataCopy.Count - 1].CarOwnerName = mySQLReader.GetString("CarOwnerName");
                            DataGrid0DataCopy[DataGrid0DataCopy.Count - 1].ShildaNumber = mySQLReader.GetString("CarShilda");
                            DataGrid0DataCopy[DataGrid0DataCopy.Count - 1].NumberOfAttempts = mySQLReader.GetInt32("NumOfTries");
                            DataGrid0DataCopy[DataGrid0DataCopy.Count - 1].EngineNumber = mySQLReader.GetString("EngineNumber");
                            DataGrid0DataCopy[DataGrid0DataCopy.Count - 1].CarLBL = mySQLReader.GetString("CarLBL");

                        }


                        mySQLReader.Close();

                        #endregion


                        foreach (ReportsFormDataGrid0DataResultTransferItem CarTestListReportEntry in DataGrid0DataCopy)
                        {

                            #region TestType Report Info


                            query = "SELECT * from moshech_aac.vehicletypes WHERE ID = " + CarTestListReportEntry.CarTypeID + "";
                            mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                            mySQLReader = mySQLCommand.ExecuteReader();

                            mySQLReader.Read();

                            CarTestListReportEntry.CarType = mySQLReader.GetString("VehicleType");

                            mySQLReader.Close();

                            #endregion

                            #region FuelType Report Info


                            query = "SELECT * from moshech_aac.fueltype WHERE ID = " + CarTestListReportEntry.FuelTypeID + "";
                            mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                            mySQLReader = mySQLCommand.ExecuteReader();

                            mySQLReader.Read();

                            CarTestListReportEntry.FuelType = mySQLReader.GetString("FuelType");

                            mySQLReader.Close();

                            #endregion

                        }

                    }

                    catch (MySqlException) { }

                    mySQLDBConnection.Close();
                    SessionID = this.SessionID;
                    DataGrid0Data.GridData = DataGrid0DataCopy;//Finally, set the object that holds all of the grid's data to the local object of that type that was created here
                    return (DataGrid0Data);

                    break;

            }
            //cases FOR OTHER REPORTS!

            SessionID = this.SessionID;
            return (null);
        }




        #endregion

        #region Reports Print Form

        //All the data for the reports that will be displayed on the ReportsPrintForm!

        #region Report Request Handler

        //Transfer information according to which report was requested
        [HandleTypeDef(typeof(ReportRequestTransferItem))]
        private Collection<TransferItem> HandleReportRequestTransferItem(TransferItem tItem)
        {

            //This transfer item contains the date range to select data from the mysql db as well as the report wanted (represented by an int)
            ReportRequestTransferItem InfoForReport = (ReportRequestTransferItem)tItem;

            /*
             * VERY IMPORTANT!
             *
             *The ReportsPrintForm is the form that calls this function. It passes the following report info:
             *
             *FromDate 
             *ToDate
             *RequestedReport - The ID that represents the report requested by the user
             *
             * Down below there is a switch statement that contains cases for each report. However, some of these reports share a common mysql DB table such as ReportIDs: 0,2,5
             * Because they share the same variables, we are able to use the same object to store information for those reports. So for example: ReportIDs: 0,2,5 share the object: TestReportTableData
             * to store all the data. 
             * 
             * NOTE: The data is the same BUT the reports themselves are different!!!
             */

            mySQLDBConnection.Open();

            switch (InfoForReport.RequestedReport)
            {

                #region דוחות:  בדיקות רישוי לפי תאריכים

                case "בדיקות רישוי לפי תאריכים":

                    try
                    {

                        TestsForPeriodResultTransferItem TestsForPeriod = new TestsForPeriodResultTransferItem();

                        //DateTime conversion for the "From" & "To" date strings
                        DateTime FromDateInDateTime = Convert.ToDateTime(InfoForReport.FromDate);
                        DateTime ToDateInDateTime = Convert.ToDateTime(InfoForReport.ToDate);

                        //Settings the strings = to a "yyyy-MM-dd" format version of them
                        InfoForReport.FromDate = FromDateInDateTime.ToString("yyyy-MM-dd");
                        InfoForReport.ToDate = ToDateInDateTime.ToString("yyyy-MM-dd");

                        string CurrentDBDate = "";//This holds the latest date from a db LastCheckDate field. This is used to compare the next db entry to see if it's the same date or not
                        //If it is then just add to the same list entry, because each entry in the report's table is based on the total tests for that same date!
                        //If it is NOT the same date then add a new list entry because we need the data for this new date

                        //Why the f doesnt the date range work?
                        //NOTE: This query is different because it is ordered by lastcheckdate, the reason for this is that we compare a string that contains the last "lastcheckdate" from the db to the one after, so the dates HAVE to be in order, otherwise the list that 
                        //contains the data for the dates will get a lot of identical date entries in it
                        query = "SELECT * from moshech_aac.receivevehicle WHERE LastCheckDate >= '" + InfoForReport.FromDate + "' AND LastCheckDate <= '" + InfoForReport.ToDate + "' ORDER BY LastCheckDate";

                        mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                        mySQLReader = mySQLCommand.ExecuteReader();

                        while (mySQLReader.Read())
                        {

                            if (!(mySQLReader.GetString("LastCheckDate")).Equals(CurrentDBDate))//If the dates are not equal then:
                            {

                                TestsForPeriod.TestsForPeriodsEntriesList.Add(new TestsForPeriodResultTransferItem());//Add a new entry for a new date 

                                CurrentDBDate = mySQLReader.GetString("LastCheckDate");//Change the current date string to this entrie's current date

                                DateTime LastCheckDateInDateTime = Convert.ToDateTime(mySQLReader.GetString("LastCheckDate"));

                                //Set the DateOfTests property to the current db entry date(just like the CurrentDBDate string) BUT IN THE DATE FORMAT THE USER WANTS TO SEE (dd-MM-yyyy)
                                TestsForPeriod.TestsForPeriodsEntriesList[TestsForPeriod.TestsForPeriodsEntriesList.Count - 1].DateOfTests = LastCheckDateInDateTime.ToString("dd-MM-yyyy");

                            }


                            //Report Information 

                            if (mySQLReader.GetInt32("NumOfTries") == 1)//If the person tested passed the test on the first attempt then increment the PassedFirstTry property
                            {
                                TestsForPeriod.TestsForPeriodsEntriesList[TestsForPeriod.TestsForPeriodsEntriesList.Count - 1].PassedFirstTry++;
                            }

                            else//The person passed AFTER the first try
                            {
                                TestsForPeriod.TestsForPeriodsEntriesList[TestsForPeriod.TestsForPeriodsEntriesList.Count - 1].PassedAfterFirstTry++;
                            }

                            //Increment the total tests every time
                            TestsForPeriod.TestsForPeriodsEntriesList[TestsForPeriod.TestsForPeriodsEntriesList.Count - 1].TotalTests++;

                        }

                        mySQLReader.Close();

                        SessionID = this.SessionID;

                        return (TestsForPeriod);//Return this report's data

                    }

                    catch (MySqlException) { }

                    finally { mySQLDBConnection.Close(); }

                    break;

                #endregion

            }

            //OTHER REPORT CASES
            SessionID = this.SessionID;
            return (null);

        }

        #endregion

        #endregion

        #endregion

        #endregion

        #region ReceiveVehicle Form Handlers

        #region ReceiveVehicle Load Global Settings Handler
        [HandleTypeDef(typeof(LoadGlobalDataRequestTransferItem))]
        private Collection<TransferItem> HandleLoadGlobalDataRequestTransferItem(TransferItem tItem)
        {

            LoadGlobalDataResultTransferItem SendGlobalSettings = new LoadGlobalDataResultTransferItem();

            try
            {

                mySQLDBConnection.Open();

                #region Load Car Manufacturer ComboBox
                //Populates the car manu combobox
                query = "select * from moshech_aac.carmanufacturers ORDER BY ManufacturerNameHeb";//The manufacturers are going to be sorted by hebrew!!! Also note: the icons for them are sorted in the hebrew order!!!
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                while (mySQLReader.Read())
                {
                    SendGlobalSettings.CarManufacturerNames.Add(mySQLReader.GetInt32("MOTManufacturerID"), mySQLReader.GetString("ManufacturerNameHeb") + " - " + mySQLReader.GetString("ManufacturerNameEng"));//Set the names in the CB
                    SendGlobalSettings.CarManuOrder.Add(mySQLReader.GetInt32("MOTManufacturerID"));//Adding the model number to our order list to later sort out so we can add all the models for that corresponding ID
                }

                SendGlobalSettings.CarManuOrder = SendGlobalSettings.CarManuOrder.Distinct().ToList();//Sort the list Into one with unique ID so we can easily loop through below

                mySQLReader.Close();

                #endregion

                #region Load Car Models

                List<string> CarModelsList = new List<string>();

                foreach (int UniqueID in SendGlobalSettings.CarManuOrder)
                {

                    query = "select * from moshech_aac.carmodels WHERE MOTManufacturerID = " + UniqueID + "";//Read where we have a unique ID
                    mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                    mySQLReader = mySQLCommand.ExecuteReader();

                    if (!(mySQLReader.HasRows)) { CarModelsList.Add(""); }
                    /*VERY IMPORTANT!!! This condition is for making sure we dont try to read Null DB entries. The way it works is that we base this loop on the carmanufacturer table 
                     * which has all the manufacturer's IDs. So what happens when we look for a model for that manufacturer ID when there is no existant model in the carmodel table?
                     * We just skip it and leave the models blank for that manufacturer, otherwise the program will crash.
                    */
                    else
                    {

                        while (mySQLReader.Read())//Second loop for going to the next value for the same manufacturer ID in the table (example: ID 11 shows up 3 times then we run this loop 3 times for that value)
                        {
                            CarModelsList.Add(mySQLReader.GetString("CarModelName"));
                        }
                    }

                    SendGlobalSettings.CarModelDictionary.Add(UniqueID, CarModelsList);//Finally add a dictionary entry and the list of string names to it
                    CarModelsList = new List<string>();
                    mySQLReader.Close();
                }//Repeat

                #region CarModelIDName Dictionary
                //Add the entries for the dictionary that has a model code and model string

                query = "select * from moshech_aac.carmodels";
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                while (mySQLReader.Read())
                {
                    SendGlobalSettings.CarModelIDNameDictionary.Add(mySQLReader.GetString("ModelCode"), mySQLReader.GetString("CarModelName"));
                }


                mySQLReader.Close();

                #endregion

                #endregion

                #region Load Vehicle Types

                query = "select * from moshech_aac.vehicletypes";//Read where we have a unique ID
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                while (mySQLReader.Read())
                {
                    SendGlobalSettings.VehicleTypeDictionary = VehicleTypeDictionary;
                }

                mySQLReader.Close();

                #endregion

                #region Load Car Colors

                query = "select * from moshech_aac.carcolors";//Read where we have a unique ID
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                while (mySQLReader.Read()) { SendGlobalSettings.CarColorDictionary.Add(mySQLReader.GetInt32("ColorID"), mySQLReader.GetString("Color")); }

                mySQLReader.Close();

                #endregion

                #region Load Fuel Types

                query = "select * from moshech_aac.fueltype";//Read where we have a unique ID
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                while (mySQLReader.Read())
                {
                    SendGlobalSettings.FuelTypesDictionary = FuelTypesDictionary;
                }

                mySQLReader.Close();

                #endregion

                #region Load Test Types

                query = "select * from moshech_aac.testtype";//Read where we have a unique ID
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                while (mySQLReader.Read())
                {
                    SendGlobalSettings.TestTypesDictionary = TestTypeDictionary;
                }

                mySQLReader.Close();

                #endregion
            }

            catch (Exception e) { SendGlobalSettings.ErrorMessage = e.ToString(); }

            finally
            {
                mySQLReader.Close();
                mySQLDBConnection.Close();
                SessionID = this.SessionID;
            }

            return (SendGlobalSettings);
        }

        #endregion

        #region ReceiveVehicle Car Number Check Handler

        [HandleTypeDef(typeof(CarNumberCheckRequestTransferItem))]
        private Collection<TransferItem> HandleCarNumberCheckRequestTransferItem(TransferItem tItem)
        {

            //Checks to see if the car number that was requested from the client exists in the DB. 
            /*
             * The following responses can return:
             * 
             * 1."New" - The vehicle does not exist in the DB 
             * 2."Passed" - The vehicle exists in the DB and has passed a test before
             * 3."Failed" - The vehicle exists in the DB and has failed a test
             */

            CarNumberCheckRequestTransferItem NumberCheckRequest = (CarNumberCheckRequestTransferItem)tItem;
            CarNumberCheckResultTransferItem NumberCheckResult = new CarNumberCheckResultTransferItem();

            try
            {

                mySQLDBConnection.Open();
                query = "select * from moshech_aac.receivevehicle";
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                NumberCheckResult.TestResult = "New";//By default, this item is set to a new test. If the car number is found in the loop below then it can either pass or fail but if the number is not found in the db/online then we simply keep this value of new

                while (mySQLReader.Read())
                {

                    if (NumberCheckRequest.CarNumber == mySQLReader.GetInt32("CarNumber"))//If the car number entered exists in our DB
                    {

                        if (mySQLReader.GetInt32("CarCheckFinal") == 1) { NumberCheckResult.TestResult = "Passed"; }//Passed
                        if (mySQLReader.GetInt32("CarCheckFinal") == 0) { NumberCheckResult.TestResult = "Failed"; }//Failed

                    }

                }

            }

            catch (Exception) { }

            finally
            {

                mySQLReader.Close();
                mySQLDBConnection.Close();
                SessionID = this.SessionID;

            }


            return NumberCheckResult;

        }


        #endregion

        #region ReceiveVehicle Car Number Data Request Handler

        [HandleTypeDef(typeof(CarNumberDataRequestTransferItem))]
        private Collection<TransferItem> HandleCarNumberDataRequestTransferItem(TransferItem tItem)
        {

            //Includes functions for starting a new test using the online system(CarData) or loading a vehicle from the local database and getting the data for the vehicle requested

            CarNumberDataRequestTransferItem CarNumberDataRequest = (CarNumberDataRequestTransferItem)tItem;
            CarNumberDataResultTransferItem CarNumberDataResult = new CarNumberDataResultTransferItem();

            switch (CarNumberDataRequest.RequestType)
            {

                #region Online Functions (CarData)

                case "New Online":
                case "Input Vehicle Permits":

                    #region Foundation for all CarData function calls

                    //Information for CarData
                    CarDataVehicle.misparRechev = CarNumberDataRequest.CarNumber;
                    CarDataVehicle.tzBaal = CarNumberDataRequest.CarOwnerID;
                    CarDataVehicle.tzMeyupe = CarNumberDataRequest.PriviledgedID;

                    CDWS.CarData CarData = new CDWS.CarData();

                    #endregion

                    #region On-Line(CarData) "New Online" Check --GetVehicleDetailsPriorAnnualTest--

                    //The check request is a "new online" check(annual test, etc...)
                    if (CarNumberDataRequest.RequestType.Equals("New Online"))
                    {

                        //create the web service interface itself
                        //call the function to get flaw table and put it into a CDWS.FlawTable object
                        CDWS.VehicleDetails CarDataVehicleDetails = new VehicleDetails();
                        CarDataVehicleDetails = CarData.GetVehicleDetailsPriorAnnualTest(CarDataLogin, CarDataID, CarDataVehicle, DateTime.Today);//Call the function that returns all information about the vehicle we have asked
                        CarNumberDataResult.CarDataResponse = new CarDataResponse();

                        if (CarDataVehicleDetails.msgNr == 1)//If the car number was found in CarData
                        {

                            //Indicate that the response from CarData is NOT an error
                            CarNumberDataResult.CarDataResponse.ResponseMessage = CarDataVehicleDetails.msgText;
                            CarNumberDataResult.CarDataResponse.IsResponseError = false;

                            CarNumberDataResult.CarNumber = CarNumberDataRequest.CarNumber;
                            CarNumberDataResult.CarMakerID = CarDataVehicleDetails.semelTozar;
                            CarNumberDataResult.CarMaker = CarDataVehicleDetails.tozar;
                            CarNumberDataResult.CarModel = CarDataVehicleDetails.degem;
                            CarNumberDataResult.FuelType = Convert.ToInt32(CarDataVehicleDetails.kodSugDelek);
                            CarNumberDataResult.TotalWeight = CarDataVehicleDetails.mishkalKolel;
                            CarNumberDataResult.CarShilda = CarDataVehicleDetails.misparMisgeret;
                            CarNumberDataResult.CarKm = CarDataVehicleDetails.kriatKm;
                            CarNumberDataResult.EngineNumber = CarDataVehicleDetails.misparManoa;
                            CarNumberDataResult.EngineModel = CarDataVehicleDetails.degemManoa;
                            CarNumberDataResult.EngineVolume = CarDataVehicleDetails.nefachManoa;
                            CarNumberDataResult.CarCheckFinal = 1;
                            CarNumberDataResult.CarFrontTire = CarDataVehicleDetails.zmigKidmi.tire;
                            CarNumberDataResult.CarRearTire = CarDataVehicleDetails.zmigAchori.tire;
                            CarNumberDataResult.CarColor = CarDataVehicleDetails.zeva.code;
                            CarNumberDataResult.CarOwnerID = CarNumberDataRequest.CarOwnerID;
                            CarNumberDataResult.CarOwnerName = CarDataVehicleDetails.shemBaal;
                            CarNumberDataResult.CarYear = CarDataVehicleDetails.shnatYizur.ToString();
                            CarNumberDataResult.PriviledgedName = CarDataVehicleDetails.shemMeyupe;

                            if (CarDataVehicleDetails.vavGrira.Equals("None")) { CarNumberDataResult.VavGrira = 0; }
                            if (CarDataVehicleDetails.vavGrira.Equals("Yes")) { CarNumberDataResult.VavGrira = 1; }

                            //If the AWD is not 0(no drive) or 1(4X2) then אין הנעה קבועה
                            if (CarDataVehicleDetails.kodHanaa.Equals("0") || CarDataVehicleDetails.kodHanaa.Equals("1")) { CarNumberDataResult.Awd = 1; }

                            //יש הנעה קבועה
                            else { CarNumberDataResult.Awd = 0; }

                            //Getting required INITIAL permits
                            CarNumberDataResult.CarDataNeededPermitList = new List<CarDataNeededPermit>();

                            foreach (NeededPermit PermitNeeded in CarDataVehicleDetails.needePermits)//Loop through all required permits returned by Car Data(these are initial permits only because this is after calling "GetVehicleDetailPriorAnnualTest"
                            {

                                //Add permit properties from CarData to our list 
                                CarNumberDataResult.CarDataNeededPermitList.Add(new CarDataNeededPermit
                                {

                                    Code = PermitNeeded.code,
                                    Description = PermitNeeded.desc,
                                    GarageApprovalNeeded = PermitNeeded.garageApprovalNeeded,
                                    ApprovalDateNeeded = PermitNeeded.approvalDateNeeded,

                                });

                            }
                        }
                        //A response other than "1" has been returned, AKA: this is an error response
                        else
                        {
                            //Set the Car Data error message text so we can display it to the user on the client side
                            CarNumberDataResult.CarDataResponse.ResponseMessage = CarDataVehicleDetails.msgText;
                            //Indicate that this is an error(true)
                            CarNumberDataResult.CarDataResponse.IsResponseError = true;
                            return (CarNumberDataResult);
                        }

                    }

                    #endregion

                    #region On-Line(CarData) "Input Initial Permits" --InputVehiclePermits--

                    if (CarNumberDataRequest.RequestType.Equals("Input Vehicle Permits"))
                    {



                    }

                    break;

                    #endregion

                #endregion

                #region Offline Functions

                case "Load":

                    #region Off-Line "Load"

                    //This section loads an existing vehicle from the DB for the user to see on the form. This is regardless of if the user requested this car number via the online or offline option(no need to call cardata here)
                    if (CarNumberDataRequest.RequestType.Equals("Load Offline"))
                    {

                        //Close the reader and connection to prepare to read data from the DB below
                        mySQLReader.Close();
                        mySQLDBConnection.Close();

                        try
                        {

                            mySQLDBConnection.Open();
                            query = "select * from moshech_aac.receivevehicle";
                            mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                            mySQLReader = mySQLCommand.ExecuteReader();

                            while (mySQLReader.Read())
                            {

                                if (CarNumberDataRequest.CarNumber == mySQLReader.GetInt32("CarNumber"))//If the car number entered exists in our DB
                                {

                                    //Fill in the info for the requested vehiclew=
                                    CarNumberDataResult.CarCheckID = mySQLReader.GetInt32("CarCheckID");
                                    CarNumberDataResult.CarNumber = mySQLReader.GetInt32("CarNumber");
                                    CarNumberDataResult.CarMakerID = mySQLReader.GetInt32("CarManuFacturerID");
                                    CarNumberDataResult.CarMaker = mySQLReader.GetString("CarMaker");
                                    CarNumberDataResult.CarModelID = mySQLReader.GetInt32("CarModelID");
                                    CarNumberDataResult.CarModel = mySQLReader.GetString("CarModel");
                                    CarNumberDataResult.FuelType = mySQLReader.GetInt32("FuelType");
                                    CarNumberDataResult.TotalWeight = mySQLReader.GetInt32("TotalWeight");
                                    CarNumberDataResult.CarShilda = mySQLReader.GetString("CarShilda");
                                    CarNumberDataResult.CarKm = mySQLReader.GetInt32("CarKm");
                                    CarNumberDataResult.CarKmStatus = mySQLReader.GetInt32("CarKmStatus");
                                    CarNumberDataResult.EngineNumber = mySQLReader.GetString("EngineNumber");//CHANGE TO STRING
                                    CarNumberDataResult.EngineModel = mySQLReader.GetString("EngineModel");
                                    CarNumberDataResult.EngineVolume = mySQLReader.GetInt32("EngineVolume");
                                    CarNumberDataResult.CarFrontTire = mySQLReader.GetString("CarFrontTire");
                                    CarNumberDataResult.CarRearTire = mySQLReader.GetString("CarRearTire");
                                    CarNumberDataResult.CarColor = mySQLReader.GetInt32("CarColor");//Can be negative!
                                    CarNumberDataResult.NumOfSeren = mySQLReader.GetInt32("NumOfSeren");
                                    CarNumberDataResult.VavGrira = mySQLReader.GetInt32("VavGrira");
                                    CarNumberDataResult.Awd = mySQLReader.GetInt32("Awd");
                                    CarNumberDataResult.CarOwnerID = mySQLReader.GetInt32("CarOwnerID");
                                    CarNumberDataResult.CarOwnerName = mySQLReader.GetString("CarOwnerName");
                                    CarNumberDataResult.LicenseCode = mySQLReader.GetString("LicenseCode");
                                    CarNumberDataResult.Phone1 = mySQLReader.GetString("Phone1");
                                    CarNumberDataResult.Phone2 = mySQLReader.GetString("Phone2");
                                    CarNumberDataResult.CarYear = mySQLReader.GetString("CarYear");
                                    CarNumberDataResult.PriviledgedID = mySQLReader.GetString("PriviledgedID");
                                    CarNumberDataResult.PriviledgedName = mySQLReader.GetString("PriviledgedName");
                                    CarNumberDataResult.OpenTestDateTime = mySQLReader.GetString("OpenTestDateTime");
                                    CarNumberDataResult.TesterName = mySQLReader.GetString("TesterName");
                                    CarNumberDataResult.TestType = mySQLReader.GetInt32("TestType");
                                    CarNumberDataResult.LastStation = mySQLReader.GetInt32("LastStation");
                                    CarNumberDataResult.VehicleType = mySQLReader.GetInt32("CarType");
                                    CarNumberDataResult.CarCheckFinal = mySQLReader.GetInt32("CarCheckFinal");

                                }

                            }

                        }

                        catch (MySqlException) { }

                        finally
                        {

                            mySQLReader.Close();
                            mySQLDBConnection.Close();
                            SessionID = this.SessionID;

                        }
                    }
                    #endregion


                    break;

                #endregion

            }

            return (CarNumberDataResult);


        }
        #endregion

        #region ReceiveVehicle Engine List for Car Model Handler

        [HandleTypeDef(typeof(EngineModelsForCarModelRequestTransferItem))]
        private Collection<TransferItem> HandleEngineModelsForCarModelRequestTransferItem(TransferItem tItem)
        {

            EngineModelsForCarModelRequestTransferItem EnginesRequestForCar = (EngineModelsForCarModelRequestTransferItem)tItem;
            EngineModelsForCarModelResultTransferItem EnginesResultForCar = new EngineModelsForCarModelResultTransferItem();

            try
            {

                mySQLDBConnection.Open();

                //Find the engine models for the specific car model requested
                query = "select * from moshech_aac.enginedata WHERE CarModelID LIKE '%" + EnginesRequestForCar.CarModelID + "%'";//Read the manufacturer ID field uniquely because we also have that field in enginedata
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                EnginesResultForCar.EngineModelList = new List<string>();

                //Add all the engine names to the list for that specific car model requested
                while (mySQLReader.Read()) { EnginesResultForCar.EngineModelList.Add(mySQLReader.GetString("EngineModel")); }

            }

            catch (MySqlException) { }

            finally
            {
                mySQLDBConnection.Close();
                mySQLReader.Close();
            }

            SessionID = this.SessionID;
            return (EnginesResultForCar);

        }


        #endregion

        #region ReceiveVehicle Save Car Handler

        [HandleTypeDef(typeof(SaveCarRequestTransferItem))]
        private Collection<TransferItem> HandleSaveCarRequestTransferItem(TransferItem tItem)
        {

            SaveCarRequestTransferItem SaveCar = (SaveCarRequestTransferItem)tItem;
            SaveCarResultTransferItem SaveResponse = new SaveCarResultTransferItem();

            mySQLDBConnection.Open();

            //Check for tests that are still open!(If a test is open for the same car number then don't save them!)

            query = "select * from moshech_aac.receivevehicle where CarNumber= " + SaveCar.CarNumber + "";
            mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
            mySQLReader = mySQLCommand.ExecuteReader();
            mySQLReader.Read();

            if (mySQLReader.HasRows)
            {

                if (SaveCar.CarNumber == mySQLReader.GetInt32("CarNumber") && mySQLReader.GetInt16("IsCheckClosedOpen") == 0)
                {
                    SaveResponse.ResponseMessage = "לרכב זה קיימת בדיקה פעילה! נא וודא שהבדיקה הקודמת נסגרה";

                    mySQLDBConnection.Close();

                    return SaveResponse;
                }
            }

            mySQLReader.Close();

            try
            {

                query = "insert into moshech_aac.receivevehicle(CarCheckID, CarNumber, CarMaker, CarManufacturerID, CarModelID, CarModel, FuelType, TotalWeight, CarShilda, CarKm, CarKmStatus, EngineNumber, EngineModel, EngineVolume, CarFrontTire, CarRearTire, CarColor, NumOfSeren, VavGrira, Awd, CarOwnerID, CarOwnerName, LicenseCode, Phone1, Phone2, CarYear, CarMonth, PriviledgedID, PriviledgedName, OpenTestDateTime, TesterName, TestType, CarType, LastStation, IsRecOnline, Transmitted, CloseTestDateTime, CarCheckFinal, IncMaxLambda,IncMinLambda, IncMaxCo, IncMaxRpm, IncMinRpm, IdleMaxCo, OilTempMax, IdleSpeedMax, IdleSpeedMin, MaxK ,IdleSpeedMaxHC, NoLoadSpeedMin, NoLoadSpeedMax, InitialVehiclePermits) values('"
                + SaveCar.CarCheckID + "','"
                + SaveCar.CarNumber + "','"
                + SaveCar.CarMaker + "','"
                + SaveCar.CarManufacturerID + "','"
                + SaveCar.CarModelID + "','"
                + SaveCar.CarModel + "','"
                + SaveCar.FuelType + "','"
                + SaveCar.TotalWeight + "','"
                + SaveCar.CarShilda + "','"
                + SaveCar.CarKm + "','"
                + SaveCar.CarKmStatus + "','"
                + SaveCar.EngineNumber + "','"
                + SaveCar.EngineModel + "','"
                + SaveCar.EngineVolume + "','"
                + SaveCar.CarFrontTire + "','"
                + SaveCar.CarRearTire + "','"
                + SaveCar.CarColor + "','"
                + SaveCar.NumOfSeren + "','"
                + SaveCar.VavGrira + "','"
                + SaveCar.Awd + "','"
                + SaveCar.CarOwnerID + "','"
                + SaveCar.CarOwnerName + "','"
                + SaveCar.LicenseCode + "','"
                + SaveCar.Phone1 + "','"
                + SaveCar.Phone2 + "','"
                + SaveCar.CarYear + "','"
                + SaveCar.CarMonth + "','"
                + SaveCar.PriviledgedID + "','"
                + SaveCar.PriviledgedName + "','"
                + SaveCar.OpenTestDateTime + "','"
                + SaveCar.TesterName + "','"
                + SaveCar.TestType + "','"
                + SaveCar.CarType + "','"
                + SaveCar.LastStation + "','"
                + SaveCar.IsRecOnline + "','"
                + SaveCar.Transmitted + "','"
                + SaveCar.CloseTestDateTime + "','"
                + SaveCar.CarCheckFinal + "','"
                + SaveCar.IncMaxLambda + "','"
                + SaveCar.IncMinLambda + "','"
                + SaveCar.IncMaxCo + "','"
                + SaveCar.IncMaxRpm + "','"
                + SaveCar.IncMinRpm + "','"
                + SaveCar.IdleMaxCo + "','"
                + SaveCar.OilTempMax + "','"
                + SaveCar.IdleSpeedMax + "','"
                + SaveCar.IdleSpeedMin + "','"
                + SaveCar.MaxK + "','"
                + SaveCar.IdleSpeedMaxHC + "','"
                + SaveCar.NoLoadSpeedMin + "','"
                + SaveCar.NoLoadSpeedMin + "','"
                + SaveCar.InitialVehiclePermitListInJSon + "')";

                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();
                mySQLReader.Close();

                SaveResponse.ResponseMessage = "רכב נקלט בהצלחה. לחץ אישור להדפסת כרטיס פעילות";
            }
            catch (MySqlException e) { SaveResponse.ResponseMessage = e.ToString(); }//Error

            finally
            {
                mySQLDBConnection.Close();
                mySQLReader.Close();
                SessionID = this.SessionID;
            }

            return (SaveResponse);
        }
        #endregion

        #region ReceiveVehicle Report Related

        #region ReceiveVehicle Save Report

        //Gets a Report Request from the Client for the specified car number. Also gets the report requested to be saved
        [HandleTypeDef(typeof(SaveReceiveVehicleReportRequestTransferItem))]
        private Collection<TransferItem> HandleSaveReceiveVehicleReportRequestTransferItem(TransferItem tItem)
        {

            SaveReceiveVehicleReportRequestTransferItem SaveReport = (SaveReceiveVehicleReportRequestTransferItem)tItem;
            int CarCheckIDForEndReport = 0;//Holds the CarCheckID for this report.

            //Save Begin Report
            if (SaveReport.ReportToSave == 0)
            {

                #region Begin Report Save

                #region ReceiveVehicle Report Table Vars

                //Used to store all the information fetched from the DB that is relevant for this report

                #region ReceiveVehicle Table Vars

                int TestTypeID = 0;//ID to look for in TestType table!
                int FuelTypeID = 0;//ID to look for in FuelType table!
                int CarManufacturerID = 0;//ID to look for in Carmanufacturer table!
                int CarTypeID = 0;//ID to look for in VehicleType table!

                string CarModel = "";
                int CarCheckIDRecwiveVehicle = 0;//Holds the carcheckid from the rec vehicle table
                string CarNumber = "";
                string CarOwnerName = "";
                int CarOwnerID = 0;
                string CarShilda = "";
                string EngineNumber = "";
                string PriviledgedName = "";
                string PriviledgedID = "";
                string TesterName = "";
                int CarKm = 0;
                string OpenTestDateTime = "";

                #endregion

                #region VehicleType Table Vars

                string CarType = "";

                #endregion

                #region FuelType Table Vars

                string FuelType = "";

                #endregion

                #region Carmanufacturer Table Vars

                string CarManufacturerNameHeb = "";

                #endregion

                #region TestType Table Vars

                string TestType = "";

                #endregion

                #endregion

                #region ReceiveVehicle Table Data
                //VERY IMPORTANT TABLE: Information is fetched according to the car number inserted. All of the data for that car will be stored locally. Some of this data is used to 
                //bridge out to other data tables involved here to get the relevant information. For example: FuelType of Car# 2222222 is 1. 
                //That "1" value will be used to look at the fueltype table in another query so we can know what 1 represents so the end result will be: FuelType of Car#222222 is סולר
                try
                {

                    mySQLDBConnection.Open();
                    query = "select * from moshech_aac.receivevehicle WHERE CarNumber = " + CarNumberDeformat(SaveReport.CarNumber) + "";
                    mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                    mySQLReader = mySQLCommand.ExecuteReader();

                    mySQLReader.Read();

                    CarNumber = SaveReport.CarNumber;
                    CarCheckIDRecwiveVehicle = mySQLReader.GetInt32("CarCheckID");
                    CarOwnerName = mySQLReader.GetString("CarOwnerName");
                    CarOwnerID = mySQLReader.GetInt32("CarOwnerID");
                    CarTypeID = mySQLReader.GetInt32("CarType");
                    CarShilda = mySQLReader.GetString("CarShilda");
                    EngineNumber = mySQLReader.GetString("EngineNumber");
                    PriviledgedName = mySQLReader.GetString("PriviledgedName");
                    PriviledgedID = mySQLReader.GetString("PriviledgedID");
                    TesterName = mySQLReader.GetString("TesterName");
                    CarKm = mySQLReader.GetInt32("CarKm");
                    CarManufacturerID = mySQLReader.GetInt32("CarManufacturerID");
                    CarModel = mySQLReader.GetString("CarModel");
                    OpenTestDateTime = mySQLReader.GetString("OpenTestDateTime");

                }

                catch (MySqlException) { }

                mySQLReader.Close();

                #endregion

                #region VehicleType Table Data

                try
                {

                    query = "select * from moshech_aac.vehicletypes WHERE ID = " + CarTypeID + "";
                    mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                    mySQLReader = mySQLCommand.ExecuteReader();

                    mySQLReader.Read();

                    CarType = mySQLReader.GetString("CodeName");

                }

                catch (MySqlException) { }

                mySQLReader.Close();

                #endregion

                #region Carmanufacturers Table Data

                try
                {

                    query = "select * from moshech_aac.carmanufacturers WHERE MOTManufacturerID = " + CarManufacturerID + "";
                    mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                    mySQLReader = mySQLCommand.ExecuteReader();

                    mySQLReader.Read();

                    CarManufacturerNameHeb = mySQLReader.GetString("ManufacturerNameHeb");

                }

                catch (MySqlException) { }

                mySQLReader.Close();

                #endregion

                #region TestType Table Data

                try
                {

                    query = "select * from moshech_aac.testtype WHERE ID = " + TestTypeID + "";
                    mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                    mySQLReader = mySQLCommand.ExecuteReader();

                    mySQLReader.Read();

                    TestType = mySQLReader.GetString("TestType");

                }

                catch (MySqlException) { }

                mySQLReader.Close();

                #endregion

                #region FuelType Table Data

                try
                {

                    query = "select * from moshech_aac.fueltype WHERE ID = " + FuelTypeID + "";
                    mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                    mySQLReader = mySQLCommand.ExecuteReader();

                    mySQLReader.Read();

                    FuelType = mySQLReader.GetString("FuelType");

                }

                catch (MySqlException) { }

                mySQLReader.Close();

                #endregion

                #region Save Report to CarBeginReportTable

                try
                {
                    //add car model
                    query = "insert Into moshech_aac.carbeginreport(CarCheckID, CarNumber, CarOwnerName, CarOwnerID, CarType, TestType, Carmanufacturer, CarModel, TesterName, PriviledgedName, PriviledgedID, CarKm, FuelType, OpenTestDateTime, DateTimeOfOriginalPrint, IsFirstCopy, CarShilda, EngineNumber) values('"
                                        + CarCheckIDRecwiveVehicle + "','"
                                        + CarNumber + "','"
                                        + CarOwnerName + "','"
                                        + CarOwnerID + "','"
                                        + CarType + "','"
                                        + TestType + "','"
                                        + CarManufacturerNameHeb + "','"
                                        + CarModel + "','"
                                        + TesterName + "','"
                                        + PriviledgedName + "','"
                                        + PriviledgedID + "','"
                                        + CarKm + "','"
                                        + FuelType + "','"
                                        + OpenTestDateTime + "','"
                                        + DateTime.Now.ToString() + "','"//For the original copy! Set the datetime of NOW
                                        + 1 + "','"//Is first copy = true. This part of the code only runs if a car has never been added to the table
                                        + CarShilda + "','"
                                        + EngineNumber + "')";

                    mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                    mySQLReader = mySQLCommand.ExecuteReader();

                    mySQLReader.Read();

                }

                catch (MySqlException) { }

                mySQLReader.Close();
                mySQLDBConnection.Close();

                #endregion

                #endregion

            }

            //Save End Report
            if (SaveReport.ReportToSave == 1)
            {

                #region End Report Save

                #region Table Vars

                #region ReceiveVehicle Table Vars

                int TestTypeID = 0;//ID to look for in TestType table!
                int CarManufacturerID = 0;//ID to look for in Carmanufacturer table!
                int CarTypeID = 0;//ID to look for in VehicleType table!
                string CarNumber = "";
                int NumOfTries = 0;
                int LastStation = 0;
                string EngineModel = "";
                int EngineVolume = 0;
                int CarKm = 0;
                int CarYear = 0;
                int Awd = 0;
                int CarCheckID = 0;
                string OpenTestDateTime = "";
                int FuelType = 0;

                #endregion

                #region VehicleType Table Vars

                string CarType = "";

                #endregion

                #region Carmanufacturer Table Vars

                string CarManufacturerNameHeb = "";

                #endregion

                #region TestType Table Vars

                string TestType = "";

                #endregion

                #region Emdadata Table Vars

                //Strings that hold all of the information on the different stations
                string IDStationDataInJson = "";
                string LightsStationDataInJson = "";
                string PitStationDataInJson = "";
                string SmokeStationDataInJson = "";
                string BrakesStationDataInJson = "";
                string WheelAlignmentStationDataInJson = "";

                string NameOfFirstTesterToSignIDStation = "";
                int IDOfFirstTesterToSignIDStation = 0;

                #endregion

                #endregion

                #region ReceiveVehicle Table Data
                //VERY IMPORTANT TABLE: Information is fetched according to the car number inserted. All of the data for that car will be stored locally. Some of this data is used to 
                //bridge out to other data tables involved here to get the relevant information. For example: FuelType of Car# 2222222 is 1. 
                //That "1" value will be used to look at the fueltype table in another query so we can know what 1 represents so the end result will be: FuelType of Car#222222 is סולר
                try
                {

                    mySQLDBConnection.Open();
                    query = "select * from moshech_aac.receivevehicle WHERE CarNumber = " + CarNumberDeformat(SaveReport.CarNumber) + "";
                    mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                    mySQLReader = mySQLCommand.ExecuteReader();

                    mySQLReader.Read();

                    CarCheckIDForEndReport = mySQLReader.GetInt32("CarCheckID");
                    TestTypeID = mySQLReader.GetInt32("TestType");
                    CarManufacturerID = mySQLReader.GetInt32("CarManufacturerID");
                    CarTypeID = mySQLReader.GetInt32("CarType");
                    CarNumber = SaveReport.CarNumber;
                    NumOfTries = mySQLReader.GetInt32("NumOfTries");
                    LastStation = mySQLReader.GetInt32("LastStation");
                    EngineModel = mySQLReader.GetString("EngineModel");
                    EngineVolume = mySQLReader.GetInt32("EngineVolume");
                    CarKm = mySQLReader.GetInt32("CarKm");
                    CarYear = mySQLReader.GetInt32("CarYear");
                    Awd = mySQLReader.GetInt32("Awd");
                    CarCheckID = mySQLReader.GetInt32("CarCheckID");
                    OpenTestDateTime = mySQLReader.GetString("OpenTestDateTime");
                    FuelType = mySQLReader.GetInt32("FuelType");

                }

                catch (MySqlException) { }

                mySQLReader.Close();

                #endregion

                #region VehicleType Table Data

                try
                {

                    query = "select * from moshech_aac.vehicletypes WHERE ID = " + CarTypeID + "";
                    mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                    mySQLReader = mySQLCommand.ExecuteReader();

                    mySQLReader.Read();

                    CarType = mySQLReader.GetString("CodeName");

                }

                catch (MySqlException) { }

                mySQLReader.Close();

                #endregion

                #region Carmanufacturers Table Data

                try
                {

                    query = "select * from moshech_aac.carmanufacturers WHERE MOTManufacturerID = " + CarManufacturerID + "";
                    mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                    mySQLReader = mySQLCommand.ExecuteReader();

                    mySQLReader.Read();

                    CarManufacturerNameHeb = mySQLReader.GetString("ManufacturerNameHeb");

                }

                catch (MySqlException) { }

                mySQLReader.Close();

                #endregion

                #region TestType Table Data

                try
                {

                    query = "select * from moshech_aac.testtype WHERE ID = " + TestTypeID + "";
                    mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                    mySQLReader = mySQLCommand.ExecuteReader();

                    mySQLReader.Read();

                    TestType = mySQLReader.GetString("TestType");

                }

                catch (MySqlException) { }

                mySQLReader.Close();

                #endregion

                #region Emdadata Table Data

                //Get the Json strings about the specified car number
                try
                {

                    query = "select * from moshech_aac.emdadata WHERE CarCheckID = " + CarCheckIDForEndReport + "";//THIS IS TEMPORARY; NEED TO FIRST FINISH THE COURSES BEFORE GETTING AN ENTRY IN EMDADATA!
                    mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                    mySQLReader = mySQLCommand.ExecuteReader();

                    mySQLReader.Read();

                    IDStationDataInJson = mySQLReader.GetString("IDStation");
                    LightsStationDataInJson = mySQLReader.GetString("LightsStation");
                    PitStationDataInJson = mySQLReader.GetString("PitStation");
                    SmokeStationDataInJson = mySQLReader.GetString("SmokeStation");
                    BrakesStationDataInJson = mySQLReader.GetString("BrakesStation");
                    WheelAlignmentStationDataInJson = mySQLReader.GetString("WheelAlignmentStation");

                    //Deseralize the IDStation string so we can get information about this first tester that signed the ID Station
                    EmdaDataInJsonGeneralStationInfo IDStationInfo = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(IDStationDataInJson);

                    //We need to save this information to know who is the first tester that has signed the ID station(so if another tester later signs it, we will still know who is the first tester to sign the station)
                    NameOfFirstTesterToSignIDStation = IDStationInfo.TesterNameOfSignedStation;
                    IDOfFirstTesterToSignIDStation = IDStationInfo.TesterIDOfSignedStation;

                }

                catch (Exception) { }

                finally
                {
                    mySQLReader.Close();

                #endregion

                    #region Save Report to CarEndReport Table

                    try
                    {
                        //add car model
                        query = "insert Into moshech_aac.carendreport(CarCheckID, CarNumber, CarType, TestType, OpenTestDateTime, FirstEmbdaBohchenName, FirstEmbdaBohchenID, NumOfTries, CarMaker, LastStation, EngineModel, EngineVolume, CarKm, CarYear, IDStation, LightsStation, PitStation, SmokeStation, BrakesStation, WheelAlignmentStation, Awd, BrakeRight,  BrakeLeft, SerenWeight, SereDifference, Findings, FuelType) values('"
                                 + CarCheckID + "','"
                                 + CarNumber + "','"
                                 + CarType + "','"
                                 + TestType + "','"
                                 + OpenTestDateTime + "','"
                                 + NameOfFirstTesterToSignIDStation + "','"
                                 + IDOfFirstTesterToSignIDStation + "','"
                                 + NumOfTries + "','"
                                 + CarManufacturerNameHeb + "','"
                                 + LastStation + "','"
                                 + EngineModel + "','"
                                 + EngineVolume + "','"
                                 + CarKm + "','"
                                 + CarYear + "','"
                                 + IDStationDataInJson + "','"
                                 + LightsStationDataInJson + "','"
                                 + PitStationDataInJson + "','"
                                 + SmokeStationDataInJson + "','"
                                 + BrakesStationDataInJson + "','"
                                 + WheelAlignmentStationDataInJson + "','"
                                 + Awd + "','"
                                 + 0 + "','"
                                 + 0 + "','"
                                 + 0 + "','"
                                 + 0 + "','"
                                 + "0" + "','"
                                 + FuelType + "')";
                        /*
                        + BrakeRight + "','"
                        + BrakeLeft + "','"
                        + SerenWeight + "','"
                        + SereDifference + "','"
                        + Findings + "','"
                         * 
                         * */


                        mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                        mySQLReader = mySQLCommand.ExecuteReader();

                        mySQLReader.Read();

                    }

                    catch (MySqlException) { }

                    mySQLReader.Close();

                    #endregion

                #endregion

                }
            }

            mySQLDBConnection.Close();
            SessionID = this.SessionID;
            return (null);

        }

        #endregion

        #region ReceiveVehicle BeginReport or EndReport Data Request

        [HandleTypeDef(typeof(PrintReportForCarNumberRequestTransferItem))]
        private Collection<TransferItem> HandlePrintReportForCarNumberRequestTransferItem(TransferItem tItem)
        {

            PrintReportForCarNumberRequestTransferItem ReportForCar = (PrintReportForCarNumberRequestTransferItem)tItem;

            PrintBeginReportResultTransferItem BeginReportInfo = new PrintBeginReportResultTransferItem();//Object containing info about begin report
            PrintEndReportResultTransferItem EndReportInfo = new PrintEndReportResultTransferItem();//Object containing info about the end report

            switch (ReportForCar.ReportToPrint)
            {

                //Being Report
                case 0:

                    try
                    {
                        mySQLDBConnection.Open();
                        query = "SELECT * from moshech_aac.carbeginreport WHERE CarNumber LIKE '%" + ReportForCar.PrintReportForCarNumber + "%'";//Get the info for this report from this carnumber entry
                        mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                        mySQLReader = mySQLCommand.ExecuteReader();

                        mySQLReader.Read();

                        //Report Information
                        BeginReportInfo.CarCheckID = mySQLReader.GetInt32("CarCheckID");
                        BeginReportInfo.CarModel = mySQLReader.GetString("CarModel");
                        BeginReportInfo.CarNumber = mySQLReader.GetString("CarNumber");
                        BeginReportInfo.CarOwnerName = mySQLReader.GetString("CarOwnerName");
                        BeginReportInfo.CarOwnerID = mySQLReader.GetInt32("CarOwnerID");
                        BeginReportInfo.CarShilda = mySQLReader.GetString("CarShilda");
                        BeginReportInfo.EngineNumber = mySQLReader.GetString("EngineNumber");
                        BeginReportInfo.PriviledgedName = mySQLReader.GetString("PriviledgedName");
                        BeginReportInfo.PriviledgedID = mySQLReader.GetString("PriviledgedID");
                        BeginReportInfo.TesterName = mySQLReader.GetString("TesterName");
                        BeginReportInfo.CarKm = mySQLReader.GetInt32("CarKm");
                        BeginReportInfo.OpenTestDateTime = mySQLReader.GetString("OpenTestDateTime");
                        BeginReportInfo.CarType = mySQLReader.GetString("CarType");
                        BeginReportInfo.FuelType = mySQLReader.GetString("FuelType");
                        BeginReportInfo.CarManufacturerNameHeb = mySQLReader.GetString("Carmanufacturer");
                        BeginReportInfo.TestType = mySQLReader.GetString("TestType");
                        BeginReportInfo.IsFirstCopy = mySQLReader.GetInt32("IsFirstCopy");
                        BeginReportInfo.DateTimeOfFirstCopy = mySQLReader.GetString("DateTimeOfFirstCopy");

                        return (BeginReportInfo);

                    }

                    catch (MySqlException) { }

                    finally
                    {
                        mySQLDBConnection.Close();
                    }

                    break;

                //End Report
                case 1:

                    try
                    {

                        mySQLDBConnection.Open();
                        query = "SELECT * from moshech_aac.carendreport WHERE CarNumber LIKE '%" + ReportForCar.PrintReportForCarNumber + "%'";//Get the info for this report from this carnumber entry
                        mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                        mySQLReader = mySQLCommand.ExecuteReader();

                        mySQLReader.Read();

                        //Report Information
                        EndReportInfo.CarCheckID = mySQLReader.GetInt32("CarCheckID");
                        EndReportInfo.CarNumber = mySQLReader.GetString("CarNumber");
                        EndReportInfo.CarType = mySQLReader.GetString("CarType");
                        EndReportInfo.TestType = mySQLReader.GetString("TestType");
                        EndReportInfo.OpenTestDate = Convert.ToDateTime(mySQLReader.GetString("OpenTestDateTime")).ToShortDateString();
                        EndReportInfo.OpenTestDate = Convert.ToDateTime(mySQLReader.GetString("OpenTestDateTime")).ToShortTimeString();
                        EndReportInfo.FirstEmbdaBohchenName = mySQLReader.GetString("FirstEmbdaBohchenName");
                        EndReportInfo.FirstEmbdaBohchenID = mySQLReader.GetInt32("FirstEmbdaBohchenID");
                        EndReportInfo.NumOfTries = mySQLReader.GetInt32("NumOfTries");
                        EndReportInfo.CarMaker = mySQLReader.GetString("CarMaker");
                        EndReportInfo.LastStation = mySQLReader.GetInt32("LastStation");
                        EndReportInfo.EngineModel = mySQLReader.GetString("EngineModel");
                        EndReportInfo.EngineVolume = mySQLReader.GetInt32("EngineVolume");
                        EndReportInfo.CarKm = mySQLReader.GetInt32("CarKm");
                        EndReportInfo.CarYear = mySQLReader.GetInt32("CarYear");
                        EndReportInfo.IDStation = mySQLReader.GetString("IDStation");
                        EndReportInfo.LightsStation = mySQLReader.GetString("LightsStation");
                        EndReportInfo.PitStation = mySQLReader.GetString("PitStation");
                        EndReportInfo.SmokeStation = mySQLReader.GetString("SmokeStation");
                        EndReportInfo.BrakesStation = mySQLReader.GetString("BrakesStation");
                        EndReportInfo.WheelAlignmentStation = mySQLReader.GetString("WheelAlignmentStation");
                        EndReportInfo.Awd = mySQLReader.GetInt32("Awd");
                        EndReportInfo.BrakeRight = mySQLReader.GetInt32("BrakeRight");
                        EndReportInfo.BrakeLeft = mySQLReader.GetInt32("BrakeLeft");
                        EndReportInfo.SerenWeight = mySQLReader.GetInt32("SerenWeight");
                        EndReportInfo.SereDifference = mySQLReader.GetInt32("SereDifference");
                        EndReportInfo.Findings = mySQLReader.GetString("Findings");
                        EndReportInfo.FuelType = mySQLReader.GetInt32("FuelType");

                        return (EndReportInfo);

                    }

                    catch (MySqlException) { }

                    finally
                    {
                        mySQLDBConnection.Close();
                    }


                    break;

            }

            SessionID = this.SessionID;
            return (null);
        }

        #endregion

        #region ReceiveVehicle BeginReport IsFirstCopyUpdate

        [HandleTypeDef(typeof(UpdateFirstCopyRequestTransferItem))]
        private Collection<TransferItem> HandleUpdateFirstCopyRequestTransferItem(TransferItem tItem)
        {

            //Sets the IsFirstCopy table entry to 0(as a copy becuase after this is printed then all prints after are copies)
            //Important! This sets the IsFirstCopy property to 0(false). In case this is the original report then that data first gets passed
            //To the report's data table(the IsFirstCopy = 1 there) and after that we set it to 1 because any print after that is a COPY

            UpdateFirstCopyRequestTransferItem UpdateInfoForCar = (UpdateFirstCopyRequestTransferItem)tItem;//Contains the car number to update "IsFirstCopy" for

            try
            {

                mySQLDBConnection.Open();
                query = "update moshech_aac.carbeginreport set IsFirstCopy = 0 WHERE CarNumber LIKE '%" + UpdateInfoForCar.CarNumber + "%'";
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                mySQLReader.Read();

            }

            catch (MySqlException) { }

            finally
            {
                mySQLDBConnection.Close();
            }

            SessionID = this.SessionID;
            return (null);

        }

        #endregion

        #region ReceiveVehicle BeginReport Update DateTime Of First Report Copy

        [HandleTypeDef(typeof(UpdateDateTimeOfFirstCopyRequestTransferItem))]
        private Collection<TransferItem> HandleUpdateDateTimeOfFirstCopyRequestTransferItem(TransferItem tItem)
        {

            //If the DateTimeOfFirstCopy table entry is empty that means that a copy has not been printed yet
            //This will update that property. When the original is printed, it will not get to this part of code because the first if statement will execute

            UpdateDateTimeOfFirstCopyRequestTransferItem UpdateInfoForCar = (UpdateDateTimeOfFirstCopyRequestTransferItem)tItem;//Contains the car number to update "DateTimeOfFirstCopy" for

            try
            {

                mySQLDBConnection.Open();
                query = "update moshech_aac.carbeginreport set DateTimeOfFirstCopy = '" + DateTime.Now + "' WHERE CarNumber LIKE '%" + UpdateInfoForCar.CarNumber + "%'";
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                mySQLReader.Read();

            }

            catch (MySqlException) { }

            finally
            {
                mySQLDBConnection.Close();
            }

            SessionID = this.SessionID;
            return (null);

        }

        #endregion

        #endregion

        #region Defect Form Handlers

        #region Defect Window Load Handler

        [HandleTypeDef(typeof(DefectWindowLoadRequestTransferItem))]
        private Collection<TransferItem> HandleDefectWindowLoadRequestTransferItem(TransferItem tItem)
        {

            DefectWindowLoadRequestTransferItem RequestedCheck = (DefectWindowLoadRequestTransferItem)tItem;
            DefectWindowLoadResultTransferItem LoadDefects = new DefectWindowLoadResultTransferItem();

            try
            {

                mySQLDBConnection.Open();
                query = "select * from moshech_aac.emdadata WHERE CarCheckID = " + RequestedCheck.CarCheckID + "";
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();
                mySQLReader.Read();

                string IDStationDataInJson = mySQLReader.GetString("IDStation");
                string LightsStationDataInJson = mySQLReader.GetString("LightSStation");
                string PitStationDataInJson = mySQLReader.GetString("PitStation");
                string SmokeStationDataInJson = mySQLReader.GetString("SmokeStation");
                string BrakesStationDataInJson = mySQLReader.GetString("BrakesStation");
                string WheelAlignmentStationDataInJson = mySQLReader.GetString("WheelAlignmentStation");

                //Deseralize the strings into objects so we can use the data in the program
                LoadDefects.IDStationData = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(IDStationDataInJson);
                LoadDefects.LightsStationData = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(LightsStationDataInJson);
                LoadDefects.PitStationData = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(PitStationDataInJson);
                LoadDefects.SmokeStationData = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(SmokeStationDataInJson);
                LoadDefects.BrakesStationData = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(BrakesStationDataInJson);
                LoadDefects.WheelAlignmentStationData = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(WheelAlignmentStationDataInJson);

            }

            catch (Exception) { }

            finally
            {
                mySQLReader.Close();
                mySQLDBConnection.Close();
                SessionID = this.SessionID;
            }

            return (LoadDefects);
        }

        #endregion

        #region Defect Window Save Permits Handler

        //Saves Permits of cars with defects into the db
        [HandleTypeDef(typeof(DefectWindowSavePermitsRequestTransferItem))]
        private Collection<TransferItem> HandleDefectWindowSavePermitsRequestTransferItem(TransferItem tItem)
        {

            DefectWindowSavePermitsRequestTransferItem PermitsToSave = (DefectWindowSavePermitsRequestTransferItem)tItem;

            try
            {

                mySQLDBConnection.Open();

                //Find the CarCheckID in the Emdadata table and store it in "DefectiveCar's" EmdadataList 
                query = "select * from moshech_aac.emdadata WHERE CarCheckID = " + PermitsToSave.CarCheckID + "";
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();
                mySQLReader.Read();

                //Get all the Json strings for that CarCheckID. These strings each contain defect information for each station 
                string IDStationDataInJson = mySQLReader.GetString("IDStation");
                string LightsStationDataInJson = mySQLReader.GetString("LightsStation");
                string PitStationDataInJson = mySQLReader.GetString("PitStation");
                string SmokeStationDataInJson = mySQLReader.GetString("SmokeStation");
                string BrakesStationDataInJson = mySQLReader.GetString("BrakesStation");
                string WheelAlignmentStationDataInJson = mySQLReader.GetString("WheelAlignmentStation");

                //Deseralize the strings into objects so we can use the data in the program
                EmdaDataInJsonGeneralStationInfo IDStationData = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(IDStationDataInJson);
                EmdaDataInJsonGeneralStationInfo LightsStationData = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(LightsStationDataInJson);
                EmdaDataInJsonGeneralStationInfo PitStationData = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(PitStationDataInJson);
                EmdaDataInJsonGeneralStationInfo SmokeStationData = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(SmokeStationDataInJson);
                EmdaDataInJsonGeneralStationInfo BrakesStationData = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(BrakesStationDataInJson);
                EmdaDataInJsonGeneralStationInfo WheelAlignmentStationData = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(WheelAlignmentStationDataInJson);

                #region Compare DefectIDs to Update Permit NumberS in DB

                //Change permit related properties of deserialized Json data

                #region ID Station Data

                //Loop through all the defect entires of this station
                for (int i = 0; i < IDStationData.DefectList.Count; i++)
                {

                    //Loop through all of the DefectCodeIDs present in the DefectCodeIDs list
                    for (int j = 0; j < PermitsToSave.DefectCodeIDs.Count; j++)
                    {

                        //Compare the Defect IDs that are registered in the DB for this station with the DefectIDs that are being asked to update the permit number for
                        if (IDStationData.DefectList[i].DefectID == PermitsToSave.DefectCodeIDs[j])
                        {

                            //Once a DefectID match is found, update the following properties of this defect field(because a permit was just supplied)
                            IDStationData.DefectList[i].PermitDate = PermitsToSave.PermitDates[j];
                            IDStationData.DefectList[i].FixDate = PermitsToSave.PermitFixDates[j];
                            IDStationData.DefectList[i].PermitNumber = PermitsToSave.PermitNumbers[j];

                            IDStationData.DefectList[i].DefectFixed = 1;
                            IDStationData.DefectList[i].PermitRequest = 0;

                        }

                    }

                }

                IDStationDataInJson = JsonConvert.SerializeObject(IDStationData);//Seralize the Object into a string to store it in Json format in our table

                #endregion

                #region Lights Station Data

                //Loop through all the defect entires of this station
                for (int i = 0; i < LightsStationData.DefectList.Count; i++)
                {

                    //Loop through all of the DefectCodeIDs present in the DefectCodeIDs list
                    for (int j = 0; j < PermitsToSave.DefectCodeIDs.Count; j++)
                    {

                        //Compare the Defect IDs that are registered in the DB for this station with the DefectIDs that are being asked to update the permit number for
                        if (LightsStationData.DefectList[i].DefectID == PermitsToSave.DefectCodeIDs[j])
                        {

                            //Once a DefectID match is found, update the following properties of this defect field(because a permit was just supplied)
                            LightsStationData.DefectList[i].PermitDate = PermitsToSave.PermitDates[j];
                            LightsStationData.DefectList[i].FixDate = PermitsToSave.PermitFixDates[j];
                            LightsStationData.DefectList[i].PermitNumber = PermitsToSave.PermitNumbers[j];

                            LightsStationData.DefectList[i].DefectFixed = 1;
                            LightsStationData.DefectList[i].PermitRequest = 0;

                        }

                    }

                }

                LightsStationDataInJson = JsonConvert.SerializeObject(LightsStationData);//Seralize the Object into a string to store it in Json format in our table

                #endregion

                #region Pit Station Data

                //Loop through all the defect entires of this station
                for (int i = 0; i < PitStationData.DefectList.Count; i++)
                {

                    //Loop through all of the DefectCodeIDs present in the DefectCodeIDs list
                    for (int j = 0; j < PermitsToSave.DefectCodeIDs.Count; j++)
                    {

                        //Compare the Defect IDs that are registered in the DB for this station with the DefectIDs that are being asked to update the permit number for
                        if (PitStationData.DefectList[i].DefectID == PermitsToSave.DefectCodeIDs[j])
                        {

                            //Once a DefectID match is found, update the following properties of this defect field(because a permit was just supplied)
                            PitStationData.DefectList[i].PermitDate = PermitsToSave.PermitDates[j];
                            PitStationData.DefectList[i].FixDate = PermitsToSave.PermitFixDates[j];
                            PitStationData.DefectList[i].PermitNumber = PermitsToSave.PermitNumbers[j];

                            PitStationData.DefectList[i].DefectFixed = 1;
                            PitStationData.DefectList[i].PermitRequest = 0;

                        }

                    }

                }

                PitStationDataInJson = JsonConvert.SerializeObject(PitStationData);//Seralize the Object into a string to store it in Json format in our table

                #endregion

                #region Smoke Station Data

                //Loop through all the defect entires of this station
                for (int i = 0; i < SmokeStationData.DefectList.Count; i++)
                {

                    //Loop through all of the DefectCodeIDs present in the DefectCodeIDs list
                    for (int j = 0; j < PermitsToSave.DefectCodeIDs.Count; j++)
                    {

                        //Compare the Defect IDs that are registered in the DB for this station with the DefectIDs that are being asked to update the permit number for
                        if (SmokeStationData.DefectList[i].DefectID == PermitsToSave.DefectCodeIDs[j])
                        {

                            //Once a DefectID match is found, update the following properties of this defect field(because a permit was just supplied)
                            SmokeStationData.DefectList[i].PermitDate = PermitsToSave.PermitDates[j];
                            SmokeStationData.DefectList[i].FixDate = PermitsToSave.PermitFixDates[j];
                            SmokeStationData.DefectList[i].PermitNumber = PermitsToSave.PermitNumbers[j];

                            SmokeStationData.DefectList[i].DefectFixed = 1;
                            SmokeStationData.DefectList[i].PermitRequest = 0;

                        }

                    }

                }

                SmokeStationDataInJson = JsonConvert.SerializeObject(SmokeStationData);//Seralize the Object into a string to store it in Json format in our table

                #endregion

                #region BrakesStationData

                //Loop through all the defect entires of this station
                for (int i = 0; i < BrakesStationData.DefectList.Count; i++)
                {

                    //Loop through all of the DefectCodeIDs present in the DefectCodeIDs list
                    for (int j = 0; j < PermitsToSave.DefectCodeIDs.Count; j++)
                    {

                        //Compare the Defect IDs that are registered in the DB for this station with the DefectIDs that are being asked to update the permit number for
                        if (BrakesStationData.DefectList[i].DefectID == PermitsToSave.DefectCodeIDs[j])
                        {

                            //Once a DefectID match is found, update the following properties of this defect field(because a permit was just supplied)
                            BrakesStationData.DefectList[i].PermitDate = PermitsToSave.PermitDates[j];
                            BrakesStationData.DefectList[i].FixDate = PermitsToSave.PermitFixDates[j];
                            BrakesStationData.DefectList[i].PermitNumber = PermitsToSave.PermitNumbers[j];

                            BrakesStationData.DefectList[i].DefectFixed = 1;
                            BrakesStationData.DefectList[i].PermitRequest = 0;

                        }

                    }

                }

                BrakesStationDataInJson = JsonConvert.SerializeObject(BrakesStationData);//Seralize the Object into a string to store it in Json format in our table

                #endregion

                #region Wheel Alignment StationData

                //Loop through all the defect entires of this station
                for (int i = 0; i < WheelAlignmentStationData.DefectList.Count; i++)
                {

                    //Loop through all of the DefectCodeIDs present in the DefectCodeIDs list
                    for (int j = 0; j < PermitsToSave.DefectCodeIDs.Count; j++)
                    {

                        //Compare the Defect IDs that are registered in the DB for this station with the DefectIDs that are being asked to update the permit number for
                        if (WheelAlignmentStationData.DefectList[i].DefectID == PermitsToSave.DefectCodeIDs[j])
                        {

                            //Once a DefectID match is found, update the following properties of this defect field(because a permit was just supplied)
                            WheelAlignmentStationData.DefectList[i].PermitDate = PermitsToSave.PermitDates[j];
                            WheelAlignmentStationData.DefectList[i].FixDate = PermitsToSave.PermitFixDates[j];
                            WheelAlignmentStationData.DefectList[i].PermitNumber = PermitsToSave.PermitNumbers[j];

                            WheelAlignmentStationData.DefectList[i].DefectFixed = 1;
                            WheelAlignmentStationData.DefectList[i].PermitRequest = 0;

                        }

                    }

                }

                WheelAlignmentStationDataInJson = JsonConvert.SerializeObject(WheelAlignmentStationData);//Seralize the Object into a string to store it in Json format in our table

                #endregion

                #endregion

                mySQLReader.Close();

                //Run a query to save all of the data modified to the Json strings from above back into the DB
                query = "update moshech_aac.emdadata set IDStation ='" + IDStationDataInJson +
                    "', LightsStation ='" + LightsStationDataInJson +
                    "', PitStation ='" + PitStationDataInJson +
                    "', SmokeStation ='" + SmokeStationDataInJson +
                    "', BrakesStation ='" + BrakesStationDataInJson +
                    "', WheelAlignmentStation ='" + WheelAlignmentStationDataInJson +
                    "' where CarCheckID ='" + PermitsToSave.CarCheckID + "'";

                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();
                mySQLReader.Read();


            }

            catch (Exception) { }

            finally
            {
                mySQLReader.Close();
                mySQLDBConnection.Close();
                SessionID = this.SessionID;
            }

            return (null);
        }

        #endregion

        #endregion

        #region TestCourse Form Handlers

        #region Load Defective Car Numbers Handler

        //FOR LOADING BASIC INFORMATION ABOUT THE RELEVANT CARS WITH DEFECTS!(ALL CARS THAT ARE RELEVANT)
        [HandleTypeDef(typeof(LoadCarNumbersWithDefectsRequestTransferItem))]
        private Collection<TransferItem> HandleLoadCarNumbersWithDefectsRequestTransferItem(TransferItem tItem)
        {

            LoadCarNumbersWithDefectsResultTransferItem DefectiveCarInfo = new LoadCarNumbersWithDefectsResultTransferItem();
            List<LoadCarNumbersWithDefectsResultTransferItem> DefectiveInfoCopy = new List<LoadCarNumbersWithDefectsResultTransferItem>();

            int IndexOfDefectiveInfo = 0;//So we can iterate through the DefectiveInfoCopy indexes

            List<int> CarCheckIDExistsInEmdadata = new List<int>();//This list is for storing all the CarCheckIDs that have failed the test in Rec vehicle. It later checks to see if these numbers exist in emdadata too because not all the numbers exist in both tables!
            List<int> RemoveCarNumberTempList = new List<int>();//This list is for storing all the CarNumbers that need to be removed after the loop is done! Otherwise errors

            mySQLDBConnection.Open();
            query = "select * from moshech_aac.receivevehicle WHERE CarCheckFinal = " + 0 + "";//Check for tests that have failed so we can gather all their numbers
            mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
            mySQLReader = mySQLCommand.ExecuteReader();

            while (mySQLReader.Read())
            {

                DefectiveInfoCopy.Add(new LoadCarNumbersWithDefectsResultTransferItem());

                //Add the car numbers that have a carcheckfinal of 0 to both the DefectiveInfo list and the CarCheckIDExists list. These car number are later to checked to see if they exist in both ActiveCarFile and Emdadata. If they don't the data gets removed
                CarCheckIDExistsInEmdadata.Add(mySQLReader.GetInt32("CarCheckID"));
                DefectiveInfoCopy[IndexOfDefectiveInfo].DefectiveCarNumber = mySQLReader.GetInt32("CarNumber");
                DefectiveInfoCopy[IndexOfDefectiveInfo].LastCheckTime = mySQLReader.GetString("LastCheckTime");

                //Rest of the information about the defective car
                DefectiveInfoCopy[IndexOfDefectiveInfo].AttemptNumber = mySQLReader.GetInt32("NumOfTries");
                DefectiveInfoCopy[IndexOfDefectiveInfo].CarManufacturerID = mySQLReader.GetInt32("CarManufacturerID");

                DefectiveInfoCopy[IndexOfDefectiveInfo].CarType = VehicleTypeDictionary[mySQLReader.GetInt32("CarType")];
                DefectiveInfoCopy[IndexOfDefectiveInfo].FuelType = FuelTypesDictionary[mySQLReader.GetInt32("FuelType")];

                IndexOfDefectiveInfo++;

            }

            IndexOfDefectiveInfo = 0;//Reset count

            mySQLReader.Close();

            for (int i = 0; i < DefectiveInfoCopy.Count; i++)
            {
                query = "select * from moshech_aac.emdadata WHERE CarCheckID = " + CarCheckIDExistsInEmdadata[i] + "";//Check for tests that have failed so we can gather all their numbers
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();

                mySQLReader.Read();

                if (mySQLReader.HasRows) { }//If they exist in both tables do nothing

                else
                {
                    RemoveCarNumberTempList.Add(DefectiveInfoCopy[i].DefectiveCarNumber);//Add the number that needs to be removed. So after the loop we know which indexes from DefectiveCarNumberList to remove
                }//If they don't then remove that CarNumber from the DefectiveCarNumbersList

                mySQLReader.Close();

            }

            foreach (int RemoveCarNumber in RemoveCarNumberTempList)
            {

                for (int j = 0; j < DefectiveInfoCopy.Count; j++)
                {

                    if (RemoveCarNumber == DefectiveInfoCopy[j].DefectiveCarNumber)//See if the car number to remove = the car number in all of the DefectiveInfo[index].DefectiveCarNumber
                    {
                        DefectiveInfoCopy.RemoveAt(j);//Remove the index from the DefectiveInfoCopy list(delete the invalid car from the list)

                        j = DefectiveInfoCopy.Count;//Exit loop
                    }

                }

            }


            DefectiveCarInfo.DefectiveCarInfo = DefectiveInfoCopy;//You know the drill

            #region Load Car Manufacturer IDs from ReceiveVehicle

            //Here we get all the car manufacturer ids into the list in DefectiveCarInfo

            query = "select * from moshech_aac.carmanufacturers ORDER BY ManufacturerNameHeb";//Check for tests that have failed so we can gather all their numbers
            mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
            mySQLReader = mySQLCommand.ExecuteReader();


            while (mySQLReader.Read()) { DefectiveCarInfo.CarManuFacturers.Add(mySQLReader.GetInt32("MOTManufacturerID")); }

            mySQLReader.Close();

            #endregion

            mySQLDBConnection.Close();
            SessionID = this.SessionID;

            //Next we check to see if these 

            return (DefectiveCarInfo);

        }

        #endregion

        #region Load Defective Car Handler

        //FOR LOADING ALL INFORMATION ABOUT 1 CAR ONLY, INCLUDES DEFECT INFORMATION
        [HandleTypeDef(typeof(LoadDefectsForCarRequestTransferItem))]
        private Collection<TransferItem> HandleLoadDefectsForCarRequestTransferItem(TransferItem tItem)
        {

            LoadDefectsForCarRequestTransferItem RequestedCarNumberToLoad = (LoadDefectsForCarRequestTransferItem)tItem;//Get the requested car number
            LoadDefectsForCarResultTransferItem DefectiveCar = new LoadDefectsForCarResultTransferItem();//Object to store all the info for the requested car number

            mySQLDBConnection.Open();

            #region CarInfo Form Data & Failed Car Numbers

            //Some of this information is taken from the shared dictionaries in this class. This is data we use in both ReceiveVehicle and here for example

            query = "select * from moshech_aac.receivevehicle WHERE CarNumber = " + RequestedCarNumberToLoad.CarNumber + "";//Check for tests that have failed so we can gather all their information below
            mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
            mySQLReader = mySQLCommand.ExecuteReader();

            while (mySQLReader.Read())
            {

                DefectiveCar.CarCheckID = mySQLReader.GetInt32("CarCheckID");//Will be used to get the right CarCheckID from the Emdadata table below
                DefectiveCar.CarNumber = mySQLReader.GetInt32("CarNumber");

                //Information from dictionaries:
                DefectiveCar.CarType = VehicleTypeDictionary[mySQLReader.GetInt32("CarType")];
                DefectiveCar.FuelType = FuelTypesDictionary[mySQLReader.GetInt32("FuelType")];
                DefectiveCar.TestType = TestTypeDictionary[mySQLReader.GetInt32("TestType")];

                DefectiveCar.CarYear = mySQLReader.GetInt32("CarYear");
                DefectiveCar.CarOwnerName = mySQLReader.GetString("CarOwnerName");
                DefectiveCar.NumberOfAttempts = mySQLReader.GetInt32("NumOfTries");

            }

            mySQLReader.Close();

            #endregion

            //Find the CarCheckID in the Emdadata table and store it in "DefectiveCar's" EmdadataList 
            query = "select * from moshech_aac.emdadata WHERE CarCheckID = " + DefectiveCar.CarCheckID + "";
            mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
            mySQLReader = mySQLCommand.ExecuteReader();
            mySQLReader.Read();

            //Get all the Json strings for that CarCheckID. These strings each contain defect information for each station 
            string IDStationDataInJson = mySQLReader.GetString("IDStation");
            string LightsStationDataInJson = mySQLReader.GetString("LightsStation");
            string PitStationDataInJson = mySQLReader.GetString("PitStation");
            string SmokeStationDataInJson = mySQLReader.GetString("SmokeStation");
            string BrakesStationDataInJson = mySQLReader.GetString("BrakesStation");
            string WheelAlignmentStationDataInJson = mySQLReader.GetString("WheelAlignmentStation");

            //Deseralize the strings into objects so we can use the data in the program
            DefectiveCar.IDStationData = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(IDStationDataInJson);
            DefectiveCar.LightsStationData = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(LightsStationDataInJson);
            DefectiveCar.PitStationData = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(PitStationDataInJson);
            DefectiveCar.SmokeStationData = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(SmokeStationDataInJson);
            DefectiveCar.BrakesStationData = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(BrakesStationDataInJson);
            DefectiveCar.WheelAlignmentStationData = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(WheelAlignmentStationDataInJson);

            mySQLReader.Close();
            mySQLDBConnection.Close();
            SessionID = this.SessionID;

            return (DefectiveCar);
        }

        #endregion

        #region Load DefectInfo Table Handler

        [HandleTypeDef(typeof(LoadDefectInfoRequestTransferItem))]
        private Collection<TransferItem> HandleLoadDefectInfoRequestTransferItem(TransferItem tItem)
        {

            LoadDefectInfoResultTransferItem DefectData = new LoadDefectInfoResultTransferItem();//Object to store all the info for a defect

            List<LoadDefectInfoResultTransferItem> DefectsListCopy = new List<LoadDefectInfoResultTransferItem>();//A list that contains items of type LoadDefectInfoResultTI, each entry represents a defect. The same list inside DefectData will set itself = to this list because each new object entry would reset the list inside the DefectData object as well.

            mySQLDBConnection.Open();


            //Some of this information is taken from the shared dictionaries in this class. This is data we use in both ReceiveVehicle and here for example

            query = "select * from moshech_aac.defectinfo";
            mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
            mySQLReader = mySQLCommand.ExecuteReader();

            while (mySQLReader.Read())
            {

                DefectData.DefectID = mySQLReader.GetInt32("DefectID");
                DefectData.DefectInfo = mySQLReader.GetString("DefectInfo").Split('/').ToList();//Split up the DefectInfo string into seperate strings if there is a "/" present
                DefectData.Remark = mySQLReader.GetString("Remark");
                DefectData.Permit = mySQLReader.GetInt32("Permit");
                DefectData.StationNumber = mySQLReader.GetInt32("StationNumber");
                DefectData.RequiresPay = mySQLReader.GetInt32("RequiresPay");

                DefectsListCopy.Add(DefectData);
                DefectData = new LoadDefectInfoResultTransferItem();

            }

            mySQLReader.Close();
            mySQLDBConnection.Close();

            DefectData.Defects = DefectsListCopy;//Set the list of defects inside DefectData to this one so we can transfer the data over
            SessionID = this.SessionID;

            return (DefectData);
        }

        #endregion

        #region Sign Station Handler

        [HandleTypeDef(typeof(SignStationRequestTransferItem))]
        private Collection<TransferItem> HandleSignStationRequestTransferItem(TransferItem tItem)
        {

            SignStationRequestTransferItem SignStationInfo = (SignStationRequestTransferItem)tItem;//Information about the signed station
            SignStationResultTransferItem SignStationResult = new SignStationResultTransferItem();

            try
            {

                mySQLDBConnection.Open();

                string StationDataInJson = JsonConvert.SerializeObject(SignStationInfo);//Seralize the Object into a string to store it in Json format in our table

                if (SignStationInfo.StationNumber == 1)//If the signed station is the ID station
                {
                    query = "update emdadata set IDStation = '" + StationDataInJson + "' WHERE CarCheckID = '" + SignStationInfo.CarCheckID + "' ";
                }

                if (SignStationInfo.StationNumber == 2)//Lights
                {
                    query = "update emdadata set LightsStation = '" + StationDataInJson + "' WHERE CarCheckID = '" + SignStationInfo.CarCheckID + "' ";
                }

                if (SignStationInfo.StationNumber == 3)//Pit
                {
                    query = "update emdadata set PitStation = '" + StationDataInJson + "' WHERE CarCheckID = '" + SignStationInfo.CarCheckID + "' ";
                }

                if (SignStationInfo.StationNumber == 4)//Smoke
                {
                    query = "update emdadata set SmokeStation = '" + StationDataInJson + "' WHERE CarCheckID = '" + SignStationInfo.CarCheckID + "' ";
                }

                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();
                mySQLReader.Read();
                mySQLReader.Close();


                SignStationResult.ResponseMessage = "עמדה נחתמה בהצלחה";

            }

            catch (Exception) { }

            finally
            {
                mySQLReader.Close();
                mySQLDBConnection.Close();
                SessionID = this.SessionID;
            }

            return (SignStationResult);
        }


        #endregion

        #region Release Station Handler

        [HandleTypeDef(typeof(ReleaseStationRequestTransferItem))]
        private Collection<TransferItem> HandleReleaseStationRequestTransferItem(TransferItem tItem)
        {

            ReleaseStationRequestTransferItem StationToRelease = (ReleaseStationRequestTransferItem)tItem;

            try
            {

                mySQLDBConnection.Open();

                EmdaDataInJsonGeneralStationInfo StationInfo = new EmdaDataInJsonGeneralStationInfo();//object to hold the station's info so we can change the IsStationClosed property
                string StationDataInJson = "";//Get the station info json string

                query = "Select * from moshech_aac.emdadata WHERE CarCheckID = " + StationToRelease.CarCheckID + "";//First query to find the station info
                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();
                mySQLReader.Read();

                //Get the json string for the right station
                if (StationToRelease.StationToRelease == 1)//ID
                {
                    StationDataInJson = mySQLReader.GetString("IDStation");
                }

                if (StationToRelease.StationToRelease == 2)//Lights
                {
                    StationDataInJson = mySQLReader.GetString("LightsStation");
                }

                if (StationToRelease.StationToRelease == 3)//Pit
                {
                    StationDataInJson = mySQLReader.GetString("PitStation");
                }

                if (StationToRelease.StationToRelease == 4)//Smoke
                {
                    StationDataInJson = mySQLReader.GetString("SmokeStation");
                }

                mySQLReader.Close();

                //Deseralize it into the type! This way we can get access the IsStationClosed property
                StationInfo = JsonConvert.DeserializeObject<EmdaDataInJsonGeneralStationInfo>(StationDataInJson);

                StationInfo.IsStationClosed = 1;//Set the property to 1(station is open)

                StationDataInJson = JsonConvert.SerializeObject(StationInfo);//Seralize BACK the new string with the changed IsStationClosed property

                //Update the right station according to StationToRelease
                if (StationToRelease.StationToRelease == 1)//ID
                {
                    query = "update emdadata set IDStation = '" + StationDataInJson + "' WHERE CarCheckID = '" + StationToRelease.CarCheckID + "' ";//Update the DB
                }

                if (StationToRelease.StationToRelease == 2)//Lights
                {
                    query = "update emdadata set LightsStation = '" + StationDataInJson + "' WHERE CarCheckID = '" + StationToRelease.CarCheckID + "' ";//Update the DB
                }

                if (StationToRelease.StationToRelease == 3)//Pit
                {
                    query = "update emdadata set PitStation = '" + StationDataInJson + "' WHERE CarCheckID = '" + StationToRelease.CarCheckID + "' ";//Update the DB
                }

                if (StationToRelease.StationToRelease == 4)//Smoke
                {
                    query = "update emdadata set SmokeStation = '" + StationDataInJson + "' WHERE CarCheckID = '" + StationToRelease.CarCheckID + "' ";//Update the DB
                }

                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();
                mySQLReader.Read();
                mySQLReader.Close();

            }

            catch (Exception) { }

            finally
            {
                mySQLReader.Close();
                mySQLDBConnection.Close();
                SessionID = this.SessionID;
            }

            return (null);
        }


        #endregion

        #region Lights Station

        #region Save Lux Mods Request

        [HandleTypeDef(typeof(SaveLuxModsRequestTransferItem))]
        private Collection<TransferItem> HandleSaveLuxModsRequestTransferItem(TransferItem tItem)
        {

            SaveLuxModsRequestTransferItem LuxModValues = (SaveLuxModsRequestTransferItem)tItem;

            try
            {

                mySQLDBConnection.Open();

                query = "update moshech_aac.station_settings set LuxModHLR ='" + LuxModValues.LuxModHLR +
                                  "', LuxModHLL ='" + LuxModValues.LuxModHLL +
                                  "', LuxModLLR ='" + LuxModValues.LuxModLLR +
                                  "', LuxModLLL ='" + LuxModValues.LuxModLLL +
                                  "' where StationID ='" + LuxModValues.StationID + "'";

                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();
                mySQLReader.Read();

            }

            catch (Exception) { }

            finally
            {

                mySQLReader.Close();
                mySQLDBConnection.Close();
                SessionID = this.SessionID;

            }

            return (null);

        }

        #endregion

        #region Load Lux Mods Request

        [HandleTypeDef(typeof(LoadLuxModsRequestTransferItem))]
        private Collection<TransferItem> HandleLoadLuxModsRequestTransferItem(TransferItem tItem)
        {

            LoadLuxModsRequestTransferItem LuxModsLoadRequest = (LoadLuxModsRequestTransferItem)tItem;

            LoadLuxModsResultTransferItem LuxModsResult = new LoadLuxModsResultTransferItem();


            try
            {

                mySQLDBConnection.Open();

                query = "select * from moshech_aac.station_settings where StationID = " + LuxModsLoadRequest.StationID + "";

                mySQLCommand = new MySqlCommand(query, mySQLDBConnection);
                mySQLReader = mySQLCommand.ExecuteReader();
                mySQLReader.Read();

                LuxModsResult.LuxModHLR = mySQLReader.GetDouble("LuxModHLR");
                LuxModsResult.LuxModHLL = mySQLReader.GetDouble("LuxModHLL");
                LuxModsResult.LuxModLLR = mySQLReader.GetDouble("LuxModLLR");
                LuxModsResult.LuxModLLL = mySQLReader.GetDouble("LuxModLLL");



            }

            catch (Exception) { }

            finally
            {

                mySQLReader.Close();
                mySQLDBConnection.Close();
                SessionID = this.SessionID;

            }

            return (LuxModsResult);

        }

        #endregion

        #endregion

        #endregion

        #region Receive Vehicle Permits Related

        #region Initial Permits for Receiveing Vehicle

        [HandleTypeDef(typeof(InputVehiclePermitsRequestTransferItem))]
        private Collection<TransferItem> HandleSaveInitialPermitsRequestTransferItem(TransferItem tItem)
        {

            //Function for sending initial permits to CarData(Online only!) and saving these permits to the DB

            InputVehiclePermitsRequestTransferItem InitialPermitsToInput = (InputVehiclePermitsRequestTransferItem)tItem;
            InputVehiclePermitsResultTransferItem InitialPermitsInputResult = new InputVehiclePermitsResultTransferItem();

            #region Sending Initial Permits to CarData

            #region Foundation for all CarData Function Calls

            //Information for CarData
            CarDataVehicle.misparRechev = InitialPermitsToInput.CarNumber;
            CarDataVehicle.tzBaal = InitialPermitsToInput.CarOwnerID;
            CarDataVehicle.tzMeyupe = InitialPermitsToInput.PriviledgedID;

            CDWS.CarData CarData = new CDWS.CarData();

            #endregion

            #region Vehicle Permits Data Setting

            //CarData object that holds all information about required permits in order to send the "InputVehiclePermits" function
            VehiclePermits InitialPermitsToSend = new VehiclePermits();

            InitialPermitsToSend.sugMivhan = (TestType)InitialPermitsToInput.TestType;
            InitialPermitsToSend.thilatMivhan = InitialPermitsToInput.TestStartDate;
            InitialPermitsToSend.thilatMivhanHozer = InitialPermitsToInput.ReTestDate;
            //??InitialPermitsToSend.statusMeyuhad

            InitialPermitsToSend.approvals = new CDWS.GarageApproval[InitialPermitsToInput.NeededPermitList.Count];//We are only sending 2 permits, so create an array of 2 spaces

            //Loop through all permit entries the user has entered and add them to CarData's approval list
            for (int i = 0; i < InitialPermitsToInput.NeededPermitList.Count; i++)
            {

                InitialPermitsToSend.approvals[i] = new CDWS.GarageApproval();

                InitialPermitsToSend.approvals[i].kodIshur = InitialPermitsToInput.NeededPermitList[i].PermitCode;
                InitialPermitsToSend.approvals[i].kodMusahMurshe = InitialPermitsToInput.NeededPermitList[i].GarrageApprovalID;
                InitialPermitsToSend.approvals[i].taarichBdika = InitialPermitsToInput.NeededPermitList[i].CheckDate;
                InitialPermitsToSend.approvals[i].taarichIshur = InitialPermitsToInput.NeededPermitList[i].PermitIssueDate;

            }

            InputVehiclePermitsResponse PermitEntryResponse = CarData.InputVehiclePermits(CarDataLogin, CarDataID, CarDataVehicle, InitialPermitsToSend);

            //If the permit entry was successful then let the user know 
            if (PermitEntryResponse.msgNr == 1)
            {

                InitialPermitsInputResult.IsResponseAnError = false;//The response is not an error               

                //Save the initial permits entered as a Json string for later saving it into the DB(because this is a positive response(permits were entered correctly) then we want to save the initial permits in the DB)
                InitialPermitsInputResult.VehiclePermitListInJSon = JsonConvert.SerializeObject(InitialPermitsToInput.NeededPermitList);

            #endregion

            }

            //An error response from CarData has returned. Set the text of the error and indicate that an error was returned so we can display it accordingly on the client side
            else { InitialPermitsInputResult.Response = PermitEntryResponse.msgText; InitialPermitsInputResult.IsResponseAnError = true; }

            #endregion

            SessionID = this.SessionID;
            return (InitialPermitsInputResult);

        }

        #endregion

        #endregion

        #region ExtraFunctions

        public static string GetExternalIP()
        {
            try
            {
                string externalIP;
                externalIP = (new WebClient()).DownloadString("http://checkip.dyndns.org/");
                externalIP = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
                .Matches(externalIP)[0].ToString();
                return externalIP;
            }
            catch { return "Ext IP Checkup Service Unavailable!"; }
        }

        public int GetDictionaryKeyByValue(Dictionary<int, string> FindInDictionary, string ValueToCompare)
        {

            foreach (KeyValuePair<int, string> Entry in FindInDictionary)//Loop through the given dictionary to try to find values that are equal(with the given parameter value) to then return the key that holds that value
            {
                if (Entry.Value.Equals(ValueToCompare)) { return Entry.Key; }
            }

            return 0;
        }


        #region CarNumber Formatting/Deformatting Functions

        public static string CarNumberFormat(string CarNumber)//Formats car numbers correctly
        {

            int CharInstanceCounter = 1;

            #region 7/8 Digit CarNumber

            if (CarNumber.Length == (7) || CarNumber.Length == 8)
            {

                var Builder = new StringBuilder();

                foreach (char Num in CarNumber)
                {

                    Builder.Append(Num);

                    if (CharInstanceCounter == 2 || CharInstanceCounter == 5) { Builder.Append("-"); }

                    CharInstanceCounter++;

                }

                CarNumber = Builder.ToString();
                return CarNumber;
            }

            #endregion

            #region 6 Digit CarNumber

            if (CarNumber.Length == (6))
            {

                var Builder = new StringBuilder();

                foreach (char Num in CarNumber)
                {

                    Builder.Append(Num);

                    if (CharInstanceCounter == 3) { Builder.Append("-"); }

                    CharInstanceCounter++;

                }

                CarNumber = Builder.ToString();
                return CarNumber;
            }

            #endregion

            return CarNumber;//If this format is not specified here then just return the CarNumber as is

        }

        public static string CarNumberDeformat(string CarNumber)//Deformats the carnumber to be a regular number(no "-")
        {

            var Builder = new StringBuilder();

            foreach (char Num in CarNumber)
            {

                if (Num != 45) { Builder.Append(Num); }//If the char that was looped is "-" then don't append!

            }

            CarNumber = Builder.ToString();

            return CarNumber;//return the deformatted car number

        }

        #endregion

        #endregion

        #endregion

        #endregion

    }
}