﻿using CommInf.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RishuiServer.Managers
{
    public class RishuiServerManager : ServerCommManager<RishuiSessionManager>
    {
        protected override void MapHandlers()
        {
            //Here you map non session specific transfer items.
        }

        //Here you define non session specific transfer items.
    }
}