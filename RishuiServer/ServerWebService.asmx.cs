﻿using CommInf.Items;
using CommInf.Items.SessionItems;
using RishuiCommDefines.TransferItems.ErrorItems;
using RishuiServer.Managers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace RishuiServer
{
    /// <summary>
    /// Summary description for ServerWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ServerWebService : System.Web.Services.WebService
    {
        private static RishuiServerManager serverManager;

        [WebMethod]
        public string[] TransferData(string inString)
        {
            if (serverManager == null)
                serverManager = new RishuiServerManager();
            try
            {
                TransferItem tItem = TransferItem.DeserializeJsonString(inString);
                Collection<TransferItem> returnTransferItems = serverManager.HandleTransferItem(tItem);

                if (returnTransferItems != null)
                {
                    TransferItem[] returnTransferArr = returnTransferItems.ToArray<TransferItem>();
                    string[] retString = new string[returnTransferArr.Length];
                    for (int i = 0; i < retString.Length; i++)
                        retString[i] = returnTransferArr[i].SerializeWithJSon();
                    return retString;
                }
                else
                {
                    return new string[] { "" };
                }

            }
            catch(Exception e)
            {
                return new string[]{new ErrorTransferItem(){ErrorDescription = e.ToString()}.SerializeWithJSon()  };
            }
        }
    }
}
